FROM srteam/mxeposixuc

RUN cd /mxe && make MXE_TARGETS='i686-w64-mingw32.static.posix' lua libzip --jobs=2 JOBS=2

RUN apt update && apt install curl zip ccache -y && apt clean

RUN curl -fsS https://dlang.org/install.sh | bash -s dmd
