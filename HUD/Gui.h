#ifndef GUI_H
#define GUI_H

#include "Config.h"
#include "IDE.h"
#include "Itemhud.h"
#include "Plugin.h"
#include <gtasa/CGame/Types.h>
#include <imgui/ImWrapper/ImWrapper.hpp>
#include <llmo/SRHook.hpp>
#include <sol/sol.hpp>
#include <tuple>

namespace ImGui {
	class FileBrowser;
}

class Gui : public SRDescent {
	sol::state *_lua = nullptr;

	SRHook::Hook<>		  showHud{ 0x58FBC4, 5 };
	SRHook::Hook<>		  showRadar{ 0x58FBF3, 5 };
	SRHook::Hook<>		  showWanted{ 0x58DD2C, 6 };
	SRHook::Hook<>		  showArmour{ 0x5890D7, 5 };
	SRHook::Hook<>		  showBreath{ 0x58EFF0, 6 };
	SRHook::Hook<>		  showAmmo{ 0x589644, 5 };
	SRHook::Hook<uint8_t> clAmmo{ 0x589603, 5 };
	SRHook::Hook<RwRGBA>  clChart{ 0x728640, 5 };
	SRHook::Hook<RwRGBA>  clPercentage{ 0x7289E0, 5 };
	SRHook::Hook<>		  hudDraw{ 0x58FBB3 /*0x53E52B*/, 5 };

	std::tuple<size_t, size_t, size_t> _loaderSlots;

	ImMenu *			_mainMenu	 = nullptr;
	ImGui::FileBrowser *fileExplorer = nullptr;

	size_t _mid	 = -1;
	size_t _kdid = -1;
	size_t _kuid = -1;

	std::deque<class Config *> _configs;
	class DefaultHud *		   _defHud;
	class IDE *				   _ide;

	Config *_activeCfg = nullptr;
	int		_activeId  = 0;

	enum class MenuPage { chose_hud, edit_hud, crosshairs, custom_items, textures, scripts, save_hud, custom_fonts };
	MenuPage _page = MenuPage::chose_hud;

	char txNameBuf[256]{ 0 };
	bool txNameAssigned = false;

	char newCustItemNameBuf[256]{ 0 };

	char		scriptNameBuf[256]{ 0 };
	std::string _edScript;

	RwRGBA percentColor;

	bool _drag	   = false;
	bool _txAdding = false;
	bool _saving   = false;

	char hudNameBuf[256]{ 0 };
	char hudAuthorBuf[256]{ 0 };
	char hudDescBuf[1024]{ 0 };
	char hudArchiveBuf[256]{ 0 };

	DWORD _lastFrameTime = 0;
	DWORD _frameCounter	 = 0;
	DWORD _fps			 = 0;

	std::deque<Plugin *> _plugins;

public:
	Gui()			   = delete;
	Gui( const Gui & ) = delete;
	Gui( SRDescent *parent, DefaultHud *pDefHud, IDE *pIDE );
	virtual ~Gui();

	void open();
	bool isOpened();

	RwTexture *texture( std::string_view name );

protected:
	sol::state &lua();
	sol::state &reset_lua();

	void draw();

	void choseHud();
	void customItems();
	void textures();
	void texturesAdd();
	void texturesList();
	void crosshairs();
	void editHud();
	void saveHud();
	void scripts();
	void fonts();

	void loadHuds();
	void saveCfg();

	void switchHud( Config *cfg = nullptr );
	void switchHud( int id );

	void					  initLuaAndIDE( IDE *ide = nullptr );
	void					  initLuaAndIDECrosshairs( IDE *ide );
	template<typename T> void initLuaAndIDEHUD( IDE *ide, std::string prefix = "item" ) {

		auto addFunction = [=]( std::string_view name, auto func, std::string_view desc ) {
			_lua->set_function( name, func );
			if ( ide ) ide->addFunction( name, desc );
		};
		auto getItem = [this]( std::string name ) -> T * {
			if ( !_activeCfg ) return nullptr;
			for ( auto &&item : _activeCfg->items() ) {
				auto hud = dynamic_cast<T *>( item );
				if ( !hud ) continue;
				if ( hud->imgName() == name ) return hud;
			}
			return nullptr;
		};

		addFunction(
			prefix + "_delete",
			[this, getItem]( std::string name ) {
				auto item = getItem( name );
				if ( !item ) return false;
				return _activeCfg->removeItem( item );
			},
			"Remove " + prefix + "\n\nArgs:\n * string - name\nReturn:\n * bool - is removed" );

		addFunction(
			prefix + "_show",
			[getItem]( std::string name, bool show ) {
				auto item = getItem( name );
				if ( !item ) return;
				item->toggleShow( show );
			},
			"Show/hide " + prefix + "\n\nArgs:\n * string - name\n * bool - show" );
		addFunction(
			prefix + "_isShowed",
			[getItem]( std::string name ) {
				auto item = getItem( name );
				if ( !item ) return false;
				return item->isShowed();
			},
			"Get current show state of " + prefix + "\n\nArgs:\n * string - name\nReturn:\n * bool - is showed" );
		addFunction(
			prefix + "_pos",
			[getItem]( std::string name ) {
				auto item = getItem( name );
				if ( !item ) return std::make_tuple( 0.0f, 0.0f );
				return std::make_tuple( item->pos().fX, item->pos().fY );
			},
			"Get position for " + prefix +
				"\n\nArgs:\n * string - name\nReturn:\n * float - position X\n * float - position Y" );
		addFunction(
			prefix + "_size",
			[getItem]( std::string name ) {
				auto item = getItem( name );
				if ( !item ) return std::make_tuple( 0.0f, 0.0f );
				return std::make_tuple( item->size().fX, item->size().fY );
			},
			"Get size for " + prefix + "\n\nArgs:\n * string - name\nReturn:\n * float - width\n * float - height" );
		addFunction(
			prefix + "_setPos",
			[getItem]( std::string name, float x, float y ) {
				auto item = getItem( name );
				if ( !item ) return;
				item->pos( { x, y } );
			},
			"Set position for " + prefix +
				"\n\nArgs:\n * string - name\n * float - position X\n * float - position Y" );
		addFunction(
			prefix + "_setSize",
			[getItem]( std::string name, float w, float h ) {
				auto item = getItem( name );
				if ( !item ) return;
				item->size( { w, h } );
			},
			"Set size for " + prefix + "\n\nArgs:\n * string - name\n * float - width\n * float - height" );
		addFunction(
			prefix + "_color",
			[getItem]( std::string name ) {
				auto item = getItem( name );
				if ( !item ) return 0;
				auto clr = item->color();
				return *(int *)&clr;
			},
			"Get " + prefix + " color\n\nArgs:\n * string - name\nReturn:\n * int - color" );
		addFunction(
			prefix + "_setColor",
			[getItem]( std::string name, int clr ) {
				auto item = getItem( name );
				if ( !item ) return;
				item->color( *(RwRGBA *)&clr );
			},
			"Set " + prefix + " color\n\nArgs:\n * string - name\n * int - color" );
		addFunction(
			prefix + "_render",
			[this, getItem]( std::string name ) {
				if ( *(bool *)0xBA67A4 ) return;
				auto hud_hidden =
					_mainMenu->isHidden() && ( !*(bool *)0xBA6769 || !*(bool *)0xA444A0 || *(bool *)0xB6F065 );
				if ( hud_hidden ) return;
				auto item = getItem( name );
				if ( !item ) return;
				auto show = item->isShowed();
				item->toggleShow( true );
				item->render();
				item->toggleShow( show );
			},
			"Force call render for " + prefix + "\n\nArgs:\n * string - name" );
	}
	void initLuaAndIDEImage( IDE *ide );
	void initLuaAndIDEFont( IDE *ide );
	void initLuaAndIDEChart( IDE *ide );

	void hudHook( SRHook::Info &info );
	void radarHook( SRHook::Info &info );
	void wantedHook( SRHook::CPU &cpu );
	void armorHook( SRHook::CPU &cpu );
	void breathHook( SRHook::CPU &cpu );
	void ammoHook( SRHook::Info &info );
	void clAmmoHook( uint8_t &alpha );
	void chartHook( RwRGBA &color );
	void percentageHook( RwRGBA &color );
	void customDrawHook();
};

#endif // GUI_H
