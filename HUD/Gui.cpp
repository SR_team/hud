#include "Gui.h"
#include "DefaultHud.h"
#include "Font.h"
#include "Image.h"
#include "Itemcrosshair.h"
#include "font.hpp"
#include "loader/loader.h"
#include <base/d3d9/color.h>
#include <filesystem>
#include <imgui-filebrowser/imfilebrowser.h>
#include <imgui/ImLoader.h>
#include <regex>
#include <shlobj.h>

Gui::Gui( SRDescent *parent, DefaultHud *pDefHud, IDE *pIDE ) : SRDescent( parent ), _defHud( pDefHud ), _ide( pIDE ) {

#ifdef FULL_DX_HOOK
	_loaderSlots =
		ImGui::ImLoader::CreateInstance( g_class.DirectX->d3d9_device(), g_vars.hwnd )
			->connectToCallbacks( g_class.DirectX->onPreReset, g_class.DirectX->onPostReset, g_class.DirectX->onDraw );
#else
	_loaderSlots =
		ImGui::ImLoader::CreateInstance( g_class.draw->d3d9_device(), g_vars.hwnd )
			->connectToCallbacks( g_class.draw->onPreReset, g_class.draw->onPostReset, g_class.draw->onDraw );
#endif

	ImGui::GetIO().Fonts->AddFontFromMemoryTTF( FiraCode_Regular_ttf, FiraCode_Regular_ttf_len, 16.0F, NULL,
												ImGui::GetIO().Fonts->GetGlyphRangesCyrillic() );

	_mainMenu = new ImMenu( PROJECT_NAME );
	_mainMenu->flags |= ImGuiWindowFlags_NoResize;
	_mainMenu->onHide += [this] {
		if ( _ide ) _ide->close();
		g_class.cursor->hideCursor();
		auto samp = GetModuleHandleA( "samp.dll" );
		if ( !samp || samp == INVALID_HANDLE_VALUE ) ImGui::GetIO().MouseDrawCursor = false;
		saveCfg();
	};
	_mainMenu->onShow += [] {
		g_class.cursor->showCursor();
		auto samp = GetModuleHandleA( "samp.dll" );
		if ( !samp || samp == INVALID_HANDLE_VALUE ) ImGui::GetIO().MouseDrawCursor = true;
	};
#ifndef NOGUI
	auto wgt = new ImDrawCustom;
	wgt->onDraw += std::tuple{ this, &Gui::draw };
	_mainMenu->chields.push_back( wgt );
#endif
	ImGui::ImLoader::get()->onDraw += [this] {
		if ( _ide ) _ide->draw();
	};
	if ( _ide ) _ide->onClose += [this] { _mainMenu->setCollapsed( false ); };
	g_class.events->onEventProc += [this]( uint32_t uMsg, WPARAM, LPARAM ) {
		if ( _mainMenu->isHidden() ) return;
		if ( uMsg == WM_LBUTTONDBLCLK )
			g_class.events->hookEvent();
		else if ( uMsg == WM_RBUTTONDOWN || uMsg == WM_RBUTTONUP || uMsg == WM_RBUTTONDBLCLK )
			g_class.events->hookEvent();
		else if ( uMsg == WM_MBUTTONDOWN || uMsg == WM_MBUTTONUP || uMsg == WM_MBUTTONDBLCLK )
			g_class.events->hookEvent();
		else if ( uMsg == WM_CHAR || uMsg == WM_MOUSEMOVE || uMsg == WM_HSCROLL || uMsg == WM_VSCROLL )
			g_class.events->hookEvent();
	};

	if ( !std::filesystem::exists( "HUD" ) ) std::filesystem::create_directory( "HUD" );

	auto defCfg	   = new Config( this, "HUD\\default.hud" );
	defCfg->author = "ROCKSTAR GAMES";
	defCfg->name   = "Default HUD";
	defCfg->desc   = "The default game HUD, dumped at game start";

	_configs.push_back( defCfg );
	switchHud();
	loadHuds();

#ifndef NOGUI
#	ifndef ENDUSER
	_mid = g_class.events->onMainLoop += [this] {
		if ( _mainMenu->isShowed() ) {
			g_class.cursor->update();
		}
		if ( _drag ) {
			for ( auto &&item : _defHud->items() ) {
				if ( item->isHidden() ) continue;
				if ( item->processDrag() ) break;
			}
			if ( _activeCfg ) {
				for ( auto &&item : _activeCfg->items() ) {
					if ( item->isHidden() ) continue;
					if ( item->processDrag() ) break;
				}
			}
		}
	};

	_kdid = g_class.events->onKeyDown += [&]( int key ) {
		if ( _mainMenu->isHidden() ) return;
		g_class.events->hookEvent();
		if ( key != VK_LBUTTON || _drag ) return;
		POINT m;
		GetCursorPos( &m );
		ScreenToClient( g_vars.hwnd, &m );
		if ( m.x >= _mainMenu->pos().x && m.y >= _mainMenu->pos().y &&
			 m.x <= _mainMenu->pos().x + _mainMenu->size().x && m.y <= _mainMenu->pos().y + _mainMenu->size().y )
			return;
		for ( auto &&item : _defHud->items() ) {
			if ( item->isHidden() ) continue;
			if ( item->isDrag() ) {
				_drag = true;
				return;
			}
		}
		if ( _activeCfg ) {
			for ( auto &&item : _activeCfg->items() ) {
				if ( item->isHidden() ) continue;
				if ( item->isDrag() ) {
					_drag = true;
					return;
				}
			}
		}
	};

	_kuid = g_class.events->onKeyUp += [&]( int key ) {
		static bool firstOpen = true;
		if ( _mainMenu->isHidden() ) {
			firstOpen = true;
			return;
		}
		if ( !firstOpen ) {
			g_class.events->hookEvent();
		} else if ( key == 'D' )
			firstOpen = false;
		if ( key != VK_LBUTTON || !_drag ) return;
		for ( auto &&item : _defHud->items() ) item->endDrag();
		if ( _activeCfg )
			for ( auto &&item : _activeCfg->items() ) item->endDrag();
		_drag = false;
	};
#	endif

	// bind callbacks
	showHud.onBefore += std::tuple{ this, &Gui::hudHook };
	showRadar.onBefore += std::tuple{ this, &Gui::radarHook };
	showWanted.onBefore += std::tuple{ this, &Gui::wantedHook };
	showArmour.onAfter += std::tuple{ this, &Gui::armorHook };
	showBreath.onAfter += std::tuple{ this, &Gui::breathHook };
	showAmmo.onBefore += std::tuple{ this, &Gui::ammoHook };
	clAmmo.onBefore += std::tuple{ this, &Gui::clAmmoHook };
#endif
	clChart.onBefore += std::tuple{ this, &Gui::chartHook };
	clPercentage.onBefore += std::tuple{ this, &Gui::percentageHook };
	hudDraw.onBefore += std::tuple{ this, &Gui::customDrawHook };
	// install hooks
#ifndef NOGUI
	showHud.install();
	showRadar.install();
	showWanted.install();
	showArmour.install();
	showBreath.install();
	showAmmo.install();
	clAmmo.install( 8 );
#endif
	clChart.install( 36 );
	clPercentage.install();
	hudDraw.install();

	memsafe::write<uint16_t>( 0x872A16, 0x2020 );
}

Gui::~Gui() {
	memsafe::write<uint16_t>( 0x872A16, 0x2525 );
	g_class.events->onKeyUp -= _kuid;
	g_class.events->onKeyDown -= _kdid;
	g_class.events->onMainLoop -= _mid;
	if ( fileExplorer ) delete fileExplorer;
	ImGui::ImLoader::DeleteInstance();
}

void Gui::open() {
	_mainMenu->show();
	_mainMenu->setCollapsed( false );
}

bool Gui::isOpened() {
	if ( !_mainMenu ) return false;
	return _mainMenu->isShowed();
}

RwTexture *Gui::texture( std::string_view name ) {
	if ( !_activeCfg ) return nullptr;
	return _activeCfg->texture( name );
}

sol::state &Gui::lua() {
	if ( !_lua ) {
		_lua = new sol::state;
		_lua->open_libraries( sol::lib::string, sol::lib::math, sol::lib::utf8, sol::lib::table, sol::lib::coroutine );
		initLuaAndIDE( _ide );

		if ( std::filesystem::exists( "HUD" ) ) {
			for ( auto &&file : std::filesystem::directory_iterator( std::filesystem::current_path() / "HUD" ) ) {
				if ( !file.is_regular_file() ) continue;
				if ( file.path().extension().string() != ".plugin" ) continue;
				auto lib = LoadLibraryA( ( std::filesystem::path( "HUD" ) / file.path().filename() ).string().data() );
				try {
					auto plugin = new Plugin( lib, this );
					plugin->init_lua( _lua );
					plugin->init_ide();
					_plugins.push_back( plugin );
				} catch ( std::exception &e ) {
					MessageBox( e.what(), file.path().filename().string().data() );
				}
			}
		}
	}
	return *_lua;
}

sol::state &Gui::reset_lua() {
	if ( _lua ) {
		for ( auto &&plugin : _plugins ) delete plugin;
		_plugins.clear();
		delete _lua;
		_lua = nullptr;
	}
	return lua();
}

void Gui::draw() {
#ifndef ENDUSER
	_mainMenu->setSize( { 680, 400 } );

	ImGui::BeginGroup();
	ImGui::PushItemWidth( 100 );
	{
		ImGui::Text( "Page:" );
		ImGui::RadioButton( "Chose HUD##page", (int *)&_page, int( MenuPage::chose_hud ) );
		ImGui::RadioButton( "Edit default HUD##page", (int *)&_page, int( MenuPage::edit_hud ) );
		ImGui::RadioButton( "Edit crosshairs##page", (int *)&_page, int( MenuPage::crosshairs ) );
		ImGui::RadioButton( "Custom items##page", (int *)&_page, int( MenuPage::custom_items ) );
		ImGui::RadioButton( "Custom fonts##page", (int *)&_page, int( MenuPage::custom_fonts ) );
		ImGui::RadioButton( "Textures##page", (int *)&_page, int( MenuPage::textures ) );
		ImGui::RadioButton( "Scripts##page", (int *)&_page, int( MenuPage::scripts ) );
		ImGui::RadioButton( "Save HUD##page", (int *)&_page, int( MenuPage::save_hud ) );
	}
	ImGui::PopItemWidth();
	ImGui::EndGroup();

	ImGui::SameLine( 0, 20 );
#else
	_mainMenu->setSize( { 450, 400 } );
#endif

	ImGui::BeginChild( "##page_data" /*, { 450, 365 }, true*/ );
	switch ( _page ) {
		// using enum MenuPage; - во влажных мечтах
		case MenuPage::chose_hud:
			choseHud();
			break;
		case MenuPage::edit_hud:
			editHud();
			break;
		case MenuPage::custom_items:
			customItems();
			break;
		case MenuPage::textures:
			textures();
			break;
		case MenuPage::crosshairs:
			crosshairs();
			break;
		case MenuPage::save_hud:
			saveHud();
			break;
		case MenuPage::scripts:
			scripts();
			break;
		case MenuPage::custom_fonts:
			fonts();
			break;
	}
	ImGui::EndChild();
}

void Gui::choseHud() {
	if ( ImGui::Button( "Reload all HUD's", { ImGui::GetWindowContentRegionWidth(), 30 } ) ) {
		saveCfg();
		switchHud();
		while ( _configs.size() > 1 ) _configs.pop_back();
		loadHuds();
		reset_lua();
	}
	ImGui::Separator();
	ImGui::BeginChild( "chose_sub" );
	int rb = _activeId;
	for ( int i = 0; auto &&cfg : _configs ) {
		ImGui::RadioButton( cfg->ImName().data(), &rb, i++ );
		if ( ImGui::IsItemHovered() )
			ImGui::SetTooltip( "Author: %s\nDescription: %s", cfg->author.data(), cfg->desc.data() );
	}
	if ( rb != _activeId ) {
		_activeId = rb;
		switchHud( _activeId );
		saveCfg();
	}
	ImGui::EndChild();
}

void Gui::customItems() {
	if ( _activeCfg ) {
		ImGui::PushItemWidth( 210 );
		ImGui::InputText( "##image_name", newCustItemNameBuf, 255 );
		ImGui::PopItemWidth();
		ImGui::SameLine( 0, 10 );
		if ( ImGui::Button( "Add image##customItems" ) && *newCustItemNameBuf ) {
			auto img = _activeCfg->addImage( newCustItemNameBuf );
			if ( img ) {
				memset( newCustItemNameBuf, 0, sizeof( newCustItemNameBuf ) );
			}
		}
		ImGui::SameLine( 0, 10 );
		if ( ImGui::Button( "Add font##customItems" ) && *newCustItemNameBuf ) {
			auto img = _activeCfg->addFont( newCustItemNameBuf );
			if ( img ) {
				memset( newCustItemNameBuf, 0, sizeof( newCustItemNameBuf ) );
			}
		}
		ImGui::SameLine( 0, 10 );
		if ( ImGui::Button( "Add chart##customItems" ) && *newCustItemNameBuf ) {
			auto img = _activeCfg->addChart( newCustItemNameBuf );
			if ( img ) {
				memset( newCustItemNameBuf, 0, sizeof( newCustItemNameBuf ) );
			}
		}
		ImGui::Separator();

		ImGui::BeginChild( "custom_sub" );
		_activeCfg->drawCustomItems();
		ImGui::EndChild();
	} else
		ImGui::BulletText( "ERROR: Current HUD don't have savable config" );
}

void Gui::textures() {
	if ( _activeCfg ) {
		if ( _txAdding && fileExplorer )
			texturesAdd();
		else
			texturesList();
	} else
		ImGui::BulletText( "ERROR: Current HUD don't have savable config" );
}

void Gui::texturesAdd() {
	fileExplorer->Display();

	if ( !fileExplorer->HasSelected() && !fileExplorer->IsOpened() ) {
		_txAdding = false;
		return;
	}

	if ( fileExplorer->HasSelected() && !txNameAssigned ) {
		txNameAssigned = true;
		auto name	   = fileExplorer->GetSelected().filename().string();
		strncpy( txNameBuf, name.substr( 0, name.length() - 4 ).data(), 255 );
	}

	ImGui::InputText( "Name##png_name", txNameBuf, 255 );
	if ( ImGui::Button( "Add##apply_png" ) && !fileExplorer->IsOpened() && fileExplorer->HasSelected() ) {
		auto res = _activeCfg->addTexture( fileExplorer->GetSelected(), txNameBuf );
		if ( res == Config::AddTexCode::success )
			_txAdding = false;
		else if ( res == Config::AddTexCode::no_access )
			fileExplorer->Open();
		else
			ImGui::OpenPopup( "err:addtex_namebusy" );
	}
	ImGui::SameLine( 0, 10 );
	if ( ImGui::Button( "Cancel##apply_png" ) ) {
		if ( fileExplorer->IsOpened() ) fileExplorer->Close();
		_txAdding = false;
	}

	if ( ImGui::BeginPopup( "err:addtex_namebusy" ) ) {
		ImGui::Text( "Texture with same name exists" );
		ImGui::EndPopup();
	}
}

void Gui::texturesList() {
	if ( ImGui::Button( "Add texture##new_png", { ImGui::GetWindowContentRegionWidth(), 30 } ) ) {
		if ( !fileExplorer ) {
			fileExplorer = new ImGui::FileBrowser();
			fileExplorer->SetTitle( "Select PNG texture" );
			fileExplorer->SetTypeFilters( { ".png" } );
		}
		fileExplorer->Open();
		_txAdding	   = true;
		txNameAssigned = false;
		memset( txNameBuf, 0, sizeof( txNameBuf ) );
	}
	ImGui::BeginChild( "textures_sub" );
	_activeCfg->drawTextureList( _defHud );
	ImGui::EndChild();
}

void Gui::crosshairs() {
	if ( _activeCfg )
		_activeCfg->drawCrossHairs( _defHud );
	else
		ImGui::BulletText( "ERROR: Current HUD don't have savable config" );
}

void Gui::editHud() {
	if ( _activeCfg )
		_activeCfg->drawDefaultHud( _defHud );
	else
		ImGui::BulletText( "ERROR: Current HUD don't have savable config" );
}

void Gui::saveHud() {
	if ( _activeCfg ) {
		if ( !_saving ) {
			_saving = true;
			memset( hudNameBuf, 0, sizeof( hudNameBuf ) );
			memset( hudAuthorBuf, 0, sizeof( hudAuthorBuf ) );
			memset( hudDescBuf, 0, sizeof( hudDescBuf ) );
			memset( hudArchiveBuf, 0, sizeof( hudArchiveBuf ) );
			strncpy( hudNameBuf, ( _activeCfg->name + "-mod" ).data(), 255 );
			DWORD username_len = sizeof( hudAuthorBuf );
			char  user[256]{ 0 };
			GetUserNameA( user, &username_len );
			if ( ( !_configs.empty() && _activeCfg == _configs.front() ) || _activeCfg->author == user ) {
				strncpy( hudAuthorBuf, user, 255 );
				strncpy( hudDescBuf, _activeCfg->desc.data(), 255 );
			} else {
				if ( _activeCfg->author.find( user ) == std::string::npos )
					strncpy( hudAuthorBuf, ( _activeCfg->author + ", " + std::string( user ) ).data(), 255 );
				else
					strncpy( hudAuthorBuf, _activeCfg->author.data(), 255 );
				strncpy( hudDescBuf, ( _activeCfg->desc + "\nModded by " + std::string( user ) ).data(), 255 );
			}
		}
		ImGui::InputText( "Displayed name", hudNameBuf, 255 );
		ImGui::InputText( "Author", hudAuthorBuf, 255 );
		ImGui::InputTextMultiline( "Description", hudDescBuf, 1024 );
		ImGui::InputText( "File name", hudArchiveBuf, 255 );

		if ( ImGui::Button( "Save" ) && *hudNameBuf && *hudAuthorBuf && *hudDescBuf && *hudArchiveBuf ) {
			auto hud = "HUD\\" + std::string( hudArchiveBuf ) + ".hud";
			if ( !_activeCfg->write( _defHud, hud ) )
				ImGui::OpenPopup( "err:save_hud" );
			else {
				std::filesystem::remove( hud );
				_activeCfg->name   = hudNameBuf;
				_activeCfg->author = hudAuthorBuf;
				_activeCfg->desc   = hudDescBuf;
				_activeCfg->write( _defHud, hud );
				auto cfg = new Config( this, hud );
				if ( cfg->read() ) {
					_activeCfg->reset( _defHud, _activeCfg == _configs.front() );
					_configs.push_back( cfg );
					switchHud( cfg );
					_activeId = _configs.size() - 1;
				} else
					switchHud( _activeCfg );
				saveCfg();
				_saving = false;
			}
		}

		if ( ImGui::BeginPopup( "err:save_hud" ) ) {
			ImGui::Text( "Can't save hud. Try to set another 'File name'" );
			ImGui::EndPopup();
		}
	} else
		ImGui::BulletText( "ERROR: Current HUD don't have savable config" );
}

void Gui::scripts() {
	if ( _activeCfg ) {
		ImGui::SetNextItemWidth( 355 );
		ImGui::InputText( "##new_script_name", scriptNameBuf, 255 );
		ImGui::SameLine( 0, 10 );
		if ( ImGui::Button( "Add script" ) && *scriptNameBuf ) {
			try {
				_activeCfg->scripts.at( scriptNameBuf );
				ImGui::OpenPopup( "err:addlua_namebusy" );
			} catch ( ... ) {
				_activeCfg->scripts[scriptNameBuf] = "";
				memset( scriptNameBuf, 0, sizeof( scriptNameBuf ) );
			}
		}
		ImGui::BeginChild( "scripts_sub" );
		for ( auto &&[name, code] : _activeCfg->scripts ) {
			if ( name.empty() ) continue;
			ImGui::Separator();
			ImGui::BeginGroup();
			if ( code.length() > 1024 * 1024 )
				ImGui::Text( "%s: %dmb", name.data(), code.length() / ( 1024 * 1024 ) );
			else if ( code.length() > 1024 )
				ImGui::Text( "%s: %dkb", name.data(), code.length() / 1024 );
			else if ( code.length() > 1 )
				ImGui::Text( "%s: %db", name.data(), code.length() - 1 );
			else
				ImGui::Text( "%s: empty", name.data() );
			ImGui::SameLine( 365 );
			if ( ImGui::Button( ( "Edit##ed_lua" + name ).data() ) && _edScript != name ) {
				_edScript = name;
				if ( _ide ) _ide->text( code );
				if ( _ide ) _ide->open();
				_mainMenu->setCollapsed( true );
			}
			ImGui::SameLine( 0, 10 );
			if ( ImGui::Button( ( "Remove##del_lua" + name ).data() ) ) {
				if ( _edScript == name ) {
					_edScript.clear();
					if ( _ide ) _ide->text( "" );
					if ( _ide ) _ide->close();
				}
				_activeCfg->scripts.erase( name );
				break;
			}
			ImGui::EndGroup();
		}
		ImGui::EndChild();

		if ( ImGui::BeginPopup( "err:addlua_namebusy" ) ) {
			ImGui::Text( "Script with same name exists" );
			ImGui::EndPopup();
		}
	} else
		ImGui::BulletText( "ERROR: Current HUD don't have savable config" );
}

void Gui::fonts() {
	if ( _activeCfg ) {
		_activeCfg->drawFonts( _defHud );
	} else
		ImGui::BulletText( "ERROR: Current HUD don't have savable config" );
}

void Gui::loadHuds() {
#ifndef NOGUI
	std::string lastHud = "Default HUD##HUD\\default.hud";
#else
	std::string lastHud;
#endif
	std::ifstream cfg_file( "HUD\\config.json" );
	if ( cfg_file.is_open() ) {
		std::string data;
		std::getline( cfg_file, data, '\0' );
		cfg_file.close();
		tiny::TinyJson config;
		config.ReadJson( data );
		lastHud = config.Get<std::string>( "LastHUD" );
	}

	if ( !std::filesystem::exists( "HUD" ) ) std::filesystem::create_directory( "HUD" );
	for ( auto &&file : std::filesystem::directory_iterator( std::filesystem::current_path() / "HUD" ) ) {
		if ( file.is_regular_file() && file.path().extension().string() == ".hud" ) {
			auto cfg = new Config( this, ( std::filesystem::path( "HUD" ) / file.path().filename() ).string() );
			if ( cfg->read() ) {
				_configs.push_back( cfg );
				if ( cfg->ImName() == lastHud ) {
					switchHud( cfg );
					_activeId = _configs.size() - 1;
				}
			}
		}
	}
#ifdef NOGUI
	if ( lastHud.empty() && _configs.size() > 1 ) {
		_activeId = _configs.size() - 1;
		switchHud( _activeId );
	}
#endif
}

void Gui::saveCfg() {
	if ( _activeCfg ) {
		std::ofstream cfg_file( "HUD\\config.json" );
		if ( cfg_file.is_open() ) {
			tiny::TinyJson config;
			config["LastHUD"].Set( _activeCfg->ImName() );
			cfg_file << config.WriteJson();
			cfg_file.close();
		}
	}
}

void Gui::switchHud( Config *cfg ) {
	try {
		_drag	  = false;
		_txAdding = false;
		_saving	  = false;
		if ( _ide ) _ide->close();
		_edScript.clear();
		for ( auto &&plugin : _plugins ) plugin->reset();
		lua().collect_garbage();
		initLuaAndIDE();
		if ( !cfg && !_configs.empty() ) {
			cfg		  = _configs.front();
			_activeId = 0;
		}
		if ( !cfg || !cfg->reset( _defHud, cfg == _configs.front() ) ) {
			for ( auto &&item : _defHud->items() ) item->reset();
			for ( auto &&item : _defHud->crossHairs() ) item->reset();
		}
		_activeCfg = cfg;
	} catch ( std::exception &e ) {
		MessageBox( e.what() );
	}
}

void Gui::switchHud( int id ) {
	if ( id < 0 || id > _configs.size() ) return;
	switchHud( _configs[id] );
}

void Gui::initLuaAndIDE( IDE *ide ) {
	for ( auto &&plugin : _plugins ) plugin->init_lua( _lua );
	initLuaAndIDECrosshairs( ide );
	initLuaAndIDEImage( ide );
	initLuaAndIDEFont( ide );
	initLuaAndIDEChart( ide );

	auto addFunction = [=]( std::string_view name, auto func, std::string_view desc ) {
		_lua->set_function( name, func );
		if ( ide ) ide->addFunction( name, desc );
	};
	_defHud->initLuaAndIDE( *_lua, ide );

	addFunction(
		"radar_textureBorders",
		[this] {
			if ( !_activeCfg ) return "";
			return _activeCfg->getRadardist().data();
		},
		"Get borders texture\n\nReturn:\n * string - texture" );
	addFunction(
		"radar_textureAirPlane",
		[this] {
			if ( !_activeCfg ) return "";
			return _activeCfg->getAirPlane().data();
		},
		"Get air plane texture\n\nReturn:\n * string - texture" );
	addFunction(
		"radar_textureIcon", [this]( std::string icon ) { return _defHud->radarIcon( icon ); },
		"Get icon texture\n\nArgs:\n * string - icon name\nReturn:\n * string - texture" );
	addFunction(
		"weapon_textureIcon",
		[this]( int id ) {
			if ( !_activeCfg ) return "";
			return _activeCfg->getWeapon( id ).data();
		},
		"Get weapon icon texture\n\nArgs:\n * int - weapon id\nReturn:\n * string - texture" );
	addFunction(
		"radar_setTextureBorders",
		[this]( std::string name ) {
			if ( !_activeCfg ) return;
			auto tx = _activeCfg->texture( name );
			_defHud->setRadarTexture( tx );
		},
		"Set borders texture\n\nArgs:\n * string - texture" );
	addFunction(
		"radar_setTextureAirPlane",
		[this]( std::string name ) {
			if ( !_activeCfg ) return;
			auto tx = _activeCfg->texture( name );
			_defHud->setAirPlaneTexture( tx );
		},
		"Set air plane texture\n\nArgs:\n * string - texture" );
	addFunction(
		"radar_setTextureIcon",
		[this]( std::string icon, std::string name ) {
			if ( !_activeCfg ) return;
			auto tx = _activeCfg->texture( name );
			_defHud->setRadarIcon( icon, name, tx );
		},
		"Set icon texture\n\nArgs:\n * string - icon\n * string - texture" );
	addFunction(
		"weapon_setTextureIcon",
		[this]( int id, std::string name ) {
			if ( !_activeCfg ) return;
			if ( id < 0 || id > 46 ) return;
			auto tx = _activeCfg->texture( name );
			_defHud->setWeaponTexture( id, tx );
		},
		"Set texture icon\n\nArgs:\n * int - weapon id\n * string - texture" );

	auto int2rgba = []( int clr ) {
		auto rgba = *(RwRGBA *)&clr;
		return std::make_tuple( rgba.red, rgba.green, rgba.blue, rgba.alpha );
	};
	auto rgba2int = []( uint8_t red, uint8_t green, uint8_t blue, uint8_t alpha ) {
		RwRGBA clr;
		clr.red	  = red;
		clr.green = green;
		clr.blue  = blue;
		clr.alpha = alpha;
		return *(int *)&clr;
	};
	addFunction( "int_to_rgba", int2rgba,
				 "Convert int to RGBA color\n\nArgs:\n * int - color\nReturns:\n * byte - red\n * byte - green\n * "
				 "byte - blue\n * byte - alpha" );
	addFunction( "rgba_from_int", int2rgba,
				 "Convert int to RGBA color\n\nArgs:\n * int - color\nReturns:\n * byte - red\n * byte - green\n * "
				 "byte - blue\n * byte - alpha" );
	addFunction( "rgba_to_int", rgba2int,
				 "Convert RGBA to color\n\nArgs:\n * byte - red\n * byte - green\n * byte - blue\n * byte - "
				 "alpha\nReturn:\n * int - color" );
	addFunction( "int_from_rgba", rgba2int,
				 "Convert RGBA to color\n\nArgs:\n * byte - red\n * byte - green\n * byte - blue\n * byte - "
				 "alpha\nReturn:\n * int - color" );
	addFunction(
		"timer", [] { return GetTickCount(); }, "Get time left from start PC\n\nReturn:\n * int - milliseconds" );
	addFunction(
		"GetTickCount", [] { return GetTickCount(); },
		"Get time left from start PC\n\nReturn:\n * int - milliseconds" );
	addFunction(
		"store_float",
		[this]( std::string name, float value ) {
			if ( !_activeCfg ) return;
			_activeCfg->globFloat[name] = value;
		},
		"Store float value in global storage\n\nArgs:\n * string - name\n * float - value" );
	addFunction(
		"store_int",
		[this]( std::string name, int value ) {
			if ( !_activeCfg ) return;
			_activeCfg->globInt[name] = value;
		},
		"Store int value in global storage\n\nArgs:\n * string - name\n * int - value" );
	addFunction(
		"store_string",
		[this]( std::string name, std::string value ) {
			if ( !_activeCfg ) return;
			_activeCfg->globString[name] = value;
		},
		"Store string value in global storage\n\nArgs:\n * string - name\n * string - value" );
	addFunction(
		"load_float",
		[this]( std::string name ) {
			if ( !_activeCfg ) return 0.0f;
			return _activeCfg->globFloat[name];
		},
		"Load float value from global storage\n\nArgs:\n * string - name\nReturn:\n * float - value" );
	addFunction(
		"load_int",
		[this]( std::string name ) {
			if ( !_activeCfg ) return 0;
			return _activeCfg->globInt[name];
		},
		"Load int value from global storage\n\nArgs:\n * string - name\nReturn:\n * int - value" );
	addFunction(
		"load_string",
		[this]( std::string name ) {
			if ( !_activeCfg ) return "";
			return _activeCfg->globString[name].c_str();
		},
		"Load string value from global storage\n\nArgs:\n * string - name\nReturn:\n * string - value" );
	addFunction(
		"fps", [this] { return _fps; }, "Get current FPS\n\nReturn:\n * int - frames per second" );
	addFunction(
		"mone", [this] { return 48.0f / _fps; }, "Get 1.0 corrector for animations\n\nReturn:\n * float - multiplier" );
	addFunction(
		"isPreview", [this] { return _mainMenu->isShowed(); },
		"Check is HUD are preview\n\nReturn:\n * bool - is preview" );
	addFunction(
		"time",
		[] {
			auto time = ::time( 0 );
			auto lt	  = ::localtime( &time );
			return std::make_tuple( lt->tm_hour, lt->tm_min, lt->tm_sec );
		},
		"Get current time\n\nReturn:\n * int - hour\n * int - min\n * int - sec" );
	addFunction(
		"date",
		[] {
			auto time = ::time( 0 );
			auto lt	  = ::localtime( &time );
			return std::make_tuple( lt->tm_year + 1900, lt->tm_mon + 1, lt->tm_mday );
		},
		"Get current time\n\nReturn:\n * int - year\n * int - month\n * int - day" );
}

void Gui::initLuaAndIDECrosshairs( IDE *ide ) {
	auto addFunction = [=]( std::string_view name, auto func, std::string_view desc ) {
		_lua->set_function( name, func );
		if ( ide ) ide->addFunction( name, desc );
	};

	for ( auto &&item : _defHud->crossHairs() ) {
		addFunction(
			item->luaPrefix() + "_texture", [=] { return item->txName(); },
			"Get crosshair texture\n\nReturn:\n * string - texture" );
		addFunction(
			item->luaPrefix() + "_setTexture",
			[=]( std::string name ) {
				if ( !_activeCfg ) return;
				auto tx = _activeCfg->texture( name );
				if ( !tx )
					item->setImage( "" );
				else
					item->setImage( name, tx );
			},
			"Set crosshair texture\n\nArgs:\n * string - texture" );
	}
}

void Gui::initLuaAndIDEImage( IDE *ide ) {
	initLuaAndIDEHUD<Image>( ide, "image" );

	auto addFunction = [=]( std::string_view name, auto func, std::string_view desc ) {
		_lua->set_function( name, func );
		if ( ide ) ide->addFunction( name, desc );
	};
	auto getItem = [this]( std::string name ) -> Image * {
		if ( !_activeCfg ) return nullptr;
		for ( auto &&item : _activeCfg->items() ) {
			auto hud = dynamic_cast<Image *>( item );
			if ( !hud ) continue;
			if ( hud->imgName() == name ) return hud;
		}
		return nullptr;
	};

	addFunction(
		"image_new",
		[this]( std::string name ) {
			if ( !_activeCfg ) return false;
			auto img = _activeCfg->addImage( name );
			return img ? true : false;
		},
		"Create new image\n\nArgs:\n * string - name\nReturn:\n * bool - is created" );
	addFunction(
		"image_exists",
		[this]( std::string name ) {
			if ( !_activeCfg ) return false;
			auto it =
				std::find_if( _activeCfg->items().begin(), _activeCfg->items().end(), [&]( const Itembase *item ) {
					auto hud = dynamic_cast<const Image *>( item );
					if ( !hud ) return false;
					return hud->imgName() == name;
				} );
			return it != _activeCfg->items().end();
		},
		"Check is image exists\n\nArgs:\n * string - name\nReturn:\n * bool - is exists" );

	addFunction(
		"image_angle",
		[getItem]( std::string name ) {
			auto item = getItem( name );
			if ( !item ) return 0.0f;
			return item->angle;
		},
		"Get image rotation algne\n\nArgs:\n * string - name\nReturn:\n * float - angle" );
	addFunction(
		"image_setAngle",
		[getItem]( std::string name, float angle ) {
			auto item = getItem( name );
			if ( !item ) return;
			item->angle = angle;
		},
		"Set image rotation angle\n\nArgs:\n * string - name\n * float - angle" );
	addFunction(
		"image_rect",
		[getItem]( std::string name ) {
			auto item = getItem( name );
			if ( !item ) return std::make_tuple( 0.0f, 0.0f, 1.0f, 1.0f );
			return std::make_tuple( item->rect.x1, item->rect.y1, item->rect.x2, item->rect.y2 );
		},
		"Get image rect\n\nArgs:\n * string - name\nReturn:\n * float - left\n * float - top\n * float - right\n * "
		"float - bottom" );
	addFunction(
		"image_setRect",
		[getItem]( std::string name, float left, float top, float right, float bottom ) {
			auto item = getItem( name );
			if ( !item ) return;
			item->rect.x1 = left;
			item->rect.y1 = top;
			item->rect.x2 = right;
			item->rect.y2 = bottom;
			for ( int i = 0; i < 4; ++i ) {
				if ( ( (float *)&item->rect )[i] < 0.0f )
					( (float *)&item->rect )[i] = 0.0f;
				else if ( ( (float *)&item->rect )[i] > 1.0f )
					( (float *)&item->rect )[i] = 1.0f;
			}
		},
		"Set image rect\n\nArgs:\n * string - name\n * float - left\n * float - top\n * float - right\n * float - "
		"bottom" );
	addFunction(
		"image_texture",
		[getItem]( std::string name ) {
			auto item = getItem( name );
			if ( !item ) return "";
			return item->txName().data();
		},
		"Get image texture\n\nArgs:\n * string - name\nReturn:\n * string - texture" );
	addFunction(
		"image_setTexture",
		[this, getItem]( std::string name, std::string texture ) {
			auto item = getItem( name );
			if ( !item ) return;
			auto tx = _activeCfg->texture( texture );
			if ( tx )
				item->setImage( texture, tx );
			else
				item->setImage( "" );
		},
		"Set image texture\n\nArgs:\n * string - name\n * string - texture" );
}

void Gui::initLuaAndIDEFont( IDE *ide ) {
	initLuaAndIDEHUD<Font>( ide, "font" );

	auto addFunction = [=]( std::string_view name, auto func, std::string_view desc ) {
		_lua->set_function( name, func );
		if ( ide ) ide->addFunction( name, desc );
	};
	auto getItem = [this]( std::string name ) -> Font * {
		if ( !_activeCfg ) return nullptr;
		for ( auto &&item : _activeCfg->items() ) {
			auto hud = dynamic_cast<Font *>( item );
			if ( !hud ) continue;
			if ( hud->imgName() == name ) return hud;
		}
		return nullptr;
	};

	addFunction(
		"font_new",
		[this]( std::string name ) {
			if ( !_activeCfg ) return false;
			auto img = _activeCfg->addFont( name );
			return img ? true : false;
		},
		"Create new font\n\nArgs:\n * string - name\nReturn:\n * bool - is created" );
	addFunction(
		"font_exists",
		[this]( std::string name ) {
			if ( !_activeCfg ) return false;
			auto it =
				std::find_if( _activeCfg->items().begin(), _activeCfg->items().end(), [&]( const Itembase *item ) {
					auto hud = dynamic_cast<const Font *>( item );
					if ( !hud ) return false;
					return hud->imgName() == name;
				} );
			return it != _activeCfg->items().end();
		},
		"Check is font exists\n\nArgs:\n * string - name\nReturn:\n * bool - is exists" );

	addFunction(
		"font_isBackground",
		[getItem]( std::string name ) {
			auto item = getItem( name );
			if ( !item ) return false;
			return item->background;
		},
		"Check is background enabled\n\nArgs:\n * string - name\nReturn:\n * bool - is enabled" );
	addFunction(
		"font_isShadow",
		[getItem]( std::string name ) {
			auto item = getItem( name );
			if ( !item ) return false;
			return item->shadow;
		},
		"Check is shadow enabled\n\nArgs:\n * string - name\nReturn:\n * bool - is enabled" );
	addFunction(
		"font_isBorder",
		[getItem]( std::string name ) {
			auto item = getItem( name );
			if ( !item ) return false;
			return item->border;
		},
		"Check is border enabled\n\nArgs:\n * string - name\nReturn:\n * bool - is enabled" );
	addFunction(
		"font_isProportional",
		[getItem]( std::string name ) {
			auto item = getItem( name );
			if ( !item ) return false;
			return item->proportional;
		},
		"Check is proportional enabled\n\nArgs:\n * string - name\nReturn:\n * bool - is enabled" );
	addFunction(
		"font_align",
		[getItem]( std::string name ) {
			auto item = getItem( name );
			if ( !item ) return 1;
			return (int)item->align;
		},
		"Get text align\n\nArgs:\n * string - name\nReturn:\n * int - align\n\nAlign:\n 0. Center\n 1. Left\n 2. "
		"Right" );
	addFunction(
		"font_style",
		[getItem]( std::string name ) {
			auto item = getItem( name );
			if ( !item ) return 1;
			return (int)item->style;
		},
		"Get text style\n\nArgs:\n * string - name\nReturn:\n * int - style\n\nStyles:\n 0. Gothic\n 1. Subtitles\n 2. "
		"Menu\n 3. "
		"Pricedown" );
	addFunction(
		"font_edge",
		[getItem]( std::string name ) {
			auto item = getItem( name );
			if ( !item ) return 0;
			return item->edge;
		},
		"Get text edge\n\nArgs:\n * string - name\nReturn:\n * int - edge" );
	addFunction(
		"font_shadowOffset",
		[getItem]( std::string name ) {
			auto item = getItem( name );
			if ( !item ) return 1;
			return item->shadow_offset;
		},
		"Get text shadow offset\n\nArgs:\n * string - name\nReturn:\n * int - shadow offset" );
	addFunction(
		"font_text",
		[getItem]( std::string name ) {
			auto item = getItem( name );
			if ( !item ) return "";
			return (const char *)item->text;
		},
		"Get text\n\nArgs:\n * string - name\nReturn:\n * string - text" );
	addFunction(
		"font_realSize",
		[getItem]( std::string name ) {
			auto item = getItem( name );
			if ( !item ) return std::make_tuple( 0.0f, 0.0f );
			return std::make_tuple( item->realSize().fX, item->realSize().fY );
		},
		"Get real size of text\n\nArgs:\n * string - name\nReturn:\n * float - width\n * float - height" );
	addFunction(
		"font_toggleBackground",
		[getItem]( std::string name, bool state ) {
			auto item = getItem( name );
			if ( !item ) return;
			item->background = state;
		},
		"Enable/disable background\n\nArgs:\n * string - name\n * bool - is enabled" );
	addFunction(
		"font_toggleShadow",
		[getItem]( std::string name, bool state ) {
			auto item = getItem( name );
			if ( !item ) return;
			item->shadow = state;
		},
		"Enable/disable shadow\n\nArgs:\n * string - name\n * bool - is enabled" );
	addFunction(
		"font_toggleBorder",
		[getItem]( std::string name, bool state ) {
			auto item = getItem( name );
			if ( !item ) return;
			item->border = state;
		},
		"Enable/disable border\n\nArgs:\n * string - name\n * bool - is enabled" );
	addFunction(
		"font_toggleProportional",
		[getItem]( std::string name, bool state ) {
			auto item = getItem( name );
			if ( !item ) return;
			item->proportional = state;
		},
		"Enable/disable proportional\n\nArgs:\n * string - name\n * bool - is enabled" );
	addFunction(
		"font_setAlign",
		[getItem]( std::string name, int align ) {
			auto item = getItem( name );
			if ( !item ) return;
			if ( align < 0 || align > 2 ) return;
			item->align = Font::Align( align );
		},
		"Set text align\n\nArgs:\n * string - name\n * int - align\n\nAlign:\n 0. Center\n 1. Left\n 2. "
		"Right" );
	addFunction(
		"font_setStyle",
		[getItem]( std::string name, int style ) {
			auto item = getItem( name );
			if ( !item ) return;
			if ( style < 0 || style > 3 ) return;
			item->style = Font::Style( style );
		},
		"Set text style\n\nArgs:\n * string - name\n * int - style\n\nStyles:\n 0. Gothic\n 1. Subtitles\n 2. "
		"Menu\n 3. "
		"Pricedown" );
	addFunction(
		"font_setEdge",
		[getItem]( std::string name, int edge ) {
			auto item = getItem( name );
			if ( !item ) return;
			if ( edge < 0 ) edge = 0;
			if ( edge > 127 ) edge = 127;
			item->edge = edge;
		},
		"Set text edge\n\nArgs:\n * string - name\n * int - edge" );
	addFunction(
		"font_setShadowOffset",
		[getItem]( std::string name, int shadow_offset ) {
			auto item = getItem( name );
			if ( !item ) return;
			if ( shadow_offset < 0 ) shadow_offset = 0;
			if ( shadow_offset > 5 ) shadow_offset = 5;
			item->shadow_offset = shadow_offset;
		},
		"Set text shadow offset\n\nArgs:\n * string - name\n * int - shadow offset" );
	addFunction(
		"font_setText",
		[getItem]( std::string name, std::string text ) {
			auto item = getItem( name );
			if ( !item ) return;
			strncpy( item->text, text.data(), 255 );
		},
		"Set text\n\nArgs:\n * string - name\n * string - text" );
}

void Gui::initLuaAndIDEChart( IDE *ide ) {
	initLuaAndIDEHUD<Chart>( ide, "chart" );

	auto addFunction = [=]( std::string_view name, auto func, std::string_view desc ) {
		_lua->set_function( name, func );
		if ( ide ) ide->addFunction( name, desc );
	};
	auto getItem = [this]( std::string name ) -> Chart * {
		if ( !_activeCfg ) return nullptr;
		for ( auto &&item : _activeCfg->items() ) {
			auto hud = dynamic_cast<Chart *>( item );
			if ( !hud ) continue;
			if ( hud->imgName() == name ) return hud;
		}
		return nullptr;
	};

	addFunction(
		"chart_new",
		[this]( std::string name ) {
			if ( !_activeCfg ) return false;
			auto img = _activeCfg->addChart( name );
			return img ? true : false;
		},
		"Create new chart\n\nArgs:\n * string - name\nReturn:\n * bool - is created" );
	addFunction(
		"chart_exists",
		[this]( std::string name ) {
			if ( !_activeCfg ) return false;
			auto it =
				std::find_if( _activeCfg->items().begin(), _activeCfg->items().end(), [&]( const Itembase *item ) {
					auto hud = dynamic_cast<const Chart *>( item );
					if ( !hud ) return false;
					return hud->imgName() == name;
				} );
			return it != _activeCfg->items().end();
		},
		"Check is chart exists\n\nArgs:\n * string - name\nReturn:\n * bool - is exists" );

	addFunction(
		"chart_isPercentage",
		[getItem]( std::string name ) {
			auto item = getItem( name );
			if ( !item ) return false;
			return item->percentage;
		},
		"Check is percentage enabled\n\nArgs:\n * string - name\nReturn:\n * bool - is enabled" );
	addFunction(
		"chart_isEndLine",
		[getItem]( std::string name ) {
			auto item = getItem( name );
			if ( !item ) return false;
			return item->end_line;
		},
		"Check is end line enabled\n\nArgs:\n * string - name\nReturn:\n * bool - is enabled" );
	addFunction(
		"chart_isBorder",
		[getItem]( std::string name ) {
			auto item = getItem( name );
			if ( !item ) return false;
			return item->border;
		},
		"Check is border enabled\n\nArgs:\n * string - name\nReturn:\n * bool - is enabled" );
	addFunction(
		"chart_value",
		[getItem]( std::string name ) {
			auto item = getItem( name );
			if ( !item ) return 0.0f;
			return item->value;
		},
		"Get chart value\n\nArgs:\n * string - name\nReturn:\n * float - value [0 - 100]" );
	addFunction(
		"chart_togglePercentage",
		[getItem]( std::string name, bool state ) {
			auto item = getItem( name );
			if ( !item ) return;
			item->percentage = state;
		},
		"Enable/disable percentage\n\nArgs:\n * string - name\n * bool - is enabled" );
	addFunction(
		"chart_toggleEndLine",
		[getItem]( std::string name, bool state ) {
			auto item = getItem( name );
			if ( !item ) return;
			item->end_line = state;
		},
		"Enable/disable end line\n\nArgs:\n * string - name\n * bool - is enabled" );
	addFunction(
		"chart_toggleBorder",
		[getItem]( std::string name, bool state ) {
			auto item = getItem( name );
			if ( !item ) return;
			item->border = state;
		},
		"Enable/disable border\n\nArgs:\n * string - name\n * bool - is enabled" );
	addFunction(
		"chart_setValue",
		[getItem]( std::string name, float value ) {
			auto item = getItem( name );
			if ( !item ) return;
			item->value = value;
		},
		"Set chart value\n\nArgs:\n * string - name\n * float - value [0 - 100]" );
}

void Gui::hudHook( SRHook::Info &info ) {
	if ( _mainMenu->isShowed() ) info.retAddr = 0x58FBD6;
}

void Gui::radarHook( SRHook::Info &info ) {
	if ( _mainMenu->isShowed() ) info.retAddr = 0x58FC4C;
}

void Gui::wantedHook( SRHook::CPU &cpu ) {
	if ( _mainMenu->isShowed() ) {
		if ( !cpu.EBP ) cpu.EBP = 3;
		cpu.ZF = 0;
		cpu.SF = cpu.OF;
	}
}

void Gui::armorHook( SRHook::CPU &cpu ) {
	if ( _mainMenu->isShowed() ) cpu.ZF = 1;
}

void Gui::breathHook( SRHook::CPU &cpu ) {
	if ( _mainMenu->isShowed() ) cpu.BL = true;
}

void Gui::ammoHook( SRHook::Info &info ) {
	if ( _mainMenu->isShowed() ) {
		info.skipOriginal = true;
		info.cpu.ESP += 4;
		info.retAddr = 0x5895F6;
	}
}

void Gui::clAmmoHook( uint8_t &alpha ) {
	if ( !alpha ) alpha = 255;
}

void Gui::chartHook( RwRGBA &color ) {
	percentColor.red   = 0xFF - color.red;
	percentColor.green = 0xFF - color.green;
	percentColor.blue  = 0xFF - color.blue;
	percentColor.alpha = color.alpha;
}

void Gui::percentageHook( RwRGBA &color ) {
	color = percentColor;
}

void Gui::customDrawHook() {
	for ( auto &&plugin : _plugins ) plugin->begin();
	auto hud_hidden = _mainMenu->isHidden() && ( !*(bool *)0xBA6769 || !*(bool *)0xA444A0 || *(bool *)0xB6F065 );
	if ( _activeCfg ) {
		if ( _ide && _ide->isOpen() )
			initLuaAndIDE();
		else {
			for ( auto &&plugin : _plugins ) {
				if ( plugin->inited ) continue;
				plugin->inited = plugin->init_lua( _lua );
			}
		}
		for ( auto &&[name, code] : _activeCfg->scripts ) {
			if ( name.empty() ) continue;
			if ( _ide && name == _edScript ) {
				if ( _ide->isOpen() )
					code = _ide->text();
				else {
					code = _ide->text();
					_edScript.clear();
					while ( code.back() == '\n' || code.back() == '\r' ) code.pop_back();
				}
			}
			if ( code.length() <= 1 ) continue;
			try {
				lua().script( code );
				if ( _ide && _ide->isOpen() && name == _edScript ) _ide->setError( 0, "" );
			} catch ( std::exception &e ) {
				if ( _ide && _ide->isOpen() && name == _edScript ) {
					static const std::regex re( R"(.*error:\s\[string\s\".+\.\.\.\"\]:(\d+):\s(.+))",
												std::regex::icase );
					std::cmatch				m;
					if ( std::regex_search( e.what(), m, re ) ) {
						auto line = std::stoull( m[1].str() );
						_ide->setError( line, m[2].str() );
					} else
						_ide->setError( -1, e.what() );
				}
			}
		}
		if ( !hud_hidden && !*(bool *)0xBA67A4 ) {
			for ( auto &&item : _activeCfg->items() ) {
				auto hud = dynamic_cast<Itemhud *>( item );
				if ( hud ) hud->render();
			}
		}
	}

	++_frameCounter;
	if ( GetTickCount() - _lastFrameTime >= 1000 ) {
		_fps		   = _frameCounter;
		_frameCounter  = 0;
		_lastFrameTime = GetTickCount();
	} else if ( !_fps )
		_fps = _frameCounter;
	for ( auto &&plugin : _plugins ) plugin->end();
}
