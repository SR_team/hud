#ifndef MAIN_H
#define MAIN_H

#include "loader/loader.h"
#include <SRDescent/SRDescent.h>

class AsiPlugin : public SRDescent {
public:
	explicit AsiPlugin();
	virtual ~AsiPlugin();

	class DefaultHud *_defHud;
	class Gui *		  _gui;
	class IDE *		  _ide = nullptr;
};

#endif // MAIN_H
