#include "loader.h"
#include "../main.h"
#include <gtasa/CGame/CPed.h>
#include <llmo/SRHook.hpp>
#include <regex>

std::string_view PROJECT_NAME = _PRODJECT_NAME;
stGlobalHandles	 g_handle;
stGlobalClasses	 g_class;
stGlobalPVars	 g_vars;

static AsiPlugin *pAsiPlugin = static_cast<AsiPlugin *>( nullptr );

#ifdef FULL_DX_HOOK
bool InstallD3DHook() {
	static bool isDxHooked = false;
	auto		device	   = *reinterpret_cast<IDirect3DDevice9 **>( 0xC97C28 );
	if ( !device ) return false;
	if ( isDxHooked ) return true;
	isDxHooked										   = true;
	g_class.DirectX									   = new hookIDirect3DDevice9( device );
	*reinterpret_cast<IDirect3DDevice9 **>( 0xC97C28 ) = dynamic_cast<IDirect3DDevice9 *>( g_class.DirectX );
	return true;
}
#endif

CALLHOOK GameLoop() {
	if ( g_vars.gameSatate < 9 ) return;
	g_class.cursor = SRCursor::Instance();
#ifdef USE_DRAW_HOOK
	if ( !g_class.draw ) g_class.draw = new DrawHook();
#endif
	g_handle.d3d9 = GetModuleHandleA( "d3d9.dll" );
	if ( g_handle.d3d9 == nullptr || g_handle.d3d9 == reinterpret_cast<HANDLE>( -1 ) ) return;
#ifdef FULL_DX_HOOK
	if ( !InstallD3DHook() ) return;
#endif

	if ( !LOCAL_PLAYER ) return;
	if ( !LOCAL_PLAYER->isValid() ) return;

	static auto timer = GetTickCount() + 2000;

	auto wndProc = GetWindowLongA( g_vars.hwnd, GWL_WNDPROC );
	auto samp	 = GetModuleHandleA( "samp.dll" );
	if ( samp && samp != INVALID_HANDLE_VALUE ) {
		HMODULE hModule = nullptr;
		GetModuleHandleEx( GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS | GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT,
						   (LPCTSTR)( wndProc ), &hModule );
		if ( hModule == samp ) timer = 0;
	} else
		timer = 0;

	if ( timer > GetTickCount() ) return;

	static bool hooked = false;
	if ( hooked ) return;
	hooked		   = true;
	g_class.events = SREvents::Instance();
	pAsiPlugin	   = new AsiPlugin();
}

void GameExit() { // crouch fix for exit from game
	TerminateProcess( GetCurrentProcess(), 0 );
}

BOOL APIENTRY DllMain( HMODULE hModule, DWORD dwReasonForCall, LPVOID ) {
	static SRHook::Hook<> gameloopHook{ 0x748DA3, 6 };
	static SRHook::Hook<> gameexitHook{ 0x748E62, 7 };

	if ( dwReasonForCall == DLL_PROCESS_ATTACH ) {
		g_handle.plugin = hModule;

		char name[256];
		GetModuleFileNameA( hModule, name, 256 );
		std::cmatch m;
		std::regex	re( R"((.*\\)((.*).asi))", std::regex::icase );
		if ( std::regex_match( name, m, re ) ) {
			static auto PLUGIN_PATH = m[1].str();
			static auto PLUGIN_NAME = m[3].str();

			g_vars.pluginPath = PLUGIN_PATH;
			g_vars.pluginName = PLUGIN_NAME;
		}

		gameloopHook.install();
		gameloopHook.onBefore += GameLoop;
		gameexitHook.install();
		gameexitHook.onBefore += GameExit;
	} else if ( dwReasonForCall == DLL_THREAD_ATTACH ) {
		//if ( g_class.events ) g_class.events->Inject();
	} else if ( dwReasonForCall == DLL_THREAD_DETACH ) {
		//if ( g_class.events ) g_class.events->Deinject();
	} else if ( dwReasonForCall == DLL_PROCESS_DETACH ) {
		return TRUE;
		gameloopHook.remove();
		delete pAsiPlugin;
		pAsiPlugin = nullptr;
		g_class.cursor->DeleteInstance();
		g_class.events->DeleteInstance();
#ifdef FULL_DX_HOOK
		if ( g_class.DirectX->d3d9_destroy() ) delete g_class.DirectX;
#endif
#ifdef USE_DRAW_HOOK
		delete g_class.draw;
#endif
	}

	return TRUE;
}

int MessageBox( std::string_view text, std::string_view title, UINT type ) {
	return MessageBoxA( g_vars.hwnd, text.data(), title.data(), type );
}
