#include "main.h"
#include "Config.h"
#include "DefaultHud.h"
#include "Gui.h"
#include "IDE.h"
#include <gtasa/CGame/CPed.h>
#include <samp/samp.hpp>

static AsiPlugin *self = nullptr;

AsiPlugin::AsiPlugin() : SRDescent() {
	// Constructor
	self	= this;
	_defHud = new DefaultHud( this );
#ifndef NOGUI
	_ide = new IDE( this );
#endif
	_gui = new Gui( this, _defHud, _ide );
#ifndef NOGUI
	g_class.events->addCode( "HUD" );
	g_class.events->onCode += [this]( std::string code ) {
		if ( g_vars.isMenuOpened ) return;
		if ( SAMP::isR1() || SAMP::isR3() || SAMP::isR4() ) {
			if ( SAMP::Input::Instance()->isInputEnabled() || SAMP::Dialog::Instance()->isActive() ) return;
		}
		if ( _ide && _ide->isOpen() ) return;
		if ( code == "HUD" ) _gui->open();
	};
#endif
}

AsiPlugin::~AsiPlugin() {
	// Destructor
	if ( SAMP::isR1() || SAMP::isR3() || SAMP::isR4() ) {
		SAMP::Dialog::DeleteInstance( true );
		SAMP::Input::DeleteInstance( true );
	}
}

extern "C" __declspec( dllexport ) void addToIDE( std::string_view func_name, std::string_view description ) {
	if ( !self ) return;
	if ( !self->_ide ) return;
	self->_ide->addFunction( func_name, description );
}

extern "C" __declspec( dllexport ) RwTexture *texture( std::string_view name ) {
	if ( !self ) return nullptr;
	if ( !self->_gui ) return nullptr;
	return self->_gui->texture( name );
}

extern "C" __declspec( dllexport ) bool hasHudHidden() {
	if ( self && self->_gui && self->_gui->isOpened() ) return false;
	if ( *(bool *)0xBA67A4 ) return true;  // game menu opened
	if ( !*(bool *)0xBA6769 ) return true; // radar showed
	if ( *(bool *)0xB6F065 ) return true;  // hud not hidden
	if ( !*(bool *)0xA444A0 ) return true; // hud not hidden
	return false;
}

extern "C" __declspec( dllexport ) bool isPreview() {
	if ( self && self->_gui && self->_gui->isOpened() ) return true;
	return false;
}

#ifdef FULL_DX_HOOK
extern "C" __declspec( dllexport ) hookIDirect3DDevice9 *dx() {
	return g_class.DirectX;
}
#else
extern "C" __declspec( dllexport ) DrawHook *dx() {
	return g_class.draw;
}
#endif
