#include "Weapon.h"
#include <gtasa/CGame/CPed.h>
#include <gtasa/CGame/CWeapon.h>
#include <gtasa/CGame/methods.h>
#include <llmo/callfunc.hpp>
#include <nlohmann/json.hpp>

Weapon::Weapon( SRDescent *parent, std::string_view id ) : Itembase( parent, id ) {
	// init def values
	_size	 = { .25, .25 };
	_wgtName = "Weapon";
	// bind callbacks
	drawWeapon.onBefore += std::tuple{ this, &Weapon::showHook };
	posWeapon.onBefore += std::tuple{ this, &Weapon::posHook };
	clWeapon.onBefore += std::tuple{ this, &Weapon::clHook };
	xluIcon.onBefore += std::tuple{ this, &Weapon::iconXLUHook };
	clWeaponXLU.onBefore += std::tuple{ this, &Weapon::clXLUHook };
	// install hooks
	drawWeapon.install();
	posWeapon.install( 0x28 + 8 );
	clWeapon.install();
	xluIcon.install();
	clWeaponXLU.install();
}

RwRGBA Weapon::color() const {
	RwRGBA color;
	color.red	= _color[0] * 255.0f;
	color.green = _color[1] * 255.0f;
	color.blue	= _color[2] * 255.0f;
	color.alpha = _color[3] * 255.0f;
	return color;
}

void Weapon::color( RwRGBA color ) {
	_color[0] = float( color.red ) / 255.0f;
	_color[1] = float( color.green ) / 255.0f;
	_color[2] = float( color.blue ) / 255.0f;
	_color[3] = float( color.alpha ) / 255.0f;
}

void Weapon::setIcon( int id, RwTexture *texture ) {
	_custom_weaponIcons[id].texture = texture;
}

void Weapon::reset() {
	Itembase::reset();
	custom_color = false;
	custom_icons = false;
}

void Weapon::draw() {
	Itembase::draw();

	ImGuiColorEditFlags flags = ImGuiColorEditFlags_AlphaBar;
	flags |= ImGuiColorEditFlags_DisplayRGB;
	flags |= ImGuiColorEditFlags_DisplayHex;
	if ( ImGui::ColorButton( name( "Icon color" ).data(), *(ImVec4 *)&_color, flags, ImVec2( 60, 40 ) ) &&
		 custom_color ) {
		ImGui::OpenPopup( name( "colorpicker" ).data() );
		_backup_color = *(ImVec4 *)&_color;
	}
	ImGui::SameLine();
	ImGui::BeginGroup();
	ImGui::Checkbox( name( "Custom icon color" ).data(), &custom_color );
	ImGui::Checkbox( name( "Custom icons" ).data(), &custom_icons );
	ImGui::EndGroup();

	popupColorpicker( *(ImVec4 *)&_color, _backup_color );
}

bool Weapon::isDrag() {
	if ( Itembase::isDrag() ) {
		_dragOffset = mousePos() - pos();
		return true;
	}
	return false;
}

bool Weapon::processDrag() {
	if ( !_drag ) return false;
	pos( mousePos() - _dragOffset );
	return true;
}

tiny::TinyJson Weapon::write() {
	auto json = Itembase::write();
	if ( custom_icons ) json["custom_icons"].Set( (int)custom_icons );

	if ( custom_color ) {
		json["custom_color"].Set( (int)custom_color );
		tiny::TinyJson color;
		color["red"].Set( _color[0] );
		color["green"].Set( _color[1] );
		color["blue"].Set( _color[2] );
		color["alpha"].Set( _color[3] );

		json["color"].Set( color );
	}

	return json;
}

nlohmann::json Weapon::write2() {
	auto json = Itembase::write2();
	if ( custom_icons ) json["custom_icons"] = custom_icons;

	if ( custom_color ) {
		json["custom_color"] = custom_color;
		auto color			 = nlohmann::json::object();
		color["red"]		 = _color[0];
		color["green"]		 = _color[1];
		color["blue"]		 = _color[2];
		color["alpha"]		 = _color[3];

		json["color"] = color;
	}

	return json;
}

void Weapon::read( tiny::xobject &json ) {
	auto readColor = []( tiny::xobject &json, float *color ) {
		for ( int i = 0; i < json.Count(); i++ ) {
			json.Enter( i );
			color[0] = json.Get<float>( "red" );
			color[1] = json.Get<float>( "green" );
			color[2] = json.Get<float>( "blue" );
			color[3] = json.Get<float>( "alpha" );
		}
	};
	for ( int i = 0; i < json.Count(); i++ ) {
		json.Enter( i );
		Itembase::read( json );
		custom_color = json.Get<int>( "custom_color", 0 );
		if ( custom_color ) {
			tiny::xobject jcolor = json.Get<tiny::xobject>( "color" );
			readColor( jcolor, _color );
		}
		custom_icons = json.Get<int>( "custom_icons", 0 );
	}
}

void Weapon::read2( nlohmann::json &json ) {
	auto readColor = []( nlohmann::json &json, float *color ) {
		color[0] = json.value<float>( "red", 1.0f );
		color[1] = json.value<float>( "green", 1.0f );
		color[2] = json.value<float>( "blue", 1.0f );
		color[3] = json.value<float>( "alpha", 1.0f );
	};
	Itembase::read2( json );
	custom_color = json.value<bool>( "custom_color", false );
	if ( custom_color ) {
		auto jcolor = json["color"];
		readColor( jcolor, _color );
	}
	custom_icons = json.value<bool>( "custom_icons", false );
}

void Weapon::initLuaAndIDE( sol::state &lua, IDE *ide, std::string prefix ) {
	if ( prefix.empty() ) prefix = "weapon";
	Itembase::initLuaAndIDE( lua, ide, prefix );

	// TODO: Текстуры оружия в Gui (Config слишком динамичен, с ним придется дергать медленный lua_reset)
	// TODO: Заменить предыдущий TODO на NOTE после реализации
	addFunction(
		lua, ide, "weapon_isCustomColor", [this]() { return custom_color; },
		"Check is custom color enabled\n\nReturn:\n * bool - is enabled" );
	addFunction(
		lua, ide, "weapon_isCustomIcons", [this]() { return custom_icons; },
		"Check is custom icons enabled\n\nReturn:\n * bool - is enabled" );
	addFunction(
		lua, ide, "weapon_color",
		[this]() {
			auto clr = color();
			return *(int *)&clr;
		},
		"Get icon color\n\nReturn:\n * int - color" );
	addFunction(
		lua, ide, "weapon_toggleCustomColor", [this]( bool state ) { custom_color = state; },
		"Enable/disable custom color\n\nArgs:\n * bool - is enabled" );
	addFunction(
		lua, ide, "weapon_toggleCustomIcons", [this]( bool state ) { custom_icons = state; },
		"Enable/disable custom icons\n\nArgs:\n * bool - is enabled" );
	addFunction(
		lua, ide, "weapon_setColor", [this]( int clr ) { color( *(RwRGBA *)&clr ); },
		"Set icon color\n\nArgs:\n * int - color" );

	addFunction(
		lua, ide, "weapon_currentSlot",
		[]() {
			if ( !LOCAL_PLAYER ) return 0;
			if ( !LOCAL_PLAYER->isValid() ) return 0;
			return (int)LOCAL_PLAYER->weapon_slot;
		},
		"Get current weapon slot\n\nReturn:\n * int - slot" );
	addFunction(
		lua, ide, "weapon_currentId",
		[]() {
			if ( !LOCAL_PLAYER ) return 0;
			if ( !LOCAL_PLAYER->isValid() ) return 0;
			return (int)LOCAL_PLAYER->WeaponSlot[LOCAL_PLAYER->weapon_slot].m_Type;
		},
		"Get current weapon id\n\nReturn:\n * int - id" );
	addFunction(
		lua, ide, "weapon_idFromSlot",
		[]( int slot ) {
			if ( slot < 0 || slot > 13 ) return 0;
			if ( !LOCAL_PLAYER ) return 0;
			if ( !LOCAL_PLAYER->isValid() ) return 0;
			return (int)LOCAL_PLAYER->WeaponSlot[slot].m_Type;
		},
		"Get weapon id from slot\n\nArgs:\n * int - slot\nReturn:\n * int - id" );
	addFunction(
		lua, ide, "weapon_state",
		[]() {
			if ( !LOCAL_PLAYER ) return 0;
			if ( !LOCAL_PLAYER->isValid() ) return 0;
			auto &weapon = LOCAL_PLAYER->WeaponSlot[LOCAL_PLAYER->weapon_slot];
			if ( ( weapon.m_Type >= 16 && weapon.m_Type <= 38 ) || weapon.m_Type == 43 )
				return (int)weapon.m_dwState;
			else {
				auto pad	= CallFunc::ccall<size_t>( 0x53FB70, 0 );
				auto target = CallFunc::thiscall<bool>( pad, 0x540670 );
				if ( CallFunc::thiscall<int>( pad, 0x540340, target ) ) return 1;
				return 0;
			}
		},
		"Get current weapon state\n\nReturn:\n * int - state\n\n 0. Wait\n 1. Shot\n 2. Reload" );
	addFunction(
		lua, ide, "weapon_currentSkill",
		[]() {
			if ( !LOCAL_PLAYER ) return 0.0f;
			if ( !LOCAL_PLAYER->isValid() ) return 0.0f;
			float Skill	   = 1000.0f;
			auto  weaponId = LOCAL_PLAYER->WeaponSlot[LOCAL_PLAYER->weapon_slot].m_Type;
			if ( ( weaponId >= 22 ) && ( weaponId <= 32 ) ) {
				if ( weaponId == 32 ) weaponId -= 4;
				Skill = *(float *)( ( weaponId - 22 ) * 4 + 0xB79494 );
			}
			return Skill / 10.0f;
		},
		"Get current weapon skill\n\nReturn:\n * float - skill [0.0 - 100.0]" );
	addFunction(
		lua, ide, "weapon_skillFromSlot",
		[]( int slot ) {
			if ( slot < 0 || slot > 13 ) return 0.0f;
			if ( !LOCAL_PLAYER ) return 0.0f;
			if ( !LOCAL_PLAYER->isValid() ) return 0.0f;
			float Skill	   = 1000.0f;
			auto  weaponId = LOCAL_PLAYER->WeaponSlot[slot].m_Type;
			if ( ( weaponId >= 22 ) && ( weaponId <= 32 ) ) {
				if ( weaponId == 32 ) weaponId -= 4;
				Skill = *(float *)( ( weaponId - 22 ) * 4 + 0xB79494 );
			}
			return Skill / 10.0f;
		},
		"Get current weapon skill\n\nReturn:\n * float - skill [0.0 - 100.0]" );
	addFunction(
		lua, ide, "weapon_currentClip",
		[]() {
			if ( !LOCAL_PLAYER ) return 0;
			if ( !LOCAL_PLAYER->isValid() ) return 0;
			auto id		 = LOCAL_PLAYER->WeaponSlot[LOCAL_PLAYER->weapon_slot].m_Type;
			auto skillId = CallFunc::thiscall<char>( LOCAL_PLAYER, 0x5E6580 );
			return (int)CWeapon::getWeapon( id, skillId )->ammoClip;
		},
		"Get current weapon clip\n\nReturn:\n * int - clip" );
	addFunction(
		lua, ide, "weapon_clipFromSlot",
		[]( int slot ) {
			if ( slot < 0 || slot > 13 ) return 0;
			if ( !LOCAL_PLAYER ) return 0;
			if ( !LOCAL_PLAYER->isValid() ) return 0;
			auto id		 = LOCAL_PLAYER->WeaponSlot[slot].m_Type;
			auto skillId = CallFunc::thiscall<char>( LOCAL_PLAYER, 0x5E6580 );
			return (int)CWeapon::getWeapon( id, skillId )->ammoClip;
		},
		"Get weapon clip from slot\n\nArgs:\n * int - slot\nReturn:\n * int - clip" );
	addFunction(
		lua, ide, "weapon_currentAccuracy",
		[]() {
			if ( !LOCAL_PLAYER ) return 0.0f;
			if ( !LOCAL_PLAYER->isValid() ) return 0.0f;
			auto id		 = LOCAL_PLAYER->WeaponSlot[LOCAL_PLAYER->weapon_slot].m_Type;
			auto skillId = CallFunc::thiscall<char>( LOCAL_PLAYER, 0x5E6580 );
			return CWeapon::getWeapon( id, skillId )->fAccuracy;
		},
		"Get current weapon accuracy\n\nReturn:\n * float - accuracy" );
	addFunction(
		lua, ide, "weapon_accuracyFromSlot",
		[]( int slot ) {
			if ( slot < 0 || slot > 13 ) return 0.0f;
			if ( !LOCAL_PLAYER ) return 0.0f;
			if ( !LOCAL_PLAYER->isValid() ) return 0.0f;
			auto id		 = LOCAL_PLAYER->WeaponSlot[slot].m_Type;
			auto skillId = CallFunc::thiscall<char>( LOCAL_PLAYER, 0x5E6580 );
			return CWeapon::getWeapon( id, skillId )->fAccuracy;
		},
		"Get weapon accuracy from slot\n\nArgs:\n * int - slot\nReturn:\n * float - accuracy" );
}

void Weapon::showHook( SRHook::Info &info ) {
	if ( isHidden() ) {
		info.skipOriginal = true;
		info.retAddr	  = 0x58D913;
	}
}

void Weapon::posHook( int &x, int &y ) {
	_origPos = WindowScreenToGameScreen( x, y );
	x		 = ( SCREEN_X / 640.0f ) * pos().fX;
	y		 = ( SCREEN_Y / 448.0f ) * pos().fY;
}

void Weapon::clHook( SRHook::Info &info, CRect *&rect, RwRGBA *&color ) {
	_origSize = WindowScreenToGameScreen( rect->r - rect->l, rect->t - rect->b );
	rect->r	  = rect->l + ( SCREEN_X / 640.0f ) * size().fX;
	rect->t	  = rect->b + ( SCREEN_Y / 448.0f ) * size().fY;
	if ( custom_color ) {
		color->red	 = _color[0] * 255.0f;
		color->green = _color[1] * 255.0f;
		color->blue	 = _color[2] * 255.0f;
		color->alpha = _color[3] * 255.0f;
	}
	if ( custom_icons && LOCAL_PLAYER && LOCAL_PLAYER->isValid() ) {
		auto id = LOCAL_PLAYER->WeaponSlot[LOCAL_PLAYER->weapon_slot].m_Type;
		if ( _custom_weaponIcons[id].texture ) info.cpu.ECX = (size_t)&_custom_weaponIcons[id];
	}
}

void Weapon::iconXLUHook( RwRaster *&raster ) {
	if ( custom_icons && LOCAL_PLAYER && LOCAL_PLAYER->isValid() ) {
		auto id = LOCAL_PLAYER->WeaponSlot[LOCAL_PLAYER->weapon_slot].m_Type;
		if ( _custom_weaponIcons[id].texture ) raster = _custom_weaponIcons[id].texture->raster;
	}
}

void Weapon::clXLUHook( float &rigth, float &bottom, float &, float &w, float &h, uint8_t &red, uint8_t &green,
						uint8_t &blue, uint8_t &, float &, uint8_t &icon_alpha ) {
	_origSize = WindowScreenToGameScreen( w * 2, h * 2 );
	rigth	  = ( rigth - w ) + ( ( SCREEN_X / 640.0f ) * size().fX * 0.5f );
	bottom	  = ( bottom - h ) + ( ( SCREEN_Y / 448.0f ) * size().fY * 0.5f );
	w		  = ( SCREEN_X / 640.0f ) * size().fX * 0.5f;
	h		  = ( SCREEN_Y / 448.0f ) * size().fY * 0.5f;
	if ( custom_color ) {
		red		   = _color[0] * 255.0f;
		green	   = _color[1] * 255.0f;
		blue	   = _color[2] * 255.0f;
		icon_alpha = _color[3] * 255.0f;
	}
}
