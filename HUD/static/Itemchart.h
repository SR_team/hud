#ifndef ITEMCHART_H
#define ITEMCHART_H

#include "Itembase.h"

class Itemchart : public Itembase {
public:
	Itemchart( SRDescent *parent, std::string_view id = "" );

	bool percentage	  = false;
	bool end_line	  = false;
	bool border		  = true;
	bool custom_color = false;

	RwRGBA color() const;
	void   color( RwRGBA color );

	virtual void		   reset() override;
	virtual void		   draw() override;
	virtual bool		   isDrag() override;
	virtual bool		   processDrag() override;
	virtual tiny::TinyJson write() override;
	virtual nlohmann::json write2() override;
	virtual void		   read( tiny::xobject &json ) override;
	virtual void		   read2( nlohmann::json &json ) override;
	virtual void		   initLuaAndIDE( sol::state &lua, class IDE *ide, std::string prefix = "" ) override;

	void posHook( float &x, float &y, WORD &w, WORD &h, float &, uint32_t &drawBlueline, bool &drawPercentage,
				  bool &drawBorder, RwRGBA &color, RwRGBA &fore );

protected:
	float  _color[4]{ .675, .796, .945, 1 };
	ImVec4 _backup_color;
	RwV2D  _dragOffset;
};

#endif // ITEMCHART_H
