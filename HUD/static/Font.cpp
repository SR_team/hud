#include "Font.h"
#include <gtasa/CGame/methods.h>
#include <llmo/callfunc.hpp>
#include <nlohmann/json.hpp>

Font::Font( SRDescent *parent, std::string_view name, std::string_view id ) : Itemhud( parent, name, id ) {
	_size	 = { 30, 70 };
	_wgtName = "Font";
}

RwV2D Font::realSize() const {
	return _realSize;
}

void Font::draw() {
	Itemhud::draw();
	ImGui::SameLine( 0, 10 );

	ImGui::BeginGroup();
	ImGui::Checkbox( name( "Background" ).data(), &background );
	ImGui::SameLine( 0, 32 );
	ImGui::Checkbox( name( "proportional" ).data(), &proportional );
	ImGui::SameLine( 0, 31 );
	ImGui::Checkbox( name( "Shadow" ).data(), &shadow );
	ImGui::Text( "Align:" );
	ImGui::SameLine( 0, 10 );
	ImGui::RadioButton( name( "Left" ).data(), (int *)&align, (int)Align::left );
	ImGui::SameLine( 0, 10 );
	ImGui::RadioButton( name( "Center" ).data(), (int *)&align, (int)Align::center );
	ImGui::SameLine( 0, 10 );
	ImGui::RadioButton( name( "Right" ).data(), (int *)&align, (int)Align::right );
	ImGui::SameLine( 0, 10 );
	ImGui::Checkbox( name( "Border" ).data(), &border );
	ImGui::EndGroup();

	ImGui::Text( "Font style:" );
	ImGui::SameLine( 0, 10 );
	ImGui::RadioButton( name( "Gothic" ).data(), (int *)&style, (int)Style::Gothic );
	ImGui::SameLine( 0, 10 );
	ImGui::RadioButton( name( "Subtitles" ).data(), (int *)&style, (int)Style::Subtitles );
	ImGui::SameLine( 0, 10 );
	ImGui::RadioButton( name( "Menu" ).data(), (int *)&style, (int)Style::Menu );
	ImGui::SameLine( 0, 10 );
	ImGui::RadioButton( name( "Pricedown" ).data(), (int *)&style, (int)Style::Pricedown );

	ImGui::DragInt( name( "Edge" ).data(), &edge, 0.05f, 0, 127 );
	ImGui::DragInt( name( "Shadow" ).data(), &shadow_offset, 0.01f, 0, 5 );

	ImGui::InputText( name( "Text" ).data(), text, 255 );
}

bool Font::isDrag() {
	float start, end;
	switch ( align ) {
		case Align::center:
			start = _pos.fX - _realSize.fX * 0.5f;
			end	  = _pos.fX + _realSize.fX * 0.5f;
			break;
		case Align::left:
			start = _pos.fX;
			end	  = _pos.fX + _realSize.fX;
			break;
		case Align::right:
			start = _pos.fX - _realSize.fX;
			end	  = _pos.fX;
			break;
	}
	if ( isShowed() && mousePos().fX >= start && mousePos().fY >= _pos.fY && mousePos().fX <= end &&
		 mousePos().fY <= _pos.fY + _realSize.fY ) {
		_drag		= true;
		_dragOffset = mousePos() - pos();
		return true;
	}
	return false;
}

bool Font::processDrag() {
	if ( !_drag ) return false;
	_pos = mousePos() - _dragOffset;
	return true;
}

tiny::TinyJson Font::write() {
	auto json = Itemhud::write();

	if ( align != Align::left ) json["align"].Set( (int)align );
	if ( background ) json["background"].Set( (int)background );
	if ( edge ) json["edge"].Set( (int)edge );
	if ( shadow_offset != 1 ) json["shadow_offset"].Set( (int)shadow_offset );
	if ( shadow ) json["shadow"].Set( (int)shadow );
	if ( border ) json["border"].Set( (int)border );
	if ( style != Style::Subtitles ) json["style"].Set( (int)style );
	if ( !proportional ) json["proportional"].Set( (int)proportional );
	std::string str = text;
	json["text"].Set( str );

	return json;
}

nlohmann::json Font::write2() {
	auto json = Itemhud::write2();

	if ( align != Align::left ) json["align"] = align;
	if ( background ) json["background"] = background;
	if ( edge ) json["edge"] = edge;
	if ( shadow_offset != 1 ) json["shadow_offset"] = shadow_offset;
	if ( shadow ) json["shadow"] = shadow;
	if ( border ) json["border"] = border;
	if ( style != Style::Subtitles ) json["style"] = style;
	if ( !proportional ) json["proportional"] = proportional;
	std::string str = text;
	json["text"]	= str;

	return json;
}

void Font::read( tiny::xobject &json ) {
	for ( int i = 0; i < json.Count(); i++ ) {
		json.Enter( i );
		Itemhud::read( json );
		background	  = json.Get<int>( "background", 0 );
		edge		  = json.Get<int>( "edge", 0 );
		shadow_offset = json.Get<int>( "shadow_offset", 1 );
		shadow		  = json.Get<int>( "shadow", 0 );
		border		  = json.Get<int>( "border", 0 );
		proportional  = json.Get<int>( "proportional", 1 );
		auto str	  = json.Get<std::string>( "text", "~d~NOTEXT~d~" );
		strncpy( text, str.data(), 255 );
		align = (Align)json.Get<int>( "align", 1 );
		style = (Style)json.Get<int>( "style", 1 );
	}
}

void Font::read2( nlohmann::json &json ) {
	Itemhud::read2( json );
	background	  = json.value<bool>( "background", false );
	edge		  = json.value<int>( "edge", 0 );
	shadow_offset = json.value<int>( "shadow_offset", 1 );
	shadow		  = json.value<bool>( "shadow", false );
	border		  = json.value<bool>( "border", false );
	proportional  = json.value<bool>( "proportional", true );
	auto str	  = json.value<std::string>( "text", "~d~NOTEXT~d~" );
	strncpy( text, str.data(), 255 );
	align = (Align)json.value<int>( "align", 1 );
	style = (Style)json.value<int>( "style", 1 );
}

void Font::render() {
	if ( isHidden() ) return;

	CallFunc::ccall( 0x719610, align );
	//	CallFunc::ccall( 0x719500, _color[3] * 255.0f );
	if ( align == Align::right ) CallFunc::ccall( 0x7194F0, 0.0f );
	CallFunc::ccall( 0x7195C0, background, false );
	CallFunc::ccall( 0x719430, color() );
	if ( !border ) CallFunc::ccall( 0x719590, edge );
	RwRGBA shd{ 0, 0, 0, 255 };
	CallFunc::ccall( 0x719510, shd );
	if ( shadow ) {
		CallFunc::ccall( 0x719570, shadow_offset );
	} else
		CallFunc::ccall( 0x719570, 0 );
	if ( border ) CallFunc::ccall( 0x719590, edge );
	CallFunc::ccall( 0x719490, style );
	CallFunc::ccall( 0x7195B0, proportional );
	CallFunc::ccall( 0x719380, ( SCREEN_X / 640.0f ) * _size.fX * 0.01f, ( SCREEN_Y / 448.0f ) * _size.fY * 0.01f );

	char gxt[256];
	auto x = ( SCREEN_X / 640.0f ) * _pos.fX;
	auto y = ( SCREEN_Y / 448.0f ) * _pos.fY;
	CallFunc::ccall( 0x718600, text, gxt );
	auto rect	 = GetTextRect( x, y, gxt );
	_realSize.fX = ( 640.0f / SCREEN_X ) * ( ( rect.x2 - rect.x1 ) );
	_realSize.fY = ( 448.0f / SCREEN_Y ) * ( ( rect.y2 - rect.y1 ) );
	CallFunc::ccall( 0x71A700, x, y, gxt );
	CallFunc::ccall( 0x719590, 0 );
}
