#include "Armour.h"
#include <gtasa/CGame/CPed.h>
#include <gtasa/CGame/methods.h>

Armour::Armour( SRDescent *parent, std::string_view id ) : Itemchart( parent, id ) {
	// init def values
	_size	  = { .25, .25 };
	_wgtName  = "Armour";
	_color[0] = .882;
	_color[1] = .882;
	_color[2] = .882;
	// bind callbacks
	drawArmour.onBefore += std::tuple{ this, &Armour::showHook };
	posArmour.onBefore += std::tuple{ this, &Armour::posHook };
	// install hooks
	drawArmour.install();
	posArmour.install();
}

void Armour::initLuaAndIDE( sol::state &lua, IDE *ide, std::string prefix ) {
	if ( prefix.empty() ) prefix = "armour";
	Itemchart::initLuaAndIDE( lua, ide, prefix );

	addFunction(
		lua, ide, "armour_amount",
		[]() {
			if ( !LOCAL_PLAYER ) return 0.0f;
			if ( !LOCAL_PLAYER->isValid() ) return 0.0f;
			return LOCAL_PLAYER->armor;
		},
		"Get armour amount\n\nReturn:\n * float - amount" );
}

void Armour::showHook( SRHook::Info &info ) {
	if ( isHidden() ) info.retAddr = 0x589183;
}

void Armour::posHook( float &x, float &y, WORD &w, WORD &h, float &percent, uint32_t &drawBlueline,
					  bool &drawPercentage, bool &drawBorder, RwRGBA &color, RwRGBA &fore ) {
	Itemchart::posHook( x, y, w, h, percent, drawBlueline, drawPercentage, drawBorder, color, fore );
	if ( !percent ) percent = 50.0f;
}
