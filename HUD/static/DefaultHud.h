#ifndef DEFAULTHUD_H
#define DEFAULTHUD_H

#include "Itembase.h"
#include <llmo/SRHook.hpp>

class DefaultHud : public SRDescent {
	SRHook::Hook<> fontTexture{ 0x719862 };
	SRHook::Hook<> fontTexture2{ 0x719B13 };
	SRHook::Hook<> charWidthProp{ 0x718793, 8 };
	SRHook::Hook<> charWidthUnprop{ 0x7187AA, 7 };
	SRHook::Hook<> getCharSizeProp{ 0x719700, 8 };
	SRHook::Hook<> getCharSizeProp2{ 0x7197A2, 8 };
	SRHook::Hook<> getCharSizeUnprop{ 0x71972B, 7 };
	SRHook::Hook<> getCharSizeUnprop2{ 0x7197DC, 7 };
	SRHook::Hook<> renderFontUnprop{ 0x719AAC, 7 };
	SRHook::Hook<> renderFontProp{ 0x719A8D, 8 };

	std::deque<Itembase *>			  _items;
	std::deque<class Itemcrosshair *> _crossHairs;

	class Wanted *wanted = nullptr;
	class Radar * radar	 = nullptr;
	class Ammo *  ammo	 = nullptr;
	class Money * money	 = nullptr;
	class Weapon *weapon = nullptr;
	class Breath *breath = nullptr;
	class Armour *armour = nullptr;
	class Health *health = nullptr;
	class Clock * clock	 = nullptr;

	class CrosshairSimple *cfSimple = nullptr;
	class CrosshairM16 *   cfM16	= nullptr;
	class CrosshairOther * cfSniper = nullptr;
	class CrosshairOther * cfRPG	= nullptr;
	class CrosshairOther * cfHSRPG	= nullptr;
	class CrosshairOther * cfCamera = nullptr;
	class CrosshairOther * cfHunter = nullptr;
	class CrosshairOther * cfHydra	= nullptr;

public:
	DefaultHud()					 = delete;
	DefaultHud( const DefaultHud & ) = delete;
	DefaultHud( SRDescent *parent );

	void setRadarTexture( RwTexture *tx = nullptr );
	void setRadarIcon( std::string icon, std::string txName, RwTexture *tx = nullptr );
	void setAirPlaneTexture( RwTexture *tx = nullptr );
	void setWeaponTexture( int id, RwTexture *tx = nullptr );
	enum class FontId { font2 = 1, font1, together };
	void setFontTexture( FontId id, RwTexture *tx = nullptr );

	struct tFontData {
		char m_propValues[208];
		char m_spaceValue;
		char m_unpropValue;
	} fontData[2];

	std::string				radarIcon( std::string icon );
	std::deque<std::string> radarIcons();

	void initLuaAndIDE( sol::state &lua, class IDE *ide );

	const std::deque<Itembase *> &items() const;

	const std::deque<Itemcrosshair *> &crossHairs() const;

	tiny::TinyJson write();
	nlohmann::json write2();
	void		   read( tiny::xobject &json );
	void		   read2( nlohmann::json &json );

protected:
	CTexture _font[2];

	void fontTextureHook( SRHook::CPU &cpu );
	void charWidthPropHook( SRHook::Info &info );
	void charWidthUnpropHook( SRHook::Info &info );
	void getCharSizeUnpropHook( SRHook::Info &info );
	void renderFontPropHook( SRHook::Info &info );
	void renderFontUnpropHook( SRHook::Info &info );
};

#endif // DEFAULTHUD_H
