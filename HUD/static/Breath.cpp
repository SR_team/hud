#include "Breath.h"
#include <gtasa/CGame/CPed.h>
#include <gtasa/CGame/methods.h>
#include <llmo/callfunc.hpp>

Breath::Breath( SRDescent *parent, std::string_view id ) : Itemchart( parent, id ) {
	// init def values
	_size	 = { .25, .25 };
	_wgtName = "Breath";
	// bind callbacks
	drawBreath.onBefore += std::tuple{ this, &Breath::showHook };
	posBreath.onBefore += std::tuple{ (Itemchart *)this, &Itemchart::posHook };
	// install hooks
	drawBreath.install();
	posBreath.install();
}

void Breath::initLuaAndIDE( sol::state &lua, IDE *ide, std::string prefix ) {
	if ( prefix.empty() ) prefix = "breath";
	Itemchart::initLuaAndIDE( lua, ide, prefix );

	addFunction(
		lua, ide, "breath_amount",
		[]() {
			if ( !LOCAL_PLAYER ) return 0.0f;
			if ( !LOCAL_PLAYER->isValid() ) return 0.0f;
			auto pData = (size_t)LOCAL_PLAYER->pPlayerData;
			if ( !pData ) return 0.0f;
			auto breath = *(float *)( pData + 68 );
			return ( breath / CallFunc::ccall<float>( 0x559AF0, 8 ) ) * 100.0f;
		},
		"Get breath amount\n\nReturn:\n * float - amount" );
}

void Breath::showHook( SRHook::Info &info ) {
	if ( isHidden() ) {
		info.skipOriginal = true;
		info.retAddr	  = 0x58F1B5;
	}
}
