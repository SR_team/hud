#ifndef BREATH_H
#define BREATH_H

#include "Itemchart.h"
#include <llmo/SRHook.hpp>

class Breath : public Itemchart {
	SRHook::Hook<>																		drawBreath{ 0x58F0DB, 6 };
	SRHook::Hook<float, float, WORD, WORD, float, uint32_t, bool, bool, RwRGBA, RwRGBA> posBreath{ 0x589252, 5 };

public:
	Breath( SRDescent *parent, std::string_view id = "" );

	virtual void initLuaAndIDE( sol::state &lua, class IDE *ide, std::string prefix = "" ) override;

protected:
	void showHook( SRHook::Info &info );
};

#endif // BREATH_H
