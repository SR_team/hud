#ifndef ITEMBASE_H
#define ITEMBASE_H

#include <SRDescent/SRDescent.h>
#include <gtasa/CGame/Types.h>
#include <imgui.h>
#include <nlohmann/json_fwd.hpp>
#include <tinyjson/tinyjson.hpp>

#include "IDE.h"
#include <sol/sol.hpp>

class Itembase : public SRDescent {
public:
	Itembase() = delete;
	Itembase( SRDescent *parent, std::string_view id = "" );
	virtual ~Itembase();

	virtual RwV2D pos() const;
	virtual RwV2D size() const;

	virtual RwV2D customPos() const;
	virtual RwV2D customSize() const;

	virtual RwV2D originalPos() const;
	virtual RwV2D originalSize() const;

	virtual bool isCustomPos() const;
	virtual bool isCustomSize() const;

	virtual bool isShowed() const;
	virtual bool isHidden() const;

	virtual void pos( const RwV2D &value );
	virtual void size( const RwV2D &value );

	virtual void show();
	virtual void hide();

	virtual void disableCustomPos();
	virtual void disableCustomSize();
	virtual void disableCustom();

	virtual void toggleShow();
	virtual void toggleShow( bool status );
	virtual void toggleCustomPos();
	virtual void toggleCustomPos( bool status );
	virtual void toggleCustomSize();
	virtual void toggleCustomSize( bool status );

	virtual void originalToCustomPos();
	virtual void originalToCustomSize();
	virtual void originalToCustom();

	virtual void reset();

	virtual void draw();

	virtual bool isDrag();
	virtual bool processDrag();
	virtual void endDrag();

	virtual tiny::TinyJson write();
	virtual nlohmann::json write2();
	virtual void		   read( tiny::xobject &json );
	virtual void		   read2( nlohmann::json &json );

	virtual void initLuaAndIDE( sol::state &lua, IDE *ide, std::string prefix = "" );

	std::string_view wgtName();

	static RwTexture *loadPNG( const char *filename );
	static RwTexture *loadPNG( uint8_t *addr, size_t size );

	bool operator==( const Itembase &r ) {
		if ( _wgtName != r._wgtName ) return false;
		if ( _customPos != r._customPos ) return false;
		if ( _customSize != r._customSize ) return false;
		if ( _show != r._show ) return false;
		if ( pos() != r.pos() ) return false;
		if ( size() != r.size() ) return false;
		return true;
	}

protected:
	std::string _wgtName;
	std::string _id;
	RwV2D		_pos, _size;
	RwV2D		_origPos, _origSize;
	bool		_customPos	= false;
	bool		_customSize = false;
	bool		_show		= true;
	bool		_drag		= false;
	bool		_initPos	= false;
	bool		_initSize	= false;

	void initPos();
	void initSize();

	std::string name( std::string_view rawName, std::string_view addId = "" );
	RwV2D		mousePos();

	bool		   popupColorpicker( ImVec4 &color, ImVec4 &backup, bool alpha = true );
	static RwVRect GetTextRect( float x, float y, const char *text );

	template<typename T>
	void addFunction( sol::state &lua, IDE *ide, std::string_view name, const T &func, std::string_view desc ) {
		lua.set_function( name, func );
		if ( ide ) ide->addFunction( name, desc );
	}
};

#endif // ITEMBASE_H
