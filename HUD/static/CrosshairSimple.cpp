#include "CrosshairSimple.h"
#include <nlohmann/json.hpp>

CrosshairSimple::CrosshairSimple( SRDescent *parent, std::string_view id ) : Itemcrosshair( parent, id ) {
	_wgtName = "Simple";
	// bind callbacks
	center.onBefore += std::tuple{ this, &CrosshairSimple::centerHook };
	lu.onBefore += std::tuple{ this, &CrosshairSimple::drawHook };
	ru.onBefore += std::tuple{ this, &CrosshairSimple::drawHook };
	lb.onBefore += std::tuple{ this, &CrosshairSimple::drawHook };
	rb.onBefore += std::tuple{ this, &CrosshairSimple::drawHook };
	// install hooks
	center.install( 4 );
	lu.install( 4 );
	ru.install( 4 );
	lb.install( 4 );
	rb.install( 4 );
}

void CrosshairSimple::reset() {
	Itemcrosshair::reset();
	drawCenter = true;
}

void CrosshairSimple::draw() {
	Itemcrosshair::draw();
	ImGui::Checkbox( name( "Show dot in center" ).data(), &drawCenter );
}

tiny::TinyJson CrosshairSimple::write() {
	auto json = Itemcrosshair::write();

	if ( !drawCenter ) json["drawCenter"].Set( (int)drawCenter );

	return json;
}

nlohmann::json CrosshairSimple::write2() {
	auto json = Itemcrosshair::write2();

	if ( !drawCenter ) json["drawCenter"] = drawCenter;

	return json;
}

void CrosshairSimple::read( tiny::xobject &json ) {
	for ( int i = 0; i < json.Count(); i++ ) {
		json.Enter( i );
		Itemcrosshair::read( json );
		drawCenter = json.Get<int>( "drawCenter", 1 );
	}
}

void CrosshairSimple::read2( nlohmann::json &json ) {
	Itemcrosshair::read2( json );
	drawCenter = json.value<bool>( "drawCenter", true );
}

void CrosshairSimple::initLuaAndIDE( sol::state &lua, IDE *ide, std::string prefix ) {
	if ( prefix.empty() ) {
		prefix = luaPrefix();
	}
	Itemcrosshair::initLuaAndIDE( lua, ide, prefix );
	addFunction(
		lua, ide, prefix + "_isDotShowed", [this] { return drawCenter; },
		"Get current show state of dot in center crosshair\n\nReturn:\n * bool - is showed" );
	addFunction(
		lua, ide, prefix + "_toggleDotShow", [this]( bool state ) { drawCenter = state; },
		"Set show state of dot in center crosshair\n\nArgs:\n * bool - show" );
}

void CrosshairSimple::centerHook( RwRGBA *&clr ) {
	if ( isHidden() || !drawCenter ) {
		clr->alpha = 0;
		return;
	}
	*clr = color();
}

void CrosshairSimple::drawHook( SRHook::Info &info, RwRGBA *&clr ) {
	if ( isHidden() ) {
		clr->alpha = 0;
		return;
	}
	*clr = color();
	if ( _texture.texture ) info.cpu.ECX = (size_t)&_texture;
}
