#include "Wanted.h"
#include <gtasa/CGame/methods.h>
#include <nlohmann/json.hpp>

Wanted::Wanted( SRDescent *parent, std::string_view id ) : Itembase( parent, id ) {
	// init def values
	_size	 = { .25, .25 };
	_wgtName = "Wanted";
	// bind callbacks
	drawWanted.onBefore += std::tuple{ this, &Wanted::showHook };
	posWantedX.onAfter += std::tuple{ this, &Wanted::posXHook };
	posWantedY.onBefore += std::tuple{ this, &Wanted::posYHook };
	posWantedYGold.onAfter += std::tuple{ this, &Wanted::posYGoldHook };
	sizeWanted.onBefore += std::tuple{ this, &Wanted::sizeHook };
	sizeWanted2.onBefore += std::tuple{ this, &Wanted::sizeHook };
	sizeWanted3.onBefore += std::tuple{ this, &Wanted::sizeMulHook };
	sizeWanted4.onBefore += std::tuple{ this, &Wanted::sizeSkipHook };
	sizeWanted5.onBefore += std::tuple{ this, &Wanted::posOffsetHook };
	colorWanted.onBefore += std::tuple{ this, &Wanted::colorHook };
	colorGoldWanted.onBefore += std::tuple{ this, &Wanted::colorGoldHook };
	colorShadowWanted.onBefore += std::tuple{ this, &Wanted::colorShdHook };
	// install hooks
	drawWanted.install();
	posWantedX.install();
	posWantedY.install( 4 );
	posWantedYGold.install();
	sizeWanted.install();
	sizeWanted2.install();
	sizeWanted3.install();
	sizeWanted4.install();
	sizeWanted5.install();
	colorWanted.install();
	colorGoldWanted.install();
	colorShadowWanted.install();
}

RwRGBA Wanted::colorEmpty() {
	RwRGBA color;
	color.red	= _color[0] * 255.0f;
	color.green = _color[1] * 255.0f;
	color.blue	= _color[2] * 255.0f;
	color.alpha = _color[3] * 255.0f;
	return color;
}

void Wanted::colorEmpty( RwRGBA color ) {
	_color[0] = float( color.red ) / 255.0f;
	_color[1] = float( color.green ) / 255.0f;
	_color[2] = float( color.blue ) / 255.0f;
	_color[3] = float( color.alpha ) / 255.0f;
}

RwRGBA Wanted::colorGold() {
	RwRGBA color;
	color.red	= _colorGld[0] * 255.0f;
	color.green = _colorGld[1] * 255.0f;
	color.blue	= _colorGld[2] * 255.0f;
	color.alpha = _colorGld[3] * 255.0f;
	return color;
}

void Wanted::colorGold( RwRGBA color ) {
	_colorGld[0] = float( color.red ) / 255.0f;
	_colorGld[1] = float( color.green ) / 255.0f;
	_colorGld[2] = float( color.blue ) / 255.0f;
	_colorGld[3] = float( color.alpha ) / 255.0f;
}

RwRGBA Wanted::colorShadow() {
	RwRGBA color;
	color.red	= _colorShd[0] * 255.0f;
	color.green = _colorShd[1] * 255.0f;
	color.blue	= _colorShd[2] * 255.0f;
	color.alpha = _colorShd[3] * 255.0f;
	return color;
}

void Wanted::colorShadow( RwRGBA color ) {
	_colorShd[0] = float( color.red ) / 255.0f;
	_colorShd[1] = float( color.green ) / 255.0f;
	_colorShd[2] = float( color.blue ) / 255.0f;
	_colorShd[3] = float( color.alpha ) / 255.0f;
}

void Wanted::reset() {
	Itembase::reset();
	custom_empty_color	= false;
	custom_gold_color	= false;
	custom_shadow_color = false;
	space				= 0.0f;
}

void Wanted::draw() {
	Itembase::draw();
	ImGui::DragFloat( name( "Space" ).data(), &space, 0.1f, -100.0f, 100.0f );

	ImGuiColorEditFlags flags = ImGuiColorEditFlags_AlphaBar;
	flags |= ImGuiColorEditFlags_DisplayRGB;
	flags |= ImGuiColorEditFlags_DisplayHex;

	if ( ImGui::ColorButton( name( "Empty color" ).data(), *(ImVec4 *)&_color, flags, ImVec2( 60, 40 ) ) &&
		 custom_empty_color ) {
		ImGui::OpenPopup( name( "colorpicker" ).data() );
		_backup_color = *(ImVec4 *)&_color;
		pickId		  = 0;
	}
	ImGui::SameLine();
	ImGui::BeginGroup();
	ImGui::Checkbox( name( "Custom empty color" ).data(), &custom_empty_color );
	ImGui::EndGroup();

	if ( ImGui::ColorButton( name( "Gold color" ).data(), *(ImVec4 *)&_colorGld, flags, ImVec2( 60, 40 ) ) &&
		 custom_gold_color ) {
		ImGui::OpenPopup( name( "colorpicker" ).data() );
		_backup_color = *(ImVec4 *)&_colorGld;
		pickId		  = 1;
	}
	ImGui::SameLine();
	ImGui::BeginGroup();
	ImGui::Checkbox( name( "Custom gold color" ).data(), &custom_gold_color );
	ImGui::EndGroup();

	if ( ImGui::ColorButton( name( "Shadow color" ).data(), *(ImVec4 *)&_colorShd, flags, ImVec2( 60, 40 ) ) &&
		 custom_shadow_color ) {
		ImGui::OpenPopup( name( "colorpicker" ).data() );
		_backup_color = *(ImVec4 *)&_colorShd;
		pickId		  = 2;
	}
	ImGui::SameLine();
	ImGui::BeginGroup();
	ImGui::Checkbox( name( "Custom shadow color" ).data(), &custom_shadow_color );
	ImGui::EndGroup();

	switch ( pickId ) {
		case 0:
			popupColorpicker( *(ImVec4 *)&_color, _backup_color );
			break;
		case 1:
			popupColorpicker( *(ImVec4 *)&_colorGld, _backup_color );
			break;
		case 2:
			popupColorpicker( *(ImVec4 *)&_colorShd, _backup_color );
			break;
	}
}

bool Wanted::isDrag() {
	if ( isShowed() && isCustomPos() && mousePos().fX >= pos().fX - _realSize.fX && mousePos().fY >= pos().fY &&
		 mousePos().fX <= pos().fX && mousePos().fY <= pos().fY + _realSize.fY ) {
		_drag		= true;
		_dragOffset = mousePos() - pos();
		return true;
	}
	return false;
}

bool Wanted::processDrag() {
	if ( !_drag ) return false;
	pos( mousePos() - _dragOffset );
	return true;
}

tiny::TinyJson Wanted::write() {
	auto writeColor = []( float *color ) {
		tiny::TinyJson json;
		json["red"].Set( color[0] );
		json["green"].Set( color[1] );
		json["blue"].Set( color[2] );
		json["alpha"].Set( color[3] );
		return json;
	};

	auto json = Itembase::write();
	if ( space ) json["space"].Set( std::to_string( space ) );

	if ( custom_empty_color ) {
		json["custom_empty_color"].Set( (int)custom_empty_color );
		json["empty_color"].Set( writeColor( _color ) );
	}
	if ( custom_gold_color ) {
		json["custom_gold_color"].Set( (int)custom_gold_color );
		json["gold_color"].Set( writeColor( _colorGld ) );
	}
	if ( custom_shadow_color ) {
		json["custom_shadow_color"].Set( (int)custom_shadow_color );
		json["shadow_color"].Set( writeColor( _colorShd ) );
	}

	return json;
}

nlohmann::json Wanted::write2() {
	auto writeColor = []( float *color ) {
		auto json	  = nlohmann::json::object();
		json["red"]	  = color[0];
		json["green"] = color[1];
		json["blue"]  = color[2];
		json["alpha"] = color[3];
		return json;
	};

	auto json = Itembase::write2();
	if ( space ) json["space"] = space;

	if ( custom_empty_color ) {
		json["custom_empty_color"] = custom_empty_color;
		json["empty_color"]		   = writeColor( _color );
	}
	if ( custom_gold_color ) {
		json["custom_gold_color"] = custom_gold_color;
		json["gold_color"]		  = writeColor( _colorGld );
	}
	if ( custom_shadow_color ) {
		json["custom_shadow_color"] = custom_shadow_color;
		json["shadow_color"]		= writeColor( _colorShd );
	}

	return json;
}

void Wanted::read( tiny::xobject &json ) {
	auto readColor = []( tiny::xobject &json, float *color ) {
		for ( int i = 0; i < json.Count(); i++ ) {
			json.Enter( i );
			color[0] = json.Get<float>( "red" );
			color[1] = json.Get<float>( "green" );
			color[2] = json.Get<float>( "blue" );
			color[3] = json.Get<float>( "alpha" );
		}
	};
	for ( int i = 0; i < json.Count(); i++ ) {
		json.Enter( i );
		Itembase::read( json );
		custom_empty_color = json.Get<int>( "custom_empty_color", 0 );
		if ( custom_empty_color ) {
			tiny::xobject jcolor = json.Get<tiny::xobject>( "empty_color" );
			readColor( jcolor, _color );
		}
		custom_gold_color = json.Get<int>( "custom_gold_color", 0 );
		if ( custom_gold_color ) {
			tiny::xobject jcolor = json.Get<tiny::xobject>( "gold_color" );
			readColor( jcolor, _colorGld );
		}
		custom_shadow_color = json.Get<int>( "custom_shadow_color", 0 );
		if ( custom_shadow_color ) {
			tiny::xobject jcolor = json.Get<tiny::xobject>( "shadow_color" );
			readColor( jcolor, _colorShd );
		}
		space = std::stof( json.Get<std::string>( "space", "0.0" ) );
	}
}

void Wanted::read2( nlohmann::json &json ) {
	auto readColor = []( nlohmann::json &json, float *color ) {
		color[0] = json.value<float>( "red", 1.0f );
		color[1] = json.value<float>( "green", 1.0f );
		color[2] = json.value<float>( "blue", 1.0f );
		color[3] = json.value<float>( "alpha", 1.0f );
	};
	Itembase::read2( json );
	custom_empty_color = json.value<bool>( "custom_empty_color", false );
	if ( custom_empty_color ) {
		auto jcolor = json["empty_color"];
		readColor( jcolor, _color );
	}
	custom_gold_color = json.value<bool>( "custom_gold_color", false );
	if ( custom_gold_color ) {
		auto jcolor = json["gold_color"];
		readColor( jcolor, _colorGld );
	}
	custom_shadow_color = json.value<bool>( "custom_shadow_color", false );
	if ( custom_shadow_color ) {
		auto jcolor = json["shadow_color"];
		readColor( jcolor, _colorShd );
	}
	space = json.value<float>( "space", 0.0f );
}

void Wanted::initLuaAndIDE( sol::state &lua, IDE *ide, std::string prefix ) {
	if ( prefix.empty() ) prefix = "wanted";
	Itembase::initLuaAndIDE( lua, ide, prefix );

	addFunction(
		lua, ide, "wanted_isCustomColorEmpty", [this]() { return custom_empty_color; },
		"Check is custom color enabled for empty wanted stars\n\nReturn:\n * bool - is custom" );
	addFunction(
		lua, ide, "wanted_isCustomColorGold", [this]() { return custom_gold_color; },
		"Check is custom color enabled for gold wanted stars\n\nReturn:\n * bool - is custom" );
	addFunction(
		lua, ide, "wanted_isCustomColorShadow", [this]() { return custom_shadow_color; },
		"Check is custom color enabled for shadow wanted stars\n\nReturn:\n * bool - is custom" );
	addFunction(
		lua, ide, "wanted_space", [this]() { return space; },
		"Get space between wanted stars\n\nReturn:\n * float - space" );
	addFunction(
		lua, ide, "wanted_colorEmpty",
		[this]() {
			auto clr = colorEmpty();
			return *(int *)&clr;
		},
		"Get color for empty wanted stars\n\nReturn:\n * int - color" );
	addFunction(
		lua, ide, "wanted_colorGold",
		[this]() {
			auto clr = colorGold();
			return *(int *)&clr;
		},
		"Get color for gold wanted stars\n\nReturn:\n * int - color" );
	addFunction(
		lua, ide, "wanted_colorShadow",
		[this]() {
			auto clr = colorShadow();
			return *(int *)&clr;
		},
		"Get color for shadow wanted stars\n\nReturn:\n * int - color" );
	addFunction(
		lua, ide, "wanted_toggleColorEmpty", [this]( bool state ) { custom_empty_color = state; },
		"Toggle custom/default color for empty wanted stars\n\nArgs:\n * bool - is custom" );
	addFunction(
		lua, ide, "wanted_toggleColorGold", [this]( bool state ) { custom_gold_color = state; },
		"Toggle custom/default color for gold wanted stars\n\nArgs:\n * bool - is custom" );
	addFunction(
		lua, ide, "wanted_toggleColorShadow", [this]( bool state ) { custom_shadow_color = state; },
		"Toggle custom/default color for shadow wanted stars\n\nArgs:\n * bool - is custom" );
	addFunction(
		lua, ide, "wanted_setSpace", [this]( float space ) { this->space = space; },
		"Set space between wanted stars\n\nArgs:\n * float - space" );
	addFunction(
		lua, ide, "wanted_setColorEmpty", [this]( int clr ) { colorEmpty( *(RwRGBA *)&clr ); },
		"Set color for empty wanted stars\n\nArgs:\n * int - color" );
	addFunction(
		lua, ide, "wanted_setColorGold", [this]( int clr ) { colorGold( *(RwRGBA *)&clr ); },
		"Set color for gold wanted stars\n\nArgs:\n * int - color" );
	addFunction(
		lua, ide, "wanted_setColorShadow", [this]( int clr ) { colorShadow( *(RwRGBA *)&clr ); },
		"Set color for shadow wanted stars\n\nArgs:\n * int - color" );

	addFunction(
		lua, ide, "wanted_amount", []() { return *(int *)0x58DB60; }, "Get wanted level\n\nReturn:\n * int - level" );
}

void Wanted::showHook( SRHook::Info &info ) {
	if ( isHidden() ) info.retAddr = 0x58E009;
}

void Wanted::posXHook( SRHook::CPU &cpu ) {
	auto &x		= *(float *)( cpu.ESP + 0x10 );
	_origPos.fX = ( 640.0f / SCREEN_X ) * x;
	x			= ( SCREEN_X / 640.0f ) * pos().fX;
}

void Wanted::posYHook( float &y, char *&text ) {
	auto rect	 = GetTextRect( ( SCREEN_X / 640.0f ) * pos().fX, y, text );
	_realSize.fX = ( 640.0f / SCREEN_X ) * ( ( ( rect.x2 - rect.x1 ) + space ) * 5 );
	_realSize.fY = ( 448.0f / SCREEN_Y ) * ( ( rect.y2 - rect.y1 ) );

	_origPos.fY = ( 448.0f / SCREEN_Y ) * y;
	y			= ( SCREEN_Y / 448.0f ) * pos().fY;
	if ( gold ) {
		gold = false;
		if ( isCustomPos() ) {
			y += ( SCREEN_Y / 448.0f ) * ( ( _realHeight - _realSize.fY ) * 0.5f );
		}
	} else
		_realHeight = _realSize.fY;
}

void Wanted::posYGoldHook() {
	gold = true;
}

void Wanted::sizeHook( float &w, float &h ) {
	_origSize = WindowScreenToGameScreen( w * 100.0f, h * 100.0f );
	w		  = ( SCREEN_X / 640.0f ) * size().fX * 0.01f;
	h		  = ( SCREEN_Y / 448.0f ) * size().fY * 0.01f;
}

void Wanted::sizeMulHook( float &w, float &h ) {
	auto mul  = *(float *)0x858F08;
	_origSize = WindowScreenToGameScreen( ( w * 100.0f ) / mul, ( h * 100.0f ) / mul );
	w		  = ( ( SCREEN_X / 640.0f ) * size().fX * 0.01f ) * mul;
	h		  = ( ( SCREEN_Y / 448.0f ) * size().fY * 0.01f ) * mul;
}

void Wanted::sizeSkipHook( SRHook::Info &info ) {
	info.skipOriginal = true;
}

void Wanted::posOffsetHook( SRHook::Info &info ) {
	info.skipOriginal = true;
	auto &x			  = *(float *)( info.cpu.ESP + 0x10 );
	auto  div		  = *(float *)0x866C60;
	auto  mul		  = *(float *)0x859008;
	auto  w			  = ( SCREEN_X / 640.0f ) * size().fX * 0.01f;
	if ( space < -100.0f )
		space = -100.0f;
	else if ( space > 100.0f )
		space = 100.0f;
	x -= ( w / div ) * mul + ( SCREEN_X / 640.0f ) * space;
}

void Wanted::colorHook( RwRGBA &color ) {
	if ( !custom_empty_color ) return;
	color.red	= _color[0] * 255.0f;
	color.green = _color[1] * 255.0f;
	color.blue	= _color[2] * 255.0f;
	color.alpha = _color[3] * 255.0f;
}

void Wanted::colorGoldHook( RwRGBA &color ) {
	if ( !custom_gold_color ) return;
	color.red	= _colorGld[0] * 255.0f;
	color.green = _colorGld[1] * 255.0f;
	color.blue	= _colorGld[2] * 255.0f;
	color.alpha = _colorGld[3] * 255.0f;
}

void Wanted::colorShdHook( RwRGBA &color ) {
	if ( !custom_shadow_color ) return;
	color.red	= _colorShd[0] * 255.0f;
	color.green = _colorShd[1] * 255.0f;
	color.blue	= _colorShd[2] * 255.0f;
	color.alpha = _colorShd[3] * 255.0f;
}
