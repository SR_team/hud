#include "Radar.h"
#include <base/d3d9/color.h>
#include <cmath>
#include <gtasa/CGame/CPed.h>
#include <gtasa/CGame/CVehicle.h>
#include <gtasa/CGame/methods.h>
#include <llmo/callfunc.hpp>
#include <nlohmann/json.hpp>

#include "airPlane.hpp"
#include "radardisc.hpp"
#include "radardisc_square.hpp"

Radar::Radar( SRDescent *parent, std::string_view id ) : Itembase( parent, id ) {
	// init def values
	_size							= { .25, .25 };
	_wgtName						= "Radar";
	_white_radardisc.texture		= loadPNG( radardisc_png, radardisc_png_len );
	_white_radardisc_square.texture = loadPNG( radardisc_square_png, radardisc_square_png_len );
	_airPlane.texture				= loadPNG( airPlane_png, airPlane_png_len );
	// bind callbacks
	drawRadar.onBefore += std::tuple{ this, &Radar::showHook };
	drawRadarSprite.onBefore += std::tuple{ this, &Radar::spriteHook };
	posRadarEnd.onAfter += std::tuple{ this, &Radar::posHook };
	borderLU.onBefore += std::tuple{ this, &Radar::borderHook };
	borderRU.onBefore += std::tuple{ this, &Radar::borderHook };
	borderLB.onBefore += std::tuple{ this, &Radar::borderHook };
	borderRB.onBefore += std::tuple{ this, &Radar::borderHook };
	limitPoint.onAfter += std::tuple{ this, &Radar::pointHook };
	mask.onAfter += std::tuple{ this, &Radar::maskHook };
	mapColor.onBefore += std::tuple{ this, &Radar::mapClHook };
	fixedZoom.onBefore += std::tuple{ this, &Radar::fZoomHook };
	airPlane.onAfter += std::tuple{ this, &Radar::airPlaneHook };
	hBar.onBefore += std::tuple{ this, &Radar::hBarHook };
	hBarLine.onBefore += std::tuple{ this, &Radar::hBarLineHook };
	drawIcon.onBefore += std::tuple{ this, &Radar::icon };
	drawCentre.onBefore += std::tuple{ this, &Radar::centre };
	// install hooks
	drawRadar.install();
	drawRadarSprite.install( 4 );
	posRadarEnd.install();
	borderLU.install();
	borderRU.install();
	borderLB.install();
	borderRB.install();
	limitPoint.install( 0, 4 );
	mask.install();
	mapColor.install( 12 );
	fixedZoom.install();
	airPlane.install();
	hBar.install();
	hBarLine.install();
	drawIcon.install();
	drawCentre.install();
}

float Radar::radius() const {
	return ( originalPos().fX + originalPos().fY ) * 0.1;
}

RwRGBA Radar::colorBorder() {
	RwRGBA color;
	color.red	= _colorBrd[0] * 255.0f;
	color.green = _colorBrd[1] * 255.0f;
	color.blue	= _colorBrd[2] * 255.0f;
	color.alpha = _colorBrd[3] * 255.0f;
	return color;
}

void Radar::colorBorder( RwRGBA color ) {
	_colorBrd[0] = float( color.red ) / 255.0f;
	_colorBrd[1] = float( color.green ) / 255.0f;
	_colorBrd[2] = float( color.blue ) / 255.0f;
	_colorBrd[3] = float( color.alpha ) / 255.0f;
}

RwRGBA Radar::colorMap() const {
	RwRGBA color;
	color.red	= _colorMap[0] * 255.0f;
	color.green = _colorMap[1] * 255.0f;
	color.blue	= _colorMap[2] * 255.0f;
	color.alpha = _colorMap[3] * 255.0f;
	return color;
}

void Radar::colorMap( RwRGBA color ) {
	_colorMap[0] = float( color.red ) / 255.0f;
	_colorMap[1] = float( color.green ) / 255.0f;
	_colorMap[2] = float( color.blue ) / 255.0f;
	_colorMap[3] = 1.0f;
}

RwRGBA Radar::colorAir() const {
	RwRGBA color;
	color.red	= _colorAir[0] * 255.0f;
	color.green = _colorAir[1] * 255.0f;
	color.blue	= _colorAir[2] * 255.0f;
	color.alpha = _colorAir[3] * 255.0f;
	return color;
}

void Radar::colorAir( RwRGBA color ) {
	_colorAir[0] = float( color.red ) / 255.0f;
	_colorAir[1] = float( color.green ) / 255.0f;
	_colorAir[2] = float( color.blue ) / 255.0f;
	_colorAir[3] = float( color.alpha ) / 255.0f;
}

void Radar::setBorder( RwTexture *texture ) {
	_custom_radardisc.texture = texture;
}

void Radar::setAirPlane( RwTexture *texture ) {
	_custom_airPlane.texture = texture;
}

void Radar::setIconTexture( std::string icon, std::string txName, RwTexture *texture ) {
	if ( !texture ) txName.clear();
	_icons[icon].first			= txName;
	_icons[icon].second.texture = texture;
}

std::string Radar::iconTxName( std::string icon ) const {
	return _icons[icon].first;
}

void Radar::reset() {
	Itembase::reset();
	north				  = true;
	borders				  = true;
	custom_border_color	  = false;
	custom_map_color	  = false;
	custom_radardisc	  = false;
	fixed_zoom			  = false;
	square				  = false;
	custom_airPlane_color = false;
	custom_airPlane		  = false;
	scale_icons			  = false;
	_icons.clear();
}

void Radar::draw() {
	Itembase::draw();
	ImGui::Checkbox( name( "Draw N icon" ).data(), &north );
	ImGui::SameLine( 0, 10 );
	ImGui::Checkbox( name( "Draw borders" ).data(), &borders );
	ImGui::SameLine( 0, 10 );
	ImGui::Checkbox( name( "Square radar" ).data(), &square );
	ImGuiColorEditFlags flags = ImGuiColorEditFlags_AlphaBar;
	flags |= ImGuiColorEditFlags_DisplayRGB;
	flags |= ImGuiColorEditFlags_DisplayHex;

	if ( ImGui::ColorButton( name( "Border color" ).data(), *(ImVec4 *)&_colorBrd, flags, ImVec2( 60, 40 ) ) &&
		 custom_border_color ) {
		ImGui::OpenPopup( name( "colorpicker" ).data() );
		_backup_color = *(ImVec4 *)&_colorBrd;
		pickId		  = 0;
	}
	ImGui::SameLine();
	ImGui::BeginGroup();
	ImGui::Checkbox( name( "Custom border color" ).data(), &custom_border_color );
	ImGui::Checkbox( name( "Custom radardisc" ).data(), &custom_radardisc );
	ImGui::EndGroup();

	ImGui::SameLine();
	if ( ImGui::ColorButton( name( "Map color" ).data(), *(ImVec4 *)&_colorMap, flags | ImGuiColorEditFlags_NoAlpha,
							 ImVec2( 60, 40 ) ) &&
		 custom_map_color ) {
		ImGui::OpenPopup( name( "colorpicker" ).data() );
		_backup_color = *(ImVec4 *)&_colorMap;
		pickId		  = 1;
	}
	ImGui::SameLine();
	ImGui::Checkbox( name( "Custom map color" ).data(), &custom_map_color );

	if ( ImGui::ColorButton( name( "AirPlane color" ).data(), *(ImVec4 *)&_colorAir, flags, ImVec2( 60, 40 ) ) &&
		 custom_airPlane_color ) {
		ImGui::OpenPopup( name( "colorpicker" ).data() );
		_backup_color = *(ImVec4 *)&_colorAir;
		pickId		  = 2;
	}
	ImGui::SameLine();
	ImGui::BeginGroup();
	ImGui::Checkbox( name( "Custom AirPlane color" ).data(), &custom_airPlane_color );
	ImGui::Checkbox( name( "Custom AirPlane" ).data(), &custom_airPlane );
	ImGui::EndGroup();

	switch ( pickId ) {
		case 0:
			popupColorpicker( *(ImVec4 *)&_colorBrd, _backup_color );
			break;
		case 1:
			popupColorpicker( *(ImVec4 *)&_colorMap, _backup_color, false );
			break;
		case 2:
			popupColorpicker( *(ImVec4 *)&_colorAir, _backup_color );
			break;
	}

	float fZoom = fixZoom;
	ImGui::DragFloat( name( "", "fixZoom" ).data(), &fZoom, 0.1f, 10.0f, 350.0f );
	ImGui::SameLine( 0, 10 );
	ImGui::Checkbox( name( "Fixed zoom" ).data(), &fixed_zoom );
	if ( fixed_zoom ) fixZoom = fZoom;

	float scale = iconsScale;
	ImGui::DragFloat( "##iconsScale_radar", &scale, 0.01f, 0.1f, 10.0f );
	ImGui::SameLine( 0, 10 );
	ImGui::Checkbox( name( "Icons scale" ).data(), &scale_icons );
	if ( scale_icons ) iconsScale = scale;
}

bool Radar::isDrag() {
	if ( isShowed() && isCustomPos() ) {
		if ( square && mousePos().fX >= pos().fX - size().fX * 0.5f && mousePos().fY >= pos().fY &&
			 mousePos().fX <= pos().fX + size().fX * 0.5f && mousePos().fY <= pos().fY + size().fY ) {
			_drag		= true;
			_dragOffset = mousePos() - pos();
			return true;
		} else {
			auto mp = mousePos();
			mp.fY -= size().fY * 0.5f;
			if ( ( mp - pos() ).length() < radius() ) {
				_drag		= true;
				_dragOffset = mousePos() - pos();
				return true;
			}
		}
	}
	return false;
}

bool Radar::processDrag() {
	if ( !_drag ) return false;
	auto mp = mousePos() - _dragOffset;
	pos( mp );
	return true;
}

tiny::TinyJson Radar::write() {
	auto json = Itembase::write();
	if ( !north ) json["north"].Set( (int)north );
	if ( !borders ) json["borders"].Set( (int)borders );
	if ( custom_radardisc ) json["custom_radardisc"].Set( (int)custom_radardisc );
	if ( square ) json["square"].Set( (int)square );

	if ( custom_border_color ) {
		json["custom_border_color"].Set( (int)custom_border_color );
		tiny::TinyJson color;
		color["red"].Set( _colorBrd[0] );
		color["green"].Set( _colorBrd[1] );
		color["blue"].Set( _colorBrd[2] );
		color["alpha"].Set( _colorBrd[3] );

		json["border_color"].Set( color );
	}

	if ( custom_map_color ) {
		json["custom_map_color"].Set( (int)custom_map_color );
		tiny::TinyJson color;
		color["red"].Set( _colorMap[0] );
		color["green"].Set( _colorMap[1] );
		color["blue"].Set( _colorMap[2] );
		color["alpha"].Set( _colorMap[3] );

		json["map_color"].Set( color );
	}

	if ( custom_airPlane_color ) {
		json["custom_airPlane_color"].Set( (int)custom_airPlane_color );
		tiny::TinyJson color;
		color["red"].Set( _colorAir[0] );
		color["green"].Set( _colorAir[1] );
		color["blue"].Set( _colorAir[2] );
		color["alpha"].Set( _colorAir[3] );

		json["air_color"].Set( color );
	}
	if ( custom_airPlane ) json["custom_airPlane"].Set( (int)custom_airPlane );

	if ( fixed_zoom ) {
		json["fixed_zoom"].Set( (int)fixed_zoom );
		json["fixZoom"].Set( fixZoom );
	}

	if ( scale_icons ) {
		json["scale_icons"].Set( (int)scale_icons );
		json["iconsScale"].Set( iconsScale );
	}

	return json;
}

nlohmann::json Radar::write2() {
	auto json = Itembase::write2();
	if ( !north ) json["north"] = north;
	if ( !borders ) json["borders"] = borders;
	if ( custom_radardisc ) json["custom_radardisc"] = custom_radardisc;
	if ( square ) json["square"] = square;

	if ( custom_border_color ) {
		json["custom_border_color"] = custom_border_color;
		auto color					= nlohmann::json::object();
		color["red"]				= _colorBrd[0];
		color["green"]				= _colorBrd[1];
		color["blue"]				= _colorBrd[2];
		color["alpha"]				= _colorBrd[3];

		json["border_color"] = color;
	}

	if ( custom_map_color ) {
		json["custom_map_color"] = custom_map_color;
		auto color				 = nlohmann::json::object();
		color["red"]			 = _colorMap[0];
		color["green"]			 = _colorMap[1];
		color["blue"]			 = _colorMap[2];
		color["alpha"]			 = _colorMap[3];

		json["map_color"] = color;
	}

	if ( custom_airPlane_color ) {
		json["custom_airPlane_color"] = custom_airPlane_color;
		auto color					  = nlohmann::json::object();
		color["red"]				  = _colorAir[0];
		color["green"]				  = _colorAir[1];
		color["blue"]				  = _colorAir[2];
		color["alpha"]				  = _colorAir[3];

		json["air_color"] = color;
	}
	if ( custom_airPlane ) json["custom_airPlane"] = custom_airPlane;

	if ( fixed_zoom ) {
		json["fixed_zoom"] = fixed_zoom;
		json["fixZoom"]	   = fixZoom;
	}

	if ( scale_icons ) {
		json["scale_icons"] = scale_icons;
		json["iconsScale"]	= iconsScale;
	}

	return json;
}

void Radar::read( tiny::xobject &json ) {
	auto readColor = []( tiny::xobject &json, float *color ) {
		for ( int i = 0; i < json.Count(); i++ ) {
			json.Enter( i );
			color[0] = json.Get<float>( "red" );
			color[1] = json.Get<float>( "green" );
			color[2] = json.Get<float>( "blue" );
			color[3] = json.Get<float>( "alpha" );
		}
	};
	for ( int i = 0; i < json.Count(); i++ ) {
		json.Enter( i );
		Itembase::read( json );
		north				= json.Get<int>( "north", 1 );
		borders				= json.Get<int>( "borders", 1 );
		custom_border_color = json.Get<int>( "custom_border_color", 0 );
		if ( custom_border_color ) {
			tiny::xobject jcolor = json.Get<tiny::xobject>( "border_color" );
			readColor( jcolor, _colorBrd );
		}
		custom_map_color = json.Get<int>( "custom_map_color", 0 );
		if ( custom_map_color ) {
			tiny::xobject jcolor = json.Get<tiny::xobject>( "map_color" );
			readColor( jcolor, _colorMap );
		}
		custom_airPlane_color = json.Get<int>( "custom_airPlane_color", 0 );
		if ( custom_airPlane_color ) {
			tiny::xobject jcolor = json.Get<tiny::xobject>( "air_color" );
			readColor( jcolor, _colorAir );
		}
		custom_airPlane = json.Get<int>( "custom_airPlane", 0 );

		custom_radardisc = json.Get<int>( "custom_radardisc", 0 );
		square			 = json.Get<int>( "square", 0 );

		fixed_zoom = json.Get<int>( "fixed_zoom", 0 );
		fixZoom	   = json.Get<float>( "fixZoom", 220.0f );

		scale_icons = json.Get<int>( "scale_icons", 0 );
		iconsScale	= json.Get<float>( "iconsScale", 1.0f );
	}
}

void Radar::read2( nlohmann::json &json ) {
	auto readColor = []( nlohmann::json &json, float *color ) {
		color[0] = json.value<float>( "red", 1.0f );
		color[1] = json.value<float>( "green", 1.0f );
		color[2] = json.value<float>( "blue", 1.0f );
		color[3] = json.value<float>( "alpha", 1.0f );
	};
	Itembase::read2( json );
	north				= json.value<bool>( "north", true );
	borders				= json.value<bool>( "borders", true );
	custom_border_color = json.value<bool>( "custom_border_color", false );
	if ( custom_border_color ) {
		auto jcolor = json["border_color"];
		readColor( jcolor, _colorBrd );
	}
	custom_map_color = json.value<bool>( "custom_map_color", false );
	if ( custom_map_color ) {
		auto jcolor = json["map_color"];
		readColor( jcolor, _colorMap );
	}
	custom_airPlane_color = json.value<bool>( "custom_airPlane_color", false );
	if ( custom_airPlane_color ) {
		auto jcolor = json["air_color"];
		readColor( jcolor, _colorAir );
	}
	custom_airPlane = json.value<bool>( "custom_airPlane", false );

	custom_radardisc = json.value<bool>( "custom_radardisc", false );
	square			 = json.value<bool>( "square", false );

	fixed_zoom = json.value<bool>( "fixed_zoom", false );
	fixZoom	   = json.value<float>( "fixZoom", 220.0f );

	scale_icons = json.value<bool>( "scale_icons", false );
	iconsScale	= json.value<float>( "iconsScale", 1.0f );
}

void Radar::initLuaAndIDE( sol::state &lua, IDE *ide, std::string prefix ) {
	if ( prefix.empty() ) prefix = "radar";
	Itembase::initLuaAndIDE( lua, ide, prefix );

	// NOTE: Текстуры радара в Gui
	addFunction(
		lua, ide, "radar_north", [this]() { return north; },
		"Check is north icon enabled\n\nReturn:\n * bool - is enabled" );
	addFunction(
		lua, ide, "radar_borders", [this]() { return borders; },
		"Check is borders enabled\n\nReturn:\n * bool - is enabled" );
	addFunction(
		lua, ide, "radar_isCustomColorBorders", [this]() { return custom_border_color; },
		"Check is custom borders color enabled\n\nReturn:\n * bool - is enabled" );
	addFunction(
		lua, ide, "radar_isCustomColorMap", [this]() { return custom_map_color; },
		"Check is custom map color enabled\n\nReturn:\n * bool - is enabled" );
	addFunction(
		lua, ide, "radar_isZoomFixed", [this]() { return fixed_zoom; },
		"Check is radar zoom fixed\n\nReturn:\n * bool - is fixed" );
	addFunction(
		lua, ide, "radar_isIconsScale", [this]() { return scale_icons; },
		"Check is enabled custom scale for icons\n\nReturn:\n * bool - is scale" );
	addFunction(
		lua, ide, "radar_isSquare", [this]() { return square; },
		"Check is radar square\n\nReturn:\n * bool - is square" );
	addFunction(
		lua, ide, "radar_isCustomColorAirPlane", [this]() { return custom_airPlane_color; },
		"Check is custom air plane color enabled\n\nReturn:\n * bool - is enabled" );
	addFunction(
		lua, ide, "radar_fixZoom", [this]() { return fixZoom; },
		"Get value of fixed zoom\n\nReturn:\n * float - fixed zoom value" );
	addFunction(
		lua, ide, "radar_iconsScale", [this]() { return iconsScale; },
		"Get value of icons scale\n\nReturn:\n * float - scale" );
	addFunction(
		lua, ide, "radar_isCustomBorders", [this]() { return custom_radardisc; },
		"Check is used custom borders (radardisc) texture\n\nReturn:\n * bool - is custom" );
	addFunction(
		lua, ide, "radar_isCustomAirPlane", [this]() { return custom_airPlane; },
		"Check is used custom air plane texture\n\nReturn:\n * bool - is custom" );
	addFunction(
		lua, ide, "radar_colorBorders",
		[this]() {
			auto clr = colorBorder();
			return *(int *)&clr;
		},
		"Get borders color\n\nReturn:\n * int - color" );
	addFunction(
		lua, ide, "radar_colorMap",
		[this]() {
			auto clr = colorMap();
			return *(int *)&clr;
		},
		"Get map color\n\nReturn:\n * int - color" );
	addFunction(
		lua, ide, "radar_colorAirPlane",
		[this]() {
			auto clr = colorAir();
			return *(int *)&clr;
		},
		"Get air plane color\n\nReturn:\n * int - color" );
	addFunction(
		lua, ide, "radar_toggleNorth", [this]( bool state ) { north = state; },
		"Enable/disable north icon\n\nArgs:\n * bool - is enabled" );
	addFunction(
		lua, ide, "radar_toggleBorders", [this]( bool state ) { borders = state; },
		"Enable/disable radar borders\n\nArgs:\n * bool - is enabled" );
	addFunction(
		lua, ide, "radar_toggleCustomColorBorders", [this]( bool state ) { custom_border_color = state; },
		"Enable/disable custom borders color\n\nArgs:\n * bool - is enabled" );
	addFunction(
		lua, ide, "radar_toggleCustomColorMap", [this]( bool state ) { custom_map_color = state; },
		"Enable/disable custom map color\n\nArgs:\n * bool - is enabled" );
	addFunction(
		lua, ide, "radar_toggleZoomFix", [this]( bool state ) { fixed_zoom = state; },
		"Enable/disable radar zoom fixed\n\nArgs:\n * bool - is fixed" );
	addFunction(
		lua, ide, "radar_toggleIconsScale", [this]( bool state ) { scale_icons = state; },
		"Enable/disable custom icons scale\n\nArgs:\n * bool - is scale" );
	addFunction(
		lua, ide, "radar_toggleSquare", [this]( bool state ) { square = state; },
		"Enable/disable radar square\n\nArgs:\n * bool - is square" );
	addFunction(
		lua, ide, "radar_toggleCustomColorAirPlane", [this]( bool state ) { custom_airPlane_color = state; },
		"Enable/disable custom air plane color\n\nArgs:\n * bool - is enabled" );
	addFunction(
		lua, ide, "radar_setFixZoom", [this]( float zoom ) { fixZoom = zoom; },
		"Set value of fixed zoom\n\nArgs:\n * float - fixed zoom value" );
	addFunction(
		lua, ide, "radar_setIconsScale", [this]( float zoom ) { iconsScale = zoom; },
		"Set value of icons scale\n\nArgs:\n * float - scale" );
	addFunction(
		lua, ide, "radar_toggleCustomBorders", [this]( bool state ) { custom_radardisc = state; },
		"Enable/disable using custom borders (radardisc) texture\n\nArgs:\n * bool - is custom" );
	addFunction(
		lua, ide, "radar_toggleCustomAirPlane", [this]( bool state ) { custom_airPlane = state; },
		"Enable/disable using custom air plane texture\n\nArgs:\n * bool - is custom" );
	addFunction(
		lua, ide, "radar_setColorBorders", [this]( int clr ) { colorBorder( *(RwRGBA *)&clr ); },
		"Set color for empty wanted stars\n\nArgs:\n * int - color" );
	addFunction(
		lua, ide, "radar_setColorMap", [this]( int clr ) { colorMap( *(RwRGBA *)&clr ); },
		"Set color for gold wanted stars\n\nArgs:\n * int - color" );
	addFunction(
		lua, ide, "radar_setColorAirPlane", [this]( int clr ) { colorAir( *(RwRGBA *)&clr ); },
		"Set color for shadow wanted stars\n\nArgs:\n * int - color" );
}

void Radar::showHook( SRHook::CPU &cpu ) {
	if ( isHidden() ) cpu.ZF = 1;
}

void Radar::spriteHook( SRHook::Info &info, int &type, float &, float &, uint8_t &alpha ) {
	if ( type == 4 && !north ) { // north icon
		info.skipOriginal = true;
		info.retAddr	  = 0x58610D;
	} else if ( north && type == 4 && !alpha )
		alpha = 255;
}

void Radar::posHook( SRHook::Info &info ) {
	float width	 = *(int *)0xC17044;
	float height = *(int *)0xC17048;

	_origPos.fX	 = *(float *)0x858A10 * 1.7f;
	_origPos.fY	 = 448.0f - *(float *)0x866B70;
	_origSize.fX = *(float *)0x866B78;
	if ( width > height ) _origSize.fX *= 1.333333f / ( width / height );
	_origSize.fY = *(float *)0x866B74;

	auto in	 = (RwV2D *)info.cpu.EAX;
	auto out = (RwV2D *)info.cpu.ECX;
	auto pxX = 0.0015625;
	auto pxY = 0.002232143;

	out->fX = ( width * pxX ) * pos().fX + size().fX * ( width * pxX ) * in->fX * 0.5f;
	out->fY =
		pxY * height * pos().fY + size().fY * ( pxY * height ) * 0.5f - size().fY * ( pxY * height ) * in->fY * 0.5f;
}

void Radar::borderHook( SRHook::Hook<float *, RwRGBA *> &hook, float *&rect, RwRGBA *&color ) {
	if ( !borders ) {
		hook.skipOriginal();
		hook.cpu.ESP += 8;
		return;
	}
	float width	 = *(int *)0xC17044;
	float height = *(int *)0xC17048;
	auto  pxX	 = 0.0015625;
	auto  pxY	 = 0.002232143;

	if ( &hook == &borderLU || &hook == &borderLB ) {
		rect[0] = width * pxX * pos().fX - ( size().fX + 4.0f ) * width * pxX * 0.5f;
		rect[2] = width * pxX * pos().fX;
	} else {
		rect[0] = width * pxX * pos().fX + ( size().fX + 4.0f ) * width * pxX * 0.5f;
		rect[2] = width * pxX * pos().fX;
	}
	if ( &hook == &borderLU || &hook == &borderRU ) {
		rect[1] = pxY * height * pos().fY + size().fY * pxY * height * 0.5f;
		// 2.0 потому что 4.0 * 0.5, в остальных местах 4.0 применяется к размуеру, а не позиции
		rect[3] = height * pxY * ( pos().fY - 2.0f );
	} else {
		rect[1] = pxY * height * pos().fY + size().fY * pxY * height * 0.5f;
		rect[3] = rect[1] + ( size().fY + 4.0f ) * pxY * height * 0.5f;
	}
	if ( !color ) return;
	if ( custom_border_color ) {
		color->red	 = _colorBrd[0] * 255.0f;
		color->green = _colorBrd[1] * 255.0f;
		color->blue	 = _colorBrd[2] * 255.0f;
		color->alpha = _colorBrd[3] * 255.0f;
	}
	if ( custom_radardisc ) {
		if ( _custom_radardisc.texture )
			hook.cpu.ECX = (size_t)&_custom_radardisc;
		else {
			if ( square )
				hook.cpu.ECX = (size_t)&_white_radardisc_square;
			else
				hook.cpu.ECX = (size_t)&_white_radardisc;
		}
	}
}

void Radar::pointHook( SRHook::Info &info, RwV2D *&pos ) {
	if ( square ) {
		pos->normalizeRect();
		info.cpu.ZF = 0;
	}
}

void Radar::maskHook( SRHook::Info &info ) {
	if ( square ) {
		auto in = (RwV2D *)( info.cpu.ESP + 0x20 );
		for ( int i = 0; i < 4; ++i ) in[i] *= 1.4f;
	}
}

void Radar::mapClHook( RwRGBA *&clr ) {
	if ( !custom_map_color ) return;
	*clr = colorMap();
}

void Radar::fZoomHook() {
	if ( fixZoom < 10.0f )
		fixZoom = 10.0f;
	else if ( fixZoom > 350.0f )
		fixZoom = 350.0f;
	if ( !fixed_zoom ) return;
	*(float *)0xBA8314 = fixZoom;
}

void Radar::airPlaneHook( SRHook::Info &info ) {
	auto  veh	= (CVehicle *)info.cpu.EAX;
	auto  az	= LOCAL_PLAYER->matrix->at.fZ;
	auto  rz	= veh->matrix->right.fZ;
	float angle = atan2( rz, az ) - 3.14159f;
	auto  mul	= *(float *)0x859AB0;

	int	  w = ( SCREEN_X / 640.0f ) * size().fX * mul;
	float x = ( SCREEN_X / 640.0f ) * pos().fX;
	int	  h = ( SCREEN_Y / 448.0f ) * size().fY * mul;
	float y = ( SCREEN_Y / 448.0f ) * pos().fY + ( SCREEN_Y / 448.0f ) * size().fY * 0.5f;

	auto color = colorAir();
	if ( !custom_airPlane_color ) {
		color.red	= 0xFF;
		color.green = 0xFF;
		color.blue	= 0xFF;
		color.alpha = 0xFF;
	}
	auto tx = (CTexture *)0xBAB20C;
	if ( custom_airPlane ) {
		if ( _custom_airPlane.texture )
			tx = &_custom_airPlane;
		else
			tx = &_airPlane;
	}

	CallFunc::ccall( 0x584850, tx, x, y, angle, w, h, color );

	info.cpu.ESP += 0x14;
	info.retAddr = 0x58A556;
}

void Radar::hBarHook( RwVRect *&rect ) {
	auto left = size().fX * 0.286f;
	if ( left < 11.0f ) left = 11.0f;
	if ( pos().fX < 320.0f ) {
		rect->x1 = ( SCREEN_X / 640.0f ) * ( ( pos().fX - size().fX * 0.5f ) - left );
		rect->x2 = ( SCREEN_X / 640.0f ) * ( ( pos().fX - size().fX * 0.5f ) - 10.0f );
	} else {
		rect->x1 = ( SCREEN_X / 640.0f ) * ( ( pos().fX + size().fX * 0.5f ) + left );
		rect->x2 = ( SCREEN_X / 640.0f ) * ( ( pos().fX + size().fX * 0.5f ) + 10.0f );
	}
	rect->y1 = ( SCREEN_Y / 448.0f ) * pos().fY;
	rect->y2 = rect->y1 + ( SCREEN_Y / 448.0f ) * size().fY;
}

void Radar::hBarLineHook( RwVRect *&rect ) {
	auto left = size().fX * 0.286f;
	if ( left < 11.0f ) left = 11.0f;
	if ( pos().fX < 320.0f ) {
		rect->x1 = ( SCREEN_X / 640.0f ) * ( ( pos().fX - size().fX * 0.5f ) - ( left + 1.0f ) );
		rect->x2 = ( SCREEN_X / 640.0f ) * ( ( pos().fX - size().fX * 0.5f ) - 9.0f );
	} else {
		rect->x1 = ( SCREEN_X / 640.0f ) * ( ( pos().fX + size().fX * 0.5f ) + ( left + 1.0f ) );
		rect->x2 = ( SCREEN_X / 640.0f ) * ( ( pos().fX + size().fX * 0.5f ) + 9.0f );
	}

	CEntity *entity = LOCAL_PLAYER;
	float	 limit	= 600.f;
	if ( LOCAL_PLAYER->isDriving() ) entity = LOCAL_PLAYER->vehicle;

	auto h = entity->getPosition().fZ + 100.0f;
	if ( h ) {
		while ( h > limit ) h -= limit;
		h /= limit;
	} else
		h = 0.0f;

	auto bar = ( SCREEN_Y / 448.0f ) * size().fY;
	rect->y2 = ( SCREEN_Y / 448.0f ) * ( pos().fY + size().fY ) - bar * h;
	rect->y1 = rect->y2 - ( SCREEN_Y / 448.0f );
}

void Radar::icon( SRHook::CPU &cpu, RwVRect *&rect ) {
	auto x = ( rect->x1 + rect->x2 ) * 0.5f;
	auto y = ( rect->y1 + rect->y2 ) * 0.5f;
	auto w = abs( rect->x1 - x );
	auto h = abs( rect->y1 - y );

	if ( scale_icons ) {
		w *= iconsScale;
		h *= iconsScale;
	}

	rect->x1 = x - w;
	rect->x2 = x + w;
	rect->y1 = y + h;
	rect->y2 = y - h;

	auto tx = (CTexture *)cpu.ECX;
	if ( tx->texture )
		if ( _icons[tx->texture->name].second.texture ) cpu.ECX = (size_t)&_icons[tx->texture->name].second;
}

void Radar::centre( CTexture *&tx ) {
	if ( _icons["radar_centre"].second.texture ) tx = &_icons["radar_centre"].second;
}
