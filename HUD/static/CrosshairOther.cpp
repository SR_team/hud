#include "CrosshairOther.h"
#include <gtasa/CGame/CPed.h>
#include <gtasa/CGame/CVehicle.h>

CrosshairOther::CrosshairOther( SRDescent *parent, int weapon, std::string_view wgtName, std::string_view id )
	: Itemcrosshair( parent, id ), _weaponId( weapon ) {
	_wgtName = wgtName;
	// bind callbacks
	raster.onBefore += std::tuple{ this, &CrosshairOther::rasterHook };
	lu.onBefore += std::tuple{ this, &CrosshairOther::drawHook };
	ru.onBefore += std::tuple{ this, &CrosshairOther::drawHook };
	lb.onBefore += std::tuple{ this, &CrosshairOther::drawHook };
	rb.onBefore += std::tuple{ this, &CrosshairOther::drawHook };
	// install hooks
	raster.install();
	lu.install( 20 );
	ru.install( 20 );
	lb.install( 20 );
	rb.install( 20 );
}

void CrosshairOther::rasterHook( RwRaster *&raster ) {
	if ( !hook() ) return;
	if ( _texture.texture ) raster = _texture.texture->raster;
}

void CrosshairOther::drawHook( uint8_t &red, uint8_t &green, uint8_t &blue, uint8_t &, float &, uint8_t &alpha ) {
	if ( !hook() ) return;
	if ( isHidden() ) {
		alpha = 0;
		return;
	}
	auto clr = color();
	red		 = clr.red;
	green	 = clr.green;
	blue	 = clr.blue;
	alpha	 = clr.alpha;
}

bool CrosshairOther::hook() {
	auto wId = LOCAL_PLAYER->WeaponSlot[LOCAL_PLAYER->weapon_slot].m_Type;
	if ( _weaponId < 400 && wId != _weaponId )
		return false;
	else if ( _weaponId >= 400 && ( !LOCAL_PLAYER->isDriver() || LOCAL_PLAYER->vehicle->modelID != _weaponId ) )
		return false;
	return true;
}
