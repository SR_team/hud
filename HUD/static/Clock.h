#ifndef CLOCK_H
#define CLOCK_H

#include "Itembase.h"
#include <llmo/SRHook.hpp>

class Clock : public Itembase {
	SRHook::Hook<>					   drawClock{ 0x58EB14, 6 };
	SRHook::Hook<float, float, char *> posClock{ 0x58EC21, 5 };
	SRHook::Hook<float, float>		   sizeClock{ 0x58EB4E, 5 };
	SRHook::Hook<RwRGBA>			   clClock{ 0x58EBDD, 5 };
	SRHook::Hook<RwRGBA>			   clClockShd{ 0x58EB90, 5 };
	SRHook::Hook<int>				   style{ 0x58EB5C, 5 };
	SRHook::Hook<bool>				   propClock{ 0x58EB55, 5 };
	SRHook::Hook<char>				   edgeClock{ 0x58EB71, 5 };
	SRHook::Hook<char>				   alignClock{ 0x58EB63, 5 };

public:
	Clock( SRDescent *parent, std::string_view id = "" );

	bool custom_color		 = false;
	bool custom_shadow_color = false;
	bool proportional		 = false;
	enum class Align { center, left, right };
	Align align		 = Align::right;
	int	  edge		 = 2;
	int	  font_style = 3;

	RwRGBA color() const;
	void   color( RwRGBA color );
	RwRGBA colorShadow() const;
	void   colorShadow( RwRGBA color );

	virtual void		   reset() override;
	virtual void		   draw() override;
	virtual bool		   isDrag() override;
	virtual bool		   processDrag() override;
	virtual nlohmann::json write2() override;
	virtual void		   read2( nlohmann::json &json ) override;
	virtual void		   initLuaAndIDE( sol::state &lua, class IDE *ide, std::string prefix = "" ) override;

protected:
	float  _colorPos[4]{ 1, 1, 1, 1 };
	float  _colorShd[4]{ 0, 0, 0, 1 };
	ImVec4 _backup_color;
	int	   pickId = 0;
	RwV2D  _realSize;
	RwV2D  _dragOffset;

	void showHook( SRHook::CPU &cpu );
	void posHook( float &x, float &y, char *&text );
	void sizeHook( float &w, float &h );
	void clHook( RwRGBA &color );
	void clShdHook( RwRGBA &color );
	void styleHook( int &fontStyle );
	void propHook( bool &prop );
	void edgeHook( char &e );
	void alignHook( char &a );
};

#endif // CLOCK_H
