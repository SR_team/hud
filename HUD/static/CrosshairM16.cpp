#include "CrosshairM16.h"

CrosshairM16::CrosshairM16( SRDescent *parent, std::string_view id ) : Itemcrosshair( parent, id ) {
	_wgtName = "Turret";
	// bind callbacks
	lu.onBefore += std::tuple{ this, &CrosshairM16::drawHook };
	ru.onBefore += std::tuple{ this, &CrosshairM16::drawHook };
	lb.onBefore += std::tuple{ this, &CrosshairM16::drawHook };
	rb.onBefore += std::tuple{ this, &CrosshairM16::drawHook };
	// install hooks
	lu.install( 4 );
	ru.install( 4 );
	lb.install( 4 );
	rb.install( 4 );
}

void CrosshairM16::drawHook( SRHook::Info &info, RwRGBA *&clr ) {
	if ( isHidden() ) {
		clr->alpha = 0;
		return;
	}
	*clr = color();
	if ( _texture.texture ) info.cpu.ECX = (size_t)&_texture;
}
