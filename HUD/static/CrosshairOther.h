#ifndef CROSSHAIROTHER_H
#define CROSSHAIROTHER_H

#include "Itemcrosshair.h"
#include <llmo/SRHook.hpp>

class CrosshairOther : public Itemcrosshair {
	SRHook::Hook<RwRaster *>										 raster{ 0x58E8C8, 5 };
	SRHook::Hook<uint8_t, uint8_t, uint8_t, uint8_t, float, uint8_t> lu{ 0x58E95B, 5 };
	SRHook::Hook<uint8_t, uint8_t, uint8_t, uint8_t, float, uint8_t> ru{ 0x58E9CA, 5 };
	SRHook::Hook<uint8_t, uint8_t, uint8_t, uint8_t, float, uint8_t> lb{ 0x58EA39, 5 };
	SRHook::Hook<uint8_t, uint8_t, uint8_t, uint8_t, float, uint8_t> rb{ 0x58EAA8, 5 };

public:
	CrosshairOther( SRDescent *parent, int weapon, std::string_view wgtName, std::string_view id = "" );

protected:
	int	 _weaponId = 0;
	void rasterHook( RwRaster *&raster );
	void drawHook( uint8_t &red, uint8_t &green, uint8_t &blue, uint8_t &, float &, uint8_t &alpha );

	bool hook();
};

#endif // CROSSHAIROTHER_H
