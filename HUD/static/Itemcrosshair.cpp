#include "Itemcrosshair.h"
#include <nlohmann/json.hpp>

Itemcrosshair::Itemcrosshair( SRDescent *parent, std::string_view id ) : Itembase( parent, id ) {}

RwRGBA Itemcrosshair::color() const {
	RwRGBA color;
	color.red	= _color[0] * 255.0f;
	color.green = _color[1] * 255.0f;
	color.blue	= _color[2] * 255.0f;
	color.alpha = _color[3] * 255.0f;
	return color;
}

void Itemcrosshair::color( RwRGBA color ) {
	_color[0] = float( color.red ) / 255.0f;
	_color[1] = float( color.green ) / 255.0f;
	_color[2] = float( color.blue ) / 255.0f;
	_color[3] = float( color.alpha ) / 255.0f;
}

void Itemcrosshair::setImage( std::string_view name, RwTexture *texture ) {
	_texture.texture = texture;
	_textureName	 = name;
	if ( !texture ) _textureName = "default";
}

void Itemcrosshair::reset() {
	Itembase::reset();
	for ( int i = 0; i < 4; ++i ) _color[i] = 1;
	_textureName	 = "default";
	_texture.texture = nullptr;
}

void Itemcrosshair::draw() {
	auto s = isShowed();
	ImGui::Checkbox( name( "Show item" ).data(), &s ); // skip calling CSprite2d::Draw
	if ( isShowed() != s ) toggleShow( s );

	ImGuiColorEditFlags flags = ImGuiColorEditFlags_AlphaBar;
	flags |= ImGuiColorEditFlags_DisplayRGB;
	flags |= ImGuiColorEditFlags_DisplayHex;
	if ( ImGui::ColorButton( name( "Color" ).data(), *(ImVec4 *)&_color, flags, ImVec2( 60, 40 ) ) ) {
		ImGui::OpenPopup( name( "colorpicker" ).data() );
		_backup_color = *(ImVec4 *)&_color;
	}
	ImGui::SameLine( 0, 10 );
	ImGui::Text( "Color" );
	if ( _texture.texture ) {
		ImGui::SameLine( 0, 10 );
		ImGui::Image( _texture.texture->raster->rwD3D9RasterExt()->texture, { 60, 40 }, { 0, 0 }, { 1, 1 },
					  *(ImVec4 *)&_color );
	}

	popupColorpicker( *(ImVec4 *)&_color, _backup_color );
}

tiny::TinyJson Itemcrosshair::write() {
	auto writeColor = []( float *color ) {
		tiny::TinyJson json;
		json["red"].Set( color[0] );
		json["green"].Set( color[1] );
		json["blue"].Set( color[2] );
		json["alpha"].Set( color[3] );
		return json;
	};

	tiny::TinyJson json;
	if ( isHidden() ) json["show"].Set( (int)isShowed() );

	auto clr = color();
	if ( *(int *)&clr != -1 ) json["color"].Set( writeColor( _color ) );

	if ( !_textureName.empty() && _textureName != "default" ) json["texture"].Set( _textureName );

	return json;
}

nlohmann::json Itemcrosshair::write2() {
	auto writeColor = []( float *color ) {
		auto json	  = nlohmann::json::object();
		json["red"]	  = color[0];
		json["green"] = color[1];
		json["blue"]  = color[2];
		json["alpha"] = color[3];
		return json;
	};

	auto json = nlohmann::json::object();
	if ( isHidden() ) json["show"] = isShowed();

	auto clr = color();
	if ( *(int *)&clr != -1 ) json["color"] = writeColor( _color );

	if ( !_textureName.empty() && _textureName != "default" ) json["texture"] = _textureName;

	return json;
}

void Itemcrosshair::read( tiny::xobject &json ) {
	auto readColor = []( tiny::xobject &json, float *color ) {
		for ( int i = 0; i < json.Count(); i++ ) {
			json.Enter( i );
			color[0] = json.Get<float>( "red", 1.0f );
			color[1] = json.Get<float>( "green", 1.0f );
			color[2] = json.Get<float>( "blue", 1.0f );
			color[3] = json.Get<float>( "alpha", 1.0f );
		}
	};

	for ( int i = 0; i < json.Count(); i++ ) {
		json.Enter( i );
		toggleShow( json.Get<int>( "show", 1 ) );
		tiny::xobject jcolor = json.Get<tiny::xobject>( "color" );
		readColor( jcolor, _color );
		_textureName = json.Get<std::string>( "texture", "default" );
	}
}

void Itemcrosshair::read2( nlohmann::json &json ) {
	auto readColor = []( nlohmann::json &json, float *color ) {
		color[0] = json.value<float>( "red", 1.0f );
		color[1] = json.value<float>( "green", 1.0f );
		color[2] = json.value<float>( "blue", 1.0f );
		color[3] = json.value<float>( "alpha", 1.0f );
	};

	toggleShow( json.value<bool>( "show", true ) );
	try {
		auto jcolor = json["color"];
		readColor( jcolor, _color );
	} catch ( ... ) {
	}
	_textureName = json.value<std::string>( "texture", "default" );
}

void Itemcrosshair::initLuaAndIDE( sol::state &lua, IDE *ide, std::string prefix ) {
	if ( prefix.empty() ) {
		prefix = luaPrefix();
	}

	addFunction(
		lua, ide, prefix + "_show", [this]( bool show ) { toggleShow( show ); },
		"Show/hide " + prefix + "\n\nArgs:\n * bool - show" );
	addFunction(
		lua, ide, prefix + "_isShowed", [this] { return isShowed(); },
		"Get current show state of " + prefix + "\n\nReturn:\n * bool - is showed" );
	addFunction(
		lua, ide, prefix + "_color",
		[this]() {
			auto clr = color();
			return *(int *)&clr;
		},
		"Get color\n\nReturn:\n * int - color" );
	addFunction(
		lua, ide, prefix + "_setColor", [this]( int clr ) { color( *(RwRGBA *)&clr ); },
		"Set color\n\nArgs:\n * int - color" );
}

std::string Itemcrosshair::txName() const {
	return _textureName;
}

std::string Itemcrosshair::luaPrefix() const {
	std::string prefix = "crosshair_" + _wgtName;
	std::transform( prefix.begin(), prefix.end(), prefix.begin(), ::tolower );
	for ( auto &&ch : prefix )
		if ( ch == ' ' || ch == '\t' || ch == '.' || ch == ':' ) ch = '_';
	return prefix;
}
