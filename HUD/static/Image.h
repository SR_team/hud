#ifndef IMAGE_H
#define IMAGE_H

#include "Itemhud.h"

class Image : public Itemhud {
public:
	Image( SRDescent *parent, std::string_view name, std::string_view id = "" );

	float	angle = 0.0f;
	RwVRect rect{ 0, 0, 1, 1 };

	virtual void		   draw() override;
	virtual bool		   isDrag() override;
	virtual bool		   processDrag() override;
	virtual tiny::TinyJson write() override;
	virtual nlohmann::json write2() override;
	virtual void		   read( tiny::xobject &json ) override;
	virtual void		   read2( nlohmann::json &json ) override;

	virtual void render() override;

	void setImage( std::string_view name, RwTexture *texture = nullptr );

	std::string_view txName();

	bool operator==( const Image &r ) {
		if ( !Itemhud::operator==( r ) ) return false;
		if ( _textureName != r._textureName ) return false;
		if ( angle != r.angle ) return false;
		if ( rect != r.rect ) return false;
		return true;
	}

protected:
	std::string _textureName;
	CTexture	_texture;
	RwV2D		_dragOffset;
};

#endif // IMAGE_H
