#ifndef HEALTH_H
#define HEALTH_H

#include "Itemchart.h"
#include <llmo/SRHook.hpp>

class Health : public Itemchart {
	SRHook::Hook<>																		drawHealth{ 0x58927C, 6 };
	SRHook::Hook<float, float, WORD, WORD, float, uint32_t, bool, bool, RwRGBA, RwRGBA> posHealth{ 0x589395, 5 };

public:
	Health( SRDescent *parent, std::string_view id = "" );

	virtual void initLuaAndIDE( sol::state &lua, class IDE *ide, std::string prefix = "" ) override;

protected:
	void showHook( SRHook::Info &info );
};

#endif // HEALTH_H
