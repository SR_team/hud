#ifndef MONEY_H
#define MONEY_H

#include "Itembase.h"
#include <llmo/SRHook.hpp>

class Money : public Itembase {
	SRHook::Hook<>					   drawMoney{ 0x58F47D, 6 };
	SRHook::Hook<float, float, char *> posMoney{ 0x58F607, 5 };
	SRHook::Hook<float, float>		   sizeMoney{ 0x58F56B, 5 };
	SRHook::Hook<RwRGBA>			   clMoneyPos{ 0x58F4A5, 5 };
	SRHook::Hook<RwRGBA>			   clMoneyNeg{ 0x58F4E7, 5 };
	SRHook::Hook<RwRGBA>			   clMoneyShd{ 0x58F5A9, 5 };
	SRHook::Hook<int>				   style{ 0x58F580, 5 };
	SRHook::Hook<bool>				   propMoney{ 0x58F52F, 5 };
	SRHook::Hook<char>				   edgeMoney{ 0x58F58E, 5 };
	SRHook::Hook<char>				   alignMoney{ 0x58F572, 5 };

public:
	Money( SRDescent *parent, std::string_view id = "" );

	bool custom_positive_color = false;
	bool custom_negative_color = false;
	bool custom_shadow_color   = false;
	bool proportional		   = false;
	enum class Align { center, left, right };
	Align align		 = Align::right;
	int	  edge		 = 2;
	int	  font_style = 3;

	RwRGBA colorPositive() const;
	void   colorPositive( RwRGBA color );
	RwRGBA colorNegative() const;
	void   colorNegative( RwRGBA color );
	RwRGBA colorShadow() const;
	void   colorShadow( RwRGBA color );

	virtual void		   reset() override;
	virtual void		   draw() override;
	virtual bool		   isDrag() override;
	virtual bool		   processDrag() override;
	virtual tiny::TinyJson write() override;
	virtual nlohmann::json write2() override;
	virtual void		   read( tiny::xobject &json ) override;
	virtual void		   read2( nlohmann::json &json ) override;
	virtual void		   initLuaAndIDE( sol::state &lua, class IDE *ide, std::string prefix = "" ) override;

protected:
	float  _colorPos[4]{ .212, .412, .173, 1 };
	float  _colorNeg[4]{ .706, .098, .114, 1 };
	float  _colorShd[4]{ 0, 0, 0, 1 };
	ImVec4 _backup_color;
	int	   pickId = 0;
	RwV2D  _realSize;
	RwV2D  _dragOffset;

	void showHook( SRHook::Info &info );
	void posHook( float &x, float &y, char *&text );
	void sizeHook( float &w, float &h );
	void clPosHook( RwRGBA &color );
	void clNegHook( RwRGBA &color );
	void clShdHook( RwRGBA &color );
	void styleHook( int &fontStyle );
	void propHook( bool &prop );
	void edgeHook( char &e );
	void alignHook( char &a );
};

#endif // MONEY_H
