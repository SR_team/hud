#ifndef CHART_H
#define CHART_H

#include "Itemhud.h"

class Chart : public Itemhud {
public:
	Chart( SRDescent *parent, std::string_view name, std::string_view id = "" );

	bool  percentage = false;
	bool  end_line	 = false;
	bool  border	 = true;
	float value		 = 50.0f;

	virtual void		   draw() override;
	virtual bool		   isDrag() override;
	virtual bool		   processDrag() override;
	virtual tiny::TinyJson write() override;
	virtual nlohmann::json write2() override;
	virtual void		   read( tiny::xobject &json ) override;
	virtual void		   read2( nlohmann::json &json ) override;

	virtual void render() override;

	bool operator==( const Chart &r ) {
		if ( !Itemhud::operator==( r ) ) return false;
		if ( percentage != r.percentage ) return false;
		if ( end_line != r.end_line ) return false;
		if ( border != r.border ) return false;
		if ( value != r.value ) return false;
		return true;
	}

protected:
	RwV2D _dragOffset;
};

#endif // CHART_H
