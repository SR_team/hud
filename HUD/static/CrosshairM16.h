#ifndef CROSSHAIRM16_H
#define CROSSHAIRM16_H

#include "Itemcrosshair.h"
#include <llmo/SRHook.hpp>

class CrosshairM16 : public Itemcrosshair {
	SRHook::Hook<RwRGBA *> lu{ 0x58E5B1, 5 };
	SRHook::Hook<RwRGBA *> ru{ 0x58E606, 5 };
	SRHook::Hook<RwRGBA *> lb{ 0x58E65B, 5 };
	SRHook::Hook<RwRGBA *> rb{ 0x58E6A8, 5 };

public:
	CrosshairM16( SRDescent *parent, std::string_view id = "" );

protected:
	void drawHook( SRHook::Info &info, RwRGBA *&clr );
};

#endif // CROSSHAIRM16_H
