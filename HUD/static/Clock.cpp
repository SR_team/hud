#include "Clock.h"
#include <gtasa/CGame/methods.h>
#include <nlohmann/json.hpp>

Clock::Clock( SRDescent *parent, std::string_view id ) : Itembase( parent, id ) {
	_size	 = { .25, .25 };
	_wgtName = "Clock";
	// bind callbacks
	drawClock.onBefore += std::tuple{ this, &Clock::showHook };
	posClock.onBefore += std::tuple{ this, &Clock::posHook };
	sizeClock.onBefore += std::tuple{ this, &Clock::sizeHook };
	clClock.onBefore += std::tuple{ this, &Clock::clHook };
	clClockShd.onBefore += std::tuple{ this, &Clock::clShdHook };
	style.onBefore += std::tuple{ this, &Clock::styleHook };
	propClock.onBefore += std::tuple{ this, &Clock::propHook };
	edgeClock.onBefore += std::tuple{ this, &Clock::edgeHook };
	alignClock.onBefore += std::tuple{ this, &Clock::alignHook };
	// install hooks
	drawClock.install();
	posClock.install();
	sizeClock.install();
	clClock.install();
	clClockShd.install();
	style.install();
	propClock.install();
	edgeClock.install();
	alignClock.install();
}

RwRGBA Clock::color() const {
	RwRGBA color;
	color.red	= _colorPos[0] * 255.0f;
	color.green = _colorPos[1] * 255.0f;
	color.blue	= _colorPos[2] * 255.0f;
	color.alpha = _colorPos[3] * 255.0f;
	return color;
}

void Clock::color( RwRGBA color ) {
	_colorPos[0] = float( color.red ) / 255.0f;
	_colorPos[1] = float( color.green ) / 255.0f;
	_colorPos[2] = float( color.blue ) / 255.0f;
	_colorPos[3] = float( color.alpha ) / 255.0f;
}


RwRGBA Clock::colorShadow() const {
	RwRGBA color;
	color.red	= _colorPos[0] * 255.0f;
	color.green = _colorPos[1] * 255.0f;
	color.blue	= _colorPos[2] * 255.0f;
	color.alpha = _colorPos[3] * 255.0f;
	return color;
}

void Clock::colorShadow( RwRGBA color ) {
	_colorPos[0] = float( color.red ) / 255.0f;
	_colorPos[1] = float( color.green ) / 255.0f;
	_colorPos[2] = float( color.blue ) / 255.0f;
	_colorPos[3] = float( color.alpha ) / 255.0f;
}

void Clock::reset() {
	Itembase::reset();
	custom_color		= false;
	custom_shadow_color = false;
	proportional		= false;
	edge				= 2;
	font_style			= 3;
	align				= Align::right;
}

void Clock::draw() {
	Itembase::draw();

	ImGuiColorEditFlags flags = ImGuiColorEditFlags_AlphaBar;
	flags |= ImGuiColorEditFlags_DisplayRGB;
	flags |= ImGuiColorEditFlags_DisplayHex;

	static char timeBuf[256]{ 0 };
	snprintf( timeBuf, 255, (const char *)0x859A6C, *(uint8_t *)0xB70153, *(uint8_t *)0xB70152 );
	auto size = ImGui::CalcTextSize( timeBuf, NULL, true );
	size.y *= 3.0f;
	size.x = size.y * 1.333333f;

	if ( ImGui::ColorButton( name( "Text color" ).data(), *(ImVec4 *)&_colorPos, flags, size ) && custom_color ) {
		ImGui::OpenPopup( name( "colorpicker" ).data() );
		_backup_color = *(ImVec4 *)&_colorPos;
		pickId		  = 0;
	}
	ImGui::SameLine();
	ImGui::BeginGroup();
	ImGui::Checkbox( name( "Custom text color" ).data(), &custom_color );
	ImGui::TextColored( *(ImVec4 *)&_colorPos, "%s", timeBuf );
	ImGui::EndGroup();

	if ( ImGui::ColorButton( name( "Shadow color" ).data(), *(ImVec4 *)&_colorShd, flags, size ) &&
		 custom_shadow_color ) {
		ImGui::OpenPopup( name( "colorpicker" ).data() );
		_backup_color = *(ImVec4 *)&_colorShd;
		pickId		  = 1;
	}
	ImGui::SameLine();
	ImGui::BeginGroup();
	ImGui::Checkbox( name( "Custom shadow color" ).data(), &custom_shadow_color );
	ImGui::TextColored( *(ImVec4 *)&_colorShd, "%s", timeBuf );
	ImGui::EndGroup();

	switch ( pickId ) {
		case 0:
			popupColorpicker( *(ImVec4 *)&_colorPos, _backup_color );
			break;
		case 1:
			popupColorpicker( *(ImVec4 *)&_colorShd, _backup_color );
			break;
	}

	ImGui::BeginGroup();
	ImGui::Text( "Font style:" );
	ImGui::SameLine( 0, 10 );
	if ( font_style < 0 )
		font_style = 0;
	else if ( font_style > 3 )
		font_style = 3;
	ImGui::RadioButton( name( "Gothic" ).data(), &font_style, 0 );
	ImGui::SameLine( 0, 10 );
	ImGui::RadioButton( name( "Subtitles" ).data(), &font_style, 1 );
	ImGui::SameLine( 0, 10 );
	ImGui::RadioButton( name( "Menu" ).data(), &font_style, 2 );
	ImGui::SameLine( 0, 10 );
	ImGui::RadioButton( name( "Pricedown" ).data(), &font_style, 3 );
	ImGui::EndGroup();
	if ( ImGui::IsItemHovered() ) ImGui::SetTooltip( "Default: Pricedown" );

	ImGui::Checkbox( name( "Proportional" ).data(), &proportional );
	ImGui::BeginGroup();
	ImGui::Text( "Edge:" );
	ImGui::SameLine( 0, 10 );
	ImGui::RadioButton( name( "Disable" ).data(), &edge, 0 );
	ImGui::SameLine( 0, 10 );
	ImGui::RadioButton( name( "Thin" ).data(), &edge, 1 );
	ImGui::SameLine( 0, 10 );
	ImGui::RadioButton( name( "Default" ).data(), &edge, 2 );
	ImGui::EndGroup();

	ImGui::BeginGroup();
	ImGui::Text( "Align:" );
	ImGui::SameLine( 0, 10 );
	ImGui::RadioButton( name( "Left" ).data(), (int *)&align, (int)Align::left );
	ImGui::SameLine( 0, 10 );
	ImGui::RadioButton( name( "Center" ).data(), (int *)&align, (int)Align::center );
	ImGui::SameLine( 0, 10 );
	ImGui::RadioButton( name( "Right" ).data(), (int *)&align, (int)Align::right );
	ImGui::EndGroup();
	if ( ImGui::IsItemHovered() ) ImGui::SetTooltip( "Default: Right" );
}

bool Clock::isDrag() {
	float start, end;
	switch ( align ) {
		case Align::center:
			start = _pos.fX - _realSize.fX * 0.5f;
			end	  = _pos.fX + _realSize.fX * 0.5f;
			break;
		case Align::left:
			start = _pos.fX;
			end	  = _pos.fX + _realSize.fX;
			break;
		case Align::right:
			start = _pos.fX - _realSize.fX;
			end	  = _pos.fX;
			break;
	}
	if ( isShowed() && mousePos().fX >= start && mousePos().fY >= _pos.fY && mousePos().fX <= end &&
		 mousePos().fY <= _pos.fY + _realSize.fY ) {
		_drag		= true;
		_dragOffset = mousePos() - pos();
		return true;
	}
	return false;
}

bool Clock::processDrag() {
	if ( !_drag ) return false;
	pos( mousePos() - _dragOffset );
	return true;
}

nlohmann::json Clock::write2() {
	auto writeColor = []( float *color ) {
		auto json	  = nlohmann::json::object();
		json["red"]	  = color[0];
		json["green"] = color[1];
		json["blue"]  = color[2];
		json["alpha"] = color[3];
		return json;
	};

	auto json = Itembase::write2();
	if ( proportional ) json["proportional"] = proportional;
	if ( edge != 2 ) json["edge"] = edge;
	if ( font_style != 3 ) json["font_style"] = font_style;
	if ( align != Align::right ) json["align"] = align;

	if ( custom_color ) {
		json["custom_color"]   = custom_color;
		json["positive_color"] = writeColor( _colorPos );
	}
	if ( custom_shadow_color ) {
		json["custom_shadow_color"] = custom_shadow_color;
		json["shadow_color"]		= writeColor( _colorShd );
	}

	return json;
}

void Clock::read2( nlohmann::json &json ) {
	auto readColor = []( nlohmann::json &json, float *color ) {
		color[0] = json.value<float>( "red", 1.0f );
		color[1] = json.value<float>( "green", 1.0f );
		color[2] = json.value<float>( "blue", 1.0f );
		color[3] = json.value<float>( "alpha", 1.0f );
	};
	Itembase::read2( json );
	custom_color = json.value<bool>( "custom_color", false );
	if ( custom_color ) {
		auto jcolor = json["positive_color"];
		readColor( jcolor, _colorPos );
	}
	custom_shadow_color = json.value<bool>( "custom_shadow_color", false );
	if ( custom_shadow_color ) {
		auto jcolor = json["shadow_color"];
		readColor( jcolor, _colorShd );
	}
	proportional = json.value<bool>( "proportional", false );
	edge		 = json.value<int>( "edge", 2 );
	font_style	 = json.value<int>( "font_style", 3 );
	align		 = (Align)json.value<int>( "align", 2 );
}

void Clock::initLuaAndIDE( sol::state &lua, IDE *ide, std::string prefix ) {
	if ( prefix.empty() ) prefix = "clock";
	Itembase::initLuaAndIDE( lua, ide, prefix );
	addFunction(
		lua, ide, "clock_realSize", [this]() { return std::make_tuple( _realSize.fX, _realSize.fY ); },
		"Get real text size\n\nReturn:\n * float - real width\n * float - real height" );

	addFunction(
		lua, ide, "clock_isCustomColor", [this]() { return custom_color; },
		"Check is custom text color enabled\n\nReturn:\n * bool - is enabled" );
	addFunction(
		lua, ide, "clock_isCustomShadowColor", [this]() { return custom_shadow_color; },
		"Check is custom shadow color enabled\n\nReturn:\n * bool - is enabled" );
	addFunction(
		lua, ide, "clock_isProportional", [this]() { return proportional; },
		"Check is text proportional\n\nReturn:\n * bool - proportional" );
	addFunction(
		lua, ide, "clock_edge", [this]() { return edge; }, "Get text edged\n\nReturn:\n * int - edge" );
	addFunction(
		lua, ide, "clock_fontStyle", [this]() { return font_style; },
		"Get clock font style id\n\nReturn:\n * int - style\n\nStyles:\n 0. Gothic\n 1. Subtitles\n 2. Menu\n 3. "
		"Pricedown (default)" );
	addFunction(
		lua, ide, "clock_color",
		[this]() {
			auto clr = color();
			return *(int *)&clr;
		},
		"Get text color\n\nReturn:\n * int - color" );
	addFunction(
		lua, ide, "clock_colorShadow",
		[this]() {
			auto clr = colorShadow();
			return *(int *)&clr;
		},
		"Get shadow color\n\nReturn:\n * int - color" );
	addFunction(
		lua, ide, "clock_toggleCustomColor", [this]( bool state ) { custom_color = state; },
		"Enable/disable custom text color\n\nArgs:\n * bool - is enabled" );
	addFunction(
		lua, ide, "clock_toggleCustomShadowColor", [this]( bool state ) { custom_shadow_color = state; },
		"Enable/disable custom shadow color\n\nArgs:\n * bool - is enabled" );
	addFunction(
		lua, ide, "clock_toggleProportional", [this]( bool state ) { proportional = state; },
		"Enable/desable text proportional\n\nArgs:\n * bool - proportional" );
	addFunction(
		lua, ide, "clock_setEdge", [this]( int e ) { return edge = e; }, "Set text edged\n\nArgs:\n * int - edge" );
	addFunction(
		lua, ide, "clock_setFontStyle", [this]( int style ) { font_style = style; },
		"Set clock font style id\n\nArgs:\n * int - style\n\nStyles:\n 0. Gothic\n 1. Subtitles\n 2. Menu\n 3. "
		"Pricedown (default)" );
	addFunction(
		lua, ide, "clock_setColor", [this]( int clr ) { color( *(RwRGBA *)&clr ); },
		"Set text color\n\nArgs:\n * int - color" );
	addFunction(
		lua, ide, "clock_setColorShadow", [this]( int clr ) { colorShadow( *(RwRGBA *)&clr ); },
		"Set shadow color\n\nArgs:\n * int - color" );

	addFunction(
		lua, ide, "clock_time",
		[]() { return std::make_tuple( *(uint8_t *)0xB70153, *(uint8_t *)0xB70152, *(uint16_t *)0xB70150 ); },
		"Get game time\n\nReturn:\n * int - hour\n * int - min\n * int - seconds" );
	addFunction(
		lua, ide, "clock_date", []() { return std::make_tuple( *(uint8_t *)0xB70155, *(uint8_t *)0xB70154 ); },
		"Get game date\n\nReturn:\n * int - months\n * int - days" );

	addFunction(
		lua, ide, "clock_align", [this]() { return (int)align; },
		"Get clock align\n\nReturn:\n * int - align\n\nAlign:\n 0. Center\n 1. Left\n 2. "
		"Right" );
	addFunction(
		lua, ide, "clock_setAlign", [this]( int align ) { this->align = Align( align ); },
		"Set clock align\n\nArgs:\n * int - align\n\nAlign:\n 0. Center\n 1. Left\n 2. "
		"Right (default)" );
}

void Clock::showHook( SRHook::CPU &cpu ) {
	if ( isHidden() ) cpu.ZF = 0;
}

void Clock::posHook( float &x, float &y, char *&text ) {
	auto rect	 = GetTextRect( x, y, text );
	_origPos.fX	 = ( 640.0f / SCREEN_X ) * rect.x2;
	_origPos.fY	 = ( 448.0f / SCREEN_Y ) * rect.y1;
	_realSize.fX = ( 640.0f / SCREEN_X ) * ( ( rect.x2 - rect.x1 ) );
	_realSize.fY = ( 448.0f / SCREEN_Y ) * ( ( rect.y2 - rect.y1 ) );
	x			 = ( SCREEN_X / 640.0f ) * pos().fX;
	y			 = ( SCREEN_Y / 448.0f ) * pos().fY;
}

void Clock::sizeHook( float &w, float &h ) {
	_origSize = WindowScreenToGameScreen( w * 100.0f, h * 100.0f );
	w		  = ( SCREEN_X / 640.0f ) * size().fX * 0.01f;
	h		  = ( SCREEN_Y / 448.0f ) * size().fY * 0.01f;
}

void Clock::clHook( RwRGBA &color ) {
	if ( !custom_color ) return;
	color.red	= _colorPos[0] * 255.0f;
	color.green = _colorPos[1] * 255.0f;
	color.blue	= _colorPos[2] * 255.0f;
	color.alpha = _colorPos[3] * 255.0f;
}

void Clock::clShdHook( RwRGBA &color ) {
	if ( !custom_shadow_color ) return;
	color.red	= _colorShd[0] * 255.0f;
	color.green = _colorShd[1] * 255.0f;
	color.blue	= _colorShd[2] * 255.0f;
	color.alpha = _colorShd[3] * 255.0f;
}

void Clock::styleHook( int &fontStyle ) {
	fontStyle = font_style;
}

void Clock::propHook( bool &prop ) {
	prop = proportional;
}

void Clock::edgeHook( char &e ) {
	if ( edge < 0 )
		edge = 0;
	else if ( edge > 2 )
		edge = 2;
	e = edge;
}

void Clock::alignHook( char &a ) {
	a = (char)align;
}
