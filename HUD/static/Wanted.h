#ifndef WANTED_H
#define WANTED_H

#include "Itembase.h"
#include <llmo/SRHook.hpp>

class Wanted : public Itembase {
	SRHook::Hook<>				drawWanted{ 0x58DC8D, 6 };
	SRHook::Hook<>				posWantedX{ 0x58DD15, 6 };
	SRHook::Hook<float, char *> posWantedY{ 0x58DFD3, 5 };
	SRHook::Hook<>				posWantedYGold{ 0x58DE38, 8 };
	SRHook::Hook<float, float>	sizeWanted{ 0x58DCC7, 5 };
	SRHook::Hook<float, float>	sizeWanted2{ 0x58DD8D, 5 };
	SRHook::Hook<float, float>	sizeWanted3{ 0x58DF86, 5 };
	SRHook::Hook<>				sizeWanted4{ 0x58DFDB, 6 };
	SRHook::Hook<>				sizeWanted5{ 0x58DFE5, 20 };
	SRHook::Hook<RwRGBA>		colorWanted{ 0x58DF4A, 5 };
	SRHook::Hook<RwRGBA>		colorGoldWanted{ 0x58DDDC, 5 };
	SRHook::Hook<RwRGBA>		colorShadowWanted{ 0x58DD5D, 5 };

	bool gold = false;

public:
	Wanted( SRDescent *parent, std::string_view id = "" );

	bool  custom_empty_color  = false;
	bool  custom_gold_color	  = false;
	bool  custom_shadow_color = false;
	float space				  = 0.0f;

	RwRGBA colorEmpty();
	void   colorEmpty( RwRGBA color );
	RwRGBA colorGold();
	void   colorGold( RwRGBA color );
	RwRGBA colorShadow();
	void   colorShadow( RwRGBA color );

	virtual void		   reset() override;
	virtual void		   draw() override;
	virtual bool		   isDrag() override;
	virtual bool		   processDrag() override;
	virtual tiny::TinyJson write() override;
	virtual nlohmann::json write2() override;
	virtual void		   read( tiny::xobject &json ) override;
	virtual void		   read2( nlohmann::json &json ) override;
	virtual void		   initLuaAndIDE( sol::state &lua, class IDE *ide, std::string prefix = "" ) override;

protected:
	float  _color[4]{ 0, 0, 0, .7 };
	float  _colorGld[4]{ .565, .384, .063, 1 };
	float  _colorShd[4]{ 0, 0, 0, 1 };
	ImVec4 _backup_color;
	int	   pickId = 0;
	RwV2D  _realSize;
	float  _realHeight;
	RwV2D  _dragOffset;

	void showHook( SRHook::Info &info );
	void posXHook( SRHook::CPU &cpu );
	void posYHook( float &y, char *&text );
	void posYGoldHook();
	void sizeHook( float &w, float &h );
	void sizeMulHook( float &w, float &h );
	void sizeSkipHook( SRHook::Info &info );
	void posOffsetHook( SRHook::Info &info );
	void colorHook( RwRGBA &color );
	void colorGoldHook( RwRGBA &color );
	void colorShdHook( RwRGBA &color );
};

#endif // WANTED_H
