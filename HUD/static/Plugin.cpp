#include "Plugin.h"
#include <stdexcept>

Plugin::Plugin( HMODULE lib, SRDescent *parent ) : SRDescent( parent ) {
	_handle = lib;
	if ( !_handle || _handle == INVALID_HANDLE_VALUE ) throw std::runtime_error( "Plugin error: invalid handle" );
	_init_lua = (init_lua_t)GetProcAddress( lib, "init_lua" );
	_init_ide = (init_ide_t)GetProcAddress( lib, "init_ide" );
	_reset	  = (reset_t)GetProcAddress( lib, "reset" );
	_begin	  = (begin_t)GetProcAddress( lib, "begin" );
	_end	  = (end_t)GetProcAddress( lib, "end" );
}

Plugin::~Plugin() {
	FreeLibrary( _handle );
}

bool Plugin::init_lua( sol::state *lua ) {
	if ( !_init_lua ) return false;
	return _init_lua( lua );
}

void Plugin::init_ide() {
	if ( !_init_ide ) return;
	_init_ide();
}

void Plugin::reset() {
	if ( !_reset ) return;
	_reset();
}

void Plugin::begin() {
	if ( !_begin ) return;
	_begin();
}

void Plugin::end() {
	if ( !_end ) return;
	_end();
}
