#include "DefaultHud.h"
#include "Ammo.h"
#include "Armour.h"
#include "Breath.h"
#include "Clock.h"
#include "CrosshairM16.h"
#include "CrosshairOther.h"
#include "CrosshairSimple.h"
#include "Health.h"
#include "IDE.h"
#include "Money.h"
#include "Radar.h"
#include "Wanted.h"
#include "Weapon.h"
#include "loader/loader.h"
#include <gtasa/CGame/CPed.h>
#include <gtasa/CGame/CVehicle.h>
#include <nlohmann/json.hpp>
#include <sol/sol.hpp>

DefaultHud::DefaultHud( SRDescent *parent ) : SRDescent( parent ) {
	auto samp = GetModuleHandleA( "samp.dll" );
	wanted	  = new Wanted( this );
	radar	  = new Radar( this );
	ammo	  = new Ammo( this );
	money	  = new Money( this );
	weapon	  = new Weapon( this );
	breath	  = new Breath( this );
	armour	  = new Armour( this );
	health	  = new Health( this );
	if ( !samp || samp == INVALID_HANDLE_VALUE ) clock = new Clock( this );
	cfSimple = new CrosshairSimple( this );
	cfSniper = new CrosshairOther( this, 34, "Sniper" );
	cfRPG	 = new CrosshairOther( this, 35, "RPG" );
	cfHSRPG	 = new CrosshairOther( this, 36, "HS RPG" );
	cfCamera = new CrosshairOther( this, 43, "Camera" );
	cfHunter = new CrosshairOther( this, 425, "Hunter" );
	cfHydra	 = new CrosshairOther( this, 520, "Hydra" );
	cfM16	 = new CrosshairM16( this );

	_items.push_back( wanted );
	_items.push_back( radar );
	_items.push_back( ammo );
	_items.push_back( money );
	_items.push_back( weapon );
	_items.push_back( breath );
	_items.push_back( armour );
	_items.push_back( health );
	if ( !samp || samp == INVALID_HANDLE_VALUE ) _items.push_back( clock );
	_crossHairs.push_back( cfSimple );
	_crossHairs.push_back( cfSniper );
	_crossHairs.push_back( cfRPG );
	_crossHairs.push_back( cfHSRPG );
	_crossHairs.push_back( cfCamera );
	_crossHairs.push_back( cfHunter );
	_crossHairs.push_back( cfHydra );
	_crossHairs.push_back( cfM16 );

	fontTexture.onBefore += std::tuple{ this, &DefaultHud::fontTextureHook };
	fontTexture2.onBefore += std::tuple{ this, &DefaultHud::fontTextureHook };
	charWidthProp.onBefore += std::tuple{ this, &DefaultHud::charWidthPropHook };
	charWidthUnprop.onBefore += std::tuple{ this, &DefaultHud::charWidthUnpropHook };
	getCharSizeProp.onBefore += std::tuple{ this, &DefaultHud::charWidthPropHook };
	getCharSizeProp2.onBefore += std::tuple{ this, &DefaultHud::charWidthPropHook };
	getCharSizeUnprop.onBefore += std::tuple{ this, &DefaultHud::getCharSizeUnpropHook };
	getCharSizeUnprop2.onBefore += std::tuple{ this, &DefaultHud::getCharSizeUnpropHook };
	renderFontUnprop.onBefore += std::tuple{ this, &DefaultHud::renderFontUnpropHook };
	renderFontProp.onBefore += std::tuple{ this, &DefaultHud::renderFontPropHook };
	fontTexture.install();
	fontTexture2.install();
	charWidthProp.install();
	charWidthUnprop.install();
	getCharSizeProp.install();
	getCharSizeProp2.install();
	getCharSizeUnprop.install();
	getCharSizeUnprop2.install();
	renderFontUnprop.install();
	renderFontProp.install();

	memcpy( fontData, (tFontData *)0xC718B0, sizeof( fontData ) );
	memsafe::nop( 0x718A22, 6 );
	memsafe::nop( 0x718A4C, 6 );
}

void DefaultHud::setRadarTexture( RwTexture *tx ) {
	radar->setBorder( tx );
}

void DefaultHud::setRadarIcon( std::string icon, std::string txName, RwTexture *tx ) {
	radar->setIconTexture( icon, txName, tx );
}

void DefaultHud::setAirPlaneTexture( RwTexture *tx ) {
	radar->setAirPlane( tx );
}

void DefaultHud::setWeaponTexture( int id, RwTexture *tx ) {
	weapon->setIcon( id, tx );
}

void DefaultHud::setFontTexture( DefaultHud::FontId id, RwTexture *tx ) {
	if ( int( id ) & int( FontId::font2 ) ) _font[0].texture = tx;
	if ( int( id ) & int( FontId::font1 ) ) _font[1].texture = tx;
}

std::string DefaultHud::radarIcon( std::string icon ) {
	return radar->iconTxName( icon );
}

std::deque<std::string> DefaultHud::radarIcons() {
	auto					icons = *(RadarIcon **)0x5827F0;
	auto					count = ( *(size_t *)0x582806 - *(size_t *)0x5827F0 ) / sizeof( RadarIcon );
	std::deque<std::string> result;
	for ( size_t i = 0; i < count; ++i ) {
		if ( !icons[i].name || !*icons[i].name ) continue;
		result.push_back( icons[i].name );
	}
	return result;
}

void DefaultHud::initLuaAndIDE( sol::state &lua, IDE *ide ) {
	for ( auto &&item : _items ) item->initLuaAndIDE( lua, ide );
	for ( auto &&item : _crossHairs ) item->initLuaAndIDE( lua, ide );

	auto addFunction = [&]( std::string_view name, auto func, std::string_view desc ) {
		lua.set_function( name, func );
		if ( ide ) ide->addFunction( name, desc );
	};

	addFunction(
		"player_power",
		[] {
			float power = *(float *)0x00B7CDB4 / 31.5;
			if ( power > 99.0f ) power = 100.0f;
			return power;
		},
		"Get player power\n\nReturn:\n * float - power" );
	addFunction(
		"player_rotationCurrent",
		[] {
			if ( !LOCAL_PLAYER ) return 0.0f;
			if ( !LOCAL_PLAYER->isValid() ) return 0.0f;
			return LOCAL_PLAYER->fCurrentRotation;
		},
		"Get player current rotation\n\nReturn:\n * float - angle" );
	addFunction(
		"player_rotationTarget",
		[] {
			if ( !LOCAL_PLAYER ) return 0.0f;
			if ( !LOCAL_PLAYER->isValid() ) return 0.0f;
			return LOCAL_PLAYER->fTargetRotation;
		},
		"Get player target rotation\n\nReturn:\n * float - angle" );
	addFunction(
		"player_isDriving",
		[] {
			if ( !LOCAL_PLAYER ) return false;
			if ( !LOCAL_PLAYER->isValid() ) return false;
			return LOCAL_PLAYER->isDriving();
		},
		"Check is player in vehicle\n\nReturn:\n * bool - is driving" );
	addFunction(
		"player_isDriver",
		[] {
			if ( !LOCAL_PLAYER ) return false;
			if ( !LOCAL_PLAYER->isValid() ) return false;
			return LOCAL_PLAYER->isDriver();
		},
		"Check is player in vehicle and driver\n\nReturn:\n * bool - is driver" );
	addFunction(
		"player_isDead",
		[] {
			if ( !LOCAL_PLAYER ) return false;
			if ( !LOCAL_PLAYER->isValid() ) return false;
			return LOCAL_PLAYER->isActorDead();
		},
		"Check is player dead\n\nReturn:\n * bool - is dead" );
	addFunction(
		"player_isTargetPed",
		[] {
			if ( !LOCAL_PLAYER ) return false;
			if ( !LOCAL_PLAYER->isValid() ) return false;
			return LOCAL_PLAYER->TargetedPed != nullptr;
		},
		"Check is player target to another player\n\nReturn:\n * bool - is target" );
	addFunction(
		"player_isTargetObject",
		[] {
			if ( !LOCAL_PLAYER ) return false;
			if ( !LOCAL_PLAYER->isValid() ) return false;
			return LOCAL_PLAYER->targetObject != nullptr;
		},
		"Check is player target to object\n\nReturn:\n * bool - is target" );
	addFunction(
		"player_isTarget",
		[] {
			if ( !LOCAL_PLAYER ) return false;
			if ( !LOCAL_PLAYER->isValid() ) return false;
			if ( LOCAL_PLAYER->isDriving() ) return false;
			return g_class.events->gameKeyState( SRKeys::GameKeysOnFoot::AimWeapon ) != 0;
		},
		"Check is player target\n\nReturn:\n * bool - is target" );
	addFunction(
		"player_speed",
		[] {
			if ( !LOCAL_PLAYER ) return 0.0f;
			if ( !LOCAL_PLAYER->isValid() ) return 0.0f;
			auto phys = (CPhysical *)LOCAL_PLAYER;
			if ( LOCAL_PLAYER->isDriving() )
				;
			phys = LOCAL_PLAYER->vehicle;
			return phys->getSpeed();
		},
		"Get player move speed\n\nReturn:\n * float - speed" );
	addFunction(
		"player_heading",
		[] {
			if ( !LOCAL_PLAYER ) return 0.0f;
			if ( !LOCAL_PLAYER->isValid() ) return 0.0f;
			auto phys = (CPhysical *)LOCAL_PLAYER;
			if ( LOCAL_PLAYER->isDriving() )
				;
			phys = LOCAL_PLAYER->vehicle;
			return phys->getAngleZ();
		},
		"Get player heading\n\nReturn:\n * float - speed" );
}

const std::deque<Itembase *> &DefaultHud::items() const {
	return _items;
}

const std::deque<Itemcrosshair *> &DefaultHud::crossHairs() const {
	return _crossHairs;
}

tiny::TinyJson DefaultHud::write() {
	tiny::TinyJson json;
	for ( auto &&item : _items ) json[item->wgtName().data()].Set( item->write() );
	tiny::TinyJson cf;
	for ( auto &&item : _crossHairs ) cf[item->wgtName().data()].Set( item->write() );

	json["CrossHairs"].Set( cf );

	return json;
}

nlohmann::json DefaultHud::write2() {
	auto json = nlohmann::json::object();
	for ( auto &&item : _items ) json[item->wgtName().data()] = item->write2();
	auto cf = nlohmann::json::object();
	for ( auto &&item : _crossHairs ) cf[item->wgtName().data()] = item->write2();

	json["CrossHairs"] = cf;

	return json;
}

void DefaultHud::read( tiny::xobject &json ) {
	for ( int i = 0; i < json.Count(); i++ ) {
		json.Enter( i );
		for ( auto &&item : _items ) {
			tiny::xobject jitem = json.Get<tiny::xobject>( item->wgtName().data() );
			item->read( jitem );
		}
		auto jCf = json.Get<tiny::xobject>( "CrossHairs" );
		for ( int ii = 0; ii < jCf.Count(); ii++ ) {
			jCf.Enter( i );
			for ( auto &&item : _crossHairs ) {
				tiny::xobject jitem = jCf.Get<tiny::xobject>( item->wgtName().data() );
				item->read( jitem );
			}
		}
	}
}

void DefaultHud::read2( nlohmann::json &json ) {
	for ( auto &&item : _items ) {
		auto jitem = json[item->wgtName().data()];
		item->read2( jitem );
	}
	auto jCf = json["CrossHairs"];
	for ( auto &&item : _crossHairs ) {
		auto jitem = jCf[item->wgtName().data()];
		item->read2( jitem );
	}
}

void DefaultHud::fontTextureHook( SRHook::CPU &cpu ) {
	auto id = *(short *)0xC71ACC;
	if ( id < 0 || id > 1 ) return;
	if ( !_font[id].texture ) return;
	cpu.ECX = (size_t)&_font[id];
}

void DefaultHud::charWidthPropHook( SRHook::Info &info ) {
	if ( info.cpu.ECX / 0xD2 > 1 ) return;
	info.skipOriginal = true;
	info.cpu.EAX	  = fontData[info.cpu.ECX / 0xD2].m_propValues[info.cpu.EDX];
}

void DefaultHud::charWidthUnpropHook( SRHook::Info &info ) {
	if ( info.cpu.ECX / 0xD2 > 1 ) return;
	info.skipOriginal = true;
	info.cpu.EDX	  = fontData[info.cpu.ECX / 0xD2].m_unpropValue;
}

void DefaultHud::getCharSizeUnpropHook( SRHook::Info &info ) {
	if ( info.cpu.EDX / 0xD2 > 1 ) return;
	info.skipOriginal = true;
	info.cpu.EAX	  = fontData[info.cpu.EDX / 0xD2].m_unpropValue;
}

void DefaultHud::renderFontPropHook( SRHook::Info &info ) {
	if ( info.cpu.EDI / 0xD2 > 1 ) return;
	info.skipOriginal = true;
	info.cpu.EAX	  = fontData[info.cpu.EDI / 0xD2].m_propValues[info.cpu.EAX];
}

void DefaultHud::renderFontUnpropHook( SRHook::Info &info ) {
	if ( info.cpu.EAX / 0xD2 > 1 ) return;
	info.skipOriginal = true;
	info.cpu.EAX	  = fontData[info.cpu.EAX / 0xD2].m_unpropValue;
}
