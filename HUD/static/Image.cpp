#include "Image.h"
#include "loader/loader.h"
#include <cmath>
#include <gtasa/CGame/methods.h>
#include <llmo/callfunc.hpp>
#include <nlohmann/json.hpp>

Image::Image( SRDescent *parent, std::string_view name, std::string_view id ) : Itemhud( parent, name, id ) {
	_size	 = { 40, 40 };
	_wgtName = "Image";
}

void Image::draw() {
	Itemhud::draw();
	if ( _texture.texture ) {
		ImGui::SameLine( 0, 10 );
		ImGui::Image( _texture.texture->raster->rwD3D9RasterExt()->texture, { 60, 40 }, { 0, 0 }, { 1, 1 },
					  *(ImVec4 *)&_color );
	}
	ImGui::DragFloat( name( "rotate" ).data(), &angle, 0.001, -3.14159f, 3.14159f );
	ImGui::PushItemWidth( 150 );
	ImGui::BeginGroup();
	ImGui::DragFloat( name( "left" ).data(), &rect.x1, 0.001, 0.0f, 1.0f );
	ImGui::DragFloat( name( "top" ).data(), &rect.y1, 0.001, 0.0f, 1.0f );
	ImGui::EndGroup();
	ImGui::SameLine( 0, 30 );
	ImGui::BeginGroup();
	ImGui::DragFloat( name( "right" ).data(), &rect.x2, 0.001, 0.0f, 1.0f );
	ImGui::DragFloat( name( "bottom" ).data(), &rect.y2, 0.001, 0.0f, 1.0f );
	ImGui::EndGroup();
	ImGui::PopItemWidth();
}

bool Image::isDrag() {
	RwV2D start, end;
	if ( size().fX >= 0 ) {
		start.fX = pos().fX - size().fX * 0.5f;
		end.fX	 = pos().fX + size().fX * 0.5f;
	} else {
		start.fX = pos().fX + size().fX * 0.5f;
		end.fX	 = pos().fX - size().fX * 0.5f;
	}
	if ( size().fY >= 0 ) {
		start.fY = pos().fY - size().fY * 0.5f;
		end.fY	 = pos().fY + size().fY * 0.5f;
	} else {
		start.fY = pos().fY + size().fY * 0.5f;
		end.fY	 = pos().fY - size().fY * 0.5f;
	}
	if ( isShowed() && mousePos().fX >= start.fX && mousePos().fY >= start.fY && mousePos().fX <= end.fX &&
		 mousePos().fY <= end.fY ) {
		_drag		= true;
		_dragOffset = mousePos() - pos();
		return true;
	}
	return false;
}

bool Image::processDrag() {
	if ( !_drag ) return false;
	pos( mousePos() - _dragOffset );
	return true;
}

tiny::TinyJson Image::write() {
	auto json = Itemhud::write();
	if ( !_textureName.empty() ) json["texture"].Set( _textureName );
	if ( rect.x1 != 0.0f ) json["left"].Set( rect.x1 );
	if ( rect.y1 != 0.0f ) json["top"].Set( rect.y1 );
	if ( rect.x2 != 1.0f ) json["right"].Set( rect.x2 );
	if ( rect.y2 != 1.0f ) json["bottom"].Set( rect.y2 );
	if ( angle != 0.0f ) json["angle"].Set( std::to_string( angle ) );
	return json;
}

nlohmann::json Image::write2() {
	auto json = Itemhud::write2();
	if ( !_textureName.empty() ) json["texture"] = _textureName;
	if ( rect.x1 != 0.0f ) json["left"] = rect.x1;
	if ( rect.y1 != 0.0f ) json["top"] = rect.y1;
	if ( rect.x2 != 1.0f ) json["right"] = rect.x2;
	if ( rect.y2 != 1.0f ) json["bottom"] = rect.y2;
	if ( angle != 0.0f ) json["angle"] = angle;
	return json;
}

void Image::read( tiny::xobject &json ) {
	for ( int i = 0; i < json.Count(); i++ ) {
		json.Enter( i );
		Itemhud::read( json );
		_textureName = json.Get<std::string>( "texture", "" );
		rect.x1		 = json.Get<float>( "left", 0.0f );
		rect.y1		 = json.Get<float>( "top", 0.0f );
		rect.x2		 = json.Get<float>( "right", 1.0f );
		rect.y2		 = json.Get<float>( "bottom", 1.0f );
		angle		 = std::stof( json.Get<std::string>( "angle", "0.0" ) );
	}
}

void Image::read2( nlohmann::json &json ) {
	Itemhud::read2( json );
	_textureName = json.value<std::string>( "texture", "" );
	rect.x1		 = json.value<float>( "left", 0.0f );
	rect.y1		 = json.value<float>( "top", 0.0f );
	rect.x2		 = json.value<float>( "right", 1.0f );
	rect.y2		 = json.value<float>( "bottom", 1.0f );
	angle		 = json.value<float>( "angle", 0.0f );
}

void Image::render() {
	if ( isHidden() ) return;

	// Backup the DX9 state
	//	IDirect3DStateBlock9 *d3d9_state_block = NULL;
	//	if ( g_class.DirectX->d3d9_device()->CreateStateBlock( D3DSBT_ALL, &d3d9_state_block ) < 0 ) return;
	//	// Backup the DX9 transform
	//	D3DMATRIX last_world, last_view, last_projection;
	//	g_class.DirectX->d3d9_device()->GetTransform( D3DTS_WORLD, &last_world );
	//	g_class.DirectX->d3d9_device()->GetTransform( D3DTS_VIEW, &last_view );
	//	g_class.DirectX->d3d9_device()->GetTransform( D3DTS_PROJECTION, &last_projection );

	auto  mul = angle != 0.0f ? 0.71f : 1.0f;
	int	  w	  = ( SCREEN_X / 640.0f ) * size().fX * mul;
	float x	  = ( SCREEN_X / 640.0f ) * pos().fX;
	int	  h	  = ( SCREEN_Y / 448.0f ) * size().fY * mul;
	float y	  = ( SCREEN_Y / 448.0f ) * pos().fY;

	float verts[10];
	if ( angle != 0.0f ) {
		auto v7 = angle - *(float *)0x859AB0;
		for ( int i = 0; i < 4; ++i ) {
			auto vertex			  = i + 1;
			auto v12			  = i * 1.5707964f + v7;
			verts[2 * vertex]	  = x + sin( v12 ) * w;
			verts[2 * vertex + 1] = y - cos( v12 ) * h;
		}
	} else {
		verts[2] = x - w * 0.5f;
		verts[3] = y - h * 0.5f;
		verts[4] = x + w * 0.5f;
		verts[5] = y - h * 0.5f;
		verts[6] = x + w * 0.5f;
		verts[7] = y + h * 0.5f;
		verts[8] = x - w * 0.5f;
		verts[9] = y + h * 0.5f;
	}

	auto clr = color();
	{ // set verticies
		CallFunc::ccall( 0x727590, verts[8], verts[9], verts[6], verts[7], verts[2], verts[3], verts[4], verts[5], &clr,
						 &clr, &clr, &clr );
		auto  verts = RwD3D9Vertex::maVerticies();
		auto &lu	= verts[0];
		auto &ru	= verts[1];
		auto &rb	= verts[2];
		auto &lb	= verts[3];
		lu.u		= rect.x1;
		ru.u		= rect.x2;
		lu.v		= rect.y1;
		ru.v		= rect.y1;
		lb.u		= rect.x1;
		rb.u		= rect.x2;
		lb.v		= rect.y2;
		rb.v		= rect.y2;
	}
	auto rwInstance		= *(size_t *)0xC97B24;
	auto setRenderState = *(size_t *)( rwInstance + 0x20 );
	if ( _texture.texture )
		CallFunc::ccall( setRenderState, 1, _texture.texture->raster );
	else
		CallFunc::ccall( setRenderState, 1, nullptr );
	CallFunc::ccall( 0x734E90, 5, RwD3D9Vertex::maVerticies(), 4 ); // render
	CallFunc::ccall( setRenderState, 1, nullptr );

	// Restore the DX9 transform
	//	g_class.DirectX->d3d9_device()->SetTransform( D3DTS_WORLD, &last_world );
	//	g_class.DirectX->d3d9_device()->SetTransform( D3DTS_VIEW, &last_view );
	//	g_class.DirectX->d3d9_device()->SetTransform( D3DTS_PROJECTION, &last_projection );
	//	// Restore the DX9 state
	//	d3d9_state_block->Apply();
	//	d3d9_state_block->Release();
}

void Image::setImage( std::string_view name, RwTexture *texture ) {
	_texture.texture = texture;
	_textureName	 = name;
}

std::string_view Image::txName() {
	return _textureName;
}
