#ifndef ARMOUR_H
#define ARMOUR_H

#include "Itemchart.h"
#include <llmo/SRHook.hpp>

class Armour : public Itemchart {
	SRHook::Hook<>																		drawArmour{ 0x5890BA, 5 };
	SRHook::Hook<float, float, WORD, WORD, float, uint32_t, bool, bool, RwRGBA, RwRGBA> posArmour{ 0x58917A, 5 };

public:
	Armour( SRDescent *parent, std::string_view id = "" );

	virtual void initLuaAndIDE( sol::state &lua, class IDE *ide, std::string prefix = "" ) override;

protected:
	void showHook( SRHook::Info &info );
	void posHook( float &x, float &y, WORD &w, WORD &h, float &percent, uint32_t &drawBlueline, bool &drawPercentage,
				  bool &drawBorder, RwRGBA &color, RwRGBA &fore );
};

#endif // ARMOUR_H
