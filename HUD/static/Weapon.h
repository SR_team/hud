#ifndef WEAPON_H
#define WEAPON_H

#include "Itembase.h"
#include <llmo/SRHook.hpp>

class Weapon : public Itembase {
	SRHook::Hook<>					drawWeapon{ 0x58D7D8, 5 };
	SRHook::Hook<int, int>			posWeapon{ 0x58D7FD, 6 };
	SRHook::Hook<CRect *, RwRGBA *> clWeapon{ 0x58D988, 5 };
	SRHook::Hook<RwRaster *>		xluIcon{ 0x58D872, 5 };
	SRHook::Hook<float, float, float, float, float, uint8_t, uint8_t, uint8_t, uint8_t, float, uint8_t> clWeaponXLU{
		0x58D8FD, 5
	};

public:
	Weapon( SRDescent *parent, std::string_view id = "" );

	bool custom_color = false;
	bool custom_icons = false;

	RwRGBA color() const;
	void   color( RwRGBA color );

	void setIcon( int id, RwTexture *texture = nullptr );

	virtual void		   reset() override;
	virtual void		   draw() override;
	virtual bool		   isDrag() override;
	virtual bool		   processDrag() override;
	virtual tiny::TinyJson write() override;
	virtual nlohmann::json write2() override;
	virtual void		   read( tiny::xobject &json ) override;
	virtual void		   read2( nlohmann::json &json ) override;
	virtual void		   initLuaAndIDE( sol::state &lua, class IDE *ide, std::string prefix = "" ) override;

	bool operator==( const Weapon &r ) {
		if ( !Itembase::operator==( r ) ) return false;
		if ( custom_color != r.custom_color ) return false;
		if ( custom_icons != r.custom_icons ) return false;
		if ( color() != r.color() ) return false;
		return true;
	}

protected:
	float	 _color[4]{ 1, 1, 1, 1 };
	ImVec4	 _backup_color;
	CTexture _custom_weaponIcons[47];
	RwV2D	 _dragOffset;

	void showHook( SRHook::Info &info );
	void posHook( int &x, int &y );
	void clHook( SRHook::Info &info, CRect *&rect, RwRGBA *&color );
	void iconXLUHook( RwRaster *&raster );
	void clXLUHook( float &rigth, float &bottom, float &, float &w, float &h, uint8_t &red, uint8_t &green,
					uint8_t &blue, uint8_t &, float &, uint8_t &icon_alpha );
};

#endif // WEAPON_H
