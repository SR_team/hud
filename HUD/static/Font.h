#ifndef FONT_H
#define FONT_H

#include "Itemhud.h"

class Font : public Itemhud {
public:
	Font( SRDescent *parent, std::string_view name, std::string_view id = "" );

	enum class Align { center, left, right };
	Align align			= Align::left;
	bool  background	= false;
	int	  edge			= 0;
	int	  shadow_offset = 1;
	bool  shadow		= false;
	bool  border		= false;
	enum class Style { Gothic, Subtitles, Menu, Pricedown };
	Style style		   = Style::Subtitles;
	bool  proportional = true;
	char  text[256]{ 0 };

	RwV2D realSize() const;

	bool operator==( const Font &r ) {
		if ( !Itemhud::operator==( r ) ) return false;
		if ( align != r.align ) return false;
		if ( background != r.background ) return false;
		if ( edge != r.edge ) return false;
		if ( shadow_offset != r.shadow_offset ) return false;
		if ( shadow != r.shadow ) return false;
		if ( border != r.border ) return false;
		if ( style != r.style ) return false;
		if ( proportional != r.proportional ) return false;
		if ( strcmp( text, r.text ) ) return false;
		return true;
	}

	virtual void		   draw() override;
	virtual bool		   isDrag() override;
	virtual bool		   processDrag() override;
	virtual tiny::TinyJson write() override;
	virtual nlohmann::json write2() override;
	virtual void		   read( tiny::xobject &json ) override;
	virtual void		   read2( nlohmann::json &json ) override;

	virtual void render() override;

protected:
	RwV2D _realSize;
	RwV2D _dragOffset;
};

#endif // FONT_H
