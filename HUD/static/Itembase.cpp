#include "Itembase.h"
#include <gtasa/CGame/methods.h>
#include <llmo/callfunc.hpp>
#include <llmo/memsafe.h>
#include <nlohmann/json.hpp>

Itembase::Itembase( SRDescent *parent, std::string_view id ) : SRDescent( parent ) {
	if ( id.empty() ) {
		_id.resize( 11, '\0' );
		sprintf( _id.data(), "##%08X", this );
	} else
		_id = "##" + std::string( id );
}

Itembase::~Itembase() {}

RwV2D Itembase::pos() const {
	if ( isCustomPos() ) return customPos();
	return originalPos();
}

RwV2D Itembase::size() const {
	if ( isCustomSize() ) return customSize();
	return originalSize();
}

RwV2D Itembase::customPos() const {
	return _pos;
}

RwV2D Itembase::customSize() const {
	return _size;
}

RwV2D Itembase::originalPos() const {
	return _origPos;
}

RwV2D Itembase::originalSize() const {
	return _origSize;
}

bool Itembase::isCustomPos() const {
	return _customPos;
}

bool Itembase::isCustomSize() const {
	return _customSize;
}

bool Itembase::isShowed() const {
	return _show;
}

bool Itembase::isHidden() const {
	return !isShowed();
}

void Itembase::pos( const RwV2D &value ) {
	_customPos = true;
	_pos	   = value;
}

void Itembase::size( const RwV2D &value ) {
	_customSize = true;
	_size		= value;
}

void Itembase::show() {
	_show = true;
}

void Itembase::hide() {
	_show = false;
}

void Itembase::disableCustomPos() {
	_customPos = false;
}

void Itembase::disableCustomSize() {
	_customSize = false;
}

void Itembase::disableCustom() {
	disableCustomPos();
	disableCustomSize();
}

void Itembase::toggleShow() {
	_show ^= true;
}

void Itembase::toggleShow( bool status ) {
	_show = status;
}

void Itembase::toggleCustomPos() {
	_customPos ^= true;
	initPos();
}

void Itembase::toggleCustomPos( bool status ) {
	_customPos = status;
	initPos();
}

void Itembase::toggleCustomSize() {
	_customSize ^= true;
	initSize();
}

void Itembase::toggleCustomSize( bool status ) {
	_customSize = status;
	initSize();
}

void Itembase::originalToCustomPos() {
	pos( originalPos() );
}

void Itembase::originalToCustomSize() {
	size( originalSize() );
}

void Itembase::originalToCustom() {
	originalToCustomPos();
	originalToCustomSize();
}

void Itembase::reset() {
	disableCustom();
	show();
	_initPos  = false;
	_initSize = false;
}

void Itembase::draw() {
	auto s = isShowed();
	ImGui::Checkbox( name( "Show item" ).data(), &s );
	if ( isShowed() != s ) toggleShow( s );

	ImGui::PushItemWidth( 150 );

	ImGui::BeginGroup();
	{
		auto s = isCustomPos();
		ImGui::Checkbox( name( "Use custom position" ).data(), &s );
		if ( isCustomPos() != s ) toggleCustomPos( s );
		if ( s ) {
			auto cpos = customPos();
			ImGui::DragFloat( name( "X" ).data(), &cpos.fX, 0.1f );
			if ( ImGui::IsItemHovered() ) ImGui::SetTooltip( "Default: %.3f", originalPos().fX );

			ImGui::DragFloat( name( "Y" ).data(), &cpos.fY, 0.1f );
			if ( ImGui::IsItemHovered() ) ImGui::SetTooltip( "Default: %.3f", originalPos().fY );

			if ( customPos().fX != cpos.fX || customPos().fY != cpos.fY ) pos( cpos );
			if ( ImGui::Button( name( "Default position", "pos" ).data() ) ) originalToCustomPos();
		} else {
			auto opos = originalPos();
			ImGui::DragFloat( name( "X" ).data(), &opos.fX, 0.000001 );
			ImGui::DragFloat( name( "Y" ).data(), &opos.fY, 0.000001 );
		}
	}
	ImGui::EndGroup();

	ImGui::SameLine( 0, 50 );

	ImGui::BeginGroup();
	{
		auto s = isCustomSize();
		ImGui::Checkbox( name( "Use custom size" ).data(), &s );
		if ( isCustomSize() != s ) toggleCustomSize( s );
		if ( s ) {
			auto csize = customSize();
			ImGui::DragFloat( name( "W" ).data(), &csize.fX, 0.1f, 0.25f );
			if ( ImGui::IsItemHovered() ) ImGui::SetTooltip( "Default: %.3f", originalSize().fX );

			ImGui::DragFloat( name( "H" ).data(), &csize.fY, 0.1f, 0.25f );
			if ( ImGui::IsItemHovered() ) ImGui::SetTooltip( "Default: %.3f", originalSize().fY );

			if ( csize.fX < .25 ) csize.fX = .25;
			if ( csize.fY < .25 ) csize.fY = .25;

			if ( customSize().fX != csize.fX || customSize().fY != csize.fY ) size( csize );
			if ( ImGui::Button( name( "Default size", "size" ).data() ) ) originalToCustomSize();
		} else {
			auto osize = originalSize();
			ImGui::DragFloat( name( "W" ).data(), &osize.fX, 0.000001 );
			ImGui::DragFloat( name( "H" ).data(), &osize.fY, 0.000001 );
		}
	}
	ImGui::EndGroup();

	ImGui::PopItemWidth();
}

bool Itembase::isDrag() {
	RwV2D start, end;
	if ( size().fX >= 0 ) {
		start.fX = pos().fX;
		end.fX	 = pos().fX + size().fX;
	} else {
		start.fX = pos().fX - size().fX;
		end.fX	 = pos().fX;
	}
	if ( size().fY >= 0 ) {
		start.fY = pos().fY;
		end.fY	 = pos().fY + size().fY;
	} else {
		start.fY = pos().fY - size().fY;
		end.fY	 = pos().fY;
	}
	if ( isShowed() && isCustomPos() && mousePos().fX >= start.fX && mousePos().fY >= start.fY &&
		 mousePos().fX <= end.fX && mousePos().fY <= end.fY ) {
		_drag = true;
		return true;
	}
	return false;
}

bool Itembase::processDrag() {
	if ( !_drag ) return false;
	pos( mousePos() );
	return true;
}

void Itembase::endDrag() {
	_drag = false;
}

tiny::TinyJson Itembase::write() {
	tiny::TinyJson json;
	if ( isHidden() ) json["show"].Set( (int)isShowed() );

	if ( isCustomPos() ) {
		json["custom_pos"].Set( (int)isCustomPos() );
		tiny::TinyJson pos;
		pos["fX"].Set( std::to_string( customPos().fX ) );
		pos["fY"].Set( std::to_string( customPos().fY ) );

		json["pos"].Set( pos );
	}

	if ( isCustomSize() ) {
		json["custom_size"].Set( (int)isCustomSize() );
		tiny::TinyJson size;
		size["fX"].Set( std::to_string( customSize().fX ) );
		size["fY"].Set( std::to_string( customSize().fY ) );

		json["size"].Set( size );
	}

	return json;
}

nlohmann::json Itembase::write2() {
	auto json = nlohmann::json::object();
	if ( isHidden() ) json["show"] = isShowed();

	if ( isCustomPos() ) {
		json["custom_pos"] = isCustomPos();
		auto pos		   = nlohmann::json::object();
		pos["fX"]		   = customPos().fX;
		pos["fY"]		   = customPos().fY;

		json["pos"] = pos;
	}

	if ( isCustomSize() ) {
		json["custom_size"] = isCustomSize();
		auto size			= nlohmann::json::object();
		size["fX"]			= customSize().fX;
		size["fY"]			= customSize().fY;

		json["size"] = size;
	}

	return json;
}

void Itembase::read( tiny::xobject &json ) {
	auto readRwV2D = []( tiny::xobject &json ) {
		RwV2D res;
		for ( int i = 0; i < json.Count(); i++ ) {
			json.Enter( i );
			res.fX = std::stof( json.Get<std::string>( "fX", "0.0" ) );
			res.fY = std::stof( json.Get<std::string>( "fY", "0.0" ) );
		}
		return res;
	};

	for ( int i = 0; i < json.Count(); i++ ) {
		json.Enter( i );
		toggleShow( json.Get<int>( "show", 1 ) );
		auto custom_pos = json.Get<int>( "custom_pos", 0 );
		if ( custom_pos ) {
			tiny::xobject jpos = json.Get<tiny::xobject>( "pos" );
			pos( readRwV2D( jpos ) );
		}
		auto custom_size = json.Get<int>( "custom_size", 0 );
		if ( custom_size ) {
			tiny::xobject jsize = json.Get<tiny::xobject>( "size" );
			size( readRwV2D( jsize ) );
		}
	}
}

void Itembase::read2( nlohmann::json &json ) {
	auto readRwV2D = []( nlohmann::json &json ) {
		RwV2D res;
		res.fX = json.value<float>( "fX", 0.0f );
		res.fY = json.value<float>( "fY", 0.0f );
		return res;
	};

	toggleShow( json.value<bool>( "show", true ) );
	auto custom_pos = json.value<bool>( "custom_pos", false );
	if ( custom_pos ) {
		auto jpos = json["pos"];
		pos( readRwV2D( jpos ) );
	}
	auto custom_size = json.value<bool>( "custom_size", false );
	if ( custom_size ) {
		auto jsize = json["size"];
		size( readRwV2D( jsize ) );
	}
}

void Itembase::initLuaAndIDE( sol::state &lua, IDE *ide, std::string prefix ) {
	if ( prefix.empty() ) {
		prefix = _wgtName;
		std::transform( prefix.begin(), prefix.end(), prefix.begin(), ::tolower );
	}

	addFunction(
		lua, ide, prefix + "_show", [this]( bool show ) { toggleShow( show ); },
		"Show/hide " + prefix + "\n\nArgs:\n * bool - show" );
	addFunction(
		lua, ide, prefix + "_isShowed", [this] { return isShowed(); },
		"Get current show state of " + prefix + "\n\nReturn:\n * bool - is showed" );
	addFunction(
		lua, ide, prefix + "_origPos", [this] { return std::make_tuple( originalPos().fX, originalPos().fY ); },
		"Get original game position of " + prefix + "\n\nReturn:\n * float - position X\n * float - position Y" );
	addFunction(
		lua, ide, prefix + "_custPos", [this] { return std::make_tuple( customPos().fX, customPos().fY ); },
		"Get custom position of " + prefix + "\n\nReturn:\n * float - position X\n * float - position Y" );
	addFunction(
		lua, ide, prefix + "_pos", [this] { return std::make_tuple( pos().fX, pos().fY ); },
		"Get current " + prefix + " position\n\nReturn:\n * float - position X\n * float - position Y" );
	addFunction(
		lua, ide, prefix + "_origSize", [this] { return std::make_tuple( originalSize().fX, originalSize().fY ); },
		"Get original game size of " + prefix + "\n\nReturn:\n * float - width\n * float - height" );
	addFunction(
		lua, ide, prefix + "_custSize", [this] { return std::make_tuple( customSize().fX, customSize().fY ); },
		"Get custom size of " + prefix + "\n\nReturn:\n * float - width\n * float - height" );
	addFunction(
		lua, ide, prefix + "_size", [this] { return std::make_tuple( size().fX, size().fY ); },
		"Get current " + prefix + " size\n\nReturn:\n * float - width\n * float - height" );
	addFunction(
		lua, ide, prefix + "_setPos",
		[this]( float x, float y ) {
			pos( { x, y } );
		},
		"Set " + prefix + " position\n\nArgs:\n * float - position X\n * float - position Y" );
	addFunction(
		lua, ide, prefix + "_setSize",
		[this]( float w, float h ) {
			size( { w, h } );
		},
		"Set " + prefix + " size\n\nArgs:\n * float - width\n * float - height" );
	addFunction(
		lua, ide, prefix + "_resetPos", [this] { disableCustomPos(); },
		"Reset " + prefix + " position to default without forget custom position" );
	addFunction(
		lua, ide, prefix + "_resetSize", [this] { disableCustomSize(); },
		"Reset " + prefix + " postion to default without forget custom size" );
	addFunction(
		lua, ide, prefix + "_toggleCustomPos", [this]( bool cust ) { return toggleCustomPos( cust ); },
		"Toggle custom/default position for " + prefix + "\n\nArgs:\n * bool - is custom" );
	addFunction(
		lua, ide, prefix + "_toggleCustomSize", [this]( bool cust ) { return toggleCustomSize( cust ); },
		"Toggle custom/default size for " + prefix + "\n\nArgs:\n * bool - is custom" );
	addFunction(
		lua, ide, prefix + "_isCustomPos", [this]() { return isCustomPos(); },
		"Get custom/default position state of " + prefix + "\n\nReturn:\n * bool - is custom" );
	addFunction(
		lua, ide, prefix + "_isCustomSize", [this]() { return isCustomSize(); },
		"Get custom/default size state of " + prefix + "\n\nReturn:\n * bool - is custom" );
	addFunction(
		lua, ide, prefix + "_reset", [this]() { return reset(); }, "Reset all " + prefix + " changes" );
}

std::string_view Itembase::wgtName() {
	return _wgtName;
}

std::string Itembase::name( std::string_view rawName, std::string_view addId ) {
	return rawName.data() + _id + addId.data();
}

RwV2D Itembase::mousePos() {
	static auto &hWnd = *reinterpret_cast<HWND *>( 0xC97C1C );
	POINT		 M;
	GetCursorPos( &M );
	ScreenToClient( hWnd, &M );
	return WindowScreenToGameScreen( M.x, M.y );
}

bool Itembase::popupColorpicker( ImVec4 &color, ImVec4 &backup, bool alpha ) {
	static auto _palette = [] {
		static ImVec4 _palette[32] = {};
		for ( int n = 0; n < 32; n++ ) {
			ImGui::ColorConvertHSVtoRGB( n / 31.0f, 0.8f, 0.8f, _palette[n].x, _palette[n].y, _palette[n].z );
			_palette[n].w = 1.0f; // Alpha
		}
		return _palette;
	}();

	ImGuiColorEditFlags flags = ImGuiColorEditFlags_AlphaBar * alpha;
	flags |= ImGuiColorEditFlags_DisplayRGB;
	flags |= ImGuiColorEditFlags_DisplayHex;
	if ( !alpha ) flags |= ImGuiColorEditFlags_NoAlpha;

	if ( ImGui::BeginPopup( name( "colorpicker" ).data() ) ) {
		if ( alpha )
			ImGui::ColorPicker4( name( "", "picker_rgba" ).data(), (float *)&color,
								 flags | ImGuiColorEditFlags_NoSidePreview | ImGuiColorEditFlags_NoSmallPreview );
		else
			ImGui::ColorPicker3( name( "", "picker_rgb" ).data(), (float *)&color,
								 flags | ImGuiColorEditFlags_NoSidePreview | ImGuiColorEditFlags_NoSmallPreview );
		ImGui::SameLine();

		ImGui::BeginGroup(); // Lock X position
		auto cbtnSz = ImVec2( 90, 70 );
		ImGui::BeginGroup();
		ImGui::Text( "Current" );
		ImGui::ColorButton( name( "Current color", "current" ).data(), *(ImVec4 *)&color,
							ImGuiColorEditFlags_NoPicker | ImGuiColorEditFlags_NoTooltip |
								( ImGuiColorEditFlags_AlphaPreview * alpha ),
							cbtnSz );
		ImGui::EndGroup();
		ImGui::SameLine();
		ImGui::BeginGroup();
		ImGui::Text( "Previous" );
		if ( ImGui::ColorButton( name( "Previous color", "previous" ).data(), backup,
								 ImGuiColorEditFlags_NoPicker | ImGuiColorEditFlags_NoTooltip |
									 ( ImGuiColorEditFlags_AlphaPreview * alpha ),
								 cbtnSz ) )
			color = backup;
		ImGui::EndGroup();
		ImGui::Separator();
		ImGui::Text( "Palette" );
		for ( int n = 0; n < 32; n++ ) {
			ImGui::PushID( n );
			if ( ( n % 8 ) != 0 ) ImGui::SameLine( 0.0f, ImGui::GetStyle().ItemSpacing.y );

			ImGuiColorEditFlags palette_button_flags =
				ImGuiColorEditFlags_NoAlpha | ImGuiColorEditFlags_NoPicker | ImGuiColorEditFlags_NoTooltip;
			if ( ImGui::ColorButton( name( "", "palette" ).data(), _palette[n], palette_button_flags,
									 ImVec2( 20, 20 ) ) )
				color = ImVec4( _palette[n].x, _palette[n].y, _palette[n].z, color.w );
			if ( ImGui::BeginDragDropTarget() ) {
				if ( const ImGuiPayload *payload = ImGui::AcceptDragDropPayload( IMGUI_PAYLOAD_TYPE_COLOR_3F ) )
					memcpy( (float *)&_palette[n], payload->Data, sizeof( float ) * 3 );
				if ( const ImGuiPayload *payload = ImGui::AcceptDragDropPayload( IMGUI_PAYLOAD_TYPE_COLOR_4F ) )
					memcpy( (float *)&_palette[n], payload->Data, sizeof( float ) * 4 );
				ImGui::EndDragDropTarget();
			}

			ImGui::PopID();
		}
		ImGui::EndGroup();
		ImGui::EndPopup();
		return true;
	}
	return false;
}

RwTexture *Itembase::loadPNG( const char *filename ) {
	auto image = CallFunc::ccall<class RwImage *>( 0x7CF9B0, filename );
	int	 width, height, depth, format;
	CallFunc::ccall( 0x8042C0, image, 4, &width, &height, &depth, &format );
	auto raster = CallFunc::ccall<RwRaster *>( 0x7FB230, width, height, depth, format );
	CallFunc::ccall( 0x804290, raster, image );
	CallFunc::ccall( 0x802740, image );
	return CallFunc::ccall<RwTexture *>( 0x7F37C0, raster );
}

RwTexture *Itembase::loadPNG( uint8_t *addr, size_t size ) {
	if ( !addr || !size ) return nullptr;
	struct {
		uint8_t *memptr;
		size_t	 memSize;
	} data{ addr, size };
	memsafe::write( 0x7CF9CA, '\x03' );
	auto texture = loadPNG( (const char *)&data );
	memsafe::write( 0x7CF9CA, '\x02' );
	return texture;
}

void Itembase::initPos() {
	if ( _customPos && !_initPos ) {
		_initPos = true;
		_pos	 = _origPos;
	}
}

void Itembase::initSize() {
	if ( _customSize && !_initSize ) {
		_initSize = true;
		_size	  = _origSize;
	}
}

RwVRect Itembase::GetTextRect( float x, float y, const char *text ) {
	RwVRect rect{ 0, 0, 1000, 1000 };
	CallFunc::ccall( 0x71A620, &rect, x, y, text );
	rect.x2 = CallFunc::ccall<float>( 0x71A0E0, text, true, false );
	if ( *(bool *)0xC71A79 ) { // center
		rect.x1 = x - ( rect.x2 * 0.5f );
		rect.x2 = x + ( rect.x2 * 0.5f );
	} else if ( *(bool *)0xC71A7A ) { // right
		rect.x1 = x - rect.x2;
		rect.x2 = x;
	} else { // left
		rect.x1 = x;
		rect.x2 += x;
	}
	return rect;
}
