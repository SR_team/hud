#ifndef IDE_H
#define IDE_H

#include <SRDescent/SRDescent.h>
#include <SRSignal/SRSignal.hpp>
#include <TextEditor.h>

class IDE : public SRDescent {
	struct FDoc {
		std::string				bief;
		std::string				detail;
		std::deque<std::string> args;
		std::deque<std::string> rets;
		struct {
			std::string args;
			std::string rets;
		} example;
	};

	TextEditor editor;
	bool	   openned	   = false;
	bool	   help_editor = false;
	bool	   help_API	   = false;

public:
	IDE( SRDescent *parent );

	void open();
	void close();
	void draw();

	SRSignal<> onClose;

	bool isOpen() const;

	std::string text() const;
	void		text( std::string_view code );

	void addFunction( std::string_view fname, std::string_view desc = "HUD function" );
	void setError( int line, std::string_view err );

	void reset();

protected:
	void helpApi();
	void helpIde();

	FDoc createFDoc( std::string_view desc );
};

#endif // IDE_H
