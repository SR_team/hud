#ifndef ITEMCROSSHAIR_H
#define ITEMCROSSHAIR_H

#include "Itembase.h"

class Itemcrosshair : public Itembase {
public:
	Itemcrosshair( SRDescent *parent, std::string_view id = "" );

	RwRGBA color() const;
	void   color( RwRGBA color );
	void   setImage( std::string_view name, RwTexture *texture = nullptr );

	virtual void		   reset() override;
	virtual void		   draw() override;
	virtual tiny::TinyJson write() override;
	virtual nlohmann::json write2() override;
	virtual void		   read( tiny::xobject &json ) override;
	virtual void		   read2( nlohmann::json &json ) override;
	virtual void		   initLuaAndIDE( sol::state &lua, IDE *ide, std::string prefix = "" ) override;

	virtual std::string txName() const;

	std::string luaPrefix() const;

protected:
	float		_color[4]{ 1, 1, 1, 1 };
	ImVec4		_backup_color;
	std::string _textureName;
	CTexture	_texture;
};

#endif // ITEMCROSSHAIR_H
