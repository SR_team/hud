#ifndef CROSSHAIRSIMPLE_H
#define CROSSHAIRSIMPLE_H

#include "Itemcrosshair.h"
#include <llmo/SRHook.hpp>

class CrosshairSimple : public Itemcrosshair {
	SRHook::Hook<RwRGBA *> center{ 0x58E2DD, 5 };
	SRHook::Hook<RwRGBA *> lu{ 0x58E3A5, 5 };
	SRHook::Hook<RwRGBA *> ru{ 0x58E3FA, 5 };
	SRHook::Hook<RwRGBA *> lb{ 0x58E44F, 5 };
	SRHook::Hook<RwRGBA *> rb{ 0x58E49C, 5 };

public:
	CrosshairSimple( SRDescent *parent, std::string_view id = "" );

	bool drawCenter = true;

	virtual void		   reset() override;
	virtual void		   draw() override;
	virtual tiny::TinyJson write() override;
	virtual nlohmann::json write2() override;
	virtual void		   read( tiny::xobject &json ) override;
	virtual void		   read2( nlohmann::json &json ) override;
	virtual void		   initLuaAndIDE( sol::state &lua, IDE *ide, std::string prefix = "" ) override;

protected:
	void centerHook( RwRGBA *&clr );
	void drawHook( SRHook::Info &info, RwRGBA *&clr );
};

#endif // CROSSHAIRSIMPLE_H
