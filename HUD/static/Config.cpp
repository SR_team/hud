#include "Config.h"
#include "DefaultHud.h"
#include "Itemcrosshair.h"
#include "Radar.h"
#include "Weapon.h"
#include <SRString/SRString.h>
#include <imgui-filebrowser/imfilebrowser.h>
#include <llmo/callfunc.hpp>
#include <nlohmann/json.hpp>
#include <windows.h>
#include <zip.h>

Config::Config( SRDescent *parent, std::string_view archive ) : SRDescent( parent ), _archive( archive ) {}

Config::~Config() {
	for ( auto &&[name, data] : _textures ) free( data.src );
	if ( fileExplorer ) delete fileExplorer;
}

const std::deque<Itembase *> &Config::items() const {
	return _items;
}

std::string Config::ImName() const {
	return name + "##" + _archive;
}

Config::AddTexCode Config::addTexture( const std::filesystem::__cxx11::path &file, std::string_view name ) {
	try {
		_textures.at( name.data() );
	} catch ( std::out_of_range & ) {
		std::ifstream f( file, std::ios::binary );
		if ( f.is_open() ) {
			_textures[name.data()].len = std::filesystem::file_size( file ) + 1;
			_textures[name.data()].src = malloc( _textures[name.data()].len );
			for ( int i = 0; !f.eof(); ++i )
				( (uint8_t *)_textures[name.data()].src )[i] = static_cast<byte>( f.get() );
			f.close();
			_textures[name.data()].tx =
				Itembase::loadPNG( (uint8_t *)_textures[name.data()].src, _textures[name.data()].len );
			return AddTexCode::success;
		}
		return AddTexCode::no_access;
	}
	return AddTexCode::name_busy;
}

RwTexture *Config::texture( std::string_view name ) {
	RwTexture *result;
	try {
		result = _textures.at( name.data() ).tx;
	} catch ( std::out_of_range & ) {
		result = nullptr;
	}
	return result;
}

bool Config::isNoTextures() {
	if ( _textures.empty() ) return true;
	for ( auto &&[_, data] : _textures )
		if ( data.len ) return false;
	return true;
}

Itembase *Config::addImage( std::string_view name ) {
	auto it = std::find_if( _items.begin(), _items.end(), [&]( Itembase *item ) {
		auto img = dynamic_cast<Image *>( item );
		if ( !img ) return false;
		return img->imgName() == name;
	} );
	if ( it == _items.end() ) {
		auto img = new Image( this, name );
		_items.push_back( img );
		return img;
	}
	return nullptr;
}

Itembase *Config::addFont( std::string_view name ) {
	auto it = std::find_if( _items.begin(), _items.end(), [&]( Itembase *item ) {
		auto fnt = dynamic_cast<Font *>( item );
		if ( !fnt ) return false;
		return fnt->imgName() == name;
	} );
	if ( it == _items.end() ) {
		auto fnt = new Font( this, name );
		_items.push_back( fnt );
		return fnt;
	}
	return nullptr;
}

Itembase *Config::addChart( std::string_view name ) {
	auto it = std::find_if( _items.begin(), _items.end(), [&]( Itembase *item ) {
		auto chrt = dynamic_cast<Chart *>( item );
		if ( !chrt ) return false;
		return chrt->imgName() == name;
	} );
	if ( it == _items.end() ) {
		auto chrt = new Chart( this, name );
		_items.push_back( chrt );
		return chrt;
	}
	return nullptr;
}

bool Config::removeItem( Itembase *item ) {
	auto it = std::find_if( _items.begin(), _items.end(), [&]( Itembase *i ) { return item == i; } );
	if ( it != _items.end() ) {
		_items.erase( it );
		ImGui::TreePop();
		return true;
	}
	return false;
}

void Config::drawTextureList( DefaultHud *defHud ) {
	for ( auto &&[txName, txData] : _textures ) {
		if ( !txData.len ) continue;
		ImGui::Separator();
		ImGui::BeginGroup();
		if ( txData.len > 1024 * 1024 )
			ImGui::Text( "%s: %dmb", txName.data(), txData.len / ( 1024 * 1024 ) );
		else if ( txData.len > 1024 )
			ImGui::Text( "%s: %dkb", txName.data(), txData.len / 1024 );
		else
			ImGui::Text( "%s: %db", txName.data(), txData.len );
		ImGui::SameLine( 415 );
		if ( ImGui::Button( ( "Remove##del_png" + txName ).data() ) ) {
			if ( _radardisc == txName ) {
				_radardisc = "built-in";
				if ( defHud ) defHud->setRadarTexture();
			}
			if ( _airPlane == txName ) {
				_airPlane = "built-in";
				if ( defHud ) defHud->setAirPlaneTexture();
			}
			for ( int i = 0; i < 47; ++i ) {
				if ( _weapons[i] == txName ) {
					_weapons[i] = "default";
					if ( defHud ) defHud->setWeaponTexture( i );
				}
			}
			for ( auto &&item : _items ) {
				auto img = dynamic_cast<Image *>( item );
				if ( img && img->txName() == txName ) img->setImage( "" );
			}
			auto radar_icons = defHud->radarIcons();
			for ( auto &&icon : radar_icons ) {
				if ( defHud->radarIcon( icon ).empty() ) continue;
				if ( defHud->radarIcon( icon ) == txName ) defHud->setRadarIcon( icon, "" );
			}
			free( txData.src );
			if ( txData.tx ) CallFunc::ccall( 0x7F3820, txData.tx );
			_textures.erase( txName );
			break;
		}
		ImGui::EndGroup();
		if ( ImGui::IsItemHovered() ) Config::texturePreview( txData.tx );
	}
}

bool Config::drawCustomItems() {
	for ( auto &&item : _items ) {
		auto img  = dynamic_cast<Image *>( item );
		auto fnt  = dynamic_cast<Font *>( item );
		auto chrt = dynamic_cast<Chart *>( item );
		if ( img ) {
			if ( ImGui::TreeNode( ( "Image: " + img->imgName() ).data() ) ) {
				if ( ImGui::Button( "Remove image" ) ) return removeItem( img );
				ImGui::SameLine( 0, 20 );
				item->draw();
				if ( !isNoTextures() ) {
					std::string txName = img->txName().data();
					textureSelector(
						"texture##" + img->imgName(), txName, [&]( RwTexture *tx ) { img->setImage( txName, tx ); },
						true );
				}
				ImGui::TreePop();
			}
		} else if ( fnt ) {
			if ( ImGui::TreeNode( ( "Font: " + fnt->imgName() ).data() ) ) {
				if ( ImGui::Button( "Remove font" ) ) return removeItem( fnt );
				ImGui::SameLine( 0, 20 );
				item->draw();
				ImGui::TreePop();
			}
		} else if ( chrt ) {
			if ( ImGui::TreeNode( ( "Chart: " + chrt->imgName() ).data() ) ) {
				if ( ImGui::Button( "Remove chart" ) ) return removeItem( chrt );
				ImGui::SameLine( 0, 20 );
				item->draw();
				ImGui::TreePop();
			}
		}
	}
	return false;
}

void Config::drawDefaultHud( DefaultHud *defHud ) {
	if ( ImGui::BeginTabBar( "Default HUD##DefaultHud" ) ) {
		for ( auto &&item : defHud->items() ) {
			if ( ImGui::BeginTabItem( item->wgtName().data() ) ) {
				{
					ImGui::BeginChild( ( item->wgtName().data() + std::string( "child" ) ).data() );
					item->draw();
					if ( !isNoTextures() ) {
						if ( dynamic_cast<Radar *>( item ) ) {
							if ( dynamic_cast<Radar *>( item )->custom_radardisc )
								textureSelector(
									"radardisc", _radardisc, [&]( RwTexture *tx ) { defHud->setRadarTexture( tx ); },
									true, "built-in" );
							if ( dynamic_cast<Radar *>( item )->custom_airPlane )
								textureSelector(
									"airPlane", _airPlane, [&]( RwTexture *tx ) { defHud->setAirPlaneTexture( tx ); },
									true, "built-in" );
							if ( ImGui::CollapsingHeader( "Icons##radar" ) ) {
								auto radar_icons = defHud->radarIcons();
								for ( auto &&icon : radar_icons ) {
									auto name = defHud->radarIcon( icon );
									textureSelector(
										icon + "##radar_icons", name,
										[&]( RwTexture *tx ) { defHud->setRadarIcon( icon, name, tx ); }, true,
										"default" );
								}
							}
						} else if ( dynamic_cast<Weapon *>( item ) ) {
							if ( dynamic_cast<Weapon *>( item )->custom_icons &&
								 ImGui::CollapsingHeader( "Icons##slots" ) ) { // too slow
								for ( int slot = 0; slot < 13; ++slot ) {
									const char *treeName = get_slot_name( slot );

									if ( ImGui::TreeNode( treeName ) ) {
										for ( int i = 0; i < 47; ++i ) {
											if ( get_slot_by_id( i ) != slot ) continue;
											ImGui::PushItemWidth( 250 );
											textureSelector(
												get_name_by_id( i ) + "##weapon_icon", _weapons[i],
												[&]( RwTexture *tx ) { defHud->setWeaponTexture( i, tx ); }, true,
												"default" );
											ImGui::PopItemWidth();
										}
										ImGui::TreePop();
									}
								}
							}
						}
					}
					if ( ImGui::Button( "Reset all changes##DefaultHud" ) ) item->reset();
					ImGui::EndChild();
				}
				ImGui::EndTabItem();
			}
		}
		ImGui::EndTabBar();
	}
}

void Config::drawCrossHairs( DefaultHud *defHud ) {
	if ( ImGui::BeginTabBar( "CrossHairs##CrossHairsHud" ) ) {
		for ( auto &&item : defHud->crossHairs() ) {
			if ( ImGui::BeginTabItem( item->wgtName().data() ) ) {
				{
					ImGui::BeginChild( ( item->wgtName().data() + std::string( "child" ) ).data() );
					item->draw();
					std::string txName = item->txName().data();
					if ( !isNoTextures() )
						textureSelector(
							"texture##" + std::string( item->wgtName() ), txName,
							[&]( RwTexture *tx ) { item->setImage( txName, tx ); }, true, "default" );
					if ( ImGui::Button( "Reset all changes##CrossHairsHud" ) ) item->reset();
					ImGui::EndChild();
				}
				ImGui::EndTabItem();
			}
		}
		ImGui::EndTabBar();
	}
}

void Config::drawFonts( DefaultHud *defHud ) {
	if ( isNoTextures() ) {
		ImGui::Text( "No find any textures" );
		return;
	}
	textureSelector(
		"font2##custom_fonts", _fonts[0],
		[&]( RwTexture *tx ) { defHud->setFontTexture( DefaultHud::FontId::font2, tx ); }, true, "default" );
	textureSelector(
		"font1##custom_fonts", _fonts[1],
		[&]( RwTexture *tx ) { defHud->setFontTexture( DefaultHud::FontId::font1, tx ); }, true, "default" );
	if ( ImGui::Button( "Select fonts.dat" ) ) {
		if ( !fileExplorer ) {
			fileExplorer = new ImGui::FileBrowser;
			fileExplorer->SetTitle( "Select fonts.dat" );
			fileExplorer->SetTypeFilters( { ".dat" } );
		}
		fileExplorer->Open();
	}
	if ( !_fonts_dat.empty() ) {
		ImGui::SameLine( 0, 10 );
		if ( ImGui::Button( "Remove fonts.dat" ) ) {
			_fonts_dat.clear();
			memcpy( defHud->fontData, (DefaultHud::tFontData *)0xC718B0, sizeof( defHud->fontData ) );
		}
	}
	if ( fileExplorer ) fileExplorer->Display();
	if ( fileExplorer && !fileExplorer->IsOpened() && fileExplorer->HasSelected() ) {
		_fonts_dat.clear();
		std::ifstream f( fileExplorer->GetSelected(), std::ios::binary );
		if ( f.is_open() ) {
			for ( int i = 0; !f.eof(); ++i ) _fonts_dat.push_back( static_cast<byte>( f.get() ) );
			f.close();
		}
		parseFontsDat( defHud );
		delete fileExplorer;
		fileExplorer = nullptr;
	}
}

void Config::parseFontsDat( DefaultHud *defHud ) {
	if ( !_fonts_dat.empty() ) {
		auto lines = SRString( _fonts_dat ).split( '\n' );
		enum class action { none, id, prop, unprop, space };
		int	   fontId	  = 0;
		int	   fontOffset = 0;
		action act		  = action::none;
		for ( auto &&line : lines ) {
			line.replace_all( "\r", " " );
			line.replace_all( ",", " " );
			auto comment = line.find( "#" );
			if ( comment != std::string::npos ) {
				if ( comment )
					line = line.substr( 0, comment );
				else
					line.clear();
			}

			while ( !line.empty() && ( line.front() == ' ' || line.front() == '\t' ) ) line.erase( 0, 1 );
			while ( !line.empty() && ( line.back() == ' ' || line.back() == '\t' ) ) line.pop_back();

			if ( line.empty() ) continue;

			if ( line.starts_with( "[FONT_ID]" ) ) {
				act = action::id;
				continue;
			} else if ( line.starts_with( "[PROP]" ) ) {
				act = action::prop;
				continue;
			} else if ( line.starts_with( "[UNPROP]" ) ) {
				act = action::unprop;
				continue;
			} else if ( line.starts_with( "[REPLACEMENT_SPACE_CHAR]" ) ) {
				act = action::space;
				continue;
			}

			if ( act == action::none || fontId < 0 || fontId > 1 ) continue;

			if ( act == action::id ) {
				sscanf( line.data(), "%d", &fontId );
				act = action::none;
			} else if ( act == action::prop && fontOffset < 208 ) {
				int p[8]{ 0 };
				sscanf( line.data(), "%d  %d  %d  %d  %d  %d  %d  %d", &p[0], &p[1], &p[2], &p[3], &p[4], &p[5], &p[6],
						&p[7] );
				defHud->fontData[fontId].m_propValues[fontOffset + 0] = p[0];
				defHud->fontData[fontId].m_propValues[fontOffset + 1] = p[1];
				defHud->fontData[fontId].m_propValues[fontOffset + 2] = p[2];
				defHud->fontData[fontId].m_propValues[fontOffset + 3] = p[3];
				defHud->fontData[fontId].m_propValues[fontOffset + 4] = p[4];
				defHud->fontData[fontId].m_propValues[fontOffset + 5] = p[5];
				defHud->fontData[fontId].m_propValues[fontOffset + 6] = p[6];
				defHud->fontData[fontId].m_propValues[fontOffset + 7] = p[7];
				fontOffset += 8;
				if ( fontOffset >= 208 ) act = action::none;
			} else if ( act == action::unprop ) {
				int value = 0;
				sscanf( line.data(), "%d", &value );
				defHud->fontData[fontId].m_unpropValue = value;
				act									   = action::none;
			} else if ( act == action::space ) {
				int value = 0;
				sscanf( line.data(), "%d", &value );
				defHud->fontData[fontId].m_spaceValue = value;
				act									  = action::none;
			}
		}
	}
}

void Config::textureSelector( std::string name, std::string &current,
							  const std::function<void( RwTexture * )> &callback, bool empty, std::string empty_name ) {
#ifdef _MSC_VER
	auto caller = size_t( _ReturnAddress() );
#else
	auto caller = size_t( __builtin_return_address( 0 ) );
#endif

	auto bufId = name + std::to_string( caller );

	if ( empty && current.empty() && !empty_name.empty() ) current = empty_name;

	ImGui::SetNextItemWidth( 220 );
	if ( ImGui::InputText( ( "##png_filter_" + name ).data(), _txFiltBuf[bufId], 255,
						   ImGuiInputTextFlags_EnterReturnsTrue ) ) {
		if ( !_txFiltBuf[bufId] && empty ) strncpy( _txFiltBuf[bufId], empty_name.data(), 255 );
		current = _txFiltBuf[bufId];
		auto tx = texture( _txFiltBuf[bufId] );
		callback( tx );
		return;
	}
	if ( !ImGui::IsItemFocused() && !*_txFiltBuf[bufId] ) {
		strncpy( _txFiltBuf[bufId], current.data(), 255 );
	}
	if ( ImGui::IsItemHovered() ) {
		auto tx = texture( _txFiltBuf[bufId] );
		if ( tx ) texturePreview( tx );
	}
	ImGui::SameLine();
	if ( ImGui::BeginCombo( ( name + "##combo_png_sel" ).data(), current.data(),
							ImGuiComboFlags_PopupAlignLeft | ImGuiComboFlags_HeightLarge |
								ImGuiComboFlags_NoPreview ) ) {

		if ( empty ) {
			bool selected = empty_name == current;
			if ( ImGui::Selectable( ( empty_name + "##png_sel_def" + name ).data(), selected ) ) {
				if ( empty_name != _txFiltBuf[bufId] ) strncpy( _txFiltBuf[bufId], empty_name.data(), 255 );
				current = empty_name;
				callback( nullptr );
			}
			if ( selected ) ImGui::SetItemDefaultFocus();
		}

		for ( int id = 0; auto &&[txName, txData] : _textures ) {
			if ( txName.empty() ) continue;
			if ( current != _txFiltBuf[bufId] && txName.find( _txFiltBuf[bufId] ) == std::string::npos ) continue;

			auto tx = _textures[txName];
			if ( !tx.len || !tx.src ) continue;

			if ( !tx.tx ) tx.tx = Itembase::loadPNG( (uint8_t *)tx.src, tx.len );

			bool selected = txName == current;

			if ( ImGui::Selectable( ( txName + "##png_sel_" + std::to_string( id++ ) + name ).data(), selected ) ) {
				if ( txName != _txFiltBuf[bufId] ) strncpy( _txFiltBuf[bufId], txName.data(), 255 );
				current = txName;
				callback( tx.tx );
			}

			if ( ImGui::IsItemHovered() ) texturePreview( tx.tx );

			if ( selected ) ImGui::SetItemDefaultFocus();
		}

		ImGui::EndCombo();
	}
}

void Config::texturePreview( RwTexture *tx ) {
	ImGui::PushStyleColor( ImGuiCol_PopupBg, ImVec4( .941f, .941f, .941f, .941f ) );
	ImGui::BeginTooltip();
	ImGui::Image( tx->raster->rwD3D9RasterExt()->texture, { 180, 120 } );
	ImGui::EndTooltip();
	ImGui::PopStyleColor();
}

void Config::initDefaultHud( DefaultHud *defHud ) {

	tiny::TinyJson json;
	json.ReadJson( _config_json );
	{
		auto jHud = json.Get<tiny::xobject>( "DefaultHUD" );
		defHud->read( jHud );
		defHud->setRadarTexture();
		defHud->setAirPlaneTexture();
		for ( int i = 0; i < 47; ++i ) defHud->setWeaponTexture( i );
	}
	auto r = json.Get<tiny::xobject>( "Replacements" );
	for ( int i = 0; i < r.Count(); ++i ) {
		r.Enter( i );
		_radardisc = r.Get<std::string>( "radardisc", "built-in" );
		if ( !_radardisc.empty() && _radardisc != "built-in" ) {
			defHud->setRadarTexture( texture( _radardisc ) );
		}
		_airPlane = r.Get<std::string>( "airplane", "built-in" );
		if ( !_airPlane.empty() && _airPlane != "built-in" ) {
			defHud->setAirPlaneTexture( texture( _airPlane ) );
		}
		auto radar_icons = defHud->radarIcons();
		for ( auto &&icon : radar_icons ) {
			auto name = r.Get<std::string>( icon, "default" );
			auto tx	  = texture( name );
			defHud->setRadarIcon( icon, name, tx );
		}
		for ( int i = 0; i < 47; ++i ) {
			auto txName = r.Get<std::string>( "Weapon" + std::to_string( i ), "default" );
			if ( txName.empty() || txName == "default" ) continue;
			_weapons[i] = txName;
			defHud->setWeaponTexture( i, texture( txName ) );
		}
		for ( auto &&cs : defHud->crossHairs() ) {
			if ( cs->txName().empty() ) continue;
			if ( cs->txName() == "default" ) continue;
			auto tx = texture( cs->txName() );
			cs->setImage( cs->txName(), tx );
		}
		_fonts[0] = r.Get<std::string>( "font2", "default" );
		if ( !_fonts[0].empty() && _fonts[0] != "default" ) {
			defHud->setFontTexture( DefaultHud::FontId::font2, texture( _fonts[0] ) );
		}
		_fonts[1] = r.Get<std::string>( "font1", "default" );
		if ( !_fonts[1].empty() && _fonts[1] != "default" ) {
			defHud->setFontTexture( DefaultHud::FontId::font1, texture( _fonts[1] ) );
		}
	}
	parseFontsDat( defHud );
	{
		{
			auto images = json.Get<tiny::xarray>( "images" );
			for ( int i = 0; i < images.Count(); ++i ) {
				images.Enter( i );
				auto name	= images.Get<std::string>();
				auto img	= new Image( this, name );
				auto jimage = images.Get<tiny::xobject>( name );
				for ( int i = 0; i < jimage.Count(); ++i ) {
					jimage.Enter( i );
					img->read( jimage );
				}
				auto tx = texture( img->txName() );
				if ( tx )
					img->setImage( img->txName(), tx );
				else
					img->setImage( "" );
				_items.push_back( img );
			}
		}
		{
			auto fonts = json.Get<tiny::xarray>( "fonts" );
			for ( int i = 0; i < fonts.Count(); ++i ) {
				fonts.Enter( i );
				auto name  = fonts.Get<std::string>();
				auto fnt   = new Font( this, name );
				auto jfont = fonts.Get<tiny::xobject>( name );
				for ( int i = 0; i < jfont.Count(); ++i ) {
					jfont.Enter( i );
					fnt->read( jfont );
				}
				_items.push_back( fnt );
			}
		}
		{
			auto charts = json.Get<tiny::xarray>( "charts" );
			for ( int i = 0; i < charts.Count(); ++i ) {
				charts.Enter( i );
				auto name	= charts.Get<std::string>();
				auto chrt	= new Chart( this, name );
				auto jchart = charts.Get<tiny::xobject>( name );
				for ( int i = 0; i < jchart.Count(); ++i ) {
					jchart.Enter( i );
					chrt->read( jchart );
				}
				_items.push_back( chrt );
			}
		}
	}
}

bool Config::initDefaultHud2( DefaultHud *defHud ) {
	auto json = nlohmann::json::parse( _config_json );
	{
		try {
			auto jHud = json["DefaultHUD"];
			defHud->read2( jHud );
		} catch ( nlohmann::detail::exception &e ) {
			return false;
		}

		defHud->setRadarTexture();
		defHud->setAirPlaneTexture();
		for ( int i = 0; i < 47; ++i ) defHud->setWeaponTexture( i );
	}
	try {
		auto r	   = json["Replacements"];
		_radardisc = r.value<std::string>( "radardisc", "built-in" );
		if ( !_radardisc.empty() && _radardisc != "built-in" ) {
			defHud->setRadarTexture( texture( _radardisc ) );
		}
		_airPlane = r.value<std::string>( "airplane", "built-in" );
		if ( !_airPlane.empty() && _airPlane != "built-in" ) {
			defHud->setAirPlaneTexture( texture( _airPlane ) );
		}
		auto radar_icons = defHud->radarIcons();
		for ( auto &&icon : radar_icons ) {
			auto name = r.value<std::string>( icon, "default" );
			auto tx	  = texture( name );
			defHud->setRadarIcon( icon, name, tx );
		}
		for ( int i = 0; i < 47; ++i ) {
			auto txName = r.value<std::string>( "Weapon" + std::to_string( i ), "default" );
			if ( txName.empty() || txName == "default" ) continue;
			_weapons[i] = txName;
			defHud->setWeaponTexture( i, texture( txName ) );
		}
		for ( auto &&cs : defHud->crossHairs() ) {
			if ( cs->txName().empty() ) continue;
			if ( cs->txName() == "default" ) continue;
			auto tx = texture( cs->txName() );
			cs->setImage( cs->txName(), tx );
		}
		_fonts[0] = r.value<std::string>( "font2", "default" );
		if ( !_fonts[0].empty() && _fonts[0] != "default" ) {
			defHud->setFontTexture( DefaultHud::FontId::font2, texture( _fonts[0] ) );
		}
		_fonts[1] = r.value<std::string>( "font1", "default" );
		if ( !_fonts[1].empty() && _fonts[1] != "default" ) {
			defHud->setFontTexture( DefaultHud::FontId::font1, texture( _fonts[1] ) );
		}
	} catch ( nlohmann::detail::exception &e ) {
		return false;
	}
	parseFontsDat( defHud );
	{
		try {
			auto images = json["images"];
			for ( auto &&[id, jVal] : images.items() ) {
				for ( auto &&[key, val] : jVal.items() ) {
					auto img = new Image( this, key );
					img->read2( val );
					auto tx = texture( img->txName() );
					if ( tx )
						img->setImage( img->txName(), tx );
					else
						img->setImage( "" );
					_items.push_back( img );
				}
			}
		} catch ( nlohmann::detail::exception &e ) {
			return false;
		}
		try {
			auto fonts = json["fonts"];
			for ( auto &&[id, jVal] : fonts.items() ) {
				for ( auto &&[key, val] : jVal.items() ) {
					auto fnt = new Font( this, key );
					fnt->read2( val );
					_items.push_back( fnt );
				}
			}
		} catch ( nlohmann::detail::exception &e ) {
			return false;
		}
		try {
			auto charts = json["charts"];
			for ( auto &&[id, jVal] : charts.items() ) {
				for ( auto &&[key, val] : jVal.items() ) {
					auto fnt = new Chart( this, key );
					fnt->read2( val );
					_items.push_back( fnt );
				}
			}
		} catch ( nlohmann::detail::exception &e ) {
			return false;
		}
	}
	return true;
}

std::string_view Config::getRadardist() {
	return _radardisc;
}

std::string_view Config::getAirPlane() {
	return _airPlane;
}

std::string_view Config::getWeapon( int id ) {
	if ( id < 0 || id > 46 ) return "default";
	return _weapons[id];
}

std::string_view Config::getFont( int id ) {
	if ( id == 2 )
		return _fonts[0];
	else if ( id == 1 )
		return _fonts[1];
	return "";
}

void Config::setFontTexture( class DefaultHud *defHud, int id, std::string txName ) {
	auto tx = texture( txName );
	if ( !tx ) txName = "default";

	if ( id == 2 ) {
		_fonts[0] = txName;
		defHud->setFontTexture( DefaultHud::FontId::font2, tx );
	} else if ( id == 1 ) {
		_fonts[1] = txName;
		defHud->setFontTexture( DefaultHud::FontId::font1, tx );
	}
}

bool Config::reset( DefaultHud *defHud, bool ignoreFile ) {
	if ( !ignoreFile && !std::filesystem::exists( _archive ) ) return false;

	for ( auto &&item : _items ) delete item;
	_items.clear();
	defHud->setRadarTexture();
	defHud->setAirPlaneTexture();
	defHud->setFontTexture( DefaultHud::FontId::together );
	memcpy( defHud->fontData, (DefaultHud::tFontData *)0xC718B0, sizeof( defHud->fontData ) );
	for ( int i = 0; i < 47; ++i ) defHud->setWeaponTexture( i );
	for ( auto &&item : defHud->items() ) item->reset();
	for ( auto &&item : defHud->crossHairs() ) item->reset();
	for ( auto &&[name, data] : _textures ) free( data.src );
	_textures.clear();
	scripts.clear();
	globFloat.clear();
	globInt.clear();
	globString.clear();
	if ( !ignoreFile ) {
		auto result = read();
		if ( result ) {
			if ( !initDefaultHud2( defHud ) ) {
				//				MessageBoxA( 0, "Fallback to old configs", "Can't read config", 0 );
				try {
					initDefaultHud( defHud );
				} catch ( ... ) {
				}
			}
		}
		return result;
	} else {
		author = "ROCKSTAR GAMES";
		name   = "Default HUD";
		desc   = "The default game HUD, dumped at game start";
	}
	return true;
}

bool Config::read() {
	SYSTEM_INFO sysInfo;
	GetSystemInfo( &sysInfo );

	int	 errorp;
	auto z = zip_open( _archive.data(), ZIP_RDONLY, &errorp );
	if ( !z ) return false;

	for ( int i = 0; i < zip_get_num_entries( z, 0 ); i++ ) {
		struct zip_stat st;
		if ( zip_stat_index( z, i, 0, &st ) != 0 ) continue;

		if ( !st.size ) continue;

		auto zf	  = zip_fopen_index( z, i, 0 );
		auto file = new char[st.size + 1]{ 0 };

		SRString			manifest;
		decltype( st.size ) sum = 0;
		while ( sum != st.size ) {
			char buf[sysInfo.dwPageSize];
			auto len = zip_fread( zf, buf, sysInfo.dwPageSize );
			buf[len] = 0;
			memcpy( file + sum, buf, len );
			sum += len;
		}

		if ( strcmp( st.name, "Manifest" ) == 0 ) {
			auto manInfo = SRString( file ).split( '\n' );
			if ( manInfo.size() >= 3 ) {
				name = manInfo.front().str_v();
				manInfo.pop_front();
				author = manInfo.front().str_v();
				manInfo.pop_front();
				desc.clear();
				for ( auto &&str : manInfo ) desc += str.str() + "\n";
			}
		} else if ( strcmp( st.name, "config.json" ) == 0 )
			_config_json = file;
		else if ( strstr( st.name, "res/" ) == st.name ) {
			auto name = std::string( st.name + 4 );
			if ( name.substr( name.length() - 4 ) == ".png" ) {
				TextureData data;
				data.src = malloc( st.size );
				data.len = st.size;
				data.tx	 = Itembase::loadPNG( (uint8_t *)file, st.size );
				memcpy( data.src, file, st.size );
				_textures[name.substr( 0, name.length() - 4 )] = data;
			} else if ( name == "fonts.dat" ) {
				_fonts_dat = file;
			}
		} else if ( strstr( st.name, "scripts/" ) == st.name ) {
			auto name = std::string( st.name + 8 );
			if ( name.substr( name.length() - 4 ) == ".lua" ) scripts[name.substr( 0, name.length() - 4 )] = file;
		}

		delete[] file;
		zip_fclose( zf );
	}

	zip_close( z );
	return true;
}

bool Config::write( DefaultHud *defHud, std::string_view archive ) {
	try {
		auto json	= nlohmann::json::object();
		auto images = nlohmann::json::array();
		auto fonts	= nlohmann::json::array();
		auto charts = nlohmann::json::array();
		for ( auto &&item : _items ) {
			auto img  = dynamic_cast<Image *>( item );
			auto fnt  = dynamic_cast<Font *>( item );
			auto chrt = dynamic_cast<Chart *>( item );
			if ( img ) {
				auto image			  = nlohmann::json::object();
				image[img->imgName()] = img->write2();
				images.push_back( image );
			} else if ( fnt ) {
				auto font			 = nlohmann::json::object();
				font[fnt->imgName()] = fnt->write2();
				fonts.push_back( font );
			} else if ( chrt ) {
				auto chart			   = nlohmann::json::object();
				chart[chrt->imgName()] = chrt->write2();
				charts.push_back( chart );
			}
		}
		json["DefaultHUD"] = defHud->write2();
		json["images"]	   = images;
		json["fonts"]	   = fonts;
		json["charts"]	   = charts;
		auto replacements  = nlohmann::json::object();
		if ( !_radardisc.empty() && _radardisc != "built-in" ) replacements["radardisc"] = _radardisc;
		if ( !_airPlane.empty() && _airPlane != "built-in" ) replacements["airplane"] = _airPlane;
		auto radar_icons = defHud->radarIcons();
		for ( auto &&icon : radar_icons ) {
			auto name = defHud->radarIcon( icon );
			if ( !name.empty() && name != "default" ) replacements[icon] = name;
		}
		if ( !_fonts[0].empty() && _fonts[0] != "default" ) replacements["font2"] = _fonts[0];
		if ( !_fonts[1].empty() && _fonts[1] != "default" ) replacements["font1"] = _fonts[1];
		for ( int i = 0; i < 47; ++i )
			if ( !_weapons[i].empty() && _weapons[i] != "default" )
				replacements["Weapon" + std::to_string( i )] = _weapons[i];
		json["Replacements"] = replacements;

		_config_json = json.dump( 4 );
	} catch ( nlohmann::detail::exception &e ) {
		MessageBoxA( 0, e.what(), "Save error", 0 );
	}

	SYSTEM_INFO sysInfo;
	GetSystemInfo( &sysInfo );

	int	 errorp;
	auto z = zip_open( archive.data(), ZIP_CREATE, &errorp );
	if ( !z ) return false;

	auto pushFile = [&]( std::string_view name, void *src, size_t len ) {
		auto source = zip_source_buffer( z, src, len, 0 );
		if ( zip_file_add( z, name.data(), source, 0 ) < 0 ) {
			zip_source_free( source );
			zip_close( z );
			return false;
		}
		return true;
	};

	auto pushDir = [&]( std::string_view name ) {
		if ( zip_dir_add( z, name.data(), 0 ) < 0 ) {
			zip_close( z );
			return false;
		}
		return true;
	};

	auto info = name + "\n" + author + "\n" + desc;
	if ( !pushFile( "Manifest", info.data(), info.length() ) ) return false;
	if ( !pushFile( "config.json", _config_json.data(), _config_json.length() ) ) return false;
	if ( !pushDir( "res" ) ) return false;
	for ( auto &&[name, data] : _textures )
		if ( data.len )
			if ( !pushFile( "res/" + name + ".png", data.src, data.len ) ) return false;
	if ( !_fonts_dat.empty() && !pushFile( "res/fonts.dat", _fonts_dat.data(), _fonts_dat.length() ) ) return false;
	if ( !pushDir( "scripts" ) ) return false;
	for ( auto &&[name, data] : scripts )
		if ( !data.empty() )
			if ( !pushFile( "scripts/" + name + ".lua", data.data(), data.length() ) ) return false;


	zip_close( z );
	return true;
}

std::string get_name_by_id( int userWeaponID ) {
	for ( UINT i = 0; i < 45; i++ )
		if ( weapon_list[i].id == userWeaponID ) return weapon_list[i].name;

	return "Error";
};

int get_slot_by_id( int userWeaponID ) {
	for ( UINT i = 0; i < 45; i++ )
		if ( weapon_list[i].id == userWeaponID ) return weapon_list[i].slot;

	return -1;
}

const char *get_slot_name( int slot ) {
	switch ( slot ) {
		case 0:
			return "Hands##slots";
		case 1:
			return "Melee##slots";
		case 2:
			return "Pistols##slots";
		case 3:
			return "Shotguns##slots";
		case 4:
			return "Submachine guns##slots";
		case 5:
			return "Machine guns##slots";
		case 6:
			return "Rifles##slots";
		case 7:
			return "Bigs##slots";
		case 8:
			return "Grenades##slots";
		case 9:
			return "Items##slots";
		case 10:
			return "Sexs##slots";
		case 11:
			return "Extra##slots";
		case 12:
			return "Remotes##slots";
		default:
			return "WTF##slots";
	}
}
