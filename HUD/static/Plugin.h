#ifndef PLUGIN_H
#define PLUGIN_H

#include <SRDescent/SRDescent.h>
#include <filesystem>
#include <windows.h>

namespace sol {
	class state;
}

class Plugin : public SRDescent {
	typedef bool ( *init_lua_t )( sol::state * );
	typedef void ( *init_ide_t )();
	typedef void ( *reset_t )();
	typedef void ( *begin_t )();
	typedef void ( *end_t )();

	HMODULE	   _handle	 = nullptr;
	init_lua_t _init_lua = nullptr;
	init_ide_t _init_ide = nullptr;
	reset_t	   _reset	 = nullptr;
	begin_t	   _begin	 = nullptr;
	end_t	   _end		 = nullptr;

public:
	Plugin() = delete;
	Plugin( HMODULE lib, SRDescent *parent );
	virtual ~Plugin();

	bool inited = false;

	bool init_lua( sol::state *lua );
	void init_ide();
	void reset();
	void begin();
	void end();
};

#endif // PLUGIN_H
