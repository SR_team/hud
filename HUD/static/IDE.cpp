#include "IDE.h"
#include "loader/loader.h"
#include <SRString/SRString.h>
#include <windows.h>

IDE::IDE( SRDescent *parent ) : SRDescent( parent ) {
	reset();

	g_class.events->onKeyPressed += [this]( int key ) {
		if ( !openned ) return;
		if ( key != VK_SPACE && key != VK_TAB && key != 'S' ) return;
		if ( !g_class.events->isKeyDown( VK_LCONTROL ) ) return;
		g_class.events->hookEvent();
		editor.ToggleCompletion();
	};
}

void IDE::open() {
	openned = true;
}

void IDE::close() {
	openned = false;
}

void IDE::draw() {
	if ( !openned ) return;
	ImGui::StyleColorsDark();
	ImGui::SetNextWindowSize( { 640, 360 }, ImGuiCond_FirstUseEver );
	ImGui::Begin( "Lua code", &openned,
				  ImGuiWindowFlags_HorizontalScrollbar | ImGuiWindowFlags_MenuBar |
					  ImGuiWindowFlags_NoBringToFrontOnFocus );
	{
		if ( ImGui::BeginMenuBar() ) {
			if ( ImGui::BeginMenu( "Edit" ) ) {
				bool ro = editor.IsReadOnly();

				if ( ImGui::MenuItem( "Undo", "Ctrl-Z", nullptr, !ro && editor.CanUndo() ) ) editor.Undo();
				if ( ImGui::MenuItem( "Redo", "Ctrl-Y", nullptr, !ro && editor.CanRedo() ) ) editor.Redo();

				ImGui::Separator();

				if ( ImGui::MenuItem( "Copy", "Ctrl-C", nullptr, editor.HasSelection() ) ) editor.Copy();
				if ( ImGui::MenuItem( "Cut", "Ctrl-X", nullptr, !ro && editor.HasSelection() ) ) editor.Cut();
				if ( ImGui::MenuItem( "Delete", "Del", nullptr, !ro && editor.HasSelection() ) ) editor.Delete();
				if ( ImGui::MenuItem( "Paste", "Ctrl-V", nullptr, !ro && ImGui::GetClipboardText() != nullptr ) )
					editor.Paste();

				ImGui::Separator();

				if ( ImGui::MenuItem( "Select all", nullptr, nullptr ) )
					editor.SetSelection( TextEditor::Coordinates(),
										 TextEditor::Coordinates( editor.GetTotalLines(), 0 ) );
				if ( ImGui::MenuItem( "Autocompletion", "Ctrl-Tab", nullptr, !ro && editor.HasCompletionAvalaible() ) )
					editor.ToggleCompletion( true );

				ImGui::EndMenu();
			}

			ImGui::EndMenuBar();
		}
		if ( ImGui::BeginMenuBar() ) {
			if ( ImGui::BeginMenu( "Help" ) ) {
				if ( ImGui::MenuItem( "HUD API", NULL, nullptr, !help_API ) ) help_API = true;
				if ( ImGui::MenuItem( "Lua" ) )
					ShellExecuteA( NULL, "open", "https://www.lua.org/manual/", NULL, NULL, SW_SHOWNORMAL );
				if ( ImGui::MenuItem( "Editor", NULL, nullptr, !help_editor ) ) help_editor = true;

				ImGui::EndMenu();
			}

			ImGui::EndMenuBar();
		}
		auto cpos = editor.GetCursorPosition();
		ImGui::Text( "%6d/%-6d %6d lines  | %s | %s", cpos.mLine + 1, cpos.mColumn + 1, editor.GetTotalLines(),
					 editor.IsOverwrite() ? "Ovr" : "Ins", editor.CanUndo() ? "*" : " " );
		editor.Render( "TextEditor" );
	}
	ImGui::End();

	helpApi();
	helpIde();

	ImGui::StyleColorsLight();

	if ( !openned ) onClose();
}

bool IDE::isOpen() const {
	return openned;
}

std::string IDE::text() const {
	return editor.GetText();
}

void IDE::text( std::string_view code ) {
	editor.SetText( code.data() );
}

void IDE::addFunction( std::string_view fname, std::string_view desc ) {
	TextEditor::Identifier id;
	id.mDeclaration					= desc;
	auto lang						= editor.GetLanguageDefinition();
	lang.mIdentifiers[fname.data()] = id;
	editor.SetLanguageDefinition( lang );
}

void IDE::setError( int line, std::string_view err ) {
	if ( !line )
		editor.SetErrorMarkers( {} );
	else if ( line == -1 ) {
		auto					 pos = editor.GetCursorPosition();
		TextEditor::ErrorMarkers markers;
		markers.insert( { pos.mLine + 1, err.data() } );
		editor.SetErrorMarkers( markers );
	} else {
		TextEditor::ErrorMarkers markers;
		markers.insert( { line, err.data() } );
		editor.SetErrorMarkers( markers );
	}
}

void IDE::reset() {
	editor.SetLanguageDefinition( TextEditor::LanguageDefinition::Lua() );
}

void IDE::helpApi() {
	if ( !help_API ) return;
	auto		funcs = editor.GetLanguageDefinition().mIdentifiers;
	static char flt[256]{ 0 };

	auto drawDoc = [this]( std::string_view func, TextEditor::Identifier id ) {
		auto doc = createFDoc( id.mDeclaration );
		if ( ImGui::CollapsingHeader( func.data() ) ) {
			if ( !doc.example.rets.empty() ) {
				ImGui::TextColored( { .333, .611, .839, 1 }, "local" );
				ImGui::SameLine();
				ImGui::TextColored( { .666, .666, .666, 1 }, "%s", doc.example.rets.data() );
				ImGui::SameLine();
				ImGui::TextColored( { 1, 1, 1, 1 }, "=" );
				ImGui::SameLine();
			}
			ImGui::TextColored( { 0.302, .776, .608, 1 }, "%s", func.data() );
			ImGui::SameLine();
			ImGui::TextColored( { 1, 1, 1, 1 }, "(" );
			if ( !doc.example.args.empty() ) {
				ImGui::SameLine();
				ImGui::TextColored( { .666, .666, .666, 1 }, "%s", doc.example.args.data() );
			}
			ImGui::SameLine();
			ImGui::TextColored( { 1, 1, 1, 1 }, ")" );
			ImGui::Separator();
			ImGui::TextWrapped( "%s", doc.bief.data() );
			if ( !doc.detail.empty() ) ImGui::TextWrapped( "%s", doc.detail.data() );
			ImGui::Spacing();
			if ( !doc.args.empty() ) {
				ImGui::Text( "Arguments:" );
				for ( auto &&arg : doc.args ) ImGui::BulletText( "%s", arg.data() );
			}
			if ( !doc.rets.empty() ) {
				ImGui::Text( "Return:" );
				for ( auto &&ret : doc.rets ) ImGui::BulletText( "%s", ret.data() );
			}
		}
		if ( ImGui::IsItemHovered() ) ImGui::SetTooltip( "%s", doc.bief.data() );
	};

	auto drawTabEx = [&]( std::string_view														 name,
						  const std::function<bool( std::string_view, TextEditor::Identifier )> &filter ) {
		if ( ImGui::BeginTabItem( name.data() ) ) {
			ImGui::InputText( "Filter", flt, 255 );
			{
				ImGui::BeginChild( ( name.data() + std::string( "child" ) ).data() );
				for ( auto &&[func, id] : funcs ) {
					if ( !filter( func, id ) ) continue;
					if ( func.find( flt ) == std::string::npos && id.mDeclaration.find( flt ) == std::string::npos )
						continue;
					drawDoc( func, id );
				}
				ImGui::EndChild();
			}
			ImGui::EndTabItem();
		}
	};

	auto drawTab = [&]( std::string_view name, std::string_view filter ) {
		return drawTabEx( name,
						  [&]( std::string_view func, TextEditor::Identifier ) { return func.starts_with( filter ); } );
	};

	ImGui::SetNextWindowSize( { 705, 512 }, ImGuiCond_FirstUseEver );
	ImGui::Begin( "HUD API", &help_API );
	if ( ImGui::BeginTabBar( "API##helpApi" ) ) {
		drawTab( "Wanted", "wanted_" );
		drawTab( "Radar", "radar_" );
		drawTab( "Ammo", "ammo_" );
		drawTab( "Money", "money_" );
		drawTab( "Weapon", "weapon_" );
		drawTab( "Breath", "breath_" );
		drawTab( "Armour", "armour_" );
		drawTab( "Health", "health_" );
		drawTab( "Images", "image_" );
		drawTab( "Fonts", "font_" );
		drawTab( "Charts", "chart_" );
		drawTab( "Crosshairs", "crosshair_" );
		return drawTabEx( "Misc", [&]( std::string_view func, TextEditor::Identifier id ) {
			if ( func.starts_with( "wanted_" ) ) return false;
			if ( func.starts_with( "radar_" ) ) return false;
			if ( func.starts_with( "ammo_" ) ) return false;
			if ( func.starts_with( "money_" ) ) return false;
			if ( func.starts_with( "weapon_" ) ) return false;
			if ( func.starts_with( "breath_" ) ) return false;
			if ( func.starts_with( "armour_" ) ) return false;
			if ( func.starts_with( "health_" ) ) return false;
			if ( func.starts_with( "image_" ) ) return false;
			if ( func.starts_with( "font_" ) ) return false;
			if ( func.starts_with( "chart_" ) ) return false;
			if ( func.starts_with( "crosshair_" ) ) return false;
			if ( id.mDeclaration == "Built-in function" ) return false;
			return true;
		} );
		ImGui::EndTabBar();
	}
	ImGui::End();
}

void IDE::helpIde() {
	if ( !help_editor ) return;
	ImGui::SetNextWindowSize( { 375, 300 }, ImGuiCond_FirstUseEver );
	ImGui::Begin( "Editor", &help_editor );
	ImGui::TextColored( { 0, 1, 0, 1 }, "Hotkeys:" );
	ImGui::BulletText( "Ctrl+Z - undo" );
	ImGui::BulletText( "Ctrl+Shift+Z - redo" );
	ImGui::BulletText( "Ctrl+Y - redo" );
	ImGui::BulletText( "Ctrl+C - copy" );
	ImGui::BulletText( "Ctrl+X - cut" );
	ImGui::BulletText( "Ctrl+V - paste" );
	ImGui::BulletText( "Ctrl+A - select all" );
	ImGui::BulletText( "Ctrl+Tab - toggle autocompletion" );
	ImGui::Separator();
	ImGui::TextColored( { 0, 1, 0, 1 }, "Autocompletion:" );
	ImGui::BulletText( "Ctrl+Tab - toggle autocompletion" );
	ImGui::BulletText( "Ctrl+S - toggle autocompletion" );
	ImGui::BulletText( "Ctrl+Space - toggle autocompletion" );
	ImGui::TextWrapped( "Autocompletion auto-enabled, when you start typing. If you want manual toggle autocompletion "
						"- just use hotkey above." );
	ImGui::TextWrapped( "Autocompletion work only for internal functions now." );
	ImGui::Separator();
	ImGui::TextColored( { 0, 1, 0, 1 }, "Errors:" );
	ImGui::TextWrapped(
		"When you typing code, this execute in real-time. All errors marked in editor and showed on hover cursor." );
	ImGui::Separator();
	ImGui::TextColored( { 0, 1, 0, 1 }, "Editor credits:" );
	ImGui::BulletText( "Original author - BalazsJako" );
	ImGui::BulletText( "Mod for partial autocompletions - SR_team" );
	ImGui::End();
}

IDE::FDoc IDE::createFDoc( std::string_view desc ) {
	enum class DType { brief, args, rets, detail };
	FDoc  result;
	auto  lines = SRString( desc ).split( '\n' );
	DType type	= DType::brief;

	for ( auto &&line : lines ) {
		if ( line.empty() ) continue;

		if ( line == "Args:" )
			type = DType::args;
		else if ( line == "Return:" )
			type = DType::rets;
		else if ( line.starts_with( " * " ) ) {
			if ( type == DType::args )
				result.args.push_back( line.substr( 3 ) );
			else if ( type == DType::rets )
				result.rets.push_back( line.substr( 3 ) );
		} else if ( type == DType::args || type == DType::rets )
			type = DType::detail;

		if ( type == DType::brief ) {
			if ( result.bief.empty() )
				result.bief = line.data();
			else
				result.bief += line.data();
		} else if ( type == DType::detail ) {
			if ( result.detail.empty() )
				result.detail = line.data();
			else
				result.detail += line.data();
		}
	}

	for ( size_t i = 0; i < result.rets.size(); ++i ) {
		result.example.rets += "res" + std::to_string( i );
		if ( i != result.rets.size() - 1 ) result.example.rets += ", ";
	}
	for ( size_t i = 0; i < result.args.size(); ++i ) {
		result.example.args += "arg" + std::to_string( i );
		if ( i != result.args.size() - 1 ) result.example.args += ", ";
	}

	return result;
}
