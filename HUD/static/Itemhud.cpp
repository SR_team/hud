#include "Itemhud.h"
#include <nlohmann/json.hpp>

Itemhud::Itemhud( SRDescent *parent, std::string_view name, std::string_view id )
	: Itembase( parent, id ), _name{ name } {
	_size	  = { 20, 20 };
	_initPos  = true;
	_initSize = true;
}

RwV2D Itemhud::originalPos() const {
	return _pos;
}

RwV2D Itemhud::originalSize() const {
	return _size;
}

bool Itemhud::isCustomPos() const {
	return true;
}

bool Itemhud::isCustomSize() const {
	return true;
}

void Itemhud::reset() {
	_initPos  = true;
	_initSize = true;
}

void Itemhud::draw() {
	auto s = isShowed();
	ImGui::Checkbox( name( "Show item" ).data(), &s );
	if ( isShowed() != s ) toggleShow( s );

	ImGui::PushItemWidth( 150 );
	ImGui::BeginGroup();
	ImGui::DragFloat( name( "X" ).data(), &_pos.fX, 0.1 );
	ImGui::DragFloat( name( "Y" ).data(), &_pos.fY, 0.1 );
	ImGui::EndGroup();
	ImGui::SameLine( 0, 50 );
	ImGui::BeginGroup();
	ImGui::DragFloat( name( "W" ).data(), &_size.fX, 0.1 );
	ImGui::DragFloat( name( "H" ).data(), &_size.fY, 0.1 );
	ImGui::EndGroup();
	ImGui::PopItemWidth();

	ImGuiColorEditFlags flags = ImGuiColorEditFlags_AlphaBar;
	flags |= ImGuiColorEditFlags_DisplayRGB;
	flags |= ImGuiColorEditFlags_DisplayHex;

	popupColorpicker( *(ImVec4 *)&_color, _backup_color );

	if ( ImGui::ColorButton( name( "Color" ).data(), *(ImVec4 *)&_color, flags, ImVec2( 60, 40 ) ) ) {
		ImGui::OpenPopup( name( "colorpicker" ).data() );
		_backup_color = *(ImVec4 *)&_color;
	}
}

tiny::TinyJson Itemhud::write() {
	auto writeColor = []( float *color ) {
		tiny::TinyJson json;
		json["red"].Set( color[0] );
		json["green"].Set( color[1] );
		json["blue"].Set( color[2] );
		json["alpha"].Set( color[3] );
		return json;
	};
	tiny::TinyJson json;
	if ( isHidden() ) json["show"].Set( (int)isShowed() );

	if ( !customPos().isClear() ) {
		tiny::TinyJson pos;
		pos["fX"].Set( std::to_string( customPos().fX ) );
		pos["fY"].Set( std::to_string( customPos().fY ) );

		json["pos"].Set( pos );
	}

	if ( !customSize().isClear() ) {
		tiny::TinyJson size;
		size["fX"].Set( std::to_string( customSize().fX ) );
		size["fY"].Set( std::to_string( customSize().fY ) );

		json["size"].Set( size );
	}
	if ( *(int *)&_color != -1 ) json["color"].Set( writeColor( _color ) );

	return json;
}

nlohmann::json Itemhud::write2() {
	auto writeColor = []( float *color ) {
		auto json	  = nlohmann::json::object();
		json["red"]	  = color[0];
		json["green"] = color[1];
		json["blue"]  = color[2];
		json["alpha"] = color[3];
		return json;
	};
	auto json = nlohmann::json::object();
	if ( isHidden() ) json["show"] = isShowed();

	if ( !customPos().isClear() ) {
		auto pos  = nlohmann::json::object();
		pos["fX"] = customPos().fX;
		pos["fY"] = customPos().fY;

		json["pos"] = pos;
	}

	if ( !customSize().isClear() ) {
		auto size  = nlohmann::json::object();
		size["fX"] = customSize().fX;
		size["fY"] = customSize().fY;

		json["size"] = size;
	}
	if ( *(int *)&_color != -1 ) json["color"] = writeColor( _color );

	return json;
}

void Itemhud::read( tiny::xobject &json ) {
	auto readRwV2D = []( tiny::xobject &json ) {
		RwV2D res;
		for ( int i = 0; i < json.Count(); i++ ) {
			json.Enter( i );
			res.fX = std::stof( json.Get<std::string>( "fX", "0.0" ) );
			res.fY = std::stof( json.Get<std::string>( "fY", "0.0" ) );
		}
		return res;
	};
	auto readColor = []( tiny::xobject &json, float *color ) {
		for ( int i = 0; i < json.Count(); i++ ) {
			json.Enter( i );
			color[0] = json.Get<float>( "red", 1.0f );
			color[1] = json.Get<float>( "green", 1.0f );
			color[2] = json.Get<float>( "blue", 1.0f );
			color[3] = json.Get<float>( "alpha", 1.0f );
		}
	};

	for ( int i = 0; i < json.Count(); i++ ) {
		json.Enter( i );
		toggleShow( json.Get<int>( "show", 1 ) );
		tiny::xobject jpos = json.Get<tiny::xobject>( "pos" );
		pos( readRwV2D( jpos ) );
		tiny::xobject jsize = json.Get<tiny::xobject>( "size" );
		size( readRwV2D( jsize ) );
		tiny::xobject jcolor = json.Get<tiny::xobject>( "color" );
		readColor( jcolor, _color );
	}
}

void Itemhud::read2( nlohmann::json &json ) {
	auto readRwV2D = []( nlohmann::json &json ) {
		RwV2D res;
		res.fX = json.value<float>( "fX", 0.0f );
		res.fY = json.value<float>( "fY", 0.0f );
		return res;
	};
	auto readColor = []( nlohmann::json &json, float *color ) {
		color[0] = json.value<float>( "red", 1.0f );
		color[1] = json.value<float>( "green", 1.0f );
		color[2] = json.value<float>( "blue", 1.0f );
		color[3] = json.value<float>( "alpha", 1.0f );
	};

	toggleShow( json.value<bool>( "show", true ) );
	try {
		auto jpos = json["pos"];
		pos( readRwV2D( jpos ) );
	} catch ( ... ) {
	}
	try {
		auto jsize = json["size"];
		size( readRwV2D( jsize ) );
	} catch ( ... ) {
	}
	try {
		auto jcolor = json["color"];
		readColor( jcolor, _color );
	} catch ( ... ) {
	}
}

RwRGBA Itemhud::color() const {
	RwRGBA color;
	color.red	= _color[0] * 255.0f;
	color.green = _color[1] * 255.0f;
	color.blue	= _color[2] * 255.0f;
	color.alpha = _color[3] * 255.0f;
	return color;
}

void Itemhud::color( RwRGBA color ) {
	_color[0] = float( color.red ) / 255.0f;
	_color[1] = float( color.green ) / 255.0f;
	_color[2] = float( color.blue ) / 255.0f;
	_color[3] = float( color.alpha ) / 255.0f;
}

std::string Itemhud::imgName() const {
	return _name;
}
