#ifndef CONFIG_H
#define CONFIG_H

#include "Chart.h"
#include "Font.h"
#include "Image.h"
#include <SRDescent/SRDescent.h>
#include <filesystem>
#include <functional>
#include <map>
#include <string>
#include <tinyjson/tinyjson.hpp>

namespace ImGui {
	class FileBrowser;
}

class Config : public SRDescent {
	struct TextureData {
		void *			 src = nullptr;
		size_t			 len = 0;
		class RwTexture *tx	 = nullptr;
		;

		bool operator<( const TextureData &right ) { return len < right.len; }
		bool operator==( const TextureData &right ) { return src == right.src && len == right.len; }
	};

	std::string						 _archive;
	std::map<std::string, char[256]> _txFiltBuf;

	std::deque<class Itembase *>	   _items;
	std::map<std::string, TextureData> _textures;

	std::string _radardisc = "built-in";
	std::string _airPlane  = "built-in";
	std::string _weapons[47]{ "default" }; // tiny::xarray
	std::string _fonts[2]{ "default" };
	std::string _fonts_dat;

	ImGui::FileBrowser *fileExplorer = nullptr;

	std::string _config_json;

public:
	Config( SRDescent *parent, std::string_view archive );
	virtual ~Config();

	bool operator==( const Config &r ) {
		// итемсы
		if ( _items.size() != r._items.size() ) return false;
		for ( auto &&item : _items ) {
			auto fnt = dynamic_cast<Font *>( item );
			auto img = dynamic_cast<Image *>( item );
			if ( fnt ) {
				auto it = std::find_if( r._items.begin(), r._items.end(), [&]( class Itembase *i ) {
					if ( !dynamic_cast<Font *>( i ) ) return false;
					return dynamic_cast<Font *>( i ) == fnt;
				} );
				if ( it == r._items.end() ) return false;
			}
			if ( img ) {
				auto it = std::find_if( r._items.begin(), r._items.end(), [&]( class Itembase *i ) {
					if ( !dynamic_cast<Image *>( i ) ) return false;
					return dynamic_cast<Image *>( i ) == img;
				} );
				if ( it == r._items.end() ) return false;
			}
		}
		// текстуры
		if ( _textures.size() != r._textures.size() ) return false;
		for ( auto &&[key, data] : _textures ) {
			try {
				auto rData = r._textures.at( key );
				if ( rData.len != data.len ) return false;
				if ( memcmp( rData.src, data.src, data.len ) ) return false;
			} catch ( ... ) {
				return false;
			}
		}
		if ( _radardisc != r._radardisc ) return false;
		if ( _airPlane != r._airPlane ) return false;
		for ( int i = 0; i < 47; ++i )
			if ( _weapons[i] != r._weapons[i] ) return false;
		for ( int i = 0; i < 2; ++i )
			if ( _fonts[i] != r._fonts[i] ) return false;
		// скрипты
		if ( scripts.size() != r.scripts.size() ) return false;
		for ( auto &&[name, code] : scripts ) {
			try {
				auto rCode = r.scripts.at( name );
				if ( code != rCode ) return false;
			} catch ( ... ) {
				return false;
			}
		}
		// тексты
		if ( _fonts_dat != r._fonts_dat ) return false;
		if ( _config_json != r._config_json ) return false;
		if ( name != r.name ) return false;
		if ( author != r.author ) return false;
		if ( desc != r.desc ) return false;

		return true;
	}

	const std::deque<Itembase *> &items() const;

	std::string ImName() const;

	std::map<std::string, std::string> scripts;

	enum class AddTexCode { success, no_access, name_busy };
	AddTexCode addTexture( const std::filesystem::path &file, std::string_view name );
	RwTexture *texture( std::string_view name );
	bool	   isNoTextures();

	Itembase *addImage( std::string_view name );
	Itembase *addFont( std::string_view name );
	Itembase *addChart( std::string_view name );

	bool removeItem( Itembase *item );

	void drawTextureList( class DefaultHud *defHud );
	bool drawCustomItems();
	void drawDefaultHud( class DefaultHud *defHud );
	void drawCrossHairs( class DefaultHud *defHud );
	void drawFonts( class DefaultHud *defHud );

	void parseFontsDat( class DefaultHud *defHud );

	void textureSelector( std::string name, std::string &current, const std::function<void( RwTexture * )> &callback,
						  bool empty = false, std::string empty_name = "" );
	static void texturePreview( RwTexture *tx );

	void initDefaultHud( class DefaultHud *defHud );
	bool initDefaultHud2( class DefaultHud *defHud );

	std::string_view getRadardist();
	std::string_view getAirPlane();
	std::string_view getWeapon( int id );
	std::string_view getFont( int id );

	void setFontTexture( class DefaultHud *defHud, int id, std::string txName = "default" );

	std::string name;
	std::string author;
	std::string desc;

	std::map<std::string, float>	   globFloat;
	std::map<std::string, int>		   globInt;
	std::map<std::string, std::string> globString;

	bool reset( class DefaultHud *defHud, bool ignoreFile = false );
	bool read();
	bool write( class DefaultHud *defHud, std::string_view archive );
};

struct weapon_entry {
	int			id;
	int			slot;
	int			model_id;
	const char *name;
};

std::string get_name_by_id( int userWeaponID );
int			get_slot_by_id( int userWeaponID );
const char *get_slot_name( int slot );

const inline weapon_entry weapon_list[] = { { 0, 0, -1, "Fist" },
											{ 1, 0, 331, "Brass Knuckles" },
											{ 2, 1, 333, "Golf Club" },
											{ 3, 1, 334, "Nitestick" },
											{ 4, 1, 335, "Knife" },
											{ 5, 1, 336, "Baseball Bat" },
											{ 6, 1, 337, "Shovel" },
											{ 7, 1, 338, "Pool Cue" },
											{ 8, 1, 339, "Katana" },
											{ 9, 1, 341, "Chainsaw" },
											{ 22, 2, 346, "Pistol" },
											{ 23, 2, 347, "Silenced Pistol" },
											{ 24, 2, 348, "Desert Eagle" },
											{ 25, 3, 349, "Shotgun" },
											{ 26, 3, 350, "Sawn-Off Shotgun" },
											{ 27, 3, 351, "SPAZ12" },
											{ 28, 4, 352, "Micro UZI" },
											{ 29, 4, 353, "MP5" },
											{ 32, 4, 372, "Tech9" },
											{ 30, 5, 355, "AK47" },
											{ 31, 5, 356, "M4" },
											{ 33, 6, 357, "Country Rifle" },
											{ 34, 6, 358, "Sniper Rifle" },
											{ 35, 7, 359, "Rocket Launcher" },
											{ 36, 7, 360, "Heat Seeking RPG" },
											{ 37, 7, 361, "Flame Thrower" },
											{ 38, 7, 362, "Minigun" },
											{ 16, 8, 342, "Grenade" },
											{ 17, 8, 343, "Teargas" },
											{ 18, 8, 344, "Molotov Cocktail" },
											{ 39, 8, 363, "Remote Explosives" },
											{ 41, 9, 365, "Spray Can" },
											{ 42, 9, 366, "Fire Extinguisher" },
											{ 43, 9, 367, "Camera" },
											{ 10, 10, 321, "Dildo 1" },
											{ 11, 10, 322, "Dildo 2" },
											{ 12, 10, 323, "Vibe 1" },
											{ 13, 10, 324, "Vibe 2" },
											{ 14, 10, 325, "Flowers" },
											{ 15, 10, 326, "Cane" },
											{ 44, 11, 368, "NV Goggles" },
											{ 45, 11, 369, "IR Goggles" },
											{ 46, 11, 371, "Parachute" },
											{ 40, 12, 364, "Detonator" },
											{ -1, -1, -1, NULL } };

#endif // CONFIG_H
