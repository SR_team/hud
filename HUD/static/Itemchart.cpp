#include "Itemchart.h"
#include <gtasa/CGame/methods.h>
#include <nlohmann/json.hpp>

Itemchart::Itemchart( SRDescent *parent, std::string_view id ) : Itembase( parent, id ) {}

RwRGBA Itemchart::color() const {
	RwRGBA color;
	color.red	= _color[0] * 255.0f;
	color.green = _color[1] * 255.0f;
	color.blue	= _color[2] * 255.0f;
	color.alpha = _color[3] * 255.0f;
	return color;
}

void Itemchart::color( RwRGBA color ) {
	_color[0] = float( color.red ) / 255.0f;
	_color[1] = float( color.green ) / 255.0f;
	_color[2] = float( color.blue ) / 255.0f;
	_color[3] = float( color.alpha ) / 255.0f;
}

void Itemchart::reset() {
	Itembase::reset();
	percentage	 = false;
	end_line	 = false;
	border		 = true;
	custom_color = false;
}

void Itemchart::draw() {
	if ( _size.fY > 79.5f ) _size.fY = 79.5f;
	Itembase::draw();
	ImGui::Checkbox( name( "Draw percentage" ).data(), &percentage );
	ImGui::SameLine( 0, 10 );
	ImGui::Checkbox( name( "Draw end line" ).data(), &end_line );

	ImGuiColorEditFlags flags = ImGuiColorEditFlags_AlphaBar;
	flags |= ImGuiColorEditFlags_DisplayRGB;
	flags |= ImGuiColorEditFlags_DisplayHex;
	if ( ImGui::ColorButton( name( "Chart color" ).data(), *(ImVec4 *)&_color, flags, ImVec2( 60, 40 ) ) &&
		 custom_color ) {
		ImGui::OpenPopup( name( "colorpicker" ).data() );
		_backup_color = *(ImVec4 *)&_color;
	}
	ImGui::SameLine();
	ImGui::BeginGroup();
	ImGui::Checkbox( name( "Custom chart color" ).data(), &custom_color );
	ImGui::Checkbox( name( "Border" ).data(), &border );
	ImGui::EndGroup();

	popupColorpicker( *(ImVec4 *)&_color, _backup_color );
}

bool Itemchart::isDrag() {
	if ( Itembase::isDrag() ) {
		_dragOffset = mousePos() - pos();
		return true;
	}
	return false;
}

bool Itemchart::processDrag() {
	if ( !_drag ) return false;
	pos( mousePos() - _dragOffset );
	return true;
}

tiny::TinyJson Itemchart::write() {
	auto json = Itembase::write();
	if ( percentage ) json["percentage"].Set( (int)percentage );
	if ( end_line ) json["end_line"].Set( (int)end_line );
	if ( !border ) json["border"].Set( (int)border );

	if ( custom_color ) {
		json["custom_color"].Set( (int)custom_color );
		tiny::TinyJson color;
		color["red"].Set( _color[0] );
		color["green"].Set( _color[1] );
		color["blue"].Set( _color[2] );
		color["alpha"].Set( _color[3] );

		json["color"].Set( color );
	}

	return json;
}

nlohmann::json Itemchart::write2() {
	auto json = Itembase::write2();
	if ( percentage ) json["percentage"] = percentage;
	if ( end_line ) json["end_line"] = end_line;
	if ( !border ) json["border"] = border;

	if ( custom_color ) {
		json["custom_color"] = custom_color;
		auto color			 = nlohmann::json::object();
		color["red"]		 = _color[0];
		color["green"]		 = _color[1];
		color["blue"]		 = _color[2];
		color["alpha"]		 = _color[3];

		json["color"] = color;
	}

	return json;
}

void Itemchart::read( tiny::xobject &json ) {
	auto readColor = []( tiny::xobject &json, float *color ) {
		for ( int i = 0; i < json.Count(); i++ ) {
			json.Enter( i );
			color[0] = json.Get<float>( "red" );
			color[1] = json.Get<float>( "green" );
			color[2] = json.Get<float>( "blue" );
			color[3] = json.Get<float>( "alpha" );
		}
	};
	for ( int i = 0; i < json.Count(); i++ ) {
		json.Enter( i );
		Itembase::read( json );
		percentage	 = json.Get<int>( "percentage", 0 );
		end_line	 = json.Get<int>( "end_line", 0 );
		border		 = json.Get<int>( "border", 1 );
		custom_color = json.Get<int>( "custom_color", 0 );
		if ( custom_color ) {
			tiny::xobject jcolor = json.Get<tiny::xobject>( "color" );
			readColor( jcolor, _color );
		}
	}
}

void Itemchart::read2( nlohmann::json &json ) {
	auto readColor = []( nlohmann::json &json, float *color ) {
		color[0] = json.value<float>( "red", 1.0f );
		color[1] = json.value<float>( "green", 1.0f );
		color[2] = json.value<float>( "blue", 1.0f );
		color[3] = json.value<float>( "alpha", 1.0f );
	};
	Itembase::read2( json );
	percentage	 = json.value<bool>( "percentage", false );
	end_line	 = json.value<bool>( "end_line", false );
	border		 = json.value<bool>( "border", true );
	custom_color = json.value<bool>( "custom_color", false );
	if ( custom_color ) {
		auto jcolor = json["color"];
		readColor( jcolor, _color );
	}
}

void Itemchart::initLuaAndIDE( sol::state &lua, IDE *ide, std::string prefix ) {
	Itembase::initLuaAndIDE( lua, ide, prefix );

	addFunction(
		lua, ide, prefix + "_isPercentage", [this]() { return percentage; },
		"Check is percentage enabled\n\nReturn:\n * bool - is enabled" );
	addFunction(
		lua, ide, prefix + "_isEndLine", [this]() { return end_line; },
		"Check is end line enabled\n\nReturn:\n * bool - is enabled" );
	addFunction(
		lua, ide, prefix + "_isBorder", [this]() { return border; },
		"Check is border enabled\n\nReturn:\n * bool - is enabled" );
	addFunction(
		lua, ide, prefix + "_isCustomColor", [this]() { return custom_color; },
		"Check is custom color enabled\n\nReturn:\n * bool - is enabled" );
	addFunction(
		lua, ide, prefix + "_color",
		[this]() {
			auto clr = color();
			return *(int *)&clr;
		},
		"Get color\n\nReturn:\n * int - color" );
	addFunction(
		lua, ide, prefix + "_togglePercentage", [this]( bool state ) { percentage = state; },
		"Enable/disable percentage\n\nArgs:\n * bool - is enabled" );
	addFunction(
		lua, ide, prefix + "_toggleEndLine", [this]( bool state ) { end_line = state; },
		"Enable/disable end line\n\nArgs:\n * bool - is enabled" );
	addFunction(
		lua, ide, prefix + "_toggleBorder", [this]( bool state ) { border = state; },
		"Enable/disable border\n\nArgs:\n * bool - is enabled" );
	addFunction(
		lua, ide, prefix + "_toggleCustomColor", [this]( bool state ) { custom_color = state; },
		"Enable/disable custom color\n\nArgs:\n * bool - is enabled" );
	addFunction(
		lua, ide, prefix + "_setColor", [this]( int clr ) { color( *(RwRGBA *)&clr ); },
		"Set color\n\nArgs:\n * int - color" );
}

void Itemchart::posHook( float &x, float &y, WORD &w, WORD &h, float &, uint32_t &drawBlueline, bool &drawPercentage,
						 bool &drawBorder, RwRGBA &color, RwRGBA &fore ) {
	_origPos  = WindowScreenToGameScreen( x, y );
	_origSize = WindowScreenToGameScreen( w, h );

	x = ( SCREEN_X / 640.0f ) * pos().fX;
	y = ( SCREEN_Y / 448.0f ) * pos().fY;

	w = ( SCREEN_X / 640.0f ) * size().fX;
	h = ( SCREEN_Y / 448.0f ) * size().fY;

	drawBlueline   = end_line;
	drawPercentage = percentage;
	drawBorder	   = border;

	if ( custom_color ) {
		color.red	= _color[0] * 255.0f;
		color.green = _color[1] * 255.0f;
		color.blue	= _color[2] * 255.0f;
		color.alpha = _color[3] * 255.0f;
	}
	fore.red   = 0xFF - color.red;
	fore.green = 0xFF - color.green;
	fore.blue  = 0xFF - color.blue;
}
