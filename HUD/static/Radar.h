#ifndef RADAR_H
#define RADAR_H

#include "Itembase.h"
#include <llmo/SRHook.hpp>

struct RadarIcon {
	const char *name;
	void *		unk;
};

class Radar : public Itembase {
	SRHook::Hook<>							 drawRadar{ 0x58A33B, 6 };
	SRHook::Hook<int, float, float, uint8_t> drawRadarSprite{ 0x585FF5, 5 };
	SRHook::Hook<>							 posRadarEnd{ 0x58351D, 5 };
	SRHook::Hook<float *, RwRGBA *>			 borderLU{ 0x58A823, 5 };
	SRHook::Hook<float *, RwRGBA *>			 borderRU{ 0x58A8CD, 5 };
	SRHook::Hook<float *, RwRGBA *>			 borderLB{ 0x58A977, 5 };
	SRHook::Hook<float *, RwRGBA *>			 borderRB{ 0x58AA25, 5 };
	SRHook::Hook<RwV2D *>					 limitPoint{ 0x583332, 5 };
	SRHook::Hook<>							 mask{ 0x585758, 6 };
	SRHook::Hook<RwRGBA *>					 mapColor{ 0x5864EB, 5 };
	SRHook::Hook<>							 fixedZoom{ 0x586C9F, 5 };
	SRHook::Hook<>							 airPlane{ 0x58A3F4, 5 };
	SRHook::Hook<RwVRect *>					 hBar{ 0x58A649, 5 };
	SRHook::Hook<RwVRect *>					 hBarLine{ 0x58A77A, 5 };
	SRHook::Hook<RwVRect *>					 drawIcon{ 0x5860F7, 5 };
	SRHook::Hook<CTexture *>				 drawCentre{ 0x588722, 5 };

public:
	Radar( SRDescent *parent, std::string_view id = "" );

	bool north				   = true;
	bool borders			   = true;
	bool custom_border_color   = false;
	bool custom_map_color	   = false;
	bool custom_radardisc	   = false;
	bool fixed_zoom			   = false;
	bool square				   = false;
	bool custom_airPlane_color = false;
	bool custom_airPlane	   = false;
	bool scale_icons		   = false;

	float fixZoom	 = 220.0f;
	float iconsScale = 1.0f;

	float  radius() const;
	RwRGBA colorBorder();
	void   colorBorder( RwRGBA color );
	RwRGBA colorMap() const;
	void   colorMap( RwRGBA color );
	RwRGBA colorAir() const;
	void   colorAir( RwRGBA color );

	void setBorder( RwTexture *texture = nullptr );
	void setAirPlane( RwTexture *texture = nullptr );
	void setIconTexture( std::string icon, std::string txName, RwTexture *texture = nullptr );

	std::string iconTxName( std::string icon ) const;

	virtual void		   reset() override;
	virtual void		   draw() override;
	virtual bool		   isDrag() override;
	virtual bool		   processDrag() override;
	virtual tiny::TinyJson write() override;
	virtual nlohmann::json write2() override;
	virtual void		   read( tiny::xobject &json ) override;
	virtual void		   read2( nlohmann::json &json ) override;
	virtual void		   initLuaAndIDE( sol::state &lua, class IDE *ide, std::string prefix = "" ) override;

protected:
	float															_colorBrd[4]{ 0, 0, 0, 1 };
	float															_colorMap[4]{ 1, 1, 1, 1 };
	float															_colorAir[4]{ 1, 1, 1, 1 };
	ImVec4															_backup_color;
	int																pickId = 0;
	CTexture														_white_radardisc;
	CTexture														_white_radardisc_square;
	CTexture														_custom_radardisc;
	CTexture														_airPlane;
	CTexture														_custom_airPlane;
	RwV2D															_dragOffset;
	mutable std::map<std::string, std::pair<std::string, CTexture>> _icons;

	void showHook( SRHook::CPU &cpu );
	void spriteHook( SRHook::Info &info, int &type, float &, float &, uint8_t &alpha );
	void posHook( SRHook::Info &info );
	void borderHook( SRHook::Hook<float *, RwRGBA *> &hook, float *&, RwRGBA *&color );
	void pointHook( SRHook::Info &info, RwV2D *&pos );
	void maskHook( SRHook::Info &info );
	void mapClHook( RwRGBA *&clr );
	void fZoomHook();
	void airPlaneHook( SRHook::Info &info );
	void hBarHook( RwVRect *&rect );
	void hBarLineHook( RwVRect *&rect );
	void icon( SRHook::CPU &cpu, RwVRect *&rect );
	void centre( CTexture *&tx );
};

#endif // RADAR_H
