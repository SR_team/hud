#ifndef ITEMHUD_H
#define ITEMHUD_H

#include "Itembase.h"

class Itemhud : public Itembase {

public:
	Itemhud( SRDescent *parent, std::string_view name, std::string_view id = "" );

	virtual RwV2D originalPos() const override;
	virtual RwV2D originalSize() const override;

	virtual bool isCustomPos() const override;
	virtual bool isCustomSize() const override;

	virtual void		   reset() override;
	virtual void		   draw() override;
	virtual tiny::TinyJson write() override;
	virtual nlohmann::json write2() override;
	virtual void		   read( tiny::xobject &json ) override;
	virtual void		   read2( nlohmann::json &json ) override;
	virtual void		   initLuaAndIDE( sol::state &, class IDE *, std::string = "" ) override {}

	RwRGBA color() const;
	void   color( RwRGBA color );

	bool operator==( const Itemhud &r ) {
		if ( !Itembase::operator==( r ) ) return false;
		if ( _name != r._name ) return false;
		if ( color() != r.color() ) return false;
		return true;
	}

	virtual void render() {}

	virtual std::string imgName() const;

protected:
	std::string _name;
	float		_color[4]{ 1, 1, 1, 1 };
	ImVec4		_backup_color;
};

#endif // ITEMHUD_H
