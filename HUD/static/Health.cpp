#include "Health.h"
#include <gtasa/CGame/CPed.h>
#include <gtasa/CGame/methods.h>

Health::Health( SRDescent *parent, std::string_view id ) : Itemchart( parent, id ) {
	// init def values
	_size	  = { .25, .25 };
	_wgtName  = "Health";
	_color[0] = .706;
	_color[1] = .098;
	_color[2] = .114;
	// bind callbacks
	drawHealth.onBefore += std::tuple{ this, &Health::showHook };
	posHealth.onBefore += std::tuple{ (Itemchart *)this, &Itemchart::posHook };
	// install hooks
	drawHealth.install();
	posHealth.install();
}

void Health::initLuaAndIDE( sol::state &lua, IDE *ide, std::string prefix ) {
	if ( prefix.empty() ) prefix = "health";
	Itemchart::initLuaAndIDE( lua, ide, prefix );

	addFunction(
		lua, ide, "health_amount",
		[]() {
			if ( !LOCAL_PLAYER ) return 0.0f;
			if ( !LOCAL_PLAYER->isValid() ) return 0.0f;
			return LOCAL_PLAYER->hitpoints;
		},
		"Get health amount\n\nReturn:\n * float - amount" );
}

void Health::showHook( SRHook::Info &info ) {
	if ( isHidden() ) info.retAddr = 0x58939F;
}
