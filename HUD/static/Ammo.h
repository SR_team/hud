#ifndef AMMO_H
#define AMMO_H

#include "Itembase.h"
#include <llmo/SRHook.hpp>

class Ammo : public Itembase {
	SRHook::Hook<>					   drawAmmo{ 0x5893C8, 7 };
	SRHook::Hook<float, float, char *> posAmmo{ 0x58962A, 5 };
	SRHook::Hook<float, float>		   sizeAmmo{ 0x5894D4, 5 };
	SRHook::Hook<RwRGBA>			   clAmmoText{ 0x58960B, 5 };
	SRHook::Hook<RwRGBA>			   clAmmoShadow{ 0x589525, 5 };
	SRHook::Hook<int>				   styleAmmo{ 0x58952C, 5 };
	SRHook::Hook<bool>				   propAmmo{ 0x5894FF, 5 };
	SRHook::Hook<char>				   edgeAmmo{ 0x589506, 5 };
	SRHook::Hook<char>				   alignAmmo{ 0x5894DB, 5 };

public:
	Ammo( SRDescent *parent, std::string_view id = "" );

	bool custom_text_color	 = false;
	bool custom_shadow_color = false;
	bool proportional		 = true;
	bool edge				 = true;
	enum class Align { center, left, right };
	Align align		 = Align::center;
	int	  font_style = 1;

	RwRGBA colorText();
	void   colorText( RwRGBA color );
	RwRGBA colorShadow();
	void   colorShadow( RwRGBA color );

	virtual void		   reset() override;
	virtual void		   draw() override;
	virtual bool		   isDrag() override;
	virtual bool		   processDrag() override;
	virtual tiny::TinyJson write() override;
	virtual nlohmann::json write2() override;
	virtual void		   read( tiny::xobject &json ) override;
	virtual void		   read2( nlohmann::json &json ) override;
	virtual void		   initLuaAndIDE( sol::state &lua, class IDE *ide, std::string prefix = "" ) override;

protected:
	float  _color[4]{ .675, .796, .945, 1 };
	float  _colorShd[4]{ 0, 0, 0, 1 };
	ImVec4 _backup_color;
	int	   pickId = 0;
	RwV2D  _realSize;
	RwV2D  _dragOffset;

	void showHook( SRHook::Info &info );
	void posHook( float &x, float &y, char *&text );
	void sizeHook( float &w, float &h );
	void clHook( RwRGBA &color );
	void clShdHook( RwRGBA &color );
	void styleHook( int &style );
	void propHook( bool &prop );
	void edgeHook( char &e );
	void alignHook( char &a );
};

#endif // AMMO_H
