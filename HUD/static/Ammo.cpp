#include "Ammo.h"
#include <gtasa/CGame/CPed.h>
#include <gtasa/CGame/methods.h>
#include <nlohmann/json.hpp>

Ammo::Ammo( SRDescent *parent, std::string_view id ) : Itembase( parent, id ) {
	_size	 = { .25, .25 };
	_wgtName = "Ammo";
	// bind callbacks
	drawAmmo.onBefore += std::tuple{ this, &Ammo::showHook };
	posAmmo.onBefore += std::tuple{ this, &Ammo::posHook };
	sizeAmmo.onBefore += std::tuple{ this, &Ammo::sizeHook };
	clAmmoText.onBefore += std::tuple{ this, &Ammo::clHook };
	clAmmoShadow.onBefore += std::tuple{ this, &Ammo::clShdHook };
	styleAmmo.onBefore += std::tuple{ this, &Ammo::styleHook };
	propAmmo.onBefore += std::tuple{ this, &Ammo::propHook };
	edgeAmmo.onBefore += std::tuple{ this, &Ammo::edgeHook };
	alignAmmo.onBefore += std::tuple{ this, &Ammo::alignHook };
	// install hooks
	drawAmmo.install();
	posAmmo.install();
	sizeAmmo.install();
	clAmmoText.install();
	clAmmoShadow.install();
	styleAmmo.install();
	propAmmo.install();
	edgeAmmo.install();
	alignAmmo.install();
}

RwRGBA Ammo::colorText() {
	RwRGBA color;
	color.red	= _color[0] * 255.0f;
	color.green = _color[1] * 255.0f;
	color.blue	= _color[2] * 255.0f;
	color.alpha = _color[3] * 255.0f;
	return color;
}

void Ammo::colorText( RwRGBA color ) {
	_color[0] = float( color.red ) / 255.0f;
	_color[1] = float( color.green ) / 255.0f;
	_color[2] = float( color.blue ) / 255.0f;
	_color[3] = float( color.alpha ) / 255.0f;
}

RwRGBA Ammo::colorShadow() {
	RwRGBA color;
	color.red	= _colorShd[0] * 255.0f;
	color.green = _colorShd[1] * 255.0f;
	color.blue	= _colorShd[2] * 255.0f;
	color.alpha = _colorShd[3] * 255.0f;
	return color;
}

void Ammo::colorShadow( RwRGBA color ) {
	_colorShd[0] = float( color.red ) / 255.0f;
	_colorShd[1] = float( color.green ) / 255.0f;
	_colorShd[2] = float( color.blue ) / 255.0f;
	_colorShd[3] = float( color.alpha ) / 255.0f;
}

void Ammo::reset() {
	Itembase::reset();
	custom_text_color	= false;
	custom_shadow_color = false;
	proportional		= true;
	edge				= true;
	font_style			= 1;
	align				= Align::center;
}

void Ammo::draw() {
	Itembase::draw();

	ImGuiColorEditFlags flags = ImGuiColorEditFlags_AlphaBar;
	flags |= ImGuiColorEditFlags_DisplayRGB;
	flags |= ImGuiColorEditFlags_DisplayHex;

	ImGui::BeginGroup();
	if ( ImGui::ColorButton( name( "Text color" ).data(), *(ImVec4 *)&_color, flags, ImVec2( 60, 40 ) ) &&
		 custom_text_color ) {
		ImGui::OpenPopup( name( "colorpicker" ).data() );
		_backup_color = *(ImVec4 *)&_color;
		pickId		  = 0;
	}
	ImGui::SameLine();
	ImGui::Checkbox( name( "Custom text color" ).data(), &custom_text_color );

	if ( ImGui::ColorButton( name( "Shadow color" ).data(), *(ImVec4 *)&_colorShd, flags, ImVec2( 60, 40 ) ) &&
		 custom_shadow_color ) {
		ImGui::OpenPopup( name( "colorpicker" ).data() );
		_backup_color = *(ImVec4 *)&_colorShd;
		pickId		  = 1;
	}
	ImGui::SameLine();
	ImGui::Checkbox( name( "Custom shadow color" ).data(), &custom_shadow_color );
	ImGui::EndGroup();

	ImGui::SameLine( 0, 10 );
	ImGui::BeginGroup();
	ImGui::Checkbox( name( "Proportional" ).data(), &proportional );
	ImGui::Checkbox( name( "Edge" ).data(), &edge );
	ImGui::EndGroup();

	switch ( pickId ) {
		case 0:
			popupColorpicker( *(ImVec4 *)&_color, _backup_color );
			break;
		case 1:
			popupColorpicker( *(ImVec4 *)&_colorShd, _backup_color );
			break;
	}

	ImGui::BeginGroup();
	ImGui::Text( "Font style:" );
	ImGui::SameLine( 0, 10 );
	if ( font_style < 0 )
		font_style = 0;
	else if ( font_style > 3 )
		font_style = 3;
	ImGui::RadioButton( name( "Gothic" ).data(), &font_style, 0 );
	ImGui::SameLine( 0, 10 );
	ImGui::RadioButton( name( "Subtitles" ).data(), &font_style, 1 );
	ImGui::SameLine( 0, 10 );
	ImGui::RadioButton( name( "Menu" ).data(), &font_style, 2 );
	ImGui::SameLine( 0, 10 );
	ImGui::RadioButton( name( "Pricedown" ).data(), &font_style, 3 );
	ImGui::EndGroup();
	if ( ImGui::IsItemHovered() ) ImGui::SetTooltip( "Default: Subtitles" );

	ImGui::BeginGroup();
	ImGui::Text( "Align:" );
	ImGui::SameLine( 0, 10 );
	ImGui::RadioButton( name( "Left" ).data(), (int *)&align, (int)Align::left );
	ImGui::SameLine( 0, 10 );
	ImGui::RadioButton( name( "Center" ).data(), (int *)&align, (int)Align::center );
	ImGui::SameLine( 0, 10 );
	ImGui::RadioButton( name( "Right" ).data(), (int *)&align, (int)Align::right );
	ImGui::EndGroup();
	if ( ImGui::IsItemHovered() ) ImGui::SetTooltip( "Default: Center" );
}

bool Ammo::isDrag() {
	float start, end;
	switch ( align ) {
		case Align::center:
			start = _pos.fX - _realSize.fX * 0.5f;
			end	  = _pos.fX + _realSize.fX * 0.5f;
			break;
		case Align::left:
			start = _pos.fX;
			end	  = _pos.fX + _realSize.fX;
			break;
		case Align::right:
			start = _pos.fX - _realSize.fX;
			end	  = _pos.fX;
			break;
	}
	if ( isShowed() && mousePos().fX >= start && mousePos().fY >= _pos.fY && mousePos().fX <= end &&
		 mousePos().fY <= _pos.fY + _realSize.fY ) {
		_drag		= true;
		_dragOffset = mousePos() - pos();
		return true;
	}
	return false;
}

bool Ammo::processDrag() {
	if ( !_drag ) return false;
	pos( mousePos() - _dragOffset );
	return true;
}

tiny::TinyJson Ammo::write() {
	auto writeColor = []( float *color ) {
		tiny::TinyJson json;
		json["red"].Set( color[0] );
		json["green"].Set( color[1] );
		json["blue"].Set( color[2] );
		json["alpha"].Set( color[3] );
		return json;
	};

	auto json = Itembase::write();
	if ( !proportional ) json["proportional"].Set( (int)proportional );
	if ( !edge ) json["edge"].Set( (int)edge );
	if ( font_style != 1 ) json["font_style"].Set( font_style );
	if ( align != Align::center ) json["align"].Set( (int)align );

	if ( custom_text_color ) {
		json["custom_text_color"].Set( (int)custom_text_color );
		json["text_color"].Set( writeColor( _color ) );
	}
	if ( custom_shadow_color ) {
		json["custom_shadow_color"].Set( (int)custom_shadow_color );
		json["shadow_color"].Set( writeColor( _colorShd ) );
	}

	return json;
}

nlohmann::json Ammo::write2() {
	auto writeColor = []( float *color ) {
		auto json	  = nlohmann::json::object();
		json["red"]	  = color[0];
		json["green"] = color[1];
		json["blue"]  = color[2];
		json["alpha"] = color[3];
		return json;
	};

	auto json = Itembase::write2();
	if ( !proportional ) json["proportional"] = proportional;
	if ( !edge ) json["edge"] = edge;
	if ( font_style != 1 ) json["font_style"] = font_style;
	if ( align != Align::center ) json["align"] = align;

	if ( custom_text_color ) {
		json["custom_text_color"] = custom_text_color;
		json["text_color"]		  = writeColor( _color );
	}
	if ( custom_shadow_color ) {
		json["custom_shadow_color"] = custom_shadow_color;
		json["shadow_color"]		= writeColor( _colorShd );
	}

	return json;
}

void Ammo::read( tiny::xobject &json ) {
	auto readColor = []( tiny::xobject &json, float *color ) {
		for ( int i = 0; i < json.Count(); i++ ) {
			json.Enter( i );
			color[0] = json.Get<float>( "red" );
			color[1] = json.Get<float>( "green" );
			color[2] = json.Get<float>( "blue" );
			color[3] = json.Get<float>( "alpha" );
		}
	};
	for ( int i = 0; i < json.Count(); i++ ) {
		json.Enter( i );
		Itembase::read( json );
		custom_text_color = json.Get<int>( "custom_text_color", 0 );
		if ( custom_text_color ) {
			tiny::xobject jcolor = json.Get<tiny::xobject>( "text_color" );
			readColor( jcolor, _color );
		}
		custom_shadow_color = json.Get<int>( "custom_shadow_color", 0 );
		if ( custom_shadow_color ) {
			tiny::xobject jcolor = json.Get<tiny::xobject>( "shadow_color" );
			readColor( jcolor, _colorShd );
		}
		proportional = json.Get<int>( "proportional", 1 );
		edge		 = json.Get<int>( "edge", 1 );
		font_style	 = json.Get<int>( "font_style", 1 );
		align		 = (Align)json.Get<int>( "align", 0 );
	}
}

void Ammo::read2( nlohmann::json &json ) {
	auto readColor = []( nlohmann::json &json, float *color ) {
		color[0] = json.value<float>( "red", 1.0f );
		color[1] = json.value<float>( "green", 1.0f );
		color[2] = json.value<float>( "blue", 1.0f );
		color[3] = json.value<float>( "alpha", 1.0f );
	};
	Itembase::read2( json );
	custom_text_color = json.value<bool>( "custom_text_color", false );
	if ( custom_text_color ) {
		auto jcolor = json["text_color"];
		readColor( jcolor, _color );
	}
	custom_shadow_color = json.value<bool>( "custom_shadow_color", false );
	if ( custom_shadow_color ) {
		auto jcolor = json["shadow_color"];
		readColor( jcolor, _colorShd );
	}
	proportional = json.value<bool>( "proportional", true );
	edge		 = json.value<bool>( "edge", true );
	font_style	 = json.value<int>( "font_style", 1 );
	align		 = (Align)json.value<int>( "align", 0 );
}

void Ammo::initLuaAndIDE( sol::state &lua, IDE *ide, std::string prefix ) {
	if ( prefix.empty() ) prefix = "ammo";
	Itembase::initLuaAndIDE( lua, ide, prefix );
	addFunction(
		lua, ide, "ammo_realSize", [this]() { return std::make_tuple( _realSize.fX, _realSize.fY ); },
		"Get real text size\n\nReturn:\n * float - real width\n * float - real height" );

	addFunction(
		lua, ide, "ammo_isCustomTextColor", [this]() { return custom_text_color; },
		"Check is custom text color enabled\n\nReturn:\n * bool - is enabled" );
	addFunction(
		lua, ide, "ammo_isCustomShadowColor", [this]() { return custom_shadow_color; },
		"Check is custom shadow color enabled\n\nReturn:\n * bool - is enabled" );
	addFunction(
		lua, ide, "ammo_isProportional", [this]() { return proportional; },
		"Check is text proportional\n\nReturn:\n * bool - proportional" );
	addFunction(
		lua, ide, "ammo_isEdge", [this]() { return edge; }, "Check is text edged\n\nReturn:\n * bool - edge" );
	addFunction(
		lua, ide, "ammo_fontStyle", [this]() { return font_style; },
		"Get ammo font style id\n\nReturn:\n * int - style\n\nStyles:\n 0. Gothic\n 1. Subtitles (default)\n 2. Menu\n "
		"3. "
		"Pricedown" );
	addFunction(
		lua, ide, "ammo_colorText",
		[this]() {
			auto clr = colorText();
			return *(int *)&clr;
		},
		"Get text color\n\nReturn:\n * int - color" );
	addFunction(
		lua, ide, "ammo_colorShadow",
		[this]() {
			auto clr = colorShadow();
			return *(int *)&clr;
		},
		"Get shadow color\n\nReturn:\n * int - color" );
	addFunction(
		lua, ide, "ammo_toggleCustomTextColor", [this]( bool state ) { custom_text_color = state; },
		"Enable/disable custom text color\n\nArgs:\n * bool - is enabled" );
	addFunction(
		lua, ide, "ammo_toggleCustomShadowColor", [this]( bool state ) { custom_shadow_color = state; },
		"Enable/disable custom shadow color\n\nArgs:\n * bool - is enabled" );
	addFunction(
		lua, ide, "ammo_toggleProportional", [this]( bool state ) { proportional = state; },
		"Enable/desable text proportional\n\nArgs:\n * bool - proportional" );
	addFunction(
		lua, ide, "ammo_toggleEdge", [this]( bool state ) { edge = state; },
		"Enable/desable text edged\n\nArgs:\n * bool - edge" );
	addFunction(
		lua, ide, "ammo_setFontStyle", [this]( int style ) { font_style = style; },
		"Set ammo font style id\n\nArgs:\n * int - style\n\nStyles:\n 0. Gothic\n 1. Subtitles (default)\n 2. Menu\n "
		"3. "
		"Pricedown" );
	addFunction(
		lua, ide, "ammo_setColorPositive", [this]( int clr ) { colorText( *(RwRGBA *)&clr ); },
		"Set text color\n\nArgs:\n * int - color" );
	addFunction(
		lua, ide, "ammo_setColorShadow", [this]( int clr ) { colorShadow( *(RwRGBA *)&clr ); },
		"Set shadow color\n\nArgs:\n * int - color" );

	addFunction(
		lua, ide, "ammo_total",
		[]() {
			if ( !LOCAL_PLAYER ) return 0;
			if ( !LOCAL_PLAYER->isValid() ) return 0;
			return (int)LOCAL_PLAYER->WeaponSlot[LOCAL_PLAYER->weapon_slot].m_dwTotalAmmo;
		},
		"Get current weapon total ammo\n\nReturn:\n * int - ammo" );
	addFunction(
		lua, ide, "ammo_totalFromSlot",
		[]( int slot ) {
			if ( slot < 0 || slot > 13 ) return 0;
			if ( !LOCAL_PLAYER ) return 0;
			if ( !LOCAL_PLAYER->isValid() ) return 0;
			return (int)LOCAL_PLAYER->WeaponSlot[slot].m_dwTotalAmmo;
		},
		"Get total ammo from slot\n\nArgs:\n * int - slot\nReturn:\n * int - ammo" );
	addFunction(
		lua, ide, "ammo_clip",
		[]() {
			if ( !LOCAL_PLAYER ) return 0;
			if ( !LOCAL_PLAYER->isValid() ) return 0;
			return (int)LOCAL_PLAYER->WeaponSlot[LOCAL_PLAYER->weapon_slot].m_dwAmmoInClip;
		},
		"Get current weapon ammo in clip\n\nReturn:\n * int - ammo" );
	addFunction(
		lua, ide, "ammo_clipFromSlot",
		[]( int slot ) {
			if ( slot < 0 || slot > 13 ) return 0;
			if ( !LOCAL_PLAYER ) return 0;
			if ( !LOCAL_PLAYER->isValid() ) return 0;
			return (int)LOCAL_PLAYER->WeaponSlot[slot].m_dwAmmoInClip;
		},
		"Get ammo in clip from slot\n\nArgs:\n * int - slot\nReturn:\n * int - ammo" );

	addFunction(
		lua, ide, "ammo_align", [this]() { return (int)align; },
		"Get ammo align\n\nReturn:\n * int - align\n\nAlign:\n 0. Center\n 1. Left\n 2. "
		"Right" );
	addFunction(
		lua, ide, "ammo_setAlign", [this]( int align ) { this->align = Align( align ); },
		"Get ammo align\n\nArgs:\n * int - align\n\nAlign:\n 0. Center (default)\n 1. Left\n 2. "
		"Right" );
}

void Ammo::showHook( SRHook::Info &info ) {
	if ( isHidden() ) {
		info.skipOriginal = true;
		info.retAddr	  = 0x58963C;
	}
}

void Ammo::posHook( float &x, float &y, char *&text ) {
	auto rect	 = GetTextRect( x, y, text );
	_realSize.fX = ( 640.0f / SCREEN_X ) * ( ( rect.x2 - rect.x1 ) );
	_realSize.fY = ( 448.0f / SCREEN_Y ) * ( ( rect.y2 - rect.y1 ) );
	_origPos.fX	 = ( 640.0f / SCREEN_X ) * rect.x2 - _realSize.fX * 0.5f;
	_origPos.fY	 = ( 448.0f / SCREEN_Y ) * rect.y1 + _realSize.fY * 0.125f;
	x			 = ( SCREEN_X / 640.0f ) * pos().fX;
	y			 = ( SCREEN_Y / 448.0f ) * pos().fY;
}

void Ammo::sizeHook( float &w, float &h ) {
	_origSize = WindowScreenToGameScreen( w * 100.0f, h * 100.0f );
	w		  = ( SCREEN_X / 640.0f ) * size().fX * 0.01f;
	h		  = ( SCREEN_Y / 448.0f ) * size().fY * 0.01f;
}

void Ammo::clHook( RwRGBA &color ) {
	if ( !custom_text_color ) return;
	color.red	= _color[0] * 255.0f;
	color.green = _color[1] * 255.0f;
	color.blue	= _color[2] * 255.0f;
	color.alpha = _color[3] * 255.0f;
}

void Ammo::clShdHook( RwRGBA &color ) {
	if ( !custom_shadow_color ) return;
	color.red	= _colorShd[0] * 255.0f;
	color.green = _colorShd[1] * 255.0f;
	color.blue	= _colorShd[2] * 255.0f;
	color.alpha = _colorShd[3] * 255.0f;
}

void Ammo::styleHook( int &style ) {
	style = font_style;
}

void Ammo::propHook( bool &prop ) {
	prop = proportional;
}

void Ammo::edgeHook( char &e ) {
	if ( !edge ) e = 0;
}

void Ammo::alignHook( char &a ) {
	a = (char)align;
}
