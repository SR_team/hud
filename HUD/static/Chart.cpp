#include "Chart.h"
#include <gtasa/CGame/methods.h>
#include <llmo/callfunc.hpp>
#include <nlohmann/json.hpp>

Chart::Chart( SRDescent *parent, std::string_view name, std::string_view id ) : Itemhud( parent, name, id ) {
	_size	 = { 60, 8 };
	_wgtName = "Chart";
}

void Chart::draw() {
	Itemhud::draw();
	ImGui::SameLine( 0, 10 );
	ImGui::BeginGroup();
	ImGui::Checkbox( name( "Draw percentage" ).data(), &percentage );
	ImGui::SameLine( 0, 10 );
	ImGui::Checkbox( name( "Draw end line" ).data(), &end_line );
	ImGui::Checkbox( name( "Border" ).data(), &border );
	ImGui::EndGroup();
	ImGui::DragFloat( name( "Value" ).data(), &value, 0.1f, 0.0f, 100.0f );
}

bool Chart::isDrag() {
	RwV2D start, end;
	if ( size().fX >= 0 ) {
		start.fX = pos().fX;
		end.fX	 = pos().fX + size().fX;
	} else {
		start.fX = pos().fX + size().fX;
		end.fX	 = pos().fX;
	}
	if ( size().fY >= 0 ) {
		start.fY = pos().fY;
		end.fY	 = pos().fY + size().fY;
	} else {
		start.fY = pos().fY + size().fY;
		end.fY	 = pos().fY;
	}
	if ( isShowed() && mousePos().fX >= start.fX && mousePos().fY >= start.fY && mousePos().fX <= end.fX &&
		 mousePos().fY <= end.fY ) {
		_drag		= true;
		_dragOffset = mousePos() - pos();
		return true;
	}
	return false;
}

bool Chart::processDrag() {
	if ( !_drag ) return false;
	pos( mousePos() - _dragOffset );
	return true;
}

tiny::TinyJson Chart::write() {
	auto json = Itemhud::write();
	if ( percentage ) json["percentage"].Set( (int)percentage );
	if ( end_line ) json["end_line"].Set( (int)end_line );
	if ( !border ) json["border"].Set( (int)border );
	if ( value != 50.0f ) json["value"].Set( value );
	return json;
}

nlohmann::json Chart::write2() {
	auto json = Itemhud::write2();
	if ( percentage ) json["percentage"] = percentage;
	if ( end_line ) json["end_line"] = end_line;
	if ( !border ) json["border"] = border;
	if ( value != 50.0f ) json["value"] = value;
	return json;
}

void Chart::read( tiny::xobject &json ) {
	for ( int i = 0; i < json.Count(); i++ ) {
		json.Enter( i );
		Itemhud::read( json );
		percentage = json.Get<int>( "percentage", 0 );
		end_line   = json.Get<int>( "end_line", 0 );
		border	   = json.Get<int>( "border", 1 );
		value	   = json.Get<float>( "value", 50.0f );
	}
}

void Chart::read2( nlohmann::json &json ) {
	Itemhud::read2( json );
	percentage = json.value<bool>( "percentage", false );
	end_line   = json.value<bool>( "end_line", false );
	border	   = json.value<bool>( "border", true );
	value	   = json.value<float>( "value", 50.0f );
}

void Chart::render() {
	if ( isHidden() ) return;

	if ( value < 0 )
		value = 0;
	else if ( value > 100 )
		value = 100;

	RwRGBA fore{ static_cast<uint8_t>( 0xFF - color().red ), static_cast<uint8_t>( 0xFF - color().green ),
				 static_cast<uint8_t>( 0xFF - color().blue ), 0xFF };
	auto   wp  = GameScreenToWindowScreen( pos().fX, pos().fY );
	auto   wsz = GameScreenToWindowScreen( size().fX, size().fY );
	CallFunc::ccall( 0x728640, wp.fX, wp.fY, (WORD)wsz.fX, (WORD)wsz.fY, value, end_line, percentage, border, color(),
					 fore );
}
