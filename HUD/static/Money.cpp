#include "Money.h"
#include <gtasa/CGame/methods.h>
#include <nlohmann/json.hpp>

Money::Money( SRDescent *parent, std::string_view id ) : Itembase( parent, id ) {
	_size	 = { .25, .25 };
	_wgtName = "Money";
	// bind callbacks
	drawMoney.onBefore += std::tuple{ this, &Money::showHook };
	posMoney.onBefore += std::tuple{ this, &Money::posHook };
	sizeMoney.onBefore += std::tuple{ this, &Money::sizeHook };
	clMoneyPos.onBefore += std::tuple{ this, &Money::clPosHook };
	clMoneyNeg.onBefore += std::tuple{ this, &Money::clNegHook };
	clMoneyShd.onBefore += std::tuple{ this, &Money::clShdHook };
	style.onBefore += std::tuple{ this, &Money::styleHook };
	propMoney.onBefore += std::tuple{ this, &Money::propHook };
	edgeMoney.onBefore += std::tuple{ this, &Money::edgeHook };
	alignMoney.onBefore += std::tuple{ this, &Money::alignHook };
	// install hooks
	drawMoney.install();
	posMoney.install();
	sizeMoney.install();
	clMoneyPos.install();
	clMoneyNeg.install();
	clMoneyShd.install();
	style.install();
	propMoney.install();
	edgeMoney.install();
	alignMoney.install();
}

RwRGBA Money::colorPositive() const {
	RwRGBA color;
	color.red	= _colorPos[0] * 255.0f;
	color.green = _colorPos[1] * 255.0f;
	color.blue	= _colorPos[2] * 255.0f;
	color.alpha = _colorPos[3] * 255.0f;
	return color;
}

void Money::colorPositive( RwRGBA color ) {
	_colorPos[0] = float( color.red ) / 255.0f;
	_colorPos[1] = float( color.green ) / 255.0f;
	_colorPos[2] = float( color.blue ) / 255.0f;
	_colorPos[3] = float( color.alpha ) / 255.0f;
}

RwRGBA Money::colorNegative() const {
	RwRGBA color;
	color.red	= _colorPos[0] * 255.0f;
	color.green = _colorPos[1] * 255.0f;
	color.blue	= _colorPos[2] * 255.0f;
	color.alpha = _colorPos[3] * 255.0f;
	return color;
}

void Money::colorNegative( RwRGBA color ) {
	_colorPos[0] = float( color.red ) / 255.0f;
	_colorPos[1] = float( color.green ) / 255.0f;
	_colorPos[2] = float( color.blue ) / 255.0f;
	_colorPos[3] = float( color.alpha ) / 255.0f;
}

RwRGBA Money::colorShadow() const {
	RwRGBA color;
	color.red	= _colorPos[0] * 255.0f;
	color.green = _colorPos[1] * 255.0f;
	color.blue	= _colorPos[2] * 255.0f;
	color.alpha = _colorPos[3] * 255.0f;
	return color;
}

void Money::colorShadow( RwRGBA color ) {
	_colorPos[0] = float( color.red ) / 255.0f;
	_colorPos[1] = float( color.green ) / 255.0f;
	_colorPos[2] = float( color.blue ) / 255.0f;
	_colorPos[3] = float( color.alpha ) / 255.0f;
}

void Money::reset() {
	Itembase::reset();
	custom_positive_color = false;
	custom_negative_color = false;
	custom_shadow_color	  = false;
	proportional		  = false;
	edge				  = 2;
	font_style			  = 3;
	align				  = Align::right;
}

void Money::draw() {
	Itembase::draw();

	ImGuiColorEditFlags flags = ImGuiColorEditFlags_AlphaBar;
	flags |= ImGuiColorEditFlags_DisplayRGB;
	flags |= ImGuiColorEditFlags_DisplayHex;

	static char moneyBuf[256]{ 0 };
	auto		money = *(int *)0xB7CE50;
	if ( money < 0 )
		snprintf( moneyBuf, 255, (const char *)0x866C8C, money );
	else
		snprintf( moneyBuf, 255, (const char *)0x866C94, money );
	auto size = ImGui::CalcTextSize( moneyBuf, NULL, true );
	size.y *= 3.0f;
	size.x = size.y * 1.333333f;

	if ( ImGui::ColorButton( name( "Positive color" ).data(), *(ImVec4 *)&_colorPos, flags, size ) &&
		 custom_positive_color ) {
		ImGui::OpenPopup( name( "colorpicker" ).data() );
		_backup_color = *(ImVec4 *)&_colorPos;
		pickId		  = 0;
	}
	ImGui::SameLine();
	ImGui::BeginGroup();
	ImGui::Checkbox( name( "Custom positive color" ).data(), &custom_positive_color );
	ImGui::TextColored( *(ImVec4 *)&_colorPos, (const char *)0x866C94, 1449 );
	ImGui::EndGroup();


	if ( ImGui::ColorButton( name( "Negative color" ).data(), *(ImVec4 *)&_colorNeg, flags, size ) &&
		 custom_negative_color ) {
		ImGui::OpenPopup( name( "colorpicker" ).data() );
		_backup_color = *(ImVec4 *)&_colorNeg;
		pickId		  = 1;
	}
	ImGui::SameLine();
	ImGui::BeginGroup();
	ImGui::Checkbox( name( "Custom negative color" ).data(), &custom_negative_color );
	ImGui::TextColored( *(ImVec4 *)&_colorNeg, (const char *)0x866C8C, 1337 );
	ImGui::EndGroup();

	if ( ImGui::ColorButton( name( "Shadow color" ).data(), *(ImVec4 *)&_colorShd, flags, size ) &&
		 custom_shadow_color ) {
		ImGui::OpenPopup( name( "colorpicker" ).data() );
		_backup_color = *(ImVec4 *)&_colorShd;
		pickId		  = 2;
	}
	ImGui::SameLine();
	ImGui::BeginGroup();
	ImGui::Checkbox( name( "Custom shadow color" ).data(), &custom_shadow_color );
	ImGui::TextColored( *(ImVec4 *)&_colorShd, (const char *)0x866C94, 1449 );
	ImGui::SameLine( 0, 10 );
	ImGui::TextColored( *(ImVec4 *)&_colorShd, (const char *)0x866C8C, 1337 );
	ImGui::EndGroup();

	switch ( pickId ) {
		case 0:
			popupColorpicker( *(ImVec4 *)&_colorPos, _backup_color );
			break;
		case 1:
			popupColorpicker( *(ImVec4 *)&_colorNeg, _backup_color );
			break;
		case 2:
			popupColorpicker( *(ImVec4 *)&_colorShd, _backup_color );
			break;
	}

	ImGui::BeginGroup();
	ImGui::Text( "Font style:" );
	ImGui::SameLine( 0, 10 );
	if ( font_style < 0 )
		font_style = 0;
	else if ( font_style > 3 )
		font_style = 3;
	ImGui::RadioButton( name( "Gothic" ).data(), &font_style, 0 );
	ImGui::SameLine( 0, 10 );
	ImGui::RadioButton( name( "Subtitles" ).data(), &font_style, 1 );
	ImGui::SameLine( 0, 10 );
	ImGui::RadioButton( name( "Menu" ).data(), &font_style, 2 );
	ImGui::SameLine( 0, 10 );
	ImGui::RadioButton( name( "Pricedown" ).data(), &font_style, 3 );
	ImGui::EndGroup();
	if ( ImGui::IsItemHovered() ) ImGui::SetTooltip( "Default: Pricedown" );

	ImGui::Checkbox( name( "Proportional" ).data(), &proportional );
	ImGui::BeginGroup();
	ImGui::Text( "Edge:" );
	ImGui::SameLine( 0, 10 );
	ImGui::RadioButton( name( "Disable" ).data(), &edge, 0 );
	ImGui::SameLine( 0, 10 );
	ImGui::RadioButton( name( "Thin" ).data(), &edge, 1 );
	ImGui::SameLine( 0, 10 );
	ImGui::RadioButton( name( "Default" ).data(), &edge, 2 );
	ImGui::EndGroup();

	ImGui::BeginGroup();
	ImGui::Text( "Align:" );
	ImGui::SameLine( 0, 10 );
	ImGui::RadioButton( name( "Left" ).data(), (int *)&align, (int)Align::left );
	ImGui::SameLine( 0, 10 );
	ImGui::RadioButton( name( "Center" ).data(), (int *)&align, (int)Align::center );
	ImGui::SameLine( 0, 10 );
	ImGui::RadioButton( name( "Right" ).data(), (int *)&align, (int)Align::right );
	ImGui::EndGroup();
	if ( ImGui::IsItemHovered() ) ImGui::SetTooltip( "Default: Right" );
}

bool Money::isDrag() {
	float start, end;
	switch ( align ) {
		case Align::center:
			start = _pos.fX - _realSize.fX * 0.5f;
			end	  = _pos.fX + _realSize.fX * 0.5f;
			break;
		case Align::left:
			start = _pos.fX;
			end	  = _pos.fX + _realSize.fX;
			break;
		case Align::right:
			start = _pos.fX - _realSize.fX;
			end	  = _pos.fX;
			break;
	}
	if ( isShowed() && mousePos().fX >= start && mousePos().fY >= _pos.fY && mousePos().fX <= end &&
		 mousePos().fY <= _pos.fY + _realSize.fY ) {
		_drag		= true;
		_dragOffset = mousePos() - pos();
		return true;
	}
	return false;
}

bool Money::processDrag() {
	if ( !_drag ) return false;
	pos( mousePos() - _dragOffset );
	return true;
}

tiny::TinyJson Money::write() {
	auto writeColor = []( float *color ) {
		tiny::TinyJson json;
		json["red"].Set( color[0] );
		json["green"].Set( color[1] );
		json["blue"].Set( color[2] );
		json["alpha"].Set( color[3] );
		return json;
	};

	auto json = Itembase::write();
	if ( proportional ) json["proportional"].Set( (int)proportional );
	if ( edge != 2 ) json["edge"].Set( edge );
	if ( font_style != 3 ) json["font_style"].Set( font_style );
	if ( align != Align::right ) json["align"].Set( (int)align );

	if ( custom_positive_color ) {
		json["custom_positive_color"].Set( (int)custom_positive_color );
		json["positive_color"].Set( writeColor( _colorPos ) );
	}
	if ( custom_negative_color ) {
		json["custom_negative_color"].Set( (int)custom_negative_color );
		json["negative_color"].Set( writeColor( _colorNeg ) );
	}
	if ( custom_shadow_color ) {
		json["custom_shadow_color"].Set( (int)custom_shadow_color );
		json["shadow_color"].Set( writeColor( _colorShd ) );
	}

	return json;
}

nlohmann::json Money::write2() {
	auto writeColor = []( float *color ) {
		auto json	  = nlohmann::json::object();
		json["red"]	  = color[0];
		json["green"] = color[1];
		json["blue"]  = color[2];
		json["alpha"] = color[3];
		return json;
	};

	auto json = Itembase::write2();
	if ( proportional ) json["proportional"] = proportional;
	if ( edge != 2 ) json["edge"] = edge;
	if ( font_style != 3 ) json["font_style"] = font_style;
	if ( align != Align::right ) json["align"] = align;

	if ( custom_positive_color ) {
		json["custom_positive_color"] = custom_positive_color;
		json["positive_color"]		  = writeColor( _colorPos );
	}
	if ( custom_negative_color ) {
		json["custom_negative_color"] = custom_negative_color;
		json["negative_color"]		  = writeColor( _colorNeg );
	}
	if ( custom_shadow_color ) {
		json["custom_shadow_color"] = custom_shadow_color;
		json["shadow_color"]		= writeColor( _colorShd );
	}

	return json;
}

void Money::read( tiny::xobject &json ) {
	auto readColor = []( tiny::xobject &json, float *color ) {
		for ( int i = 0; i < json.Count(); i++ ) {
			json.Enter( i );
			color[0] = json.Get<float>( "red" );
			color[1] = json.Get<float>( "green" );
			color[2] = json.Get<float>( "blue" );
			color[3] = json.Get<float>( "alpha" );
		}
	};
	for ( int i = 0; i < json.Count(); i++ ) {
		json.Enter( i );
		Itembase::read( json );
		custom_positive_color = json.Get<int>( "custom_positive_color", 0 );
		if ( custom_positive_color ) {
			tiny::xobject jcolor = json.Get<tiny::xobject>( "positive_color" );
			readColor( jcolor, _colorPos );
		}
		custom_negative_color = json.Get<int>( "custom_negative_color", 0 );
		if ( custom_negative_color ) {
			tiny::xobject jcolor = json.Get<tiny::xobject>( "negative_color" );
			readColor( jcolor, _colorNeg );
		}
		custom_shadow_color = json.Get<int>( "custom_shadow_color", 0 );
		if ( custom_shadow_color ) {
			tiny::xobject jcolor = json.Get<tiny::xobject>( "shadow_color" );
			readColor( jcolor, _colorShd );
		}
		proportional = json.Get<int>( "proportional", 0 );
		edge		 = json.Get<int>( "edge", 2 );
		font_style	 = json.Get<int>( "font_style", 3 );
		align		 = (Align)json.Get<int>( "align", 2 );
	}
}

void Money::read2( nlohmann::json &json ) {
	auto readColor = []( nlohmann::json &json, float *color ) {
		color[0] = json.value<float>( "red", 1.0f );
		color[1] = json.value<float>( "green", 1.0f );
		color[2] = json.value<float>( "blue", 1.0f );
		color[3] = json.value<float>( "alpha", 1.0f );
	};
	Itembase::read2( json );
	custom_positive_color = json.value<bool>( "custom_positive_color", false );
	if ( custom_positive_color ) {
		auto jcolor = json["positive_color"];
		readColor( jcolor, _colorPos );
	}
	custom_negative_color = json.value<bool>( "custom_negative_color", false );
	if ( custom_negative_color ) {
		auto jcolor = json["negative_color"];
		readColor( jcolor, _colorNeg );
	}
	custom_shadow_color = json.value<bool>( "custom_shadow_color", false );
	if ( custom_shadow_color ) {
		auto jcolor = json["shadow_color"];
		readColor( jcolor, _colorShd );
	}
	proportional = json.value<bool>( "proportional", false );
	edge		 = json.value<int>( "edge", 2 );
	font_style	 = json.value<int>( "font_style", 3 );
	align		 = (Align)json.value<int>( "align", 2 );
}

void Money::initLuaAndIDE( sol::state &lua, IDE *ide, std::string prefix ) {
	if ( prefix.empty() ) prefix = "money";
	Itembase::initLuaAndIDE( lua, ide, prefix );
	addFunction(
		lua, ide, "money_realSize", [this]() { return std::make_tuple( _realSize.fX, _realSize.fY ); },
		"Get real text size\n\nReturn:\n * float - real width\n * float - real height" );

	addFunction(
		lua, ide, "money_isCustomPositiveColor", [this]() { return custom_positive_color; },
		"Check is custom positive color enabled\n\nReturn:\n * bool - is enabled" );
	addFunction(
		lua, ide, "money_isCustomNegativeColor", [this]() { return custom_negative_color; },
		"Check is custom negative color enabled\n\nReturn:\n * bool - is enabled" );
	addFunction(
		lua, ide, "money_isCustomShadowColor", [this]() { return custom_shadow_color; },
		"Check is custom shadow color enabled\n\nReturn:\n * bool - is enabled" );
	addFunction(
		lua, ide, "money_isProportional", [this]() { return proportional; },
		"Check is text proportional\n\nReturn:\n * bool - proportional" );
	addFunction(
		lua, ide, "money_edge", [this]() { return edge; }, "Get text edged\n\nReturn:\n * int - edge" );
	addFunction(
		lua, ide, "money_fontStyle", [this]() { return font_style; },
		"Get money font style id\n\nReturn:\n * int - style\n\nStyles:\n 0. Gothic\n 1. Subtitles\n 2. Menu\n 3. "
		"Pricedown (default)" );
	addFunction(
		lua, ide, "money_colorPositive",
		[this]() {
			auto clr = colorPositive();
			return *(int *)&clr;
		},
		"Get positive color\n\nReturn:\n * int - color" );
	addFunction(
		lua, ide, "money_colorNegative",
		[this]() {
			auto clr = colorNegative();
			return *(int *)&clr;
		},
		"Get negative color\n\nReturn:\n * int - color" );
	addFunction(
		lua, ide, "money_colorShadow",
		[this]() {
			auto clr = colorShadow();
			return *(int *)&clr;
		},
		"Get shadow color\n\nReturn:\n * int - color" );
	addFunction(
		lua, ide, "money_toggleCustomPositiveColor", [this]( bool state ) { custom_positive_color = state; },
		"Enable/disable custom positive color\n\nArgs:\n * bool - is enabled" );
	addFunction(
		lua, ide, "money_toggleCustomNegativeColor", [this]( bool state ) { custom_negative_color = state; },
		"Enable/disable custom negative color\n\nArgs:\n * bool - is enabled" );
	addFunction(
		lua, ide, "money_toggleCustomShadowColor", [this]( bool state ) { custom_shadow_color = state; },
		"Enable/disable custom shadow color\n\nArgs:\n * bool - is enabled" );
	addFunction(
		lua, ide, "money_toggleProportional", [this]( bool state ) { proportional = state; },
		"Enable/desable text proportional\n\nArgs:\n * bool - proportional" );
	addFunction(
		lua, ide, "money_setEdge", [this]( int e ) { return edge = e; }, "Set text edged\n\nArgs:\n * int - edge" );
	addFunction(
		lua, ide, "money_setFontStyle", [this]( int style ) { font_style = style; },
		"Set money font style id\n\nArgs:\n * int - style\n\nStyles:\n 0. Gothic\n 1. Subtitles\n 2. Menu\n 3. "
		"Pricedown (default)" );
	addFunction(
		lua, ide, "money_setColorPositive", [this]( int clr ) { colorPositive( *(RwRGBA *)&clr ); },
		"Set positive color\n\nArgs:\n * int - color" );
	addFunction(
		lua, ide, "money_setColorNegative", [this]( int clr ) { colorNegative( *(RwRGBA *)&clr ); },
		"Set negative color\n\nArgs:\n * int - color" );
	addFunction(
		lua, ide, "money_setColorShadow", [this]( int clr ) { colorShadow( *(RwRGBA *)&clr ); },
		"Set shadow color\n\nArgs:\n * int - color" );

	addFunction(
		lua, ide, "money_amount", []() { return *(int *)0xB7CE50; }, "Get money\n\nReturn:\n * int - money" );

	addFunction(
		lua, ide, "money_align", [this]() { return (int)align; },
		"Get money align\n\nReturn:\n * int - align\n\nAlign:\n 0. Center\n 1. Left\n 2. "
		"Right" );
	addFunction(
		lua, ide, "money_setAlign", [this]( int align ) { this->align = Align( align ); },
		"Get money align\n\nArgs:\n * int - align\n\nAlign:\n 0. Center\n 1. Left\n 2. "
		"Right (default)" );
}

void Money::showHook( SRHook::Info &info ) {
	if ( isHidden() ) info.retAddr = 0x58F618;
}

void Money::posHook( float &x, float &y, char *&text ) {
	auto rect	 = GetTextRect( x, y, text );
	_origPos.fX	 = ( 640.0f / SCREEN_X ) * rect.x2;
	_origPos.fY	 = ( 448.0f / SCREEN_Y ) * rect.y1;
	_realSize.fX = ( 640.0f / SCREEN_X ) * ( ( rect.x2 - rect.x1 ) );
	_realSize.fY = ( 448.0f / SCREEN_Y ) * ( ( rect.y2 - rect.y1 ) );
	x			 = ( SCREEN_X / 640.0f ) * pos().fX;
	y			 = ( SCREEN_Y / 448.0f ) * pos().fY;
}

void Money::sizeHook( float &w, float &h ) {
	_origSize = WindowScreenToGameScreen( w * 100.0f, h * 100.0f );
	w		  = ( SCREEN_X / 640.0f ) * size().fX * 0.01f;
	h		  = ( SCREEN_Y / 448.0f ) * size().fY * 0.01f;
}

void Money::clPosHook( RwRGBA &color ) {
	if ( !custom_positive_color ) return;
	color.red	= _colorPos[0] * 255.0f;
	color.green = _colorPos[1] * 255.0f;
	color.blue	= _colorPos[2] * 255.0f;
	color.alpha = _colorPos[3] * 255.0f;
}

void Money::clNegHook( RwRGBA &color ) {
	if ( !custom_negative_color ) return;
	color.red	= _colorNeg[0] * 255.0f;
	color.green = _colorNeg[1] * 255.0f;
	color.blue	= _colorNeg[2] * 255.0f;
	color.alpha = _colorNeg[3] * 255.0f;
}

void Money::clShdHook( RwRGBA &color ) {
	if ( !custom_shadow_color ) return;
	color.red	= _colorShd[0] * 255.0f;
	color.green = _colorShd[1] * 255.0f;
	color.blue	= _colorShd[2] * 255.0f;
	color.alpha = _colorShd[3] * 255.0f;
}

void Money::styleHook( int &fontStyle ) {
	fontStyle = font_style;
}

void Money::propHook( bool &prop ) {
	prop = proportional;
}

void Money::edgeHook( char &e ) {
	if ( edge < 0 )
		edge = 0;
	else if ( edge > 2 )
		edge = 2;
	e = edge;
}

void Money::alignHook( char &a ) {
	a = (char)align;
}
