#include <base/d3d9/d3drender.h>
#ifdef FULL_DX_HOOK
#	include <base/d3d9/proxydirectx.h>
#else
#	include <base/d3d9/DrawHook.h>
#	include <d3dx9.h>
#endif
#include <base/d3d9/texture.h>
#include <gtasa/CGame/CPed.h>
#include <gtasa/CGame/CTxdStore.h>
#include <gtasa/CGame/methods.h>
#include <llmo/SRHookFast.h>
#include <llmo/callfunc.hpp>
#include <map>
#include <sol/sol.hpp>
#include <windows.h>

sol::state *g_lua = nullptr;
#ifdef FULL_DX_HOOK
hookIDirect3DDevice9 *g_dx = nullptr;
#else
DrawHook *g_dx = nullptr;
#endif
std::map<std::string, SRTexture *> textures;
std::map<std::string, CD3DFont *>  fonts;
CD3DRender *					   d3dRender;
SRHook::Fast::Hook				   hudDraw{ 0x53E52B, 5 };

std::deque<std::function<void()>> drawer;
void							  onDraw() {
	 // Backup the DX9 state
	 IDirect3DStateBlock9 *d3d9_state_block = NULL;
	 if ( g_dx->d3d9_device()->CreateStateBlock( D3DSBT_ALL, &d3d9_state_block ) < 0 ) return;
	 // Backup the DX9 transform
	 //	 D3DMATRIX last_world, last_view, last_projection;
	 //	 g_dx->d3d9_device()->GetTransform( D3DTS_WORLD, &last_world );
	 //	 g_dx->d3d9_device()->GetTransform( D3DTS_VIEW, &last_view );
	 //	 g_dx->d3d9_device()->GetTransform( D3DTS_PROJECTION, &last_projection );
	 for ( auto &&d : drawer ) d();
	 for ( auto &&[name, tx] : textures ) tx->End();
	 // Restore the DX9 transform
	 //	 g_dx->d3d9_device()->SetTransform( D3DTS_WORLD, &last_world );
	 //	 g_dx->d3d9_device()->SetTransform( D3DTS_VIEW, &last_view );
	 //	 g_dx->d3d9_device()->SetTransform( D3DTS_PROJECTION, &last_projection );
	 // Restore the DX9 state
	 d3d9_state_block->Apply();
	 d3d9_state_block->Release();
	 drawer.clear();
}

template<typename Fx> void render( const Fx &func ) {
	drawer.push_back( func );
}

void addDescription( std::string_view name, std::string_view desc ) {
	typedef void ( *addToIDE_t )( std::string_view, std::string_view );
	static auto lib = GetModuleHandleA( "HUD.asi" );
	if ( !lib || lib == INVALID_HANDLE_VALUE ) return;
	static auto addToIDE = (addToIDE_t)GetProcAddress( lib, "addToIDE" );
	addToIDE( name, desc );
}
RwTexture *texture( std::string_view name ) {
	typedef RwTexture *( *texture_t )( std::string_view );
	static auto lib = GetModuleHandleA( "HUD.asi" );
	if ( !lib || lib == INVALID_HANDLE_VALUE ) return nullptr;
	static auto texture = (texture_t)GetProcAddress( lib, "texture" );
	if ( !texture ) return nullptr;
	return texture( name );
}
bool hasHudHidden() {
	typedef bool ( *hasHudHidden_t )();
	static auto lib = GetModuleHandleA( "HUD.asi" );
	if ( !lib || lib == INVALID_HANDLE_VALUE ) return false;
	static auto hasHudHidden = (hasHudHidden_t)GetProcAddress( lib, "hasHudHidden" );
	if ( !hasHudHidden ) return false;
	return hasHudHidden();
}
bool isPreview() {
	typedef bool ( *isPreview_t )();
	static auto lib = GetModuleHandleA( "HUD.asi" );
	if ( !lib || lib == INVALID_HANDLE_VALUE ) return false;
	static auto isPreview = (isPreview_t)GetProcAddress( lib, "isPreview" );
	if ( !isPreview ) return false;
	return isPreview();
}
void getDx() {
#ifdef FULL_DX_HOOK
	typedef hookIDirect3DDevice9 *( *dx_t )();
#else
	typedef DrawHook *( *dx_t )();
#endif
	static auto lib = GetModuleHandleA( "HUD.asi" );
	if ( !lib || lib == INVALID_HANDLE_VALUE ) return;
	static auto dx = (dx_t)GetProcAddress( lib, "dx" );
	if ( !dx ) return;
	g_dx = dx();
#ifdef FULL_DX_HOOK
	d3dRender = g_dx->d3d9_CreateRender();
#else
	d3dRender = g_dx->d3d9_createRender();
#endif
}

template<typename T> void addFunction( const T &func, std::string_view name ) {
	if ( g_lua ) g_lua->set_function( name, func );
}

extern "C" __declspec( dllexport ) void reset() {
	if ( !g_dx ) return;
#ifdef FULL_DX_HOOK
	for ( auto &&[name, tx] : textures ) g_dx->d3d9_ReleaseTexture( tx );
#else
	for ( auto &&[name, tx] : textures ) g_dx->d3d9_releaseTexture( tx );
#endif
	textures.clear();
#ifdef FULL_DX_HOOK
	for ( auto &&[name, fnt] : fonts ) g_dx->d3d9_ReleaseFont( fnt );
#else
	for ( auto &&[name, fnt] : fonts ) g_dx->d3d9_releaseFont( fnt );
#endif
	fonts.clear();
}

extern "C" __declspec( dllexport ) bool init_lua( sol::state *lua ) {
	g_lua = lua;
	if ( !g_dx ) getDx();
	if ( !g_dx ) return false;

	// textures
	addFunction(
		[]( std::string name ) {
			try {
				textures.at( name );
				return false;
			} catch ( ... ) {
				return true;
			}
		},
		"d3dTexture_exists" );
	addFunction(
		[]( std::string name ) {
			try {
				auto tx = textures.at( name );
				textures.erase( name );
#ifdef FULL_DX_HOOK
				g_dx->d3d9_ReleaseTexture( tx );
#else
				g_dx->d3d9_releaseTexture( tx );
#endif
				return true;
			} catch ( ... ) {
				return false;
			}
		},
		"d3dTexture_delete" );
	addFunction(
		[]( std::string name, float w, float h ) {
			try {
				textures.at( name );
				return false;
			} catch ( ... ) {
#ifdef FULL_DX_HOOK
				textures[name] = g_dx->d3d9_CreateTexture( w, h );
#else
				textures[name] = g_dx->d3d9_createTexture( w, h );
#endif
				return true;
			}
		},
		"d3dTexture_new" );
	addFunction(
		[]( std::string name, float w, float h ) {
			try {
				auto tx = textures.at( name );
				tx->ReInit( w, h );
				return true;
			} catch ( ... ) {
				return false;
			}
		},
		"d3dTexture_resize" );
	addFunction(
		[]( std::string name ) {
			if ( hasHudHidden() ) return false;
			render( [=] {
				try {
					auto tx = textures.at( name );
					tx->Clear();
				} catch ( ... ) {
				}
			} );
			return true;
		},
		"d3dTexture_clear" );
	addFunction(
		[]( std::string name ) {
			if ( hasHudHidden() ) return false;
			render( [=] {
				try {
					auto tx = textures.at( name );
					tx->Begin();
				} catch ( ... ) {
				}
			} );
			return true;
		},
		"d3dTexture_begin" );
	addFunction(
		[]( std::string name ) {
			if ( hasHudHidden() ) return false;
			render( [=] {
				try {
					auto tx = textures.at( name );
					tx->End();
				} catch ( ... ) {
				}
			} );
			return true;
		},
		"d3dTexture_end" );
	addFunction(
		[]( std::string name, float x, float y, float w, float h ) {
			if ( hasHudHidden() ) return false;
			render( [=] {
				try {
					auto tx	 = textures.at( name );
					auto pos = GameScreenToWindowScreen( x, y );
					if ( isPreview() ) {
						auto  txSize = tx->GetSize();
						float _w	 = ( w != 0.0f ? w : txSize.x );
						float _h	 = ( h != 0.0f ? h : txSize.y );
						d3dRender->D3DBoxEmpty( pos.fX, pos.fY, _w, _h, tx->rotZ(), 0x80000000 );
					}
					if ( !tx->Render( pos.fX, pos.fY, w, h ) ) {
						tx->End();
						tx->Render( pos.fX, pos.fY, w, h );
					}
				} catch ( ... ) {
				}
			} );
			return true;
		},
		"d3dTexture_render" );
	addFunction(
		[]( std::string name, float x, float y, float z, float x2, float y2, float z2, bool zEnable ) {
			if ( hasHudHidden() ) return false;
			render( [=] {
				try {
					auto tx = textures.at( name );
					if ( !LOCAL_PLAYER || !LOCAL_PLAYER->isValid() ) return;
					auto phys = (CPhysical *)LOCAL_PLAYER;
					if ( LOCAL_PLAYER->isDriving() ) phys = (CPhysical *)LOCAL_PLAYER->vehicle;
					POINT3DQUAD pos;
					*(RwV3D *)&pos.lu = LOCAL_PLAYER->getPositionOffset( { x, y, z } );
					*(RwV3D *)&pos.lb = LOCAL_PLAYER->getPositionOffset( { x, y, z2 } );
					*(RwV3D *)&pos.ru = LOCAL_PLAYER->getPositionOffset( { x2, y2, z } );
					*(RwV3D *)&pos.rb = LOCAL_PLAYER->getPositionOffset( { x2, y2, z2 } );
					d3dRender->D3DBindTexture( tx->GetTexture() );
					d3dRender->D3DTexQuad3D( pos, zEnable );
					d3dRender->D3DBindTexture( nullptr );
				} catch ( ... ) {
				}
			} );
			return true;
		},
		"d3dTexture_renderAtPlayer" );
	addFunction(
		[]( std::string name, float lux, float luy, float luz, float rux, float ruy, float ruz, float lbx, float lby,
			float lbz, float rbx, float rby, float rbz, bool zTest ) {
			if ( hasHudHidden() ) return false;
			render( [=] {
				try {
					auto		tx = textures.at( name );
					POINT3DQUAD pos;
					*(RwV3D *)&pos.lu = { lux, luy, luz };
					*(RwV3D *)&pos.lb = { lbx, lby, lbz };
					*(RwV3D *)&pos.ru = { rux, ruy, ruz };
					*(RwV3D *)&pos.rb = { rbx, rby, rbz };
					d3dRender->D3DBindTexture( tx->GetTexture() );
					d3dRender->D3DTexQuad3D( pos, zTest );
					d3dRender->D3DBindTexture( nullptr );
				} catch ( ... ) {
				}
			} );
			return true;
		},
		"d3dTexture_render3D" );
	addFunction(
		[]( std::string name ) {
			try {
				auto tx = textures.at( name );
				return tx->rotZ();
			} catch ( ... ) {
				return 0.0f;
			}
		},
		"d3dTexture_angle" );
	addFunction(
		[]( std::string name, float angle ) {
			try {
				auto tx = textures.at( name );
				tx->setRotZ( angle );
				return true;
			} catch ( ... ) {
				return false;
			}
		},
		"d3dTexture_setAngle" );
	addFunction(
		[]( std::string name ) {
			if ( isPreview() ) return false;
			try {
				auto tx = textures.at( name );
				return tx->IsRendered();
			} catch ( ... ) {
				return false;
			}
		},
		"d3dTexture_isRendered" );

	// fonts
	addFunction(
		[]( std::string name ) {
			try {
				fonts.at( name );
				return false;
			} catch ( ... ) {
				return true;
			}
		},
		"d3dFont_exists" );
	addFunction(
		[]( std::string name ) {
			try {
				auto tx = fonts.at( name );
				fonts.erase( name );
#ifdef FULL_DX_HOOK
				g_dx->d3d9_ReleaseFont( tx );
#else
				g_dx->d3d9_releaseFont( tx );
#endif
				return true;
			} catch ( ... ) {
				return false;
			}
		},
		"d3dFont_delete" );
	addFunction(
		[]( std::string name, std::string font, int size, int flags ) {
			try {
				fonts.at( name );
				return false;
			} catch ( ... ) {
#ifdef FULL_DX_HOOK
				fonts[name] = g_dx->d3d9_CreateFont( font.data(), size, flags );
#else
				fonts[name] = g_dx->d3d9_createFont( font.data(), size, flags );
#endif
				return true;
			}
		},
		"d3dFont_new" );
	addFunction(
		[]( std::string name, std::string text, float x, float y, int color ) {
			if ( hasHudHidden() ) return false;
			render( [=] {
				try {
					auto	font	= fonts.at( name );
					auto	pos		= GameScreenToWindowScreen( x, y );
					auto	rwColor = *(RwRGBA *)&color;
					SRColor clr( rwColor.alpha, rwColor.red, rwColor.green, rwColor.blue );
					font->Print( pos.fX, pos.fY, text.data(), clr.argb );
				} catch ( ... ) {
				}
			} );
			return true;
		},
		"d3dFont_print" );
	addFunction(
		[]( std::string name, std::string text, float x, float y, int color ) {
			if ( hasHudHidden() ) return false;
			render( [=] {
				try {
					auto	font	= fonts.at( name );
					auto	pos		= GameScreenToWindowScreen( x, y );
					auto	rwColor = *(RwRGBA *)&color;
					SRColor clr( rwColor.alpha, rwColor.red, rwColor.green, rwColor.blue );
					font->PrintShadow( pos.fX, pos.fY, text.data(), clr.argb );
				} catch ( ... ) {
				}
			} );
			return true;
		},
		"d3dFont_printShadow" );
	addFunction(
		[]( std::string name, std::string text ) {
			try {
				auto font = fonts.at( name );
				return ( 640.0f / SCREEN_X ) * font->DrawLength( text.data() );
			} catch ( ... ) {
				return 0.0f;
			}
		},
		"d3dFont_width" );
	addFunction(
		[]( std::string name ) {
			try {
				auto font = fonts.at( name );
				return ( 448.0f / SCREEN_Y ) * font->DrawHeight();
			} catch ( ... ) {
				return 0.0f;
			}
		},
		"d3dFont_height" );

	// render
	addFunction(
		[]( float x, float y, float w, float h, int color ) {
			if ( hasHudHidden() ) return false;
			auto	pos		= GameScreenToWindowScreen( x, y );
			auto	size	= GameScreenToWindowScreen( w, h );
			auto	rwColor = *(RwRGBA *)&color;
			SRColor clr( rwColor.alpha, rwColor.red, rwColor.green, rwColor.blue );
			render( [=] { d3dRender->D3DBox( pos.fX, pos.fY, size.fX, size.fY, clr.argb ); } );
			return true;
		},
		"d3dRender_box" );
	addFunction(
		[]( float x, float y, float x2, float y2, int color ) {
			if ( hasHudHidden() ) return false;
			auto	pos		= GameScreenToWindowScreen( x, y );
			auto	pos2	= GameScreenToWindowScreen( x2, y2 );
			auto	rwColor = *(RwRGBA *)&color;
			SRColor clr( rwColor.alpha, rwColor.red, rwColor.green, rwColor.blue );
			render( [=] { d3dRender->D3DLine( pos.fX, pos.fY, pos2.fX, pos2.fY, clr.argb ); } );
			return true;
		},
		"d3dRender_line" );
	addFunction(
		[]( float x, float y, float z, float x2, float y2, float z2, int color ) {
			if ( hasHudHidden() ) return false;
			auto		rwColor = *(RwRGBA *)&color;
			SRColor		clr( rwColor.alpha, rwColor.red, rwColor.green, rwColor.blue );
			D3DXVECTOR3 start{ x, y, z };
			D3DXVECTOR3 end{ x2, y2, z2 };
			render( [=] { d3dRender->DrawLine( start, end, clr.argb ); } );
			return true;
		},
		"d3dRender_line3D" );
	addFunction(
		[]( std::string name, float x, float y, float w, float h, int color ) {
			if ( hasHudHidden() ) return false;
			auto	pos		= GameScreenToWindowScreen( x, y );
			auto	size	= GameScreenToWindowScreen( w, h );
			auto	rwColor = *(RwRGBA *)&color;
			SRColor clr( rwColor.alpha, rwColor.red, rwColor.green, rwColor.blue );
			render( [=] {
				if ( texture( name ) && texture( name )->raster )
					d3dRender->D3DBindTexture( texture( name )->raster->rwD3D9RasterExt()->texture );
				d3dRender->D3DTexQuad( pos.fX, pos.fY, pos.fX + size.fX, pos.fY + size.fY, clr.argb );
				d3dRender->D3DBindTexture( nullptr );
			} );
			return true;
		},
		"d3dRender_texture" );

	// player
	addFunction(
		[]() {
			if ( !LOCAL_PLAYER ) return std::tuple{ 0.0f, 0.0f, 0.0f };
			if ( !LOCAL_PLAYER->isValid() ) return std::tuple{ 0.0f, 0.0f, 0.0f };
			auto pos = LOCAL_PLAYER->getPosition();
			return std::tuple{ pos.fX, pos.fY, pos.fZ };
		},
		"player_pos3D" );
	addFunction(
		[]( float x, float y, float z ) {
			if ( !LOCAL_PLAYER ) return std::tuple{ 0.0f, 0.0f, 0.0f };
			if ( !LOCAL_PLAYER->isValid() ) return std::tuple{ 0.0f, 0.0f, 0.0f };
			auto pos = LOCAL_PLAYER->getPositionOffset( { x, y, z } );
			return std::tuple{ pos.fX, pos.fY, pos.fZ };
		},
		"player_offset3D" );
	addFunction(
		[]( int boneId ) {
			if ( !LOCAL_PLAYER ) return std::tuple{ 0.0f, 0.0f, 0.0f };
			if ( !LOCAL_PLAYER->isValid() ) return std::tuple{ 0.0f, 0.0f, 0.0f };
			auto pos = LOCAL_PLAYER->getBonePos( boneId );
			return std::tuple{ pos.fX, pos.fY, pos.fZ };
		},
		"player_bone3D" );

	// misc
	addFunction( []() { return std::tuple{ SCREEN_X, SCREEN_Y }; }, "d3dResolution" );

	// TODO: d3dRadar_render - свой вывод карты (только квадрат (отдельная SRTexture) ) и всей херни на ней, то что ниже хуйня

	return true;
}

extern "C" __declspec( dllexport ) void init_ide() {
	// textures
	addDescription(
		"d3dTexture_exists",
		"Check is texture exists\n\nArgs:\n * string - name\nReturn:\n * bool - is exists\n\nSource: D3D.plugin" );
	addDescription( "d3dTexture_delete",
					"Delete texture\n\nArgs:\n * string - name\nReturn:\n * bool - is deleted\n\nSource: D3D.plugin" );
	addDescription( "d3dTexture_new", "Create a new texture\n\nArgs:\n * string - name\n * float - width\n * float - "
									  "height\nReturn:\n * bool - is created\n\nSource: D3D.plugin" );
	addDescription( "d3dTexture_resize", "Resize existing texture\n\nArgs:\n * string - name\n * float - width\n * "
										 "float - height\nReturn:\n * bool - is resized\n\nSource: D3D.plugin" );
	addDescription(
		"d3dTexture_clear",
		"Clear texture data\n\nArgs:\n * string - name\nReturn:\n * bool - is cleared\n\nSource: D3D.plugin" );
	addDescription(
		"d3dTexture_begin",
		"Begin render into texture\n\nArgs:\n * string - name\nReturn:\n * bool - is begined\n\nSource: D3D.plugin" );
	addDescription(
		"d3dTexture_end",
		"End render into texture\n\nArgs:\n * string - name\nReturn:\n * bool - is ended\n\nSource: D3D.plugin" );
	addDescription( "d3dTexture_render",
					"Render texture\n\nArgs:\n * string - name\n * float - pos X\n * float pos Y\n * float [optional] "
					"- width\n * float [optional] - height\nReturn:\n * bool - is rendered\n\nSource: D3D.plugin" );
	addDescription(
		"d3dTexture_renderAtPlayer",
		"Render texture at player skin (or vehicle, if driving)\n\nArgs:\n * string - name\n * float - pos X\n * float "
		"pos Y\n * float  "
		"- pos Z\n * float - end X\n * float - end Y\n * float - end Z\n * bool [optional] - enable Z test\nReturn:\n "
		"* bool - is rendered\n\nSource: "
		"D3D.plugin" );
	addDescription( "d3dTexture_render3D",
					"Render texture in world\n\nArgs:\n * string - name\n * float - left-up X\n * float left-up Y\n * "
					"float - left-up Z\n * float - right-up X\n * float - right-up Y\n * float - right-up Z\n * float "
					"- left-bottom X\n * float left-bottom Y\n * float - left-bottom Z\n * float - right-bottom X\n * "
					"float - right-bottom Y\n * float - right-bottom Z\n * bool [optional] - enable Z test\nReturn:\n "
					"* bool - is rendered\n\nSource: "
					"D3D.plugin" );
	addDescription(
		"d3dTexture_angle",
		"Get texture rotation\n\nArgs:\n * string - name\nReturn:\n * float - angle\n\nSource: D3D.plugin" );
	addDescription( "d3dTexture_setAngle", "set texture rotation\n\nArgs:\n * string - name\n * float - "
										   "angle\nReturn:\n * bool - is rotated\n\nSource: D3D.plugin" );
	addDescription( "d3dTexture_isRendered", "Check is texture data not empty\n\nArgs:\n * string - name\nReturn:\n * "
											 "bool - is texture with data\n\nSource: D3D.plugin" );

	// fonts
	addDescription(
		"d3dFont_exists",
		"Check is font exists\n\nArgs:\n * string - name\nReturn:\n * bool - is exists\n\nSource: D3D.plugin" );
	addDescription( "d3dFont_delete",
					"Delete font\n\nArgs:\n * string - name\nReturn:\n * bool - is deleted\n\nSource: D3D.plugin" );
	addDescription( "d3dFont_new",
					"Create a new font\n\nArgs:\n * string - name\n * string font name\n * int - size\n * int - "
					"flags\nReturn:\n * bool - is created\n\nSource: D3D.plugin" );
	addDescription( "d3dFont_print",
					"Print text\n\nArgs:\n * string - name\n * string - text\n * float - pos X\n * float - pos Y\n * "
					"int - color\nReturn:\n * bool - is printed\n\nSource: D3D.plugin" );
	addDescription( "d3dFont_printShadow",
					"Print text with shadow\n\nArgs:\n * string - name\n * string - text\n * float - pos X\n * float - "
					"pos Y\n * int - color\nReturn:\n * bool - is printed\n\nSource: D3D.plugin" );
	addDescription( "d3dFont_width", "Get text width\n\nArgs:\n * string - name\n * string - text\nReturn:\n * float - "
									 "width\n\nSource: D3D.plugin" );
	addDescription( "d3dFont_height", "Get text height\n\nArgs:\n * string - name\nReturn:\n * float - "
									  "height\n\nSource: D3D.plugin" );

	// render
	addDescription( "d3dRender_box", "Draw D3D box\n\nArgs:\n * float - pos x\n * float - pos y\n * float - width\n * "
									 "float - height\n * int - color\n\nSource: D3D.plugin" );
	addDescription( "d3dRender_line", "Draw D3D line\n\nArgs:\n * float - pos x\n * float - pos y\n * float - end x\n "
									  "* float - end y\n * int - color\n\nSource: D3D.plugin" );
	addDescription( "d3dRender_line3D",
					"Draw D3D line in world\n\nArgs:\n * float - pos x\n * float - pos y\n * float - pos z\n * float - "
					"end x\n * float - end y\n * float - end z\n * int - color\n\nSource: D3D.plugin" );
	addDescription(
		"d3dRender_texture",
		"Draw D3D texture\n\nArgs:\n * string - name\n * float - pos x\n * float - pos y\n * float - width\n * "
		"float - height\n * int - color\n\nSource: D3D.plugin" );

	// player
	addDescription( "player_pos3D", "Get player position in world\n\nReturn:\n * float - pos x\n * float - pos y\n * "
									"float - pos z\n\nSource: D3D.plugin" );
	addDescription(
		"player_offset3D",
		"Get player position in world with offset\n\nArgs:\n * float - offset X\n * float - offset Y\n * float - "
		"offset Z\nReturn:\n * float - pos x\n * float - pos y\n * float - pos z\n\nSource: D3D.plugin" );
	addDescription( "player_bone3D", "Get player position in world by bone\n\nArgs:\n * int - bone id\nReturn:\n * "
									 "float - pos x\n * float - pos y\n * float - pos z\n\nSource: D3D.plugin" );

	// misc
	addDescription( "d3dResolution",
					"Get screen resolution\n\nReturn:\n * int - width\n * int - height\n\nSource: D3D.plugin" );
}

BOOL APIENTRY DllMain( HMODULE, DWORD dwReasonForCall, LPVOID ) {
	if ( dwReasonForCall == DLL_PROCESS_ATTACH ) {
		if ( !hudDraw.isHooked() ) {
			hudDraw.install();
			hudDraw.onBefore += &onDraw;
		}
	} else if ( dwReasonForCall == DLL_PROCESS_DETACH ) {
		if ( g_dx ) {
#ifdef FULL_DX_HOOK
			g_dx->d3d9_ReleaseRender( d3dRender );
#else
			g_dx->d3d9_releaseRender( d3dRender );
#endif
			reset();
		}
	}

	return TRUE;
}
