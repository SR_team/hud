#ifndef CHATPOS_H
#define CHATPOS_H

#include <llmo/SRHook.hpp>

class ChatPos {
	SRHook::Hook<>		  renderScrollBar{ 0x63D7E, 6, "samp" };
	SRHook::Hook<>		  updateScrollBar{ 0x6364B, 7, "samp" };
	SRHook::Hook<>		  renderEditBox{ 0x65855, 7, "samp" };
	SRHook::Hook<>		  renderEdit1{ 0x63FE6, 6, "samp" };
	SRHook::Hook<>		  renderEdit2{ 0x63FF9, 6, "samp" };
	SRHook::Hook<float *> drawChat{ 0x642C4, 5, "samp" };

	int x = 40, y = 5;

public:
	ChatPos();
	~ChatPos();

	void				 pos( int x, int y );
	std::tuple<int, int> pos();
	std::tuple<int, int> origPos();

	void reset();

protected:
	void onRenderScrollBar( SRHook::CPU &cpu );
	void onUpdateScrollBar( SRHook::CPU &cpu );
	void onRenderEditBox( SRHook::CPU &cpu );
	void onRenderEdit( SRHook::CPU &cpu );
	void onDrawChat( float *&pos );
};

#endif // CHATPOS_H
