#include "ChatPos.h"
#include <dxut/DXUT.h>
#include <samp/samp.hpp>

ChatPos::ChatPos() {
	if ( SAMP::isR3() ) {
		renderScrollBar.changeAddr( 0x671CE );
		updateScrollBar.changeAddr( 0x66A9B );
		renderEditBox.changeAddr( 0x68D85 );
		drawChat.changeAddr( 0x67714 );
		renderEdit1.changeAddr( 0x67436 );
		renderEdit2.changeAddr( 0x67449 );
	}
	renderScrollBar.onAfter += std::tuple{ this, &ChatPos::onRenderScrollBar };
	updateScrollBar.onAfter += std::tuple{ this, &ChatPos::onUpdateScrollBar };
	renderEditBox.onAfter += std::tuple{ this, &ChatPos::onRenderEditBox };
	drawChat.onBefore += std::tuple{ this, &ChatPos::onDrawChat };
	renderEdit1.onAfter += std::tuple{ this, &ChatPos::onRenderEdit };
	renderEdit2.onAfter += std::tuple{ this, &ChatPos::onRenderEdit };
	renderScrollBar.install();
	updateScrollBar.install();
	renderEditBox.install();
	drawChat.install( 8 );
	renderEdit1.install();
	renderEdit2.install();

	reset();
}

ChatPos::~ChatPos() {
	//	SAMP::Input::DeleteInstance();
}

void ChatPos::pos( int x, int y ) {
	this->x = x;
	this->y = y;
}

std::tuple<int, int> ChatPos::pos() {
	return std::tuple{ x, y };
}

std::tuple<int, int> ChatPos::origPos() {
	if ( SAMP::isR1() )
		return std::tuple{ 40, 5 };
	else
		return std::tuple{ 45, 10 };
}

void ChatPos::reset() {
	auto [ox, oy] = origPos();

	x = ox;
	y = oy;
}

void ChatPos::onRenderScrollBar( SRHook::CPU &cpu ) {
	auto scroll = (CDXUTScrollBar *)cpu.EBX;
	if ( scroll->controlData.pos[0] != x - ( scroll->controlData.size[0] + 10 ) ||
		 scroll->controlData.pos[1] != y + 35 ) {
		scroll->controlData.pos[0] = x - ( scroll->controlData.size[0] + 10 );
		scroll->controlData.pos[1] = y + 35;
		scroll->UpdateRects();
	}
}

void ChatPos::onUpdateScrollBar( SRHook::CPU &cpu ) {
	auto scroll				   = (CDXUTScrollBar *)cpu.ECX;
	scroll->controlData.pos[0] = x - ( scroll->controlData.size[0] + 10 );
	scroll->controlData.pos[1] = y + 35;
}

void ChatPos::onRenderEditBox( SRHook::CPU &cpu ) {
	*(int *)( cpu.ECX + 8 ) = x - 5;
	auto [ox, oy]			= origPos();
	cpu.EAX += y - oy;
}

void ChatPos::onRenderEdit( SRHook::CPU &cpu ) {
	auto &bottom  = *(int *)( cpu.ESI + 0x12E );
	auto [ox, oy] = origPos();
	auto edit	  = SAMP::Base::Instance()->Input()->edit();
	if ( edit->controlData.pos[0] != x - 5 || edit->controlData.pos[1] != bottom + y - oy ) {
		edit->controlData.pos[0] = x - 5;
		edit->controlData.pos[1] = bottom + y - oy;
		edit->UpdateRects();
	}
}

void ChatPos::onDrawChat( float *&pos ) {
	static float _pos[3];
	auto [ox, oy] = origPos();
	_pos[0]		  = x - ox;
	_pos[1]		  = y - oy;
	pos			  = _pos;
}
