#include "Pagesize.h"
#include <llmo/callfunc.hpp>
#include <samp/samp.hpp>

Pagesize::Pagesize() {
	if ( SAMP::isR3() ) {
		chatRender.changeAddr( 0x75372 );
		chatDraw.changeAddr( 0xA202B );
	}
	chatRender.onBefore += std::tuple{ this, &Pagesize::before };
	chatRender.onAfter += std::tuple{ this, &Pagesize::after };
	chatDraw.onBefore += std::tuple{ this, &Pagesize::before };
	chatDraw.onAfter += std::tuple{ this, &Pagesize::after };
	chatRender.install();
	chatDraw.install();
}

void Pagesize::pagesize( int ps ) {
	pageSize = ps;
	initPS	 = true;
}

int Pagesize::pagesize() {
	if ( !initPS ) return origPS;
	return pageSize;
}

int Pagesize::origPagesize() {
	return origPS;
}

void Pagesize::reset() {
	updateScrollbar( origPS );
	initPS = false;
}

void Pagesize::before( SRHook::CPU &cpu ) {
	ECX	   = cpu.ECX;
	origPS = *(int *)ECX;
	if ( initPS ) {
		*(int *)ECX = pageSize;
		updateScrollbar( pageSize );
	}
}

void Pagesize::after() {
	*(int *)ECX = origPS;
}

void Pagesize::updateScrollbar( int ps ) {
	if ( !ECX ) return;
	auto origPS = *(int *)ECX;
	*(int *)ECX = ps;
	if ( ps != last ) CallFunc::thiscall( ECX, SAMP::Library() + ( SAMP::isR1() ? 0x63630 : 0x66A80 ) );
	*(int *)ECX = origPS;
	last		= ps;
}
