#include "KillPos.h"
#include <samp/samp_pimpl.hpp>

KillPos::KillPos() {
	if ( SAMP::isR3() ) renderKillist.changeAddr( 0x69BA5 );
	renderKillist.onBefore += std::tuple{ this, &KillPos::onRender };
	renderKillist.install();
}

void KillPos::pos( int x, int y ) {
	inited	= true;
	this->x = x;
	this->y = y;
}

std::tuple<int, int> KillPos::pos() {
	if ( inited )
		return std::tuple{ x, y };
	else
		return std::tuple{ ox, oy };
}

std::tuple<int, int> KillPos::origPos() {
	return std::tuple{ ox, oy };
}

void KillPos::reset() {
	inited = false;
}

void KillPos::onRender( SRHook::CPU &cpu ) {
	ox = *(int *)&cpu.EBX;
	oy = *(int *)&cpu.EBP;
	if ( inited ) {
		*(int *)&cpu.EBX = x;
		*(int *)&cpu.EBP = y;
	}
}
