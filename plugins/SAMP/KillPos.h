#ifndef KILLPOS_H
#define KILLPOS_H

#include <llmo/SRHook.hpp>

class KillPos {
	SRHook::Hook<> renderKillist{ 0x66675, 6, "samp" };

	int ox, oy;
	int x, y;

	bool inited = false;

public:
	KillPos();

	void				 pos( int x, int y );
	std::tuple<int, int> pos();
	std::tuple<int, int> origPos();

	void reset();

protected:
	void onRender( SRHook::CPU &cpu );
};

#endif // KILLPOS_H
