#ifndef FONTSIZE_H
#define FONTSIZE_H

#include <llmo/SRHook.hpp>

class FontSize {
	SRHook::Hook<size_t, char *> readInt{ 0x62250, 5, "samp" };
	int							 _size;
	bool						 readOrig = false;

public:
	FontSize();

	void size( int sz );
	int	 size();
	int	 origSize();

	void reset();

protected:
	void onReadInt( SRHook::Info &info, size_t &ret, char *&key );
};

#endif // FONTSIZE_H
