#ifndef PAGESIZE_H
#define PAGESIZE_H

#include <llmo/SRHook.hpp>

class Pagesize {
	SRHook::Hook<> chatRender{ 0x71482, 5, "samp" };
	SRHook::Hook<> chatDraw{ 0x9DA9B, 5, "samp" };

	int	   origPS	= 0;
	int	   pageSize = 0;
	int	   last		= 0;
	bool   initPS	= false;
	size_t ECX		= 0;

public:
	Pagesize();

	void pagesize( int ps );
	int	 pagesize();
	int	 origPagesize();

	void reset();

protected:
	void before( SRHook::CPU &cpu );
	void after();

	void updateScrollbar( int ps );
};

#endif // PAGESIZE_H
