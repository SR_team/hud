#include "FontSize.h"
#include <llmo/callfunc.hpp>
#include <llmo/memsafe.h>
#include <samp/samp_pimpl.hpp>
#include <string>

FontSize::FontSize() {
	if ( SAMP::isR3() ) readInt.changeAddr( 0x656A0 );
	readInt.onBefore += std::tuple{ this, &FontSize::onReadInt };
	readInt.install();
	_size = origSize();
}

void FontSize::size( int sz ) {
	if ( sz == _size ) return;
	_size = sz;

	void * pCmdFontSizeChange = nullptr;
	size_t pCmdFontSize		  = 0;
	if ( SAMP::isR1() ) {
		pCmdFontSizeChange = (void *)( SAMP::Library() + 0x64ADA );
		pCmdFontSize	   = SAMP::Library() + 0x64AA0;
	} else {
		pCmdFontSizeChange = (void *)( SAMP::Library() + 0x67F3A );
		pCmdFontSize	   = SAMP::Library() + 0x67F00;
	}

	memsafe::copy( pCmdFontSizeChange, { 0xEB } ); // NOP saving font size
	CallFunc::ccall<void>( pCmdFontSize, std::to_string( _size ).data() );
	memsafe::copy( pCmdFontSizeChange, { 0x74 } ); // restore saving font size
}

int FontSize::size() {
	return _size;
}

int FontSize::origSize() {
	readOrig = true;
	return ( CallFunc::ccall<int>( SAMP::Library() + ( SAMP::isR1() ? 0xB3CF0 : 0xC5BB0 ) ) - 20 ) * 0.5f;
}

void FontSize::reset() {
	size( origSize() );
}

void FontSize::onReadInt( SRHook::Info &info, size_t &ret, char *&key ) {
	if ( strcmp( key, "fontsize" ) ) return;
	if ( readOrig ) {
		readOrig = false;
		return;
	}
	info.skipOriginal = true;
	info.cpu.ESP += 8;
	info.retAddr		  = ret;
	*(int *)&info.cpu.EAX = _size;
}
