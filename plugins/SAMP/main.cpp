#include "ChatPos.h"
#include "FontSize.h"
#include "KillPos.h"
#include "Pagesize.h"
#include <samp/samp.hpp>
#include <sol/sol.hpp>
#include <windows.h>

/// Ширина окна в пикселях
#define SCREEN_X *(int *)0x00C9C040
/// Высота окна в пикселях
#define SCREEN_Y *(int *)0x00C9C044

sol::state * g_lua	  = nullptr;
SAMP::Base * samp	  = nullptr;
ChatPos *	 chatPos  = nullptr;
KillPos *	 killPos  = nullptr;
Pagesize *	 pagesize = nullptr;
FontSize *	 fontsize = nullptr;
static float lx, ly;

void addDescription( std::string_view name, std::string_view desc ) {
	typedef void ( *addToIDE_t )( std::string_view, std::string_view );
	static auto lib = GetModuleHandleA( "HUD.asi" );
	if ( !lib || lib == INVALID_HANDLE_VALUE ) return;
	static auto addToIDE = (addToIDE_t)GetProcAddress( lib, "addToIDE" );
	addToIDE( name, desc );
}
template<typename T> void addFunction( const T &func, std::string_view name ) {
	if ( g_lua ) g_lua->set_function( name, func );
}

extern "C" __declspec( dllexport ) void reset() {
	if ( chatPos ) {
		auto [cx, cy] = chatPos->pos();
		auto cps	  = pagesize->pagesize();
		chatPos->reset();
		pagesize->reset();
		auto [x, y] = chatPos->pos();
		auto ps		= pagesize->pagesize();
		lx			= x;
		ly			= y;
		if ( cx != x || cy != y || cps != ps ) samp->Chat()->updateRender();
	}
	if ( killPos ) killPos->reset();
	if ( fontsize ) fontsize->reset();
}

extern "C" __declspec( dllexport ) bool init_lua( sol::state *lua ) {
	g_lua = lua;

	if ( !samp && SAMP::Base::TestInitilize() ) samp = SAMP::Base::Instance();
	if ( !samp ) return false;

	if ( !chatPos ) chatPos = new ChatPos();
	if ( !killPos ) killPos = new KillPos();
	if ( !pagesize ) pagesize = new Pagesize();
	if ( !fontsize ) fontsize = new FontSize();

	// chat
	addFunction( []( std::string msg ) { samp->Chat()->addMsgInfo( msg ); }, "addChatInfoMessage" );
	addFunction( []( std::string msg ) { samp->Chat()->addMsgDbg( msg ); }, "addChatDebugMessage" );
	addFunction( []() { return samp->Chat()->chatWinMode(); }, "chatWindowMode" );
	addFunction( []( std::string msg ) { samp->Input()->sendCommand( msg ); }, "sendChatCommand" );
	addFunction(
		[]( float x, float y ) {
			x *= ( SCREEN_X / 640.0f );
			y *= ( SCREEN_Y / 448.0f );
			auto [cx, cy] = chatPos->pos();
			if ( cx != x || cy != y ) {
				chatPos->pos( x, y );
				if ( lx != x || ly != y ) {
					lx = x;
					ly = y;
					samp->Chat()->updateRender();
				}
			}
		},
		"setChatPos" );
	addFunction(
		[]() {
			auto [x, y] = chatPos->pos();
			return std::tuple{ float( x ) * ( 640.0f / SCREEN_X ), float( y ) * ( 448.0f / SCREEN_Y ) };
		},
		"getChatPos" );
	addFunction(
		[]() {
			auto [x, y] = chatPos->origPos();
			return std::tuple{ float( x ) * ( 640.0f / SCREEN_X ), float( y ) * ( 448.0f / SCREEN_Y ) };
		},
		"getChatPosOrig" );
	addFunction(
		[]() {
			float bottom = samp->Chat()->chatWinBottom();
			if ( !bottom ) bottom = 110.0f;
			return bottom * ( 448.0f / SCREEN_Y );
		},
		"getChatBottom" );
	addFunction( []() { return 550.0f * ( 448.0f / SCREEN_Y ); }, "getChatWidth" );
	addFunction(
		[]() {
			auto [cx, cy] = chatPos->pos();
			chatPos->reset();
			auto [x, y] = chatPos->pos();
			if ( cx != x || cy != y ) samp->Chat()->updateRender();
		},
		"resetChatPos" );
	addFunction( [] { return samp->Input()->isInputEnabled(); }, "isChatOpened" );

	//pagesize
	addFunction(
		[]( int ps ) {
			if ( ps < 1 ) ps = 1;
			if ( ps > 100 ) ps = 100;
			auto cps = pagesize->pagesize();
			if ( cps != ps ) {
				pagesize->pagesize( ps );
				samp->Chat()->updateRender();
			}
		},
		"setChatPagesize" );
	addFunction( []() { return pagesize->pagesize(); }, "getChatPagesize" );
	addFunction( []() { return pagesize->origPagesize(); }, "getChatPagesizeOrig" );
	addFunction(
		[]() {
			auto cps = pagesize->pagesize();
			pagesize->reset();
			auto ps = pagesize->pagesize();
			if ( cps != ps ) samp->Chat()->updateRender();
		},
		"resetChatPagesize" );

	// font
	addFunction( []( int size ) { fontsize->size( size ); }, "setFontsize" );
	addFunction( [] { return fontsize->size(); }, "getFontsize" );
	addFunction( [] { return fontsize->origSize(); }, "getFontsizeOrig" );
	addFunction( [] { return fontsize->reset(); }, "resetFontsize" );

	// kill
	addFunction(
		[]( float x, float y ) {
			x *= ( SCREEN_X / 640.0f );
			y *= ( SCREEN_Y / 448.0f );
			auto [cx, cy] = killPos->pos();
			if ( cx != x || cy != y ) killPos->pos( x, y );
		},
		"setKillPos" );
	addFunction(
		[]() {
			auto [x, y] = killPos->pos();
			return std::tuple{ float( x ) * ( 640.0f / SCREEN_X ), float( y ) * ( 448.0f / SCREEN_Y ) };
		},
		"getKillPos" );
	addFunction(
		[]() {
			auto [x, y] = killPos->origPos();
			return std::tuple{ float( x ) * ( 640.0f / SCREEN_X ), float( y ) * ( 448.0f / SCREEN_Y ) };
		},
		"getKillPosOrig" );
	addFunction( []() { killPos->reset(); }, "resetKillPos" );

	// other
	addFunction( []() { return samp->isConnected(); }, "isConnected" );
	addFunction( []() { return samp->Pools()->PlayerPool()->localPlayerId(); }, "localPlayerId" );
	addFunction( []() { return samp->Pools()->PlayerPool()->localPlayerNick(); }, "localPlayerNick" );

	return true;
}

extern "C" __declspec( dllexport ) void init_ide() {
	// chat
	addDescription( "addChatInfoMessage",
					"Add info message to SAMP chat\n\nArgs:\n * string - message\n\nSource: SAMP.plugin" );
	addDescription( "addChatDebugMessage",
					"Add debug message to SAMP chat\n\nArgs:\n * string - message\n\nSource: SAMP.plugin" );
	addDescription( "chatWindowMode", "Get SAMP chat window mode\n\nReturn:\n * int - mode id\n\nSource: SAMP.plugin" );
	addDescription( "sendChatCommand",
					"Send command to SAMP server\n\nArgs:\n * string - command\n\nSource: SAMP.plugin" );
	addDescription( "setChatPos",
					"Set SAMP chat position\n\nArgs:\n * float - pos x\n * float - pos y\n\nSource: SAMP.plugin" );
	addDescription( "getChatPos",
					"Get SAMP chat position\n\nReturn:\n * float - pos x\n * float - pos y\n\nSource: SAMP.plugin" );
	addDescription(
		"getChatPosOrig",
		"Get SAMP original chat position\n\nReturn:\n * float - pos x\n * float - pos y\n\nSource: SAMP.plugin" );
	addDescription( "getChatBottom",
					"Get SAMP chat bottom position\n\nReturn:\n * float - bottom\n\nSource: SAMP.plugin" );
	addDescription( "getChatWidth", "Get SAMP chat width\n\nReturn:\n * float - width\n\nSource: SAMP.plugin" );
	addDescription( "resetChatPos", "reset SAMP chat position to original\n\nSource: SAMP.plugin" );
	addDescription( "isChatOpened", "Check is chat opened\n\nSource: SAMP.plugin" );

	// pagesize
	addDescription( "setChatPagesize", "Set SAMP chat pagesize\n\nArgs:\n * int - pagesize\n\nSource: SAMP.plugin" );
	addDescription( "getChatPagesize", "Get SAMP chat pagesize\n\nReturn:\n * int - pagesize\n\nSource: SAMP.plugin" );
	addDescription( "getChatPagesizeOrig",
					"Get SAMP original chat pagesize\n\nReturn:\n * int - pagesize\n\nSource: SAMP.plugin" );
	addDescription( "resetChatPagesize", "reset SAMP chat pagesize to original\n\nSource: SAMP.plugin" );

	// font
	addDescription( "setFontsize", "Set SAMP font size\n\nArgs:\n * int - size\n\nSource: SAMP.plugin" );
	addDescription( "getFontsize", "Get SAMP font size\n\nReturn:\n * int - size\n\nSource: SAMP.plugin" );
	addDescription( "getFontsizeOrig", "Get original SAMP font size\n\nReturn:\n * int - size\n\nSource: SAMP.plugin" );
	addDescription( "resetFontsize", "reset SAMP font size to original\n\nSource: SAMP.plugin" );

	// kill
	addDescription( "setKillPos",
					"Set SAMP kill list position\n\nArgs:\n * float - pos x\n * float - pos y\n\nSource: SAMP.plugin" );
	addDescription(
		"getKillPos",
		"Get kill list chat position\n\nReturn:\n * float - pos x\n * float - pos y\n\nSource: SAMP.plugin" );
	addDescription(
		"getKillPosOrig",
		"Get SAMP original kill list position\n\nReturn:\n * float - pos x\n * float - pos y\n\nSource: SAMP.plugin" );
	addDescription( "resetKillPos", "reset SAMP kill list position to original\n\nSource: SAMP.plugin" );

	// other
	addDescription( "isConnected",
					"Check is connect to server\n\nReturn:\n * bool - is connected\n\nSource: SAMP.plugin" );
	addDescription( "localPlayerId",
					"Get SAMP id of local player\n\nReturn:\n * int - player id\n\nSource: SAMP.plugin" );
	addDescription( "localPlayerNick",
					"Get SAMP nick of local player\n\nReturn:\n * string - nick\n\nSource: SAMP.plugin" );
}

BOOL APIENTRY DllMain( HMODULE, DWORD dwReasonForCall, LPVOID ) {
	if ( !SAMP::isR1() && !SAMP::isR3() ) return FALSE;

	if ( dwReasonForCall == DLL_PROCESS_DETACH ) {
		if ( fontsize ) delete fontsize;
		if ( pagesize ) delete pagesize;
		if ( killPos ) delete killPos;
		if ( chatPos ) delete chatPos;
		if ( samp ) SAMP::Base::DeleteInstance( true );
	}

	return TRUE;
}
