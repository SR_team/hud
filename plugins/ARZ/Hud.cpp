#include "Hud.h"
#include <llmo/callfunc.hpp>

Hud::Hud() {
	draw.onAfter += std::tuple{ this, &Hud::onDraw };
	draw.install();
}

void Hud::onDraw( SRHook::Info &info ) {
	if ( show ) return;
	if ( !info.cpu.AL ) return;
	CallFunc::ccall( 0x58EAF0 ); // draw hud
	CallFunc::ccall( 0x58D9A0 ); // draw wanted
	info.retAddr = 0x58FBE0;
}
