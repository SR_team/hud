#ifndef RADAR_H
#define RADAR_H

#include <llmo/SRHook.hpp>

class Radar {
	SRHook::Hook<> draw{ 0x58FC4C, 7 };

public:
	Radar();

	bool show = true;

protected:
	void onDraw( SRHook::Info &info );
};

#endif // RADAR_H
