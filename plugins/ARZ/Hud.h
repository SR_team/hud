#ifndef HUD_H
#define HUD_H

#include <llmo/SRHook.hpp>

class Hud {
	SRHook::Hook<> draw{ 0x58FBCD, 5 };

public:
	Hud();

	bool show = true;

protected:
	void onDraw( SRHook::Info &info );
};

#endif // HUD_H
