#include <RakNet/BitStream.h>
#include <RakNet/Network.hpp>
#include <RakNet/RakClient.h>
#include <llmo/SRHook.hpp>
#include <llmo/callfunc.hpp>
#include <samp/samp.hpp>
#include <sol/sol.hpp>
#include <windows.h>

sol::state *   g_lua	   = nullptr;
SAMP::Base *   samp		   = nullptr;
uint8_t		   hungry	   = 0;
uint8_t		   hudId	   = 0;
uint8_t		   radarId	   = 0;
uint8_t		   fakeHudId   = 0;
uint8_t		   fakeRadarId = 0;
bool		   drawHud	   = true;
bool		   drawRadar   = true;
SRHook::Hook<> samp_recv{ 0x3CE91, 6, "samp" };

void addDescription( std::string_view name, std::string_view desc ) {
	typedef void ( *addToIDE_t )( std::string_view, std::string_view );
	static auto lib = GetModuleHandleA( "HUD.asi" );
	if ( !lib || lib == INVALID_HANDLE_VALUE ) return;
	static auto addToIDE = (addToIDE_t)GetProcAddress( lib, "addToIDE" );
	addToIDE( name, desc );
}
template<typename T> void addFunction( const T &func, std::string_view name ) {
	if ( g_lua ) g_lua->set_function( name, func );
}

bool fakePacket( BitStream &bs ) {
	if ( !samp ) return false;
	if ( !samp->pRakClient() ) return false;
	if ( !samp->isConnected() ) return false;
	bs.Write( true ); //fake pkg
	auto data = CallFunc::ccall<uint8_t *>( SAMP::Library() + ( SAMP::isR1() ? 0xB5454 : 0xC73F4 ),
											bs.GetNumberOfBytesUsed() ); // malloc
	memcpy( data, bs.GetData(), bs.GetNumberOfBytesUsed() );
	auto pkg =
		CallFunc::ccall<Packet *>( SAMP::Library() + ( SAMP::isR1() ? 0x347E0 : 0x37B90 ), 1 ); // allocate packet
	pkg->data		= data;
	pkg->bitSize	= bs.GetNumberOfBitsUsed();
	pkg->length		= bs.GetNumberOfBytesUsed();
	pkg->deleteData = true;
	samp->pRakClient()->PushBackPacket( pkg, false );
	return true;
}

extern "C" __declspec( dllexport ) void reset() {
	{
		drawHud = true;
		BitStream bs;
		bs.Write( (uint8_t)220 );
		bs.Write( (uint8_t)10 );
		bs.Write( hudId );
		fakePacket( bs );
		fakeHudId = hudId;
	}
	{
		drawRadar = true;
		BitStream bs;
		bs.Write( (uint8_t)220 );
		bs.Write( (uint8_t)11 );
		bs.Write( radarId );
		fakePacket( bs );
		fakeRadarId = radarId;
	}
}

extern "C" __declspec( dllexport ) bool init_lua( sol::state *lua ) {
	g_lua = lua;
	if ( !samp && SAMP::Base::TestInitilize() ) samp = SAMP::Base::Instance();

	// hud
	addFunction( [] { return drawHud; }, "arzHud_isShowed" );
	addFunction(
		[]( bool state ) {
			drawHud = state;
			if ( state ) {
				BitStream bs;
				bs.Write( (uint8_t)220 );
				bs.Write( (uint8_t)10 );
				bs.Write( hudId );
				fakePacket( bs );
				fakeHudId = hudId;
			} else if ( fakeHudId ) {
				BitStream bs;
				bs.Write( (uint8_t)220 );
				bs.Write( (uint8_t)10 );
				bs.Write( (uint8_t)0 );
				if ( fakePacket( bs ) ) fakeHudId = 0;
			}
		},
		"arzHud_show" );

	// radar
	addFunction( [] { return drawRadar; }, "arzRadar_isShowed" );
	addFunction(
		[]( bool state ) {
			drawRadar = state;
			if ( state ) {
				BitStream bs;
				bs.Write( (uint8_t)220 );
				bs.Write( (uint8_t)11 );
				bs.Write( radarId );
				fakePacket( bs );
				fakeRadarId = radarId;
			} else if ( fakeRadarId ) {
				BitStream bs;
				bs.Write( (uint8_t)220 );
				bs.Write( (uint8_t)11 );
				bs.Write( (uint8_t)0 );
				if ( fakePacket( bs ) ) fakeRadarId = 0;
			}
		},
		"arzRadar_show" );

	// other
	addFunction( [] { return (int)hungry; }, "arz_hungry" );

	return true;
}

extern "C" __declspec( dllexport ) void init_ide() {
	// hud
	addDescription( "arzHud_isShowed",
					"Check is enabled drawing custom Arizona HUD's\n\nReturn:\n * bool - draw\n\nSource: ARZ.plugin" );
	addDescription( "arzHud_show",
					"Enable/disable drawing custom Arizona HUD's\n\nArgs:\n * bool - draw\n\nSource: ARZ.plugin" );

	// radar
	addDescription( "arzRadar_isShowed",
					"Check is enabled drawing custom Arizona radar\n\nReturn:\n * bool - draw\n\nSource: ARZ.plugin" );
	addDescription( "arzRadar_show",
					"Enable/disable drawing custom Arizona radar\n\nArgs:\n * bool - draw\n\nSource: ARZ.plugin" );

	// other
	addDescription( "arz_hungry", "Get arizona hungry (work only with custom huds)\n\nReturn:\n * int - hungry [0 - "
								  "100]\n\nSource: ARZ.plugin" );
}

void recvHook( SRHook::CPU &cpu ) {
	auto &data	  = *(uint8_t **)( cpu.EDI + 0x10 );
	auto &bitSize = *(size_t *)( cpu.EDI + 0xC );
	if ( bitSize < 8 || !data || data[0] != 220 ) return;

	BitStream bs( (char *)data, ( bitSize / 8 ) + 1, false );
	bs.IgnoreBits( 8 ); // skip package id;
	uint8_t act;
	bs.Read( act );
	if ( act == 4 )
		bs.Read( hungry );
	else if ( act == 10 ) {
		uint8_t id;
		bs.Read( id );
		bool fake = false;
		if ( !bs.Read( fake ) || !fake ) fakeHudId = hudId = id;
		if ( !drawHud && !fake ) data[0] = 255;
	} else if ( act == 11 ) {
		uint8_t id;
		bs.Read( id );
		bool fake = false;
		if ( !bs.Read( fake ) || !fake ) fakeRadarId = radarId = id;
		if ( !drawRadar && !fake ) data[0] = 255;
	}
}

BOOL APIENTRY DllMain( HMODULE, DWORD dwReasonForCall, LPVOID ) {
	if ( !SAMP::isR1() && !SAMP::isR3() ) return FALSE;

	if ( dwReasonForCall == DLL_PROCESS_ATTACH ) {
		if ( !samp_recv.isHooked() ) {
			if ( SAMP::isR3() ) samp_recv.changeAddr( 0x40241 );
			samp_recv.onBefore += &recvHook;
			samp_recv.install();
		}
	} else if ( dwReasonForCall == DLL_PROCESS_DETACH ) {
		if ( samp ) SAMP::Base::DeleteInstance( true );
	}

	return TRUE;
}
