#include "Radar.h"
#include <llmo/callfunc.hpp>

Radar::Radar() {
	draw.onAfter += std::tuple{ this, &Radar::onDraw };
	draw.install();
}

void Radar::onDraw( SRHook::Info &info ) {
	if ( show ) return;
	CallFunc::ccall( 0x58A330 ); // draw radar
	info.retAddr = 0x58FC58;
}
