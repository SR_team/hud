cmake_minimum_required(VERSION 3.0)

get_filename_component(PROJECT_NAME ${CMAKE_CURRENT_SOURCE_DIR} NAME)
project(${PROJECT_NAME}.plugin)
set(CMAKE_SHARED_LIBRARY_PREFIX "../")
set(CMAKE_SHARED_LIBRARY_SUFFIX "")

option(MAP "Generate .map file [ON/OFF]" OFF)
option(UPX "Execute UPX after build [ON/OFF]" ON)
option(EXPORT_LIST "Use list of export symbols [ON/OFF]" ON)

ADD_DEFINITIONS(-D_PRODJECT_NAME="${PROJECT_NAME}")

ADD_DEFINITIONS(-DWIN32=1)

find_program(CCACHE_FOUND ccache)
if(CCACHE_FOUND)
	set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE ccache)
	set_property(GLOBAL PROPERTY RULE_LAUNCH_LINK ccache)
endif(CCACHE_FOUND)

execute_process(COMMAND git submodule update --init plugins/ARZ/sol2 WORKING_DIRECTORY ${CMAKE_SOURCE_DIR})
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/sol2/include)


aux_source_directory(${CMAKE_CURRENT_SOURCE_DIR} ${PROJECT_NAME}_LIST)
file(GLOB ${PROJECT_NAME}_LIST_H
	${CMAKE_CURRENT_SOURCE_DIR}/*.h
	${CMAKE_CURRENT_SOURCE_DIR}/*.hpp
	${CMAKE_CURRENT_SOURCE_DIR}/loader/*.h
	${CMAKE_CURRENT_SOURCE_DIR}/loader/*.hpp
	${CMAKE_CURRENT_SOURCE_DIR}/ImGuiColorTextEdit/*.h
)


if (${CMAKE_CXX_COMPILER_ID} STREQUAL "Clang")
	add_definitions("-ffunction-sections -pipe -w")
	set(CMAKE_SHARED_LINKER_FLAGS "-Wl,--strip-all -Wl,--gc-sections -ffast-math -fno-stack-protector")
elseif (${CMAKE_CXX_COMPILER_ID} STREQUAL "GNU")
	add_definitions("-ffunction-sections -fexec-charset=CP1251 -pipe -w")
	set(CMAKE_SHARED_LINKER_FLAGS "-Wl,--strip-all -Wl,--gc-sections -ffast-math")
endif()
if (${CMAKE_CXX_COMPILER_ID} STREQUAL "GNU" OR ${CMAKE_CXX_COMPILER_ID} STREQUAL "Clang")
	if (EXPORT_LIST)
		set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -Wl,--version-script='${CMAKE_CURRENT_SOURCE_DIR}/symbols.export'")
	endif(EXPORT_LIST)
	if(MAP)
		set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -Wl,-Map=${PROJECT_NAME}.map")
	endif(MAP)
endif()

add_library(${PROJECT_NAME} SHARED ${${PROJECT_NAME}_LIST} ${${PROJECT_NAME}_LIST_H})

set_target_properties(${PROJECT_NAME} PROPERTIES
    CXX_STANDARD 17
    CXX_STANDARD_REQUIRED YES
    CXX_EXTENSIONS NO
)

target_link_libraries(${PROJECT_NAME} llmo SRDescent SRString samp lua raknet)

if (UPX)
	find_program(UPX_BIN upx)
	if(UPX_BIN)
		add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD COMMAND ${UPX_BIN} -9 $<TARGET_FILE:${PROJECT_NAME}>)
	else(UPX_BIN)
		message("UPX enabled, but not found in $PATH")
	endif(UPX_BIN)
endif(UPX)
